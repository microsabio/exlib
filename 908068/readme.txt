LIBXL README

This directory contains an A-Shell function library that implements a 
general purpose API to read/write XLS (spreadsheet) files. It is built 
around the LibXL external library which can be licensed separately.
The library is packaged as LIBXL.DLL for Windows and libxl.so.x.x.x.x for
Linux, and can be downloaded from http://www.microsabio.net/dist/other/libxl.

It can be used for testing without a license, but will output a banner
message in the first row of each spreadsheet, and is also limited to about
100 API calls per instance of A-Shell before it stops working.

INSTALLATION

For Windows, just drop the LIBXL.DLL into the A-Shell bin directory.
For Linux, drop the libxl.so.x.x.x.x file into the A-Shell bin directory and
then create a symbolic link from /usr/lib/libxl.so.1 to the file in the 
bin directory.

Then install/update the SOSLIB and EXLIB packages from the MicroSabio
repository (http://www.bitbucket.org/microsabio). Most of it is in 
[908,68], for which you should create a LIBXL: ersatz. But it also 
relies on updated files in SOSFUNC: and ASHINC: (the [907,10] and
[907,16] directories from the SOSLIB package).

The sample programs also use the BAS:ATEAPX.SBX and BAS:XSHLEX.SBX
routines if you select the option to launch the resulting spreadsheet.

A-SHELL VERSION 

A-Shell 6.3 is the minimum supported version. For support of accented
and other special Latin1 characters under Linux, 6.3.1542.0 is the 
minimum version.

COMPILING & RUNNING SAMPLE PROGRAMS

You must specify the /P (or preferably /PX) switch to compile any 
program using the LIBXL.BSI file, e.g.

    .COMPIL TSTACB/X:2/M/PX

Most of the sample programs, particularly those starting with TST*,
simply create a sample spreadsheet file illustrating some set of features, 
and give you an option to launch it. Any provides a quick verification 
that the library is properly installed.

WHERE TO GO NEXT

See the A-Shell reference for more information.

