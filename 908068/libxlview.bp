program libxlview, 1.0(106)  ! LibXL analysis/debug utility
!------------------------------------------------------------------------
!EDIT HISTORY
!Version 1.0:-
![106] 20-Sep-18 / jdm / Support XLSX (ask for file first in order to set book type!)
![105] 20-Oct-17 / jdm / add a sheet which duplicates the original source
!                           (convenient reference and proof of read/write)
![104] 16-Jan-17 / jdm / Use Fn'Shell'Exec() for smarter launch (including xfer to pc)   
![103] 16-Jan-17 / jdm / Update for new DLF_xxx flags, libxl.bsi 1.1;
![102] 11-Jan-17 / jdm / Add choice of target page to view
![101] 11-Feb-16 / jdm / Use row split to lock header in place; minor/cosmetic cleaning
![100] 12-Dec-15 / jdm / Created
!------------------------------------------------------------------------
!NOTES 
!  For an existing XLS, creates a new spreadsheet with additional pages
!  showing information about the format, fonts, and other attributes
!  and objects in the original file
!------------------------------------------------------------------------
significance 11
++include'once ashinc:ashell.def
++include'once libxl:libxl.bsi          ! core library
++include'once libxl:libxl2.bsi         ! auxliary routines
++include'once sosfunc:fnshellex.bsi    ! [104] Fn'Shell'Exec()

map1 handles
	map2 hBook1,BookHandle
	map2 hBook2,BookHandle
	map2 hSheetSrc,SheetHandle			! handle to source sheet
	map2 hSheetFonts,SheetHandle		! handle to dest sheet showing all fonts
	map2 hSheetFormats,SheetHandle		! handle to dest sheet showing all fonts
	map2 hSheetCellFormats,SheetHandle	! handle to dest sheet showing format of each cell
    map2 hSheetData,SheetHandle         ! [105] handle to dest sheet containing copy of source
	map2 hFormat1,FormatHandle
	map2 hFormatText,FormatHandle
	map2 hFormatNum,FormatHandle
	map2 hFormatHex,FormatHandle
	map2 hFormatHdr,FormatHandle
	map2 hFormatTitle,FormatHandle
	map2 hFont1,FontHandle
	map2 hFontHdr,FontHandle
	map2 hFontTitle,FontHandle
	map2 dlflags,b,4
	map2 hFontSum,FontHandle
	map2 hFormat,FormatHandle			! general purpose working format handle
	
map1 misc
	map2 spreadsheet'in$,s,260
    map2 spreadsheet'out$,s,260
	map2 row,i,4
	map2 col,i,4
	map2 ifontsize,i,4
	map2 firstrow,b,4
	map2 firstcol,b,4
	map2 lastrow,b,4
	map2 lastcol,b,4
	map2 testno,b,2
	map2 sheetcount,b,2
	map2 sheetno,b,2
	map2 picfile$,s,260
	map2 pictureid,i,4
	map2 dlc, ST_DYNLIBCTL
	map2 count,i,4
	map2 picidx,i,4
	map2 iheight,i,4
	map2 iwidth,i,4
	map2 offsetx,i,4
	map2 offsety,i,4
	map2 colleft,i,4
	map2 colright,i,4
	map2 rowtop,i,4
	map2 rowbottom,i,4
	map2 i,i,4
	map2 rcbase,b,1
	map2 fontcount,i,2
	map2 formatcount,i,2
	map2 font,ST_XLFONT
	map2 format,ST_XLFORMAT
	map2 format$,s,10
	map2 rowcount,i,4
	map2 colcount,i,4
	map2 hColor(32),FormatHandle
	map2 nextcolor,i,2
    map2 sheetidx,i,2
    map2 sheetname$,s,50
    map2 status,i,4
    map2 bookformat,b,2     ! [106]
	
dimx $hcolor,ordmap(varstr;varstr)		! hColor(x) to use for specified hFormat
dimx $hformat,ordmap(varstr;varstr)     ! [105] dest book format hdl for given src format hdl
dimx $hfont,ordmap(varstr;varstr)       ! [105] dest book font hdl for given src font hdl

	on error goto TRAP
	
	? tab(-1,0);"LibXLView - reveal font/format info for existing XLS file"
	
	?
	? "Loading/initializing libxl library..." 
	dlflags = DLF_STATUS_BASERR or DLF_STATUS_BASERR
	dlflags = dlflags or DLF_PRINT_ERRORS		! print status/syserr errors to screen
	dlflags = dlflags or DLF_ANSI               ! [103] (might contain latin1 chars)
	if Fn'LibXL'Load(dlflags=dlflags) then      ! [103] was flags=
		? "Unable to load LibXL library" 	! (probably shouldn't even get here - error trap instead)
		end
	endif

    ! [106] get file first so we can set the bookformat properly
    do while spreadsheet'in$ = ""
        input line "Enter file to load (or skip): ",spreadsheet'in$
        if spreadsheet'in$ # "" then
            if ucs(spreadsheet'in$[-5,-1]) = ".XLSX" then
                bookformat = XLBT_XLSX
            elseif ucs(spreadsheet'in$[-4,-1]) = ".XLS" then
                bookformat = XLBT_XLS
            else
                ? "Must specify extension"
                spreadsheet'in$ = ""
                repeat
            endif
            if lookup(spreadsheet'in$) = 0 then
                ? "File not found"
                spreadsheet'in$ = ""
            endif
        else
            exit
        endif
    loop

    if spreadsheet'in$ # "" then
        ? "Create book handle (";ifelse$(bookformat=XLBT_XLS,"XLS","XLXS");" format..."  ! [106]
        hBook1 = Fn'LibXL'CreateBook(bookformat,rcbase=1)       ! [106]
        ? "hBook1 = ";Fn'Dec2Hex$(hBook1)

        call LIBXL'VIEW
    endif
    
UNLOAD:
	? "Unloading lib..."
	call Fn'LibXL'Unload()
	
	end
	
!--------------------------------------------------------------------------------	
LIBXL'VIEW:
    
	if Fn'LibXL'LoadBook(spreadsheet'in$) = 0 then
		? "Unable to load ";spreadsheet'in$
		call Fn'LibXL'Handle'DynLib'Errors()
		? "Last library error : ";Fn'LibXL'GetErrorMessage$()
		return
	endif
	
	? "Book loaded"
	
    ! [106] get output file too so we can set the bookformat properly
    spreadsheet'out$ = "libxlview." + ifelse$(bookformat=XLBT_XLSX,"xlsx","xls")  ! [106]
    do 
        ? "Output file [";spreadsheet'out$;"]: ";
        input "",spreadsheet'out$
        if spreadsheet'out$ # "" then
            if ucs(spreadsheet'out$[-5,-1]) = ".XLSX" then
                bookformat = XLBT_XLSX
            elseif ucs(spreadsheet'out$[-4,-1]) = ".XLS" then
                bookformat = XLBT_XLS
            else
                ? "Must specify extension"
                spreadsheet'out$ = ""
                repeat
            endif
        else
            exit
        endif
    loop until spreadsheet'out$ # ""
    if spreadsheet'out$ = "" then
        ? "Aborting"
        return
    endif
    
	? "Creating ";spreadsheet'out$;" ..."
	hBook2 = Fn'LibXL'CreateBook(bookformat,rcbase=1)       ! [106]
	? "hBook2 = ";Fn'Dec2Hex$(hBook2)

	sheetcount = Fn'LibXL'GetSheetCount(hBook1)
	? "Sheet count: ";sheetcount

	! create the font/format to use for the output sheet headers
	hFontHdr = Fn'LibXL'AddFont'SetAttributes(bold=1,color=COLOR_BLUE,hbook=hBook2)
	hFormatHdr = Fn'LibXL'AddFormat'SetAttributes(numfmtid=NUMFMT_TEXT,hbook=hBook2,hfont=hFontHdr)

	! and another set for the page titles
	hFontTitle = Fn'LibXL'AddFont'SetAttributes(bold=1,size=16,hbook=hBook2)
	hFormatTitle = Fn'LibXL'AddFormat'SetAttributes(numfmtid=NUMFMT_TEXT,alignh=ALIGNH_CENTER, &
			fillpattern=FILLPATTERN_SOLID,patternfgc=COLOR_GRAY25,hbook=hBook2,hfont=hFontTitle)

	hFormatHex = Fn'LibXL'AddFormat'SetAttributes(numfmtid=NUMFMT_TEXT,shrinkfit=1,hbook=hBook2)
	hFormatNum = Fn'LibXL'AddFormat'SetAttributes(numfmtid=NUMFMT_GENERAL,hbook=hBook2)
	hFormatText = Fn'LibXL'AddFormat'SetAttributes(numfmtid=NUMFMT_TEXT,hbook=hBook2)
	
	! create a sheet showing the fonts defined
	! Note: Excel and Open Office seem to define a bunch of fonts even if you don't use them!!!
	fontcount = Fn'LibXL'GetFontCount(hBook1)
	? "Font count: ";fontcount
	if fontcount > 0 then
		? "Adding new 'Fonts' sheet...";tab(-1,254);				
		hSheetFonts = Fn'LibXL'AddSheet("Fonts",hbook=hBook2)		

		if hSheetFonts then		

			! Note that the sheet handle will indirectly set the book handle to match, so we don't need to worry about 
			! establishing the correct book when using Sheet functions
			
			! output a title for the page
			call Fn'LibXL'SheetSetMerge(hSheetFonts,1,1,1,9)		! create a single cell across top
			call Fn'LibXL'SheetSetRow(hSheetFonts,1,24)				! and make it 24 points tall
			call Fn'LibXL'SheetWrite(hSheetFonts,1,1,"Fonts defined in "+spreadsheet'in$,hFormatTitle)
			            
			! output the column headers for the font attributes
			call Fn'LibXL'SheetWrite(hSheetFonts,2,1,"hFont",hFormatHdr)
			call Fn'LibXL'SheetWrite(hSheetFonts,2,2,"Name",hFormatHdr)
			call Fn'LibXL'SheetWrite(hSheetFonts,2,3,"Size",hFormatHdr)
			call Fn'LibXL'SheetWrite(hSheetFonts,2,4,"Bold",hFormatHdr)
			call Fn'LibXL'SheetWrite(hSheetFonts,2,5,"Ital",hFormatHdr)
			call Fn'LibXL'SheetWrite(hSheetFonts,2,6,"Strike",hFormatHdr)
			call Fn'LibXL'SheetWrite(hSheetFonts,2,7,"Under",hFormatHdr)
			call Fn'LibXL'SheetWrite(hSheetFonts,2,8,"Color",hFormatHdr)
			call Fn'LibXL'SheetWrite(hSheetFonts,2,9,"Script",hFormatHdr)
            
			call LibXL'SheetSetSplit(hSheetFonts,row=3)     ! [101] freeze-scroll top 2 rows
            
			for i = 1 to fontcount
				font.hfont = Fn'LibXL'GetFont(hbook=hBook1, fntidx=i)
				call LibXL'FontGetAttributes(font.hfont, font.name, font.size, font.bold, font.italic, font.strikeout, &
								font.underline, font.color, font.script)
								
				call Fn'LibXL'SheetWrite(hSheetFonts,i+2,1,Fn'Dec2Hex$(font.hfont),hFormatHex)
				call Fn'LibXL'SheetWrite(hSheetFonts,i+2,2,font.name,hFormatText)
				call Fn'LibXL'SheetWrite(hSheetFonts,i+2,3,font.size,hFormatNum)
				call Fn'LibXL'SheetWrite(hSheetFonts,i+2,4,font.bold,hFormatNum)
				call Fn'LibXL'SheetWrite(hSheetFonts,i+2,5,font.italic,hFormatNum)
				call Fn'LibXL'SheetWrite(hSheetFonts,i+2,6,font.strikeout,hFormatNum)
				call Fn'LibXL'SheetWrite(hSheetFonts,i+2,7,font.underline,hFormatNum)
				call Fn'LibXL'SheetWrite(hSheetFonts,i+2,8,str(font.color)+" "+Fn'LibXL'xltColor$(font.color),hFormatText)
				call Fn'LibXL'SheetWrite(hSheetFonts,i+2,9,str(font.script)+" "+Fn'LibXL'xltScript$(font.script),hFormatText)
                
                ![105] add the font to the destination book (for the benefit of the OrigData sheet)
                ![105] note that font handle in dest book will be different than handle in source;
                ![105] use an ordmap to map the src font handle to the dst font handle
                $hfont(font.hfont) = Fn'LibXL'AddFont(hinitfont = font.hfont, hbook = hBook2)
                ? ".";tab(-1,254);
			next i

			! use SetCol to force autowidth 
			call Fn'LibXL'SheetSetCol(hSheetFonts,2,9,width=-1)
			
			! for some reason, width=-1 doesn't work reliably, so try specific widths for problem columns
			call Fn'LibXL'SheetSetCol(hSheetFonts,1,1,width=10)
			
		endif
	endif
    ?
	! create another sheet showing the formats defined
	formatcount = Fn'LibXL'GetFormatCount(hBook1)
	? "Format count: ";formatcount
	if formatcount > 0 then
		? "Adding new 'Formats' sheet...";				
		hSheetFormats = Fn'LibXL'AddSheet("Formats",hbook=hBook2)		
		
		if hSheetFormats then

			! output a title for the page
			call Fn'LibXL'SheetSetMerge(hSheetFormats,1,1,1,16)		! create a single cell across top
			call Fn'LibXL'SheetSetRow(hSheetFormats,1,24)			! and make it 24 points tall
			call Fn'LibXL'SheetWrite(hSheetFormats,1,1,"Formats defined in "+spreadsheet'in$,hFormatTitle)

			! output the column headers for the format attributes
			call Fn'LibXL'SheetWrite(hSheetFormats,2,1,"hFormat",hFormatHdr)		! (so we don't need hbook=hBook2 on each of these)
			call Fn'LibXL'SheetWrite(hSheetFormats,2,2,"hFont",hFormatHdr)	
			call Fn'LibXL'SheetWrite(hSheetFormats,2,3,"NumFmtId",hFormatHdr)
			call Fn'LibXL'SheetWrite(hSheetFormats,2,4,"AlignH",hFormatHdr)
			call Fn'LibXL'SheetWrite(hSheetFormats,2,5,"AlignV",hFormatHdr)
			call Fn'LibXL'SheetWrite(hSheetFormats,2,6,"Wrap",hFormatHdr)
			call Fn'LibXL'SheetWrite(hSheetFormats,2,7,"ShrinkFit",hFormatHdr)
			call Fn'LibXL'SheetWrite(hSheetFormats,2,8,"Rotation",hFormatHdr)
			call Fn'LibXL'SheetWrite(hSheetFormats,2,9,"Indent",hFormatHdr)
			call Fn'LibXL'SheetWrite(hSheetFormats,2,10,"Borderstyle",hFormatHdr)
			call Fn'LibXL'SheetWrite(hSheetFormats,2,11,"Bordercolor",hFormatHdr)
			call Fn'LibXL'SheetWrite(hSheetFormats,2,12,"Fillpattern",hFormatHdr)
			call Fn'LibXL'SheetWrite(hSheetFormats,2,13,"PatternFGC",hFormatHdr)
			call Fn'LibXL'SheetWrite(hSheetFormats,2,14,"PatternBGC",hFormatHdr)
			call Fn'LibXL'SheetWrite(hSheetFormats,2,15,"Locked",hFormatHdr)
			call Fn'LibXL'SheetWrite(hSheetFormats,2,16,"Hidden",hFormatHdr)
            
            call LibXL'SheetSetSplit(hSheetFormats,row=3)     ! [101] freeze-scroll top 2 rows
            
			for i = 1 to formatcount
				format.hformat = Fn'LibXL'GetFormat(hbook=hBook1, fmtidx=i)
				call LibXL'FormatGetAttributes(format.hformat, format.numfmtid, format.alignh, format.alignv, format.wrap, &
							format.shrinkfit, format.rotation, format.indent, format.borderstyle, format.bordercolor, &
							format.fillpattern, format.patternfgc, format.patternbgc, format.locked, format.hidden)
				format.hfont = Fn'LibXL'FormatGetFont(format.hformat)
				
				call Fn'LibXL'SheetWrite(hSheetFormats,i+2,1,Fn'Dec2Hex$(format.hformat),hFormatHex)
				call Fn'LibXL'SheetWrite(hSheetFormats,i+2,2,Fn'Dec2Hex$(format.hfont),hFormatHex)
				call Fn'LibXL'SheetWrite(hSheetFormats,i+2,3,str(format.numfmtid)+" ("+Fn'LibXL'xltNumFmt$(format.numfmtid)+")",hFormatText)
				call Fn'LibXL'SheetWrite(hSheetFormats,i+2,4,str(format.alignh)+" ("+Fn'LibXL'xltAlignH$(format.alignh)+")",hFormatText)
				call Fn'LibXL'SheetWrite(hSheetFormats,i+2,5,str(format.alignv)+" ("+Fn'LibXL'xltAlignV$(format.alignv)+")",hFormatText)
				call Fn'LibXL'SheetWrite(hSheetFormats,i+2,6,format.wrap,hFormatNum)
				call Fn'LibXL'SheetWrite(hSheetFormats,i+2,7,format.shrinkfit,hFormatNum)
				call Fn'LibXL'SheetWrite(hSheetFormats,i+2,8,format.rotation,hFormatNum)
				call Fn'LibXL'SheetWrite(hSheetFormats,i+2,9,format.indent,hFormatNum)
				call Fn'LibXL'SheetWrite(hSheetFormats,i+2,10,str(format.borderstyle)+" ("+Fn'LibXL'xltBorderStyle$(format.borderstyle)+")",hFormatText)
				call Fn'LibXL'SheetWrite(hSheetFormats,i+2,11,str(format.bordercolor)+" ("+Fn'LibXL'xltColor$(format.bordercolor)+")",hFormatText)
				call Fn'LibXL'SheetWrite(hSheetFormats,i+2,12,str(format.fillpattern)+" ("+Fn'LibXL'xltFillPattern$(format.fillpattern)+")",hFormatText)
				call Fn'LibXL'SheetWrite(hSheetFormats,i+2,13,str(format.patternfgc)+" ("+Fn'LibXL'xltColor$(format.patternfgc)+")",hFormatText)
				call Fn'LibXL'SheetWrite(hSheetFormats,i+2,14,str(format.patternbgc)+" ("+Fn'LibXL'xltColor$(format.patternbgc)+")",hFormatText)
				call Fn'LibXL'SheetWrite(hSheetFormats,i+2,15,format.locked,hFormatNum)
				call Fn'LibXL'SheetWrite(hSheetFormats,i+2,16,format.hidden,hFormatNum)
                
                ![105] add the format to the destination book (for the benefit of the OrigData sheet)
                

                ![105] add the format to the destination book (for the benefit of the OrigData sheet)
                ![105] note that format handle in dest book will be different than handle in source;
                ![105] use an ordmap to map the src format handle to the dst handle
                $hformat(format.hformat) = Fn'LibXL'AddFormat(hinitformat = format.hformat, hbook = hBook2)
                ? ".";tab(-1,254);  ![106] lifesigns
			next i
			
			! use SetCol to force autowidth 
			call Fn'LibXL'SheetSetCol(hSheetFormats,1,16,width=-1)

			! for some reason, width=-1 doesn't work reliably, so try specific widths for problem columns
			call Fn'LibXL'SheetSetCol(hSheetFormats,1,2,width=10)
			
		endif
	endif
    ?

	! now create a page showing the format handles for each of the cells in the first page of the source file
    ! [102] change to prompt for sheet #
    do
        input "Enter sheet # of source book to view [1]: ",sheetidx
        sheetidx = sheetidx max 1
        hSheetSrc = Fn'LibXL'GetSheet(sheetidx,hbook=hBook1)
        if hSheetSrc = 0 then
            ? "That sheet # doesn't exist!  Try again..."
            repeat
        endif
        exit
    loop
    
    ! [102] retrieve sheet name
    sheetname$ = Fn'LibXL'SheetGetName$(hSheetSrc)
    
	? "Adding new '";sheetname$;" Cell Formats' sheet...";
	hSheetCellFormats = Fn'LibXL'AddSheet(sheetname$+" Cell Formats",hbook=hBook2)		

	! we'll loop through the source page one, retrieving the format handle for each cell, then outputting to same cell in dst page 3
	
	! to make them easier to identify, we'll assign a different color to unique format
	! (unfortunately that requires a separate format handle for each, so lets start by creating a bunch of them

	for i = 1 to 32
		hColor(i) = Fn'LibXL'AddFormat'SetAttributes(numfmtid=NUMFMT_TEXT,fillpattern=FILLPATTERN_SOLID,patternfgc=i + COLOR_BLACK)
	next i

	nextcolor = 1
	
	![102] hSheetSrc = Fn'LibXL'GetSheet(1,hbook=hBook1)
	rowcount = Fn'LibXL'SheetGetLastRow(hSheetSrc) - 1		! LastRow is actually row past last used row
	colcount = Fn'LibXL'SheetGetLastCol(hSheetSrc) - 1		! LastCol is actually col past last used col
	
	
	for row = 1 to rowcount
		for col = 1 to colcount
			hFormat = Fn'LibXL'SheetGetCellFormat(hSheetSrc,row,col)
			if $hcolor(hFormat) = .NULL then
				$hcolor(hFormat) = hColor(nextcolor)			! if format not yet seen, assign next color to it
				nextcolor += 1								! increment the color index
				if nextcolor > .extent(hColor()) then		! wrap around if necessary
					nextcolor = 1
				endif
			endif
			call Fn'LibXL'SheetWrite(hSheetCellFormats,row,col,Fn'Dec2Hex$(hFormat),$hcolor(hFormat))
		next col
        if (row mod 25) = 0 then    ! [106] 
            ? "."; tab(-1,254);
        endif
	next row
    ?
    ![105] add a sheet which is a copy of the original data sheet 
    ![105] both as a convenience and to prove/demo ability to read/write
    ![105] note that we have 
    ? "Adding new 'OrigData' sheet...";				
    hSheetData = Fn'LibXL'AddSheet("OrigData",hbook=hBook2)		

    if hSheetData then		
        ! loop through every cell in source sheet, recreating it in target sheet
        ! (similar to loop for hSheetCellFormats except here we are reading the
        ! formant AND data instead of just the formats)
        for row = 1 to rowcount
            for col = 1 to colcount
                call Fn'CopyCell(hSheetSrc, hSheetData, row, col)
            next col
            if (row mod 25) = 0 then
                ? ".";tab(-1,254);  ![106] lifesigns
            endif
        next row
    endif
    ?
    
	! add a comment to the first cell to explain the page
	call LibXL'SheetWriteComment(hSheetCellFormats,1,1, &
		"This sheet shows the hFormat for each cell in the first sheet of "+spreadsheet'in$ &
		+" (different colors are used to make it easy to identify differing formats)" ,"libxlview",250,75)

	!------------------------------------------------------------------------------------------------------
	! now close/save the spreadsheet
	!------------------------------------------------------------------------------------------------------
!>![106]    
!>!	spreadsheet$ = ""
!>!	input "Name to save spreadsheet file [libxlview.xls]: ";spreadsheet$
!>!	if spreadsheet$ = "" then
!>!		spreadsheet$ = "libxlview.xls"
!>!	endif
	
	if lookup(spreadsheet'out$) then		! SaveBook refuses to overwrite?
		kill spreadsheet'out$
	endif
	call Fn'LibXL'SaveBook(spreadsheet'out$)
	
	? "Releasing book1"
	call LibXL'ReleaseBook(hBook1)
	? "Releasing book2"
	call LibXL'ReleaseBook(hBook2)
	
	input "Enter 1 to launch spreadsheet: ",i
	if i then
		![104] xcall MIAMEX, MX_SHELLEX, i, spreadsheet$
        status = Fn'Shell'Exec(spreadsheet'out$)    
        if i then
            ? Fn'Shell'Exec'Status'Description$(status)
        endif
	endif
	return

TRAP:
	? "Trapped error ";err(0);
	if err(0) = 65 then
		? "Error trapped in DYNLIB/LIBXL"
		! note: since the basic error might have been trapped before LibXL had a chance
		! to display/log the errors, force the log and print flags and call the hander again
		call Fn'LibXL'Handle'DynLib'Errors(DLF_LOG_ERRORS+DLF_PRINT_ERRORS)
		resume UNLOAD
	endif
	end


!---------------------------------------------------------------------
!Function:
!   duplicate a cell from one sheet to another
!Params:
!   hSheetSrc (SheetHandle) [in] - source sheet handle
!   hSheetDst (SheetHandle) [in] - dest sheet handle
!   row (num) [in] - row #
!   col (num) [in] - col #
!Returns:
!   0 for success, else error code
!Globals:
!   $hformat() maps source format handles to dest format handles
!Notes:
!---------------------------------------------------------------------
Function Fn'CopyCell(hsheetsrc as SheetHandle:inputonly, &
                     hsheetdst as SheetHandle:inputonly, &
                     row as b4:inputonly, col as b4:inputonly) as i4 

++extern $hformat()

	map1 locals
		map2 celltype,b,2
		map2 numfmtid,b,2
		map2 hformat,FormatHandle
		map2 hfont,FontHandle
!>!		map2 number,f,8
		map2 value$,s,0
		map2 bool,i,2
!>!		map2 year,b,4
!>!		map2 month,b,2
!>!		map2 day,b,2
!>!		map2 hour,b,2
!>!		map2 minute,b,2
!>!		map2 sec,b,2
!>!		map2 msec,b,2
!>!		map2 halign,b,1
!>!		map2 valign,b,1
!>!		map2 bwrap,i,2
!>!		map2 bshrinkfit,i,2
!>!		map2 nrotation,b,1
!>!		map2 nindent,b,1
!>!		map2 nborderstyle,b,1
!>!		map2 bordercolor, XLCOLOR
!>!		map2 nfillpattern,b,1
!>!		map2 patternfgc, XLCOLOR
!>!		map2 patternbgc, XLCOLOR
!>!		map2 blocked,i,2
!>!		map2 bhidden,i,2
!>!		map2 name$,s,64
!>!		map2 nsize,b,2
!>!		map2 bbold,i,2
!>!		map2 bitalic,i,2
!>!		map2 bstrikeout,i,2
!>!		map2 bunderline,i,2
!>!		map2 color,XLCOLOR
!>!		map2 nscript,b,2
        map2 rowheight,f,8
        map2 colwidth,f,8
        
    ! adjust row/col height to match cell (this will be hugely redundant 
    ! if the function is called in a loop for each cell, but...)
	rowheight = Fn'LibXL'SheetGetRowHeight(hsheetsrc,row)
	colwidth = Fn'LibXL'SheetGetColWidth(hsheetsrc,row)
    call Fn'LibXL'SheetSetRow(hsheetdst, row, rowheight)
   
	celltype = Fn'LibXL'SheetGetCellType(hsheetsrc,row,col)
    if celltype # CELLTYPE_EMPTY then
     	if Fn'LibXL'SheetIsFormula(hsheetsrc, row, col) then
            value$ = Fn'LibXL'SheetReadFormula$(hsheetsrc, row,col,hformat)
        else
            value$ = Fn'LibXL'SheetReadValue$(hsheetsrc,row,col,hformat)
            
!>!            switch celltype
!>!                case CELLTYPE_EMPTY
!>!                    ! blank cell, nothing to copy
!>!                    exit
!>!                case CELLTYPE_NUMBER
!>!                    value$ = Fn'LibXL'SheetReadNum(hsheetsrc, row, col, hformat)   
!>!                    exit
!>!                case CELLTYPE_STRING
!>!                    value$ = Fn'LibXL'SheetReadStr$(hsheet, row, col, hformat)
!>!                    exit
!>!                case CELLTYPE_BOOLEAN
!>!                    value$ = Fn'LibXL'SheetReadBool(hsheet, row, col, hformat)
!>!                    exit
!>!                case CELLTYPE_BLANK
!>!                    value$ = Fn'LibXL'SheetReadBlank(hsheet, row, col, hformat)
!>!                    exit
!>!                case CELLTYPE_ERROR
!>!                    value$ = "Err?"
!>!                    exit
!>!                default 
!>!                    value$ = "Unknown?"
!>!                    exit
!>!            endswitch
        endif
        call Fn'LibXL'SheetWrite(hsheetdst, row, col, value$, $hformat(hformat))
    endif

EndFunction
