:<(LIBXL) LIBXL Samples, Utilities, Libraries
  See www.bitbucket.org/microsabio/exlib/908068 for latest versions
  See readme.txt for more info

  LIBXL.DEF - definitions for LibXL library routines
  LIBXL.BSI - LibXL library routines
  LIBXL2.BSI - Additional routines (not typically needed in apps)
  LIBXLVIEW.BP - Create spreadsheet illustrating formatting of another
  LIBXLIAR.BP - Interactive read/query/display cell data 
  AXLDIFF.BP - Create workbook highlighting differences between two others
  
  TSTACB.BP - Sample illustrating alignment, borders, colors
  TSTFORMULA.BP - Sample illustrating formulas
  TSTGROUP.BP - Sample illustrating groups
  TSTNUMFOR.BP - Sample of numeric formats
  TSTACCENTS.BP - Test handling of Latin1 chars 129-255
  TSTXLCSV.BP - Generate CSV data for CSVXLS 
  TSTR1C1 - Test conversion between normal and R1C1 addressing
  SAMXLINV.BP - Sample invoice (with PNG image)

Note: this directory should have ersatz LIBXL: pointing to it.

>
 
