program TABXYZ,1.0(100)		! test/demo 3 dimensional tab
!-------------------------------------------------------------------------
!EDIT HISTORY
![100] November 29, 2005 04:59 PM       Edited by joaquin
!	Created
!
!NOTES
! 3 dimensional tabs, e.g. TAB(x,y,z) are like two dimensional tabs
! except that the third "dimension" allows you to associate a variety
! of text attributes with whatever prints next (in that PRINT statement).
! The Z value is used as an index into a table of attributes which you
! can fill out using xcall MIAMEX,MX_DEFTABXYZ,...
!
!	XCALL MIAMEX,MX_DEFTABXYZ,STS,ZIDX,FGC,BGC,PTYPE, &
!            FONTATTR,FONTSCALE,FONTFACE
!
! where
!  STS (F,6) [out] returns >= 0 for success, else error
!  ZIDX (numeric) [in] is the index (Z in the TAB example)
!  FGC,BGC (numeric) [in] are the fg and bg color values (-1 for current
!	ambient colors, -2 for standard windows colors)
!  PTYPE (S,2) may specify style and justification.  First character is
!	T (tprint), D (dprint) or E (eprint); second character is L (left),
!	C (center), R (right), or A (autojust).  
!	If left blank or null, then output will
!	use standard text (non GUI) and remaining params ignored.
!  FONTATTR, FONTSCALE, FONTFACE [in] are as in AUI_CONTROL
!
!  All are optional after FGC.
!-------------------------------------------------------------------------

++pragma ERROR_IF_NOT_MAPPED "ON"
++include ASHINC:ASHELL.DEF

MAP1 PARAMS
    MAP2 ZIDX,B,1
    MAP2 FGC,F,6
    MAP2 BGC,F,6
    MAP2 PTYPE,S,2
    MAP2 FATTR,B,4
    MAP2 FSCALE,B,2
    MAP2 FFACE,S,32
    MAP2 STS,F

! set up some definitions..
! ZIDX,fgc,bgc,gui,attr,scale,face

! Yellow on cyan, non-GUI
data 1,5,7,"",0,0,""

! blue text, non-GUI
data 2,2,5,"",0,0,""

! Standard GUI (TPRINT)
data 3,-2,-2,"TL",0,0,""

! Magenta, Large, bold, italic GUI
data 4,67,-2,"TL",14401,350,""

! Sunken Lucida
data 5,-2,-2,"DL",0,0,"Lucida Console"

! eprint, right just
data 6,-2,-2,"ER",0,0,""

! end of data
data 0

    ? tab(-1,0);"TABXYZ - test 3 dimensional tabl"

    ! Note: default table size is at least 20, but if we wanted more,
    ! we could allocate it with the following:
    ! xcall MIAMEX,MX_DEFTABXYZ,STS,0,<TABLE'ENTRIES>
    ! (return STS will indicate the new max entries allowed)

    do 
        read ZIDX
        if ZIDX > 0 then
            read FGC,BGC,PTYPE,FATTR,FSCALE,FFACE
            xcall MIAMEX,MX_DEFTABXYZ,STS,ZIDX,FGC,BGC,PTYPE,FATTR, &
                    FSCALE,FFACE
            ? "Define ZIDX ";ZIDX;",";FGC;",";BGC;",";PTYPE;","; &
		FATTR;",";FSCALE;",";FFACE;"... status = ";STS
        endif       
    loop until ZIDX = 0 or STS < 0


    tprint tab(9,10,1);"This is TAB(9,10,1)";
    print tab(11,10,2);"This is TAB(11,10,2)"
    print tab(13,10,3);"This is TAB(13,10,3)"
    print tab(15,10,4);"This is TAB(15,10,4)"
    print tab(17,10,5);" This is TAB(17,10,5) "	! lead/trail space to autoctr
    print tab(19,10,6);"This is TAB(19,10,6)"

    ! now undefine one and repeat the output
    xcall MIAMEX,MX_DEFTABXYZ,STS,6
    print tab(20,10,6);"This is TAB(20,10,6) (after undefining ZIDX 6)"

end
