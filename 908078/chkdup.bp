program CHKDUP, 1.0(100)  ! check if another job running prog and exit if so
!------------------------------------------------------------------------
!EDIT HISTORY
!Version 1.0:-
! [100] 03-Jun-18 / jdm / created
!------------------------------------------------------------------------
!DESCRIPTION
!   Checks to see if the specified program is running on another job. 
!   If so, we terminate this session, otherwise do nothing.  The intent
!   is to allow it to be inserted into a CTL or CMD file that launches
!   a background job of which you want to be sure that only one is running.
!
!USAGE
!   .RUN CHKDUP <program>
! or
!   .RUN CHKDUP
!   Enter program: 
!
!REQUIREMENTS
!
!NOTES
!   program name 1-10 chars, no extension, not case sensitive
!------------------------------------------------------------------------

++include'once ashinc:ashell.def
++include'once sosfunc:fnisrun.bsi

MAP1 GLOBALS
    MAP2 PROG$,S,10
    MAP2 JOBNO,I,2
    MAP2 JOBNAM$,S,10
    MAP2 PID,B,4
    
    if CMDLIN # "" then
        if CMDLIN[1,1] = "/" then
            goto HELP
        else
            PROG$ = CMDLIN
        endif
    else
        input line "Enter program to check for: ",PROG$
    endif
	PROG$ = UCS(PROG$)
    ? "Checking if program ";PROG$;" running..."
    JOBNO = Fn'Is'Running'Program(prog$=PROG$,jobnam$=JOBNAM$,pid=PID)
    if JOBNO > 0 then
        ? "%Aborting this process since ";PROG$;" already running."
        sleep 1
       	xcall MIAMEX, MX_EXIT       ! exit A-Shell
    else
        if JOBNO < 0 then
            ? PROG$;" was running on job ";JOBNAM$;" but has exited"
        else
            ? PROG$;" not running"
        endif
        ? "OK to proceed..."
    endif
	end

HELP:
    ? "Usage:"
    ? "    .CHKDUP <program>"
    ? 
    ? "If <program> is running on another job, the current job exits A-Shell."
    ? "Otherwise it does nothing. Useful in CMD files that launch programs"
    ? "which should only be run by one instance at a time."
    end
