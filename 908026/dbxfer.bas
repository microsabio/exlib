program DBXFER,1.0(100)  ! Perform Transfers / Opens on Various Systems 
!-------------------------------------------------------------------------------------
!NOTES:
!This is actually just an excerpt of code contributed by Joe Leibel, 
!which handles file transfers between various kinds of systems (including
!AMOS) using various emulators (ZTERM, ATE, pcVision, TODOS/FMDOS).
!It's not actually a working program in it's present form and I (Jack)
!haven't had a chance to play with it, clean it up, etc.  But it could
!still be a useful resource for anyone faced with the same issues.
!--------------------------------------------------------------------------
!EDIT HISTORY
![100] February 05, 2008 12:59 PM       Edited by JACK
!    Added "as-is" to SOSLIB (from Joe Leibel's contribution). 
        RETURN


    ? "DBXFER - Routines for transferring files in various environments"
    ? "Please see source code for details.  (Not currently a running program.)"
    end

SEND'FILE'TO'PC: REM ************ Send a File to the PC **************
        GOSUB GET'HOST
        IF HOST$[1;4] <> "AMOS" THEN GOTO SEND'FILE'USING'ASHELL
        IF PARENT$[1;3] = "TCP" OR PARENT$[1;3] = "TSK" THEN GOTO SEND'FILE'USING'FTP
        IF FALCON <> 0 THEN GOTO SEND'FILE'USING'FALCON
        GOTO SEND'FILE'USING'ZTERM


SEND'FILE'USING'ASHELL: REM ******* Transfer to PC using Ashell ******
        IF ATE'ASHELL$ = "Y" THEN GOTO SEND'FILE'USING'ATEAPX 
        OPEN # 60099,AMOS'DIR$ + AMOS'FILE$,INPUT
        OPEN # 60090,PC'DIR$ + PC'FILE$,OUTPUT
        XCALL MIAMEX,27,60099,60090
        CLOSE # 60090
        CLOSE # 60099
        GOTO SEND'FILE'COMPLETE


SEND'FILE'USING'ATEAPX: REM ****** Transfer if Ashell using ATE ****** 
        XCALL ATEAPX,AMOS'DIR$ + AMOS'FILE$,PC'DIR$,PC'FILE$,2,STATUS ! 28-Sep-06
        GOTO SEND'FILE'COMPLETE


SEND'FILE'USING'ZTERM: REM ******* Transfer to PC using Zterm ******
        S0$ = "ZTXFER " + AMOS'DIR$ +  AMOS'FILE$ + " " + PC'DIR$ + PC'FILE$
        ?TAB(-1,242);   ! Turn Off Multi
        XCALL AMOS,S0$
        GOTO SEND'FILE'COMPLETE


SEND'FILE'USING'FTP: REM ***** Transfer to PC with FTP using Zterm ******
        ?TAB(-1,242);
        ?CHR$(27);CHR$(1);CHR$(127);            ! Start Z-Term FTP Transfer
        ?"0";                                   ! Host to PC
        ?AMOS'DIR$;CHR$(0);                     ! Host Directory
        ?AMOS'FILE$;CHR$(0);                    ! Host File Name
        ?PC'DIR$ + PC'FILE$;CHR$(0);            ! PC File Name
        INPUT "";S0$                            ! Wait till done
        GOTO SEND'FILE'COMPLETE


SEND'FILE'USING'FALCON: REM ******* Transfer to PC using Falcon ******
        S0$ = "TODOS " + PC'DIR$ + PC'FILE$ + "=" + AMOS'FILE$ 
        XCALL AMOS,S0$
        GOTO SEND'FILE'COMPLETE


SEND'FILE'USING'PCV: REM ******* Transfer to PC using PC-Visions ******
        S0$ = "RECEIVE " + PC'DIR$ + PC'FILE$ + " FROM " + AMOS'DIR$ + AMOS'FILE$ + " DELETE"
        ?TAB(-1,242);           ! Turn off Multi
        COSTAR$ = CHR$(27) + "[!v" + CHR$(27) + "&oC"
        ?COSTAR$;S0$;CHR$(13);
        GOTO SEND'FILE'COMPLETE


SEND'FILE'COMPLETE:     ! 07-May-02
        ?TAB(-1,243);   ! Turn On  Multi
        IF EXECUTE'PC'FILE$ = "Y" THEN GOTO EXECUTE'PC'FILE
        RETURN


SEND'FILE'OK'TO'PROCEED:
        IF CONFIRM'TRANSFER$ = "N" THEN RETURN  ! 07-Jul-07
        HELP$ = "'" + PC'FILE$ + "' has been sent.  Press 'CR' 'R'etry or 'E'nd :"
        ROW = 23 : COL = LEN(HELP$) + 3 : LN = 2 : INFLD'SPECIAL$ = "^"
        GOSUB GET'INPUT
        ?TAB(-1,243);   ! Turn On  Multi
        IF S0$ = "R" THEN GOTO DBXFER
        RETURN


! ******************************************************************** !


RECEIVE'FILE'FROM'PC:
        GOSUB GET'HOST
        IF HOST$[1;4] <> "AMOS" THEN GOTO RECEIVE'FILE'USING'ASHELL
        IF PARENT$[1;3] = "TCP" OR PARENT$[1;3] = "TSK" THEN GOTO RECEIVE'FILE'USING'FTP
        IF FALCON <> 0 THEN GOTO RECEIVE'FILE'USING'FALCON
        GOTO RECEIVE'FILE'USING'ZTERM


RECEIVE'FILE'USING'ASHELL: REM ******* Transfer to PC using Ashell ******
        END ! Need to Try this
        OPEN # 60099,AMOS'FILE$,OUTPUT 
        OPEN # 60090,PC'DIR$ + PC'FILE$,INPUT
        XCALL MIAMEX,27,60099,60090
        CLOSE # 60090
        CLOSE # 60099
        GOTO RECEIVE'FILE'COMPLETE


RECEIVE'FILE'USING'ZTERM: REM ******* Transfer to PC using Zterm ******
        S0$ = "ZTXFER " + AMOS'DIR$ + AMOS'FILE$ + "=" + PC'DIR$ + PC'FILE$
        ?TAB(-1,242);   ! Turn Off Multi
        XCALL AMOS,S0$
        GOTO RECEIVE'FILE'COMPLETE


RECEIVE'FILE'USING'FALCON: REM ******* Retrieve from PC on Falcon ******
        S0$ = "FMDOS " + AMOS'FILE$ + "=" + PC'DIR$ + PC'FILE$
        XCALL AMOS,S0$
        GOTO RECEIVE'FILE'COMPLETE


RECEIVE'FILE'USING'FTP: REM ***** Retrieve from PC with FTP using Zterm ******
        ?TAB(-1,242);
        IF AMOS'GUI$ = "YES" THEN XCALL SLEEP,1
        ?CHR$(27);CHR$(1);CHR$(127);            ! Start Z-Term FTP Transfer
        ?"1";                                   ! PC to Host
        ?AMOS'DIR$;CHR$(0);                     ! Host Directory
        ?AMOS'FILE$;CHR$(0);                    ! Host File Name
        ?PC'DIR$ + PC'FILE$;CHR$(0);            ! PC File Name
        INPUT "";S0$                            ! Wait till done
        ?TAB(-1,243);
        GOTO RECEIVE'FILE'COMPLETE


RECEIVE'FILE'COMPLETE:
        IF CONFIRM'TRANSFER$ = "N" THEN RETURN  ! 07-Jul-07
        HELP$ = "File '" + AMOS'FILE$ + "' has been received.  Press 'CR' or 'R' to Retry :"
        ROW = 23 : COL = LEN(HELP$) + 3 : LN = 2 : INFLD'SPECIAL$ = "^"
        GOSUB GET'INPUT
        ?TAB(-1,243);   ! Turn On  Multi
        IF S0$ = "R" THEN GOTO DBXFER
        RETURN



! ******************************************************************** !


EXECUTE'PC'FILE: REM ******** Open the File on a PC *******
        IF FALCON <> 0 THEN GOTO EXECUTE'FAILED
        IF AMOS'GUI$ = "YES" THEN GOTO EXECUTE'USING'ATE
        IF HOST$[1;4] = "AMOS" THEN GOTO EXECUTE'USING'ZTERM
        IF TDVNAM$ = "AM62CG" THEN GOTO EXECUTE'USING'ATE ! ATE/Ashell 21/Mar/06 


EXECUTE'USING'ASHELL: REM ****** Execute File under Ashell ******
        XCALL MIAMEX,96,STATUS,PC'DIR$ + PC'FILE$
        RETURN


EXECUTE'USING'ATE: REM ****** Execute File under ATE/AMOS ****** 02-Sep-05
        ?TAB(-10,24);PC'DIR$ + PC'FILE$;CHR$(127);
        RETURN


EXECUTE'USING'ZTERM: REM ****** Execute File using Zterm *******
        ?TAB(-1,242);   ! Turn Off Multi
        ?CHR$(27);CHR$(12); "O" ; PC'DIR$ + PC'FILE$ ;CHR$(0);
        XCALL SLEEP,5
        ?TAB(-1,243);   ! Turn On Multi
        RETURN


PRINT'FILE'USING'ASHLITE: REM ******* Print using Ashell Lite *******
        S0$ = "Ashlite -e -z PRINT " + PC'DIR$ + PC'FILE$
        ?TAB(-1,242);
        ?CHR$(27);CHR$(22);S0$;CHR$(0); 
        XCALL SLEEP,3
        ?TAB(-1,243);
        INPUT LINE ZTERM$
        RETURN


PRINT'FILE'USING'ASHELL: REM ******* Print using Ashell *******
        S0$ = "C:\VM\MIAME\BIN\ASHW32.EXE -i C:\VM\MIAME\MIAME.INI -e -z PRINTX.DO " + PC'DIR$ + PC'FILE$
        ?TAB(-1,242);
        ?CHR$(27);CHR$(22);S0$;CHR$(0); 
        XCALL SLEEP,3
        ?TAB(-1,243);
        INPUT LINE ZTERM$
        RETURN


GET'HOST: REM ******** Determine if its AMOS/ASHELL/FALCON ********
        LOOKUP "SYS:FALCON.INI",FALCON !   'SYSTEM
        IF HOST$[1;4] <> "AMOS" AND TDVNAM$ = "AM62CG" THEN ATE'ASHELL$ = "Y" ! 28-Sep-06
        IF HOST$[1;4] = "AMOS" THEN GOTO GET'AMOS'DIR


GET'AMOS'DIR: REM ***** Get PPN the User is Logged into *******
        IF AMOS'DIR$ <> "" THEN RETURN
        AMOS'DIR$ = JOBDEV$ + ":[" + JOB'PRJ$ + "," + JOB'PRG$ + "]"
        RETURN


EXECUTE'FAILED:
        HELP$ = "Command cannot be executed.  Press 'CR' :"
        ROW = 23 : COL = LEN(HELP$) + 3 : LN = 2
        GOSUB GET'INPUT
        RETURN
