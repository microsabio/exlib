:<(ZTERM) Programs illustrating ZTERM developer sequences
    ZTEST  - Illustrates various ZTERM ESC sequences (menu driven)
    ZXPWIN - Transfer a file from UNIX to PC; launch associated app
    DBXFER - Illustrates file transfers between ASHELL/AMOS/PC using various
             methods (ZTERM, ATE, pcVision, TODOS/FMDOS) From Joe Leibel
>

