program ZXPWIN,1.0(008)
!	ZXPWIN.BAS
!
!	Utility to transfer a file to the remote PC and then open it
!	with the associated application.  Requires that the emulator
!	be ZTERM.  By default, it transfers the file to the default
!	current PC directory (probably where ZTERM was launched from)
!	but you can pass an optional directory specifier.  You can
!	also pass a new file extension, in case the file extension on
!	on the host doesn't associate with a registered file type on
!	Windows.
!--------------------------------------------------------------------------
!EDIT HISTORY
![001] December 13, 2001  09:16 AM	Edited by JDM
!	Created (variation of EXPZT2)
![002] February 15, 2002 08:16 AM       Edited by jack
!	Fix bug when native filespec specified; improve error message;
!	strip out any quotes surrounding (ext)
![003] September 13, 2002 06:49 AM      Edited by jack
!	This version is ASCII 
![004] June 16, 2003 05:23 PM           Edited by jack
!      Reinstate check for ZTERM
![005] January 25, 2005 02:18 PM        Edited by jack
!	/NO switch to just transfer file (no launch of app);
!	also supports ATE now too /jdm
![006] January 26, 2005 08:35 AM        Edited by jack
!	Fix launch bug in [005]
![007] December 04, 2005 11:16 AM       Edited by joaquin
!	Improve error message, use ashell.def symbols
![008] August 07, 2013 04:43 PM         Edited by jackmc
!	Fix test for ATE
!--------------------------------------------------------------------------

MAP1 MISC
	MAP2 CMD$,S,500
	MAP2 SETCMD$,S,500
	MAP2 OP,F
	MAP2 VAR$,S,20
	MAP2 VALUE$,S,60
	MAP2 CARD$,S,60
	MAP2 PLINE,S,500
	MAP2 HOSTFILE$,S,100
	MAP2 PCFILE$,S,100
	MAP2 PCFILEXT$,S,32
	MAP2 WINCMD$,S,80
	MAP2 HOSTDIR$,S,100
	MAP2 STATUS,F
	MAP2 LOCALPATH$,S,200
	MAP2 X,F
	MAP2 Y,F
        MAP2 ABORT,F
	MAP2 CR$,S,2
	MAP2 S$,S,2
	MAP2 R$,S,2
	MAP2 A,B,1
	MAP2 B,B,1
	MAP2 C,B,1
	MAP2 EXT$,S,5
	MAP2 JOBNAM$,S,6
	MAP2 TRMNAM$,S,6
	MAP2 TRMTDV$,S,6
	MAP2 NOLAUNCH,B,1		! [5]
	MAP2 PRGVER,S,20		! [5]
	MAP2 DISK$,S,6			! [7] DSK01
	MAP2 PPN$,S,10			! [7] [p,pn]
	MAP2 ENVFLAGS,B,4		! [8]

++include ashinc:ashell.bsi
++include ashinc:ashell.def		! [8]

	on error goto TRAP
	xcall GETVER,PRGVER		! [5]
	? tab(-1,0);"ZXPWIN ";PRGVER;" - UNIX to PC (via ZTERM) File Export Utility"
	? "Command Line: ";A2S
	?
	? "[Note: if process appears to die, hit Control-C to abort]"
	?

	! First see if we are running zterm...

	? tab(2,1);"Checking to see if ZTERM is running...";tab(-1,254);

	!Set image mode on so that ACCEPT works
	XCALL NOECHO

	! [8] check directly for ATE
	xcall AUI, AUI_ENVIRONMENT, 2, ENVFLAGS
	if (ENVFLAGS and AGF_ATE) then
            ? "OK [ATE]"
        else
	    ! [4] First see if we are in Wyse50 emulation (because it sends
	    ! [4] back an extra CR in the sequence below...
	    xcall JOBTRM,JOBNAM$,TRMNAM$,TRMTDV$
	
            !Send three ESC ? characters back to back. Normally sends the row/col
	    print CHR$(27);"?";
	    xcall ACCEPN,A : xcall ACCEPN,B : print A;B;tab(-1,254);
	    if TRMTDV$="WYSE50" xcall ACCEPN,C : print C;		! [4]
	    print CHR$(27);"?";
	    xcall ACCEPN,A : xcall ACCEPN,B : print A;B;tab(-1,254);
	    if TRMTDV$="WYSE50" xcall ACCEPN,C : print C;		! [4]
	    print CHR$(27);"?";
	    xcall ACCEPN,A : xcall ACCEPN,B : print A;B;tab(-1,254);
	    if TRMTDV$="WYSE50" xcall ACCEPN,C : print C;		! [4]

	    xcall TINKEY,C	! [4] WYSE50 may send an extra CR

	    !ZTERM sends "ZT" back on the third - non-ZTERM terminals and emulators
	    !send the row and column again
	    if (A=ASC("Z") or A=ASC("A")) and B=ASC("T") ! [8] AT for ATE
		print " OK" 
	    else
		print
		print "ERROR: Cannot continue - PC not running ZTERM/ATE in AM62x/WY50 emulation"
		goto ABORT
	    endif
        endif

	xcall ECHO

	! pick up the command line args 
	PLINE = A2S[A2,-1]
	xcall TRIM,PLINE
	if PLINE="" or PLINE = "/?" or PLINE="?" or PLINE="/H" goto HELP
	
	! [5] check for /NL
	if ucs(PLINE[1,3]) = "/NL" then
	    PLINE = PLINE[4,-1]
  	    xcall TRIM,PLINE
	    NOLAUNCH = 1
	endif

	! see if we got a target dir arg
	PCFILE$ = ""			! start by assuming no
	X = instr(1,PLINE,"=")
	if X > 1 then 
		PCFILE$ = PLINE[1,X-1]
		PLINE = PLINE[X+1,-1]
		xcall TRIM,PCFILE$
		if PCFILE$[-1,-1] # "\" then PCFILE$ = PCFILE$ + "\"
	endif
	
	! now see if we have (<ext>)
	xcall TRIM,PLINE

	! convert "(xxx)" to (xxx)
	if PLINE[1,2]="""(" then
	    X = instr(1,PLINE,")""")
	    if X < 1 or X > 6 then
	        print "?Syntax error - (ext)"
		goto HELP
	    endif
	    PLINE = PLINE[2,X] + PLINE[X+2,-1]  
	endif
	if PLINE[1,1]="(" then
	    X = instr(1,PLINE,")") 
	    if X < 1 or X > 6 then 
	        print "?Syntax error - (ext)"
		goto HELP
	    endif
	    EXT$ = PLINE[2,X-1]
	    PLINE = PLINE[X+1,-1]
	endif

	! remainder of line is HOSTFILE$
	HOSTFILE$ = PLINE
	xcall TRIM,HOSTFILE$

	! convert HOSTFILE$ from AMOS format to native format

	if instr(1,HOSTFILE$,"/") < 1 then	
	    xcall MIAMEX,MX_FSPEC,HOSTFILE$,LOCALPATH$,"CSV", &
		FS_FMA+FS_TOH,DDB,STATUS	! [7]
	    if STATUS # 0 then 
		? tab(-1,10);					! [7]
		? "Error ";STATUS;
		if STATUS=1 then ? "(File specification error)";! [7]
		if STATUS=13 then ? "(Device does not exist)"; 	! [7]
		if STATUS=20 then ? "(Invalid filename)"; 	! [7]
		? " returned from MIAMEX (MX_FSPEC!) " 
		? "Hostfile: ";HOSTFILE$;   
		? "    Current PPN: ";				! [7]
		xcall DSKPPN,DISK$,PPN$				! [7]
		? DISK$;":";PPN$				! [7]
		goto ABORT
	    endif

            HOSTFILE$ = LOCALPATH$

	else    ! specified file already in native format

	    LOCALPATH$ = HOSTFILE$  	! already in native format

	endif

	! LOCALPATH$ and HOSTFILE$ now same (native format)

        ! Reduce HOSTFILE$ to just file.ext

        Y = 1
        do while Y > 0
            Y = instr(1,HOSTFILE$,"/")
            if Y > 0 then HOSTFILE$ = HOSTFILE$[Y+1,-1]
        loop

        ! Set HOSTDIR$ to be just the path without the file.ext
        xcall STRIP,LOCALPATH$
        xcall STRIP,HOSTFILE$
        HOSTDIR$ = LOCALPATH$[1,len(LOCALPATH$)-len(HOSTFILE$)-1]

	! append file.ext to PCFILE$
	PCFILEXT$ = HOSTFILE$
	if EXT$ # "" then		! replace extension
		X = instr(1,PCFILEXT$,".")
		if X < 1 then
		    PCFILEXT$ = PCFILEXT$ + "." + EXT$
		else
		    PCFILEXT$ = PCFILEXT$[1,X] + EXT$
		endif
	endif
	PCFILE$ = PCFILE$ + lcs(PCFILEXT$)
	
	? "Transferring ";HOSTDIR$;"/";HOSTFILE$;" to ";PCFILE$
	
	! Transfer the file...
	ABORT = 1		  ! in case error trapped
	? chr(27);chr(1);chr(127);"2";HOSTDIR$;chr(0);   ! ASCII mode
	? HOSTFILE$;chr(0);
	? PCFILE$;chr(0);

	? tab(-1,9);
	input "Waiting for transfer acknowledgement... ",CR$
	if asc(CR$)=3 goto ABORT  
	? 
	? "Acknowledgment received"
	? "Transfer complete"

	if NOLAUNCH = 0 then			! [5]
	    ! Use Shell Execute to launch associated app...
	    ABORT = 2		  ! in case error trapped

	    ? chr(27);chr(12);"O";PCFILE$;chr(0);
!? "chr(27);chr(12);O;";PCFILE$;";chr(0);..."
	    input "Waiting for launch acknowledgement... ",CR$
	    if asc(CR$)=3 goto ABORT
	    ? 
	    ? "Acknowledgment received"
	    ? "Launch complete"
	endif
	end

HELP:
	? "Usage:  .ZXPWIN {/NL} {<dir>=}{(<ext>)}<file>"	! [5]
	? "        /NL (No Launch) defeats launch of app (xfer only)" ![5]
	? "Examples:"
	? "        .ZXPWIN TEST.DOC"
	? "        .ZXPWIN C:\TEMP=DSK1:TEST.DOC[77,33]"
	? "        .ZXPWIN C:\TEMP=(XLS)TEST.LST"
	? "        .ZXPWIN/NL C:\TEMP=""(DOC)""TEST.LST"   ! [5]
	? "Notes:"
	? " 1. <file> is an AMOS or native filespec"
	?
	? " 2. If <dir>= not specified, file is tranferred to 'current' directory, "
	? "    which is either where ZTERM was launched from (typically "
	? "    C:\Program Files\COOL.STF\ZTERM 2000\) or the  directory specified "
	? "    in the Transfer Folders configuration,  or the last directory you manually"
	? "    saved a file to.   Otherwise it is transferred to the specified <dir>."
	? "    <dir> may contain environment variables (e.g. %TEMP%). "
	? 
	? " 3. If (<ext>) specified (e.g. ""(DOC)""), then the file extension"
	? "    will be changed to <ext> on the PC.  This is useful if the "
	? "    original file extension doesn't match Windows conventions.  Note"
	? "    that you may need to enclose it in quotes if specifying it on a"
	? "    UNIX command line (to pass the parentheses by the shell)."
	? 
	input "Hit RETURN: ",CR$
	end

ABORT:
	xcall ECHO
	? 
	? "%Error during operation - please check setup and try again."
	if ABORT > 0 then 
          ? "%Likely causes of the error:"
          ? "%  - Specified PC directory does not exist."
          ? "%  - FTP is not configured properly.  (Try a manual transfer.)"
	endif
	if ABORT=2 then 
          ? "%  - There is no Windows app associated with the file extension."
	  ? "%  - You may need to specify the target directory (e.g. C:\TEMP=)"
	endif
	input "Hit RETURN to recover: ",CR$
	end

TRAP:
	? 
	? "% Error # ";err(0)
	goto ABORT
