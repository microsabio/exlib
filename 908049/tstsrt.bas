program TSTSRT,2.0(6)
! Sample program to test sorting of random files
!
! Prompts you for a number of records, then generates a file with that many
! records, plus 100 empty records.  It fills the records with random data
! (entire record the same ascii char except for the floating point sort
! field which gets a random number 0-999999).  It then sorts the file by
! the floating pt number as a primary key, with 11 bytes of string as the
! secondary.  Finally, it reads the file to see if sorted ok.
!
! [5] 23-Jun-03 Add rec # to test if the sort is "stable"
! [6] 26-Jan-09 Add Natural sort, I types; display option; modernize;
!               allow up to 6 keys /jdm

++include ashinc:ashell.def

define CC_SPAN = 1              ! define for span blocks mode
define CC_MMAP = 1              ! define for memory mapping

defstruct ST_TSTSRT				! Sample record
	map2 B2	    ,B	,2	! 1  Test B2 field
	map2 VENNO	,S	,3	! 3  Vendor Number (not used)
	map2 XXXXX  ,F	,6	! 6  junk
	map2 INVNO	,S	,8	! 12 Invoice Number (not used)
	map2 INVDT	,S	,6	! 20 Invoice Date (not used)
	map2 F6    	,F	,6	! 26 Test F6
	map2 RECNO	,F	,6	! 32 Orig recno
	map2 F4	    ,F	,4	! 38 Test F4
	map2 F8	    ,F	,8	! 42 Test F8
	map2 I1     ,I  ,1  ! 50 Test I1
	map2 FLAG	,S	,1	! 51 Control Flags (not used)
	map2 CHKSUM	,F	,6	! 52 Check sum
	map2 CHKDT	,S	,6	! 58 (not used)
	map2 I2	    ,I	,2	! 64 Test I2
    map2 I4	    ,I	,4	! 66 Test I4
	map2 S20	,S	,20	! 70 Test S20 (99999 Name		[4]
	map2 B5 	,B	,5	! 90 Test B4
	map2 B1     ,B	,1	! 95 Test B1
	map2 B3     ,B 	,3	! 96 Test B3
	map2 B4     ,B	,4 	! 99 Test B4
Endstruct

map1 TREC,ST_TSTSRT

defstruct ST_CTL        ! Control record
	map2 DFL	,S	,1	! Delete Flag (0=on, 1=off)
	map2 SFL	,S	,1	! Sort Flag   (0=on, 1=off)
	map2 ORG	,F	,6	! # of Sorted Records in File
	map2 REC	,F	,6	! # of Records in File
	map2 MAXREC	,F	,6	! Maximum # of Records in File
	map2 DEL	,F	,6	! # of Records flagged for Deletion
	map2 REV	,S	,3	! File Revision Level
    map2 CUTDT2	,S	,6	! Payment Cut Off Date
    map2 CHKSOK	,B	,1	! Checks OK Flag
	map2 CHKDTE	,S	,6	! Check Date
	map2 CHEKNO	,F	,6	! Last Check Number
	map2 MCKFLG	,B	,1	! Manual Checks Entered in DEFER.RUN
	map2 MARKER	,S	,9
Endstruct

map1 CTLREC,ST_CTL

map1 TRECX'FD
	map2 TRECX'LEN	,F	,6,102	! Record Length
	map2 TRECX'RNO	,F	,6	    ! Record Pointer
	map2 TRECX'FCH	,F	,6,7	! File Channel Number

map1 SORTFILE$,S,20,"TSTSRT.DAT"

map1 MISC
    map2 KEY$,S,50
    map2 LASTKEY$,S,50
    map2 I,F
    map2 K1ORD,F
    map2 x,F
    map2 i,F
    map2 kno,I,2
    map2 K(6),b,1           ! sort field #'s
    map2 RR,F
    map2 TALLOC,F
    map2 A,F
    map2 TLOAD,F
    map2 TSORT,F
    map2 RMAX,F
    map2 TBASE,F
    map2 A$,S,10
    map2 LAST'F6,F,6
    map2 LAST'B3,B,3
    map2 LAST'B1,B,1
    map2 LAST'I4,I,4
    map2 LAST'B5,B,5
    map2 CHKSUM,F
    map2 CHKTOT,F
    map2 NEWCHKTOT,F
    map2 TPREVER,F
    map2 TPOSTVER,F
    map2 DUPCNT,F
    map2 BADDUP,F
    map2 LAST'RECNO,F
    map2 PASS,F
    map2 SORT'ERRORS,F
    map2 T1,B,4

    filebase 1
    significance 11

++ifdef CC_MMAP
    xcall ASFLAG,AF_MMAP
++endif

TOP:

    ? TAB(-1,0);"Test of Sort  - ";

++ifdef CC_MMAP
    xcall ASFLAG,AF_MMAP
    ? "Memory map mode activated ";
++endif
++ifdef CC_SPAN
    ? "Span blocks activated";
++endif
    ?

    input "Enter sort order (0=up,1=down,2=display file) ",K1ORD
    if K1ORD=2 then
        call Display'File()
        goto TOP
    endif
	if K1ORD < 0 or K1ORD > 1 goto TOP

    for kno = 1 to 6        ! input up to 6 keys
	    ? "Key";kno;"(1-5=B1-B5; 6-8=F4,6,8; 9-11=S20/nat/fold; 12-14=I1/2/4) ";
	    input "",K(kno)
        if K(kno) = 0 exit
    next kno

	input "Enter # of records to generate (0 to reuse file) ",RR
	if RR=0	then
	    if lookup("MEM:"+SORTFILE$) # 0 then
	       SORTFILE$ = "MEM:" + SORTFILE$
	       trace.print "Opening file in memory: "+SORTFILE$
	   endif
++ifdef CC_SPAN
        open #TRECX'FCH, SORTFILE$, random, TRECX'LEN, TRECX'RNO, wait'file, span'blocks
++else
        open #TRECX'FCH, SORTFILE$, random, TRECX'LEN, TRECX'RNO, wait'file
++endif
        TRECX'RNO = 1 : READ #TRECX'FCH, CTLREC
        ? "Processing existing file with ";CTLREC.REC;" records"
        TALLOC = 0 : TLOAD = 0 : TBASE = TIME
    else
        call Create'File()
    endif

	TLOAD = TIME - TBASE - TALLOC
	? "TIME = ";TIME

START:
	TRECX'RNO = 1 : read #TRECX'FCH, CTLREC

    CHKTOT = Fn'Verify'File()

	TPREVER = TIME - TBASE - TALLOC - TLOAD  ! : ? tab(11,65);"TIME = ";TIME

    call Sort'File(K1ORD,K(1),K(2),K(3),K(4),K(5),K(6))

	TSORT = TIME - TBASE - TALLOC - TLOAD - TPREVER
	? "TIME = ";TIME

    if Fn'Verify'Sort(K1ORD,K(1),K(2),K(3),K(4),K(5),K(6)) # CHKTOT then
        ? "***ERROR: File checksum after sort doesn't match pre-sort!!!"
        stop
    endif

	TPOSTVER = TIME - TBASE - TALLOC - TLOAD - TPREVER - TSORT
	? "(Dup Count: ";DUPCNT;")"
	if BADDUP > 0 then &
		? "***WARNING: SORT NOT STABLE: ";BADDUP;" errors!" : stop

	? "Times: File Allocation =";TALLOC
	? "       Generate Data   =";TLOAD
	? "       Pre Verify      =";TPREVER
	? "       Sort            =";TSORT
	? "       Post Verify     =";TPOSTVER
	? "                      ==========="
	? "Total                  =";TIME-TBASE

	PASS = PASS + 1
	?
	? "==============================================================================="
	? "End of Pass # ";PASS
	? "Hit a key in next 5 seconds or a new pass will start (D to display): "
	T1 = TIME
	NOECHO
	do
	   sleep 0.25
	   A = getkey(0)
	loop until A>0 or TIME-T1>=5
    if ucs(chr(A))="D" then
        call Display'File()
    elseif A # -1 then
        goto ENDIT
    endif
    ECHO
    ?
    ? "Starting new pass (resorting file with opposite order)"
    ? "==================================================================="

    ! reverse the sort order for the next pass
	if K1ORD = 0 then
		K1ORD = 1
	else
		K1ORD = 0
    endif
	TLOAD = 0
	TBASE = TIME
	goto START

ENDIT:
    ECHO
	close #TRECX'FCH
	? "END"
	END

!-------------------------------------------------------------------------
! Display the file (create a list file and use EZTYP to display)
! Notes: opens main file if necessary
!-------------------------------------------------------------------------
Procedure Display'File()
++pragma auto_extern

    map1 locals
        map2 open'flag,b,1
        map2 chktotfile,f

    if eof(TRECX'FCH) = -1 then

        if SORTFILE$[1,4] # "MEM:" then
    	    if lookup("MEM:"+SORTFILE$) # 0 then
    	       SORTFILE$ = "MEM:" + SORTFILE$
    	       trace.print "Opening file in memory: "+SORTFILE$
    	   endif
        endif

        if lookup(SORTFILE$) = 0 then
            ? "Sorry, ";SORTFILE$;" does not exist to display"
            exitprocedure
        endif

++ifdef CC_SPAN
        open #TRECX'FCH, SORTFILE$, random, TRECX'LEN, TRECX'RNO, wait'file, span'blocks
++else
        open #TRECX'FCH, SORTFILE$, random, TRECX'LEN, TRECX'RNO, wait'file
++endif
        TRECX'RNO = 1 : READ #TRECX'FCH, CTLREC
        open'flag = 1
    endif

    ?
    ? "Creating display...";

    open #9, "TSTSRT.LST", output
    ? #9,"Sort order: ";K1ORD;"  Key fields: ";K(1);K(2);K(3);K(4);K(5);K(6)
    ? #9,"* Note: O(riginal) Rec column values reset prior to each sort (for dup stability check)"
    ? #9

    ? #9,"Key#s:(1)  (12) (2)   (13)    (3)      (6)     (7)      (8)     (9/10/11)              (4)          (14)       (5) "
    ? #9,"ORec* B1   I1   B2    I2      B3       F4      F6       F8     S20                     B4           I4         B5 "
    ? #9,"----- --- ---- ----- ------ -------- -------- ------- --------- -------------------- ----------  ---------- ------------"

    for TRECX'RNO = 2 to CTLREC.REC
	read #TRECX'FCH, TREC

        ? #9, (TREC.RECNO using "#####");" ";
        ? #9, (TREC.B1 using "###");" ";(TREC.I1 using "####");" ";
        ? #9, (TREC.B2 using "#####");" ";(TREC.I2 using "######");" ";
        ? #9, (TREC.B3 using "########");" ";
        ? #9, (TREC.F4 using "#####.##");" ";(TREC.F6 using "#######");" ";(TREC.F8 using "#####.###");" ";
        ? #9, pad(TREC.S20,20);" ";
        ? #9, (TREC.B4 using "##########");" ";(TREC.I4 using "###########");" ";
        ? #9, (TREC.B5 using "############")
        if TRECX'RNO/1000 = int(TRECX'RNO/1000) ? ".";tab(-1,254);
        chktotfile = chktotfile + TREC.CHKSUM
    next TRECX'RNO
    ? #9
    ? #9,"Checksum = ";chktotfile

    close #9
    ?
    if open'flag then
        close #TRECX'FCH
    endif

    xcall EZTYP,"TSTSRT.LST"
End Procedure

!------------------------------------------------------------------------------
!Return position, size, type for specified logical sort field number
!------------------------------------------------------------------------------
Procedure Get'Sort'Coordinates(sortfield as b1,xpos as b2,xsiz as b2,xtyp as b2)
    switch (sortfield)
    case 1                         ! B1
    	xpos = 95
    	xsiz = 1
    	xtyp = 2
    	exit
    case 2                         ! B2
    	xpos = 1
    	xsiz = 2
    	xtyp = 2
    	exit
    case 3                         ! B3
    	xpos = 96
    	xsiz = 3
    	xtyp = 2
    	exit
    case 4                         ! B4
    	xpos = 99
    	xsiz = 4
    	xtyp = 2
    	exit
    case 5                         ! B5
    	xpos = 90
    	xsiz = 5
    	xtyp = 2
    	exit
    case 6                         ! F4
    	xpos = 38
    	xsiz = 4
    	xtyp = 1
    	exit
    case 7                         ! F6
    	xpos = 26
    	xsiz = 6
    	xtyp = 1
    	exit
    case 8                         ! F8
    	xpos = 42
    	xsiz = 8
    	xtyp = 1
    	exit
    case 9                         ! S20
    	xpos = 70
    	xsiz = 20
    	xtyp = 0
    	exit
    case 10                        ! S20 nat
    	xpos = 70
    	xsiz = 20
    	xtyp = 4
    	exit
    case 11                        ! S20 nat+
    	xpos = 70
    	xsiz = 20
    	xtyp = 5
    	exit
    case 12                        ! I1
    	xpos = 50
    	xsiz = 1
    	xtyp = 3
    	exit
    case 13                        ! I2
    	xpos = 64
    	xsiz = 2
    	xtyp = 3
    	exit
    case 14                        ! I4
    	xpos = 66
    	xsiz = 4
    	xtyp = 3
    	exit
    endswitch

    xputarg 2,xpos                  ! return the values
    xputarg 3,xsiz
    xputarg 4,xtyp
End Procedure


!--------------------------------------------------------------------
! Create a file full of sample data
!--------------------------------------------------------------------
Procedure Create'File()
++pragma auto_extern

	RMAX = RR
	TBASE = TIME
	? "Creating SORTFILE$ with ";RMAX;" 102 byte records"
	lookup SORTFILE$, x
	if x <> 0 kill SORTFILE$

    allocate SORTFILE$, int(RMAX / int(512/TRECX'LEN)) + 1
    TALLOC = TIME - TBASE
    ? "TIME = ";TIME

    ? "Opening ";SORTFILE$;" for random"
++ifdef CC_SPAN
        open #TRECX'FCH, SORTFILE$, random, TRECX'LEN, TRECX'RNO, wait'file, span'blocks
++else
        open #TRECX'FCH, SORTFILE$, random, TRECX'LEN, TRECX'RNO, wait'file
++endif

	? "Loading file with random data..."
    CTLREC.DFL	 = 1
    CTLREC.SFL	 = 0
    CTLREC.ORG	 = 1
    CTLREC.REC	 = 1
    CTLREC.MAXREC= RMAX
    CTLREC.DEL	 = 0
    CTLREC.REV	 = "5.0"
    CTLREC.CUTDT2	 = ""
    CTLREC.CHKSOK	 = 0
    CTLREC.CHKDTE	 = ""
    CTLREC.CHEKNO	 = 0
    CTLREC.MCKFLG	 = 0
    CTLREC.MARKER = "[CONTROL]"
    TRECX'RNO = 1 : write #TRECX'FCH, CTLREC

    x = 0
    for TRECX'RNO = 2 to RR
        read #TRECX'FCH, TREC
        A = int(rnd(1) * 95 + 32)
        A$ = chr(A)
        TREC = fill$(A$,sizeof(TREC))

        ! Generate semi-random values for the various keys

		! let the float key be duplicate 20% of the time
		if rnd(1) < .2
  			TREC.F6 = LAST'F6
		else
			TREC.F6 = int(rnd(1) * 999999)
			LAST'F6=TREC.F6
        endif

		! let the b1 key be duplicate 50% of the time
		if rnd(1) < .5
			TREC.B1 = LAST'B1
		else
			TREC.B1 = int(rnd(1) * 255)
			LAST'B1 = TREC.B1
		endif

		! let the b3 key be duplicate 30% of the time
		if rnd(1) < .33
			TREC.B3 = LAST'B3
		else
			TREC.B3 = int(rnd(1) * 99999)
			LAST'B3 = TREC.B3
		endif

		TREC.B5 = TREC.B2 * TREC.B3
		if rnd(1) < .4 then TREC.B5 = -TREC.B5

		TREC.B4 = TREC.B2 + TREC.B3
		if TREC.B1 = 0 then TREC.B1 = 1
 		TREC.B2 = TREC.F6 / TREC.B1
        TREC.I1 = TREC.B1
        TREC.I2 = TREC.B2
        TREC.I4 = TREC.B5 / (TREC.B1 + 25)

        if rnd(1) < .25 then
            TREC.B5 = LAST'B5
        endif
        LAST'B5 = TREC.B5
        if rnd(1) < .15 then
            TREC.I4 = LAST'I4
        endif
        LAST'I4 = TREC.I4

		TREC.CHKSUM = int(rnd(1) * 999)

        TREC.F4 = TREC.F6/100
        TREC.F8 = TREC.F6/1000

        ! for the S20 key, generate a string of all-same chars
        A$ = chr$(int(rnd(1)*90+32))
        ! shift some of the A-Z up to accented char range...
        if rnd(1) < .3 and A$ >= "A" and A$ < "Z" then
            A$ = chr(asc(A$)+160)
        endif
        TREC.S20 = FILL$(A$,sizeof(TREC.S20))
        ! except for some of them, prepend a decimal value (like an address)
        ! e.g. "123 AAAAAAAAAAAAA"
        ! (this tests the natural sort)
        if rnd(1) < .3 then
            TREC.S20 = str(int(int(rnd(1)*90000)/30)) + " " + TREC.S20
        endif
        TREC.S20 = TREC.S20 + SPACE(20)
		TREC.RECNO = TRECX'RNO			! [5] save orig recno in rec
		write #TRECX'FCH, TREC
		x = x + 1
		if x >= 1000 then
		  x = 0
		  ? TRECX'RNO;A$;
		endif
	next TRECX'RNO

    TRECX'RNO = 1 : read #TRECX'FCH, CTLREC
    CTLREC.DFL	 = 1
    CTLREC.SFL	 = 0
    CTLREC.ORG	 = 1
    CTLREC.REC	 = RR
    CTLREC.MAXREC= RMAX
    CTLREC.DEL	 = 0
    CTLREC.REV	 = "5.0"
    CTLREC.CUTDT2	 = ""
    CTLREC.CHKSOK	 = 0
    CTLREC.CHKDTE	 = ""
    CTLREC.CHEKNO	 = 0
    CTLREC.MCKFLG	 = 0
    TRECX'RNO = 1 : write #TRECX'FCH, CTLREC

End Procedure


!-----------------------------------------------------------------
! Pass through file, generating an overall CHKSUM, checking
! for certain anomalies
! Computes file checksum tot and returns it
!-----------------------------------------------------------------
Function Fn'Verify'File() as F6
++pragma auto_extern

    map1 locals
        map2 chktotfile,f
        map2 x,f
	?
	? "Verifying data prior to sort...";
	x = 0
	for TRECX'RNO = 2 to CTLREC.REC
		read #TRECX'FCH, TREC
		if len(TREC.S20) # 20 then
			? "Data lost in record #";TRECX'RNO;" (I4=";TREC.I4;" orig recno #";TREC.RECNO;")" : STOP
	    endif
		x = x + 1
		if x >= 1000 then
			x = 0
			? ".";tab(-1,254);  !TRECX'RNO;
        endif
		chktotfile = chktotfile + TREC.CHKSUM
		TREC.RECNO = TRECX'RNO	! [5] reset orig recno prior to sort (to check stability)
		write #TRECX'FCH,TREC	    ! [5]
 	next TRECX'RNO
	?
	? "PreSort Checksum = ";chktotfile
	Fn'Verify'File = chktotfile
End Function

!----------------------------------------------------------------------------
! Sort file according to the specified keys
!----------------------------------------------------------------------------
Procedure Sort'File(k1ord as b1, k1 as b1, k2 as b1, k3 as b1, k4 as b1, k5 as b1, k6 as b1)
++pragma auto_extern

    map1 locals
        map2 k2ord,F
        map2 k3ord,F
        map2 k4ord,F
        map2 k5ord,F
        map2 k6ord,F
        map2 k1pos,F
        map2 k2pos,F
        map2 k3pos,F
        map2 k4pos,F
        map2 k5pos,F
        map2 k6pos,F
        map2 k1siz,F
        map2 k2siz,F
        map2 k3siz,F
        map2 k4siz,F
        map2 k5siz,F
        map2 k6siz,F
        map2 k1typ,F
        map2 k2typ,F
        map2 k3typ,F
        map2 k4typ,F
        map2 k5typ,F
        map2 k6typ,F
        map2 low'ctl,x,102
        map2 high'ctl,X,102
        map2 low'val(5),x,5
        map2 high'val(5),x,5
        map2 fsize1,f
        map2 fsize2,f

    call Get'Sort'Coordinates(k1,k1pos,k1siz,k1typ)
    call Get'Sort'Coordinates(k2,k2pos,k2siz,k2typ)
    call Get'Sort'Coordinates(k3,k3pos,k3siz,k3typ)
    call Get'Sort'Coordinates(k4,k4pos,k4siz,k4typ)
    call Get'Sort'Coordinates(k5,k5pos,k5siz,k5typ)
    call Get'Sort'Coordinates(k6,k6pos,k6siz,k6typ)

    k2ord = k1ord       ! this test program doesn't support
    k3ord = k1ord       ! different orders for the keys
    k4ord = k1ord       !
    k5ord = k1ord       !
    k6ord = k1ord       !


    ! Create special high and low fill control records so control record
    ! remains in place.

    high'ctl = fill$(chr$(255),sizeof(high'ctl))
    low'ctl = fill$(chr$(0),sizeof(low'ctl))

    ! However, if sorting by any I (integer) fields, we'll need to
    ! adjust for fact that 0 is not the lowest and 0xFFFF not the highest
    ! set up byte patterns for signed integer low and high values
    low'val(1) = chr(128)                                      ! min I1 (-128)
    low'val(2) = chr(0)+chr(128)                               ! min I2 (-32768)
    low'val(4) = chr(0)+chr(128)+chr(0)+chr(0)                 ! min I4 (-2GB)
    low'val(5) = chr(0)+chr(0)+chr(0)+chr(0)+chr(128)          ! min B5
    high'val(1) = chr(127)                                     ! max I1 (127)
    high'val(2) = chr(255)+chr(127)                            ! max I2 (32767)
    high'val(4) = chr(255)+chr(127)+chr(255)+chr(255)          ! max I4 (2GB-1)
    high'val(5) = chr(255)+chr(255)+chr(255)+chr(255)+chr(127) ! max B5

    if k1 = 5 or k1 >= 12 then        ! 5=B5, 12=I1, 13=I2, 14=I4
        low'ctl[k1pos;k1siz] = low'val(k1siz)[1;k1siz]
        high'ctl[k1pos;k1siz] = high'val(k1siz)[1;k1siz]
    endif
    if k2 = 5 or k2 >= 12 then
        low'ctl[k2pos;k2siz] = low'val(k2siz)[1;k2siz]
        high'ctl[k2pos;k2siz] = high'val(k2siz)[1;k2siz]
    endif
    if k3 = 5 or k3 >= 12 then
        low'ctl[k3pos;k3siz] = low'val(k3siz)[1;k3siz]
        high'ctl[k3pos;k3siz] = high'val(k3siz)[1;k3siz]
    endif
    if k4 = 5 or k4 >= 12 then
        low'ctl[k4pos;k3siz] = low'val(k4siz)[1;k4siz]
        high'ctl[k4pos;k3siz] = high'val(k4siz)[1;k4siz]
    endif
    if k5 = 5 or k5 >= 12 then
        low'ctl[k5pos;k5siz] = low'val(k5siz)[1;k5siz]
        high'ctl[k5pos;k5siz] = high'val(k5siz)[1;k5siz]
    endif
    if k6 = 5 or k6 >= 12 then
        low'ctl[k6pos;k6siz] = low'val(k6siz)[1;k6siz]
        high'ctl[k6pos;k6siz] = high'val(k6siz)[1;k6siz]
    endif

	! make sure ctl rec stays put
	TRECX'RNO = 1
	if k1ord = 0 then
		write #TRECX'FCH, low'ctl
	else
		write #TRECX'FCH,high'ctl	![5]
    endif

	? "Beginning sort...(";CTLREC.REC;"recs) "

    xcall SIZE,SORTFILE$,fsize1

    xcall BASORT,TRECX'FCH,CTLREC.REC,TRECX'LEN, &
        k1siz,k1pos,k1ord, &
		k2siz,k2pos,k2ord, &
		k3siz,k3pos,k3ord, &
		k1typ,k2typ,k3typ, &
        k4siz,k4pos,k4ord, &
		k5siz,k5pos,k5ord, &
		k6siz,k6pos,k6ord, &
		k4typ,k5typ,k6typ

    xcall SIZE,SORTFILE$,fsize2

    ? "Sort complete"

    if fsize1 # fsize2 then
        ? "Error: BASORT changed file size from ";fsize1;" to ";fsize2 : stop
    endif

    ! restore the control record
    TRECX'RNO = 1 : write #TRECX'FCH, CTLREC
End Procedure

!---------------------------------------------------------------
! Verify that sort actually worked properly
! Returns file checksum (sum of rec "CHKSUM" fields)
! Outputs:
!   DUPCNT,BADDUP
!---------------------------------------------------------------
Function Fn'Verify'Sort(k1ord as b1, k1 as b1, k2 as b1, k3 as b1, &
                        k4 as b1, k5 as b1, k6 as b1) as f6
++pragma auto_extern

    map1 locals
        map2 lastrec,ST_TSTSRT      ! 2nd copy of global TREC record
        map2 sort'errors,f
        map2 display'key$,s,80
        map2 last'display'key$,s,80
        map2 rc,i,2
        map2 chktotfile,f

	DUPCNT = 0
	BADDUP = 0

    x = 0

    ? "Verifying sort..."

    TRECX'RNO = 2
    read #TRECX'FCH, lastrec         ! start off comparing recs 2 and 3 (skip 1 = ctl)
    chktotfile = lastrec.CHKSUM
    call Fn'Compare'Rec(lastrec,lastrec,k1,k2,k3,k4,k5,k6,last'display'key$) ! dummy call to get display key
!trace.print "2: last'key = "+last'display'key$
    for TRECX'RNO = 3 to CTLREC.REC
        read #TRECX'FCH, TREC

        rc = Fn'Compare'Rec(TREC,lastrec,k1,k2,k3,k4,k5,k6,display'key$)
!trace.print str(TREC.RECNO)+" key="+display'key$
!trace.print "last'key = "+last'display'key$
        ! returns 1 if TREC > lastrec, -1 if TREC < lastrec
        if (rc > 0 and k1ord # 0) or (rc < 0 and k1ord # 1) then
            ?
            ? "****Error in sort order at cur recno = ";TREC.RECNO; " prev recno = ";lastrec.RECNO
            ? " ";display'key$
            ? " vs"
            ? " ";last'display'key$
            ? "(rc = ";rc;")"        
            STOP
            sort'errors = sort'errors + 1
        endif

        x = x + 1
        if x >= 1000 then
            x = 0
            ? ".";tab(-1,254);  !(TRECX'RNO using "######");
        endif

!? TRECX'RNO;TREC.B1;TREC.RECNO

		! [5] check if duplicate recs still in recno order
        if display'key$ = last'display'key$ then
            if TREC.RECNO < lastrec.RECNO then
                ? : ? "****Sort not STABLE: ";display'key$
                ? "Rec #: ";TREC.RECNO;"<";lastrec.RECNO; : STOP
                BADDUP = BADDUP + 1
            endif
            DUPCNT = DUPCNT + 1
        endif

        last'display'key$ = display'key$
        if len(TREC.S20) # 20 then
            ? "Data lost in record #";TRECX'RNO;" (I4=";TREC.I4;", orig rec #";TREC.RECNO;")" : STOP
        endif

		chktotfile = chktotfile + TREC.CHKSUM
        lastrec = TREC
 	next TRECX'RNO
    Fn'Verify'Sort = chktotfile
    ?
    ? "Post Sort Checksum = ";chktotfile
End Function


!---------------------------------------------------------------------------------
! Compare current rec with last rec, return -1 (cur<last), 0(=) 1 (cur>last)
! Also return a display key allowing the key values to be displayed
!---------------------------------------------------------------------------------

Function Fn'Compare'Rec(currec as ST_TSTSRT, lastrec as ST_TSTSRT, &
                k1 as b1, k2 as b1, k3 as b1, &
                k4 as b1, k5 as b1, k6 as b1, display'key$ as s80) as i2

    map1 locals
        map2 kfields(6),b,1
            kfields(1) = k1
            kfields(2) = k2
            kfields(3) = k3
            kfields(4) = k4
            kfields(5) = k5
            kfields(6) = k6

        map2 kno,f
        map2 flags,b,4
        map2 rc,i,2

    display'key$ = ""
    for kno = 1 to 6
        switch kfields(kno)
        case 1                         ! B1
            if Fn'Compare'Rec = 0 then
                if currec.B1 > lastrec.B1 then
                    Fn'Compare'Rec = 1
                elseif currec.B1 < lastrec.B1 then
                    Fn'Compare'Rec = -1
                endif
            endif
            display'key$ = display'key$ +"[" + str(currec.B1) + "] "
            exit
        case 2                         ! B2
            if Fn'Compare'Rec = 0 then
                if currec.B2 > lastrec.B2 then
                    Fn'Compare'Rec = 1
                elseif currec.B2 < lastrec.B2 then
                    Fn'Compare'Rec = -1
                endif
            endif
            display'key$ = display'key$ +"[" + str(currec.B2) + "] "
            exit
        case 3                         ! B3
            if Fn'Compare'Rec = 0 then
                if currec.B3 > lastrec.B3 then
                    Fn'Compare'Rec = 1
                elseif currec.B3 < lastrec.B3 then
                    Fn'Compare'Rec = -1
                endif
            endif
            display'key$ = display'key$ +"[" + str(currec.B3) + "] "
            exit
        case 4                         ! B4
            if Fn'Compare'Rec = 0 then
                if currec.B4 > lastrec.B4 then
                    Fn'Compare'Rec = 1
                elseif currec.B4 < lastrec.B4 then
                    Fn'Compare'Rec = -1
                endif
            endif
            display'key$ = display'key$ +"[" + str(currec.B4) + "] "
            exit
        case 5                         ! B5
            if Fn'Compare'Rec = 0 then
                if currec.B5 > lastrec.B5 then
                    Fn'Compare'Rec = 1
                elseif currec.B5 < lastrec.B5 then
                    Fn'Compare'Rec = -1
                endif
            endif
            display'key$ = display'key$ +"[" + str(currec.B5) + "] "
            exit
        case 6                         ! F4
            if Fn'Compare'Rec = 0 then
                if currec.F4 > lastrec.F4 then
                    Fn'Compare'Rec = 1
                elseif currec.F4 < lastrec.F4 then
                    Fn'Compare'Rec = -1
                endif
            endif
            display'key$ = display'key$ +"[" + str(currec.F4) + "] "
            exit
        case 7                         ! F6
            if Fn'Compare'Rec = 0 then
                if currec.F6 > lastrec.F6 then
                    Fn'Compare'Rec = 1
                elseif currec.F6 < lastrec.F6 then
                    Fn'Compare'Rec = -1
                endif
            endif
            display'key$ = display'key$ +"[" + str(currec.F6) + "] "
            exit
        case 8                         ! F8
            if Fn'Compare'Rec = 0 then
                if currec.F8 > lastrec.F8 then
                    Fn'Compare'Rec = 1
                elseif currec.F8 < lastrec.F8 then
                    Fn'Compare'Rec = -1
                endif
            endif
            display'key$ = display'key$ +"[" + str(currec.F8) + "] "
            exit
        case 9                         ! S20
        case 10:                        ! S20 natural
        case 11:                        ! S20 natural + fold
            if Fn'Compare'Rec = 0 then
                if kfields(kno) = 9
                    flags = STRCMPF_LDF
                elseif kfields(kno) = 10
                    flags = STRCMPF_LDF or STRCMPF_NAT
                else
                    flags = STRCMPF_LDF or STRCMPF_NAT or STRCMPF_FOLD
                endif
                ! use new MX_STRCMP to mimic the LDF-based and natural string compare
                xcall MIAMEX, MX_STRCMP, currec.S20, lastrec.S20, flags, rc
                if rc > 0 then
                    Fn'Compare'Rec = 1
                elseif rc < 0 then
                    Fn'Compare'Rec = -1
                endif
            endif
            display'key$ = display'key$ +"[" + currec.S20 + "] "
            exit

        case 12                        ! I1
            if Fn'Compare'Rec = 0 then
                if currec.I1 > lastrec.I1 then
                    Fn'Compare'Rec = 1
                elseif currec.I1 < lastrec.I1 then
                    Fn'Compare'Rec = -1
                endif
            endif
            display'key$ = display'key$ +"[" + str(currec.I1) + "] "
            exit
        case 13                        ! I2
            if Fn'Compare'Rec = 0 then
                if currec.I2 > lastrec.I2 then
                    Fn'Compare'Rec = 1
                elseif currec.I2 < lastrec.I2 then
                    Fn'Compare'Rec = -1
                endif
            endif
            display'key$ = display'key$ +"[" + str(currec.I2) + "] "
            exit
        case 14                        ! I4
            if Fn'Compare'Rec = 0 then
                if currec.I4 > lastrec.I4 then
                    Fn'Compare'Rec = 1
                elseif currec.I4 < lastrec.I4 then
                    Fn'Compare'Rec = -1
                endif
            endif
            display'key$ = display'key$ +"[" + str(currec.I4) + "] "
            exit
        endswitch
        ! we can't exit even when we know which rec is greater because we want
        ! to build up the complete display key string
    next kno
    xputarg 9,display'key$
End Function
