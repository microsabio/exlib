program GTLANG,1.0(105)  ! display LDF values
!------------------------------------------------------------------
!EDIT HISTORY
! [101] 28-Jan-09 / jdm / resurrected (ancient version), updated
! [102] 23-Jan-11 / jdm / show example of numeric formatting
! [103] 17-Feb-23 / jdm / modernize; fix bogus 'ambiguous' warning; add
!                         option to generate std upper collating sequence
! [104] 22-Feb-23 / jdm / bug fix: falsely indicated upper sequence invalid
! [105] 23-Feb-23 / jdm / bug fix: date/time format corrupted when regenerating;
!                         refinement: allow change of ld_nm1 when regenerating; 
!                         add notes and pointers
!------------------------------------------------------------------
!NOTES: [105]
! Displays LDF data from the active LDF (set either by the LANGUAGE
! directive in the miame.ini or via the SET LANG command). Note that
! the active LDF data format doesn't match the raw binary layout
! of the LDF file. And in particular, the ST_GTLANG structure
! used by GTLANG.SBR to retrieve the active LDF data uses F,6
! for all numeric fields, whereas the raw format is likely to be
! B,1. 
!
! Usage:
!   To display the current LDF data, just RUN GTLANG
!   To display the LDF data for an LDF file that isn't current,
!       use SET LANG <lang> to first make it current.
!   To add the extended collating sequence to an existing LDF,
!       first make it current, then answer affirmatively to the 
!       option presented, then specify a new file to save it as.
!       (You can rename it back to the original separately)
!------------------------------------------------------------------
++include'once ashinc:ashell.def     ! [103]
++include'once ashinc:gtlang.sdf     ! [103] was .map
++include'once sosfunc:fnprogver.bsi ! [103] 
++include'once sosfunc:fnameext.bsi  ! [105]

defstruct ST_LDF_BINARY     ! actual binary layout of LDF file        
        map2 ld_header,x,10 ! LDF program header 
	    map2 ld_nm1,s,20    ! Name of language in language 1 
	    map2 ld_nm2,s,20    ! Name of language in language 2 
	    map2 ld_ext,b,2     ! Message file extention (
	    map2 ld_csy,s,4     ! Currency symbol, null terminated 
	    map2 ld_cps,b,2     ! Currency symbol position: 0=before, 1=after 
	    map2 ld_csp,b,2     ! Currency symbol spacing 
	    map2 ld_csz,b,2     ! Currency size 
	    map2 ld_tsp,s,1     ! Thousands separator 
	    map2 ld_dec,s,1     ! Decimal symbol 
	    map2 ld_dts,s,1     ! Date separator 
	    map2 ld_dtf,b,1     ! Date format: 0=MDY, 1=DMY, 2=YMD [105] was s
	    map2 ld_tms,s,1     ! Time separator 
	    map2 ld_tmf,b,1     ! Time format: 0=12-hour 1=24-hour [105] was s
	    map2 ld_dls,s,1     ! Data list separator 
	    map2 ld_ppl,s,1     ! left PPN symbol 
	    map2 ld_ppr,s,1     ! right PPN symbol 
	    map2 ld_chs,s,1     ! Character set number 
	    map2 ld_yes,s,6     ! Word for "YES", null terminated 
	    map2 ld_no,s,6      ! Word for "NO", null terminated 
	    map2 ld_ych,s,1     ! Single character for "YES" 
	    map2 ld_nch,s,1     ! Single character for "NO" 
	    map2 ld_wrd,x,30    ! word characters (besides A-Z, a-z) 
	    map2 ld_ulc,x,30    ! upper/lower case translations 
	    map2 ld_spr,x,30    !   SPARE (reserved for expansion) 
	    map2 ld_usr,x,30    ! user definable area 
	    map2 ld_col,x,128   ! collating sequence table 
	    map2 ld_jan,s,20    ! months of the year 
	    map2 ld_feb,s,20
	    map2 ld_mar,s,20
	    map2 ld_apr,s,20
	    map2 ld_may,s,20
	    map2 ld_jun,s,20
	    map2 ld_jul,s,20
	    map2 ld_aug,s,20
	    map2 ld_sep,s,20
	    map2 ld_oct,s,20
	    map2 ld_nov,s,20
	    map2 ld_dcm,s,20
	    map2 ld_mon,s,20    ! days of the week 
	    map2 ld_tue,s,20
	    map2 ld_wed,s,20
	    map2 ld_thu,s,20
	    map2 ld_fri,s,20
	    map2 ld_sat,s,20
	    map2 ld_sun,s,20
        map2 ld_extend1,s,1 ! 0x5A flag indicating extended format 
        map2 ld_extend2,s,1 ! 0x5A flag (reset to 1 if colate seq meaningful, else 0 
        map2 ld_col2,x,128  ! collating sequence table (upper 128) 
        map2 ld_fill,x,180  ! fills it out to 1024 bytes
endstruct

map1 LDF, ST_LDF_BINARY          ! [103] raw LDF file structure
map1 I,F
map1 L$,S,1
map1 LANG'STATUS,F
map1 COLX
    map2 COL(128),B,1

++pragma PRIVATE_BEGIN
    map1 LANG, ST_GTLANG         ! [103] Basic translated structure 
++pragma PRIVATE_END


    XCALL GTLANG,LANG'STATUS,LANG     ! [103] 

    ? "LANG'STATUS = ";LANG'STATUS;" size of LANG structure = ";sizeof(ST_GTLANG)  ! [103] 

    ? "NAME1:           ";LANG.NAME1;     tab(40);"NAME2: ";LANG.NAME2
    ? "EXTENSION:       ";LANG.EXTENSION
    ? "CURRENCY SYM:    ";LANG.CURRENCY'SYM;tab(40);" POS:   ";LANG.CURRENCY'POS
    ? "CURRENCY SPC:   ";LANG.CURRENCY'SPC;tab(40);" SIZE:  ";LANG.CURRENCY'SIZE
    ? "THOUSANDS:       ";LANG.THOUSANDS;   tab(40);" DECIMAL:";LANG.DECIMAL;

        ! [102] example of numeric format
        ? tab(60);(12345.78) using "###,###.##"

    ? "DATE FORMAT:    ";LANG.DATE'FORMAT
    ? "TIME FORMAT:    ";LANG.TIME'FORMAT
    ? "SEPARATORS: DATE:";LANG.DATE'SEP;    tab(40);" TIME:   ";LANG.TIME'SEP; &
        " DATA: ";LANG.DATA'SEP
    ? "YES WORD:       ";LANG.YES'WORD;     tab(40);" CHAR:   ";LANG.YES'CHAR
        ? "NO  WORD:       ";LANG.NO'WORD;      tab(40);" CHAR:   ";LANG.NO'CHAR
    ? "MONTHS:         ";LANG.JAN;",";LANG.FEB;",";LANG.MAR;",";LANG.APR;",";LANG.MAY;",";LANG.JUN
    ? "                ";LANG.JUL;",";LANG.AUG;",";LANG.SEP;",";LANG.OCT;",";LANG.NOV;",";LANG.DEC
    ? "WEEKDAYS:       ";LANG.MON;",";LANG.TUE;",";LANG.WED;",";LANG.THU; &	
                ",";LANG.FRI;",";LANG.SAT;",";LANG.SUN
                
    ?  "Display lower collating sequence? [";LANG.NO'CHAR;"] ? ";
    input "",L$
    if ucs(L$) = LANG.YES'CHAR then
        ? "Lower Collating Sequence: ";
        COLX = LANG.COL
        call Display'Collating'Sequence(COLX,0)
    endif
    ? "Upper Collating Sequence valid: ";LANG.EXTEND2;
    if LANG.EXTEND2=0 then              ! [103] LANG.EXTEND2 is B,1!
        ? " [";LANG.NO'WORD;"]"
    else
        if LANG.EXTEND2 # 1 then   ! [103][104] 
            ? " ??"
            ? "  ** Warning: Sequence validity flag '0' is ambiguous - "
            ? "  ** It should be null unless upper collating sequence is valid"
        else
            ? " [";LANG.YES'WORD;"]"     ! [104]
        endif   

        ? "Display UPPER collating sequence? [";LANG.NO'CHAR;"] ? ";
        input "",L$
        if ucs(L$) = LANG.YES'CHAR then
          ? "Upper Collating Sequence: ";
          COLX = LANG.COL2
          call Display'Collating'Sequence(COLX,128)
        endif
    endif
    
    ! [103] Add option to generate upper collating sequence
    L$ = ""
    ? "Do you want to (re)generate a standard upper collating sequence"
    ? " to sort accented and non-accented characters together? [";LANG.NO'CHAR;"] ? ";
    input "",L$
    if ucs(L$) = LANG.YES'CHAR then
        call Fn'Update'LDF'From'GTLANG(gtlang=LANG)
    endif
  
    END

!------------------------------------------------------------------------------
Procedure Display'Collating'Sequence(colx as x128,base as b2)

    map1 locals
        map2 i,i,2
        map2 j,i,2
        map2 k,i,2,base
        map2 x,i,2,1
        map2 colx1,@colx
            map3 col(128),b,1
        map2 l$,s,1
        map2 options1,b,4       ! [103]
        map2 options2,b,4       ! [103]
        
    ! [103] option to display using GUI font (since it may have a different
    ! [103] symbol set than the system fixed pitch font)
    ? "Use GUI font (else current text font) [";LANG.NO'CHAR;"] ? ";
    input "",l$
    if ucs(l$) = LANG.YES'CHAR then
        xcall MIAMEX, MX_GETOPTIONS, options1, options2
        xcall MIAMEX, MX_SETOPTIONS, options1, options2 or GOP2_AUTOTPRINT
        ?
    endif    

    for i = 1 to 16
        for j = 1 to 8       !   A > A    31>31   � > e
            ! display the source char or value
            if k > 32 and k # 127 then
                ? chr(k);" >";
            else
                ? str(k);">";
            endif
            ! display the dest char or value
            if col(x) > 32 and col(x) # 127 then
                ? " ";chr(col(x));
            else
                ? str(col(x));
            endif
            
            k = k + 1
            x = x + 1
            if j < 8 then
                ? tab(j*10);
            else
                ?
            endif
        next j
    next i

    if ucs(l$) = LANG.YES'CHAR then     ! [103] restore original options
        xcall MIAMEX, MX_SETOPTIONS, options1, options2
        ?
    endif    


EndProcedure

!---------------------------------------------------------------------
!Function:
!   Create a NEW.LDF file based on input LANG (from GTLANG) and
!   generate a new extended collating sequence
!Params:
!   gtlang  (ST_GTLANG) [in] - translated form of existing structure
!Returns:
!   1 if successful
!Globals:
!Notes:
!---------------------------------------------------------------------
Function Fn'Update'LDF'From'GTLANG(gtlang as ST_GTLANG:inputonly) as i2

    map1 locals
        map2 ldf, ST_LDF_BINARY
        map2 srcldf$,s,100
        map2 dstldf$,s,100
        map2 rcv,i,4
        map2 i,i,2
        map2 rad50,b,2
        map2 colx
            map3 col(128),b,1
        map2 vrecord                    ! version record
            map3 vbinary(10),b,1 
        map2 vmajor,b,1
        map2 vminor,b,1
        map2 vsub,b,1
        map2 vedit,b,2
        map2 vwho,b,1
        
    ? "Note: source LDF used only to get version; actual source data"
    ? "      comes from the current LANGUAGE definition"
    input line "Source LDF filespec: ",srcldf$
    open #1, srcldf$, input
    xcall GET,vrecord,1,sizeof(vrecord),rcv   ! get the old phdr
    close #1
    if rcv # sizeof(ldf.ld_header) then
        ? "Error reading header of ";srcldf$
        exitfunction
    endif
    
    ? "Old LDF version: ";Fn'ProgVer$(srcldf$)    
    do
        input line "Enter new LDF filespec: ",dstldf$
        if dstldf$ = srcldf$ then
            ? "Cannot be same as source!"
        endif
        exit
    loop

    ! update the vwho, changing x.y(z) to x.y(z)-1
    vwho = vbinary(3) and &hf0
    vwho += 1
    vbinary(3) = (vbinary(3) and &h0f) + (vwho * 16)
    ldf.ld_header = vrecord
    
    open #2, dstldf$, output
    
    ! translate the incoming gtlang back to ldf raw format
    
    ! ldf.phdr1 = gtlang.       ! First section of PHDR
    ! ldf.phdr2 = gtlang.       ! Second section of PHDR
    ! ldf.phdr3 = gtlang.       ! Third section
    ldf.ld_nm1 = gtlang.NAME1    ! Name of language in language 1 
    ? "Enter language name for new file [";ldf.ld_nm1;"] : ";   ! [105]
    input "",ldf.ld_nm1                 ! [105]
    ldf.ld_nm2 = gtlang.NAME2           ! Name of language in language 2 
    xcall PACK,gtlang.EXTENSION,rad50
    ldf.ld_ext = rad50                  ! Message file extention (rad50)
    ldf.ld_csy = gtlang.CURRENCY'SYM    ! Currency symbol, null terminated 
    ldf.ld_cps = gtlang.CURRENCY'POS    ! Currency symbol position: 0=before, 1=after 
    ldf.ld_csp = gtlang.CURRENCY'SPC    ! Currency symbol spacing 
    ldf.ld_csz = gtlang.CURRENCY'SIZE   ! Currency size 
    ldf.ld_tsp = gtlang.THOUSANDS       ! Thousands separator 
    ldf.ld_dec = gtlang.DECIMAL         ! Decimal symbol 
    ldf.ld_dts = gtlang.DATE'SEP        ! Date seperator 
    ldf.ld_dtf = gtlang.DATE'FORMAT     ! Date format: 0=MDY, 1=DMY, 2=YMD 
    ldf.ld_tms = gtlang.TIME'SEP        ! Time separator 
    ldf.ld_tmf = gtlang.TIME'FORMAT     ! Time format: 0=12-hour 1=24-hour 
    ldf.ld_dls = gtlang.DATA'SEP        ! Data list seperator 
    ldf.ld_ppl = gtlang.PPN'LEFT        ! left PPN symbol 
    ldf.ld_ppr = gtlang.PPN'RIGHT       ! right PPN symbol 
    ldf.ld_chs = gtlang.CHARSET         ! Character set number 
    ldf.ld_yes = gtlang.YES'WORD        ! Word for "YES", null terminated 
    ldf.ld_no = gtlang.NO'WORD          ! Word for "NO", null terminated 
    ldf.ld_ych = gtlang.YES'CHAR        ! Single character for "YES" 
    ldf.ld_nch = gtlang.NO'CHAR         ! Single character for "NO" 
    ldf.ld_wrd = gtlang.WORD'CHARS      ! word characters (besides A-Z, a-z) 
    ldf.ld_ulc = gtlang.ULC             ! upper/lower case translations 
    ldf.ld_spr = gtlang.SPARE           !   SPARE (reserved for expansion) 
    ldf.ld_usr = gtlang.USR             ! user definable area 
    ldf.ld_col = gtlang.COL             ! collating sequence table 
    ldf.ld_jan = gtlang.JAN             ! months of the year 
    ldf.ld_feb = gtlang.FEB
    ldf.ld_mar = gtlang.MAR
    ldf.ld_apr = gtlang.APR
    ldf.ld_may = gtlang.MAY
    ldf.ld_jun = gtlang.JUN
    ldf.ld_jul = gtlang.JUL
    ldf.ld_aug = gtlang.AUG
    ldf.ld_sep = gtlang.SEP
    ldf.ld_oct = gtlang.OCT
    ldf.ld_nov = gtlang.NOV
    ldf.ld_dcm = gtlang.DEC
    ldf.ld_mon = gtlang.MON             ! days of the week 
    ldf.ld_tue = gtlang.TUE
    ldf.ld_wed = gtlang.WED
    ldf.ld_thu = gtlang.THU
    ldf.ld_fri = gtlang.FRI
    ldf.ld_sat = gtlang.SAT
    ldf.ld_sun = gtlang.SUN
    ldf.ld_extend1 = gtlang.EXTEND1     ! 0x5A flag indicating extended format 
!    ldf.ld_extend2 = gtlang.EXTEND2    ! 0x5A flag (reset to 1 if colate seq meaningful, else 0 
!    ldf.ld_col2 = gtlang.COL2

    ldf.ld_extend2 = 1
    ldf.ld_col2 = fill(chr(0),sizeof(ldf.ld_col2))

    ! generate an extended collating sequence which maps accented characters
    ! to the corresponding non-accented ones and otherwise leaves the upper
    ! characters to sort above 0-127
    
    colx = ldf.ld_col2
    ! sort the special symbols as themselves, above 0-127 (indexed here as 1-128)
    for i = 1 to 63
        col(i) = i + 128
    next i
    ! sort upside down ? and  accented chars same as the non-accented
    col(64) = 64            ! upside down ? -> ?
    for i = 65 to 71        ! accented A -> A
        col(i) = asc("A")
    next i
    col(72) = asc("C")      ! Cap Cedilla -> C
    for i = 73 to 76        ! accented E -> E
        col(i) = asc("E")
    next i
    for i = 77 to 80        ! accented I -> I
        col(i) = asc("I")
    next i
    col(81) = asc("D")      ! D with strike -> C
    col(82) = asc("N")      ! Cap enya -> N
    for i = 83 to 87        ! accented O -> O
        col(i) = asc("O")
    next i
    col(88) = i + 128       ! leave x alone
    col(89) = asc("O")      ! O with slash -> O
    for i = 90 to 93        ! accented U -> U
        col(i) = asc("U")
    next i
    col(94) = asc("Y")      ! accented Y -> Y
    col(95) = i + 128       ! leave little D-thing alone
    col(96) = asc("S")      ! digraph SS -> S

    for i = 97 to 103       ! accented a -> a, ae -> a
        col(i) = asc("a")
    next i
    col(104) = asc("c")     ! lower cedilla -> c
    for i = 105 to 108      ! accented e -> e
        col(i) = asc("e")
    next i
    for i = 109 to 112      ! accented i -> i
        col(i) = asc("i")
    next i
    col(113) = asc("o")     ! o with sailor hat -> o
    col(114) = asc("n")     ! lower enya with -> n
    for i = 115 to 119      ! accented o -> o
        col(i) = asc("o")
    next i
    col(120) = i + 128      ! division sign alone
    col(121) = asc("o")     ! o with slash -> o
    for i = 122 to 125      ! accented u -> u
        col(i) = asc("u")
    next i
    col(126) = asc("y")     ! accented y -> y
    col(127) = i + 128      ! leave little D-thing alone
    col(128) = asc("y")     ! y umlaut -> y
    
    ldf.ld_col2 = colx 
    
    ? #2, ldf;
    close #2
    
    ? "Updated LDF saved as ";dstldf$;" Version ";Fn'ProgVer$(dstldf$)   
    if instr(1,ucs(dstldf$),"DVR:") = 0 and instr(1,dstldf$,"[1,6]") = 0 then       ! [105]
        ? "You must move ";dstldf$;" to the DVR: account in order to use it!"
    else
        ? "Use SET LANG ";Fn'Name'Only$(dstldf$);" to activate it"
        ? "(and then RUN GTLANG again to see the results)"
    endif
    .fn = 1
EndFunction
