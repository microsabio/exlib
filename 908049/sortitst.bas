program SORTITST,1.0(100)     ! test sorting of same-key recs

MAP1 X,F
MAP1 MY'ARRAY
MAP2 SRTARY(20)
 MAP3 CUST,S,6
 MAP3 FLAG,S,2
 MAP3 PTR,F,6


CUST(1)="AAAAAA" : PTR(1)=1 : FLAG(1)="1"
CUST(2)="      " : PTR(2)=2 : FLAG(2)="2"
CUST(3)="AAAAAA" : PTR(3)=3 : FLAG(3)="3"
CUST(4)="AAAAAA" : PTR(4)=4 : FLAG(4)="4"
CUST(5)="AAAAAA" : PTR(5)=5 : FLAG(5)="5"

CUST(3) = ""
CUST(1) = CHR(140)+CHR(140)+CHR(140)+CHR(129)+CHR(129)+CHR(129)

define MAX_CUS = 300
MAP1 CUSMASX
 MAP2 CUSMAS(MAX_CUS)
    MAP3 CUSNO,S,5
    MAP3 CNAME,S,25
    MAP3 CADD1,S,25
    MAP3 CITY,S,15
    MAP3 I2,I,2     ! 71
    MAP3 I4,I,4     ! 73
    MAP3 F6,F,6     ! 77
    MAP3 F4,F,4     ! 83
    MAP3 F8,F,8     ! 87

MAP1 I,F
MAP1 MAXCUS,F

?TAB(-1,0);
FOR X=1 TO 5
 PRINT "B4:>";CUST(X);" ";FLAG(X);" ";PTR(X)
NEXT X

XCALL SORTIT,MY'ARRAY,5,14,6,1,0,0,0,0,0,0,0,4,0,0

PRINT

FOR X=1 TO 5
 PRINT "AFTER:>";CUST(X);" ";PTR(X)
NEXT X

STOP
    open #1, "SORTITST.SEQ", input
    I = 0
DATALOOP:
    do
        I = I + 1
        input csv #1, CUSNO(I),CNAME(I),CADD1(I), CITY(I)
        I2(I) = (I - 50) * 200
        I4(I) = (I - 50) * 15000000
        F8(I) = I4(I)
        F6(I) = I4(I)
        F4(I) = I4(I) / 100000000
? I4(I);F8(I)
!       ? XTREE'DATA(I)
    loop until I >= MAX_CUS or eof(1)=1
DATADONE:
STOP
    close #1
    MAXCUS = I - 1

    xcall SORTIT,CUSMASX,MAXCUS,94,4,83,0,2,71,1,4,73,0,1,3,3

    open #1, "SORTIT.OUT", OUTPUT
    for I = 1 to MAXCUS
        ? #1, I using "###";".  "; &
            (CUSNO(I) using "#####");" ";pad(CADD1(I),25);" ";pad(CITY(I),15);" "; &
            (I2(I) using "######");" ";(I4(I) using "############");" "; &
            (F4(I) using "###.########");" ";(F6(I) using "############");" "; &
            (F8(I) using "############")

    next I
    close #1
    xcall eztyp,"SORTIT.OUT"
end
