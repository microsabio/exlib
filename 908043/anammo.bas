!	ANAMMO.B14	[AMOS Version for Basic 1.4] - See lines marked [B13]
!			for converting to Basic 1.3
!
!	Copyright (c) MicroSabio
!----------------------------------------------------------------------------
!	Program to analyze and correct memo file linkage problems, for
!	use with INMEMO.SBR. 
!	Also compatible with Dravac's ANDI memopads (pre 4.0)
!----------------------------------------------------------------------------
!EDIT HISTORY:
!	1. 12-16-83 Created /bbb
!	2. 07-17-84 Accomodate 4 byte link variable./bbb
!	3. 11-12-84 Display program version [1.1a] /jdm
!	4. 11-12-84 Better handling of no eof markers; get link size [1.2] /jdm
!	5. 11-14-84 Correct no-continue option /jdm [1.2(101)]
!	6. 12-11-84 Support ISAM primary file /jdm [1.3(102)]
!	7. 12-12-84 Silence timer display /jdm [1.3(103)]
!	8. 12-12-84 Fix bug in determination of link size /jdm [1.3(104)]
!	9. 12-12-84 Use INMEMO to display help /jdm [1.4(105)]
!      10. 07-07-85 Fix bug in del ch rebuild/bbb [1.4(106)]
!      11. 04-21-86 Fix display bug in stranded record count [1.4(107)]
!      12. 04-21-86 New help system and password time-out revive [1.5(108)]
!      13. 04-21-86 Fix bug in eof link update & primary link update (109)/jdm
!      14. 04-22-86 Allow input from command file (110)/jdm
!      15. 07-15-86 Cosmetic improvements (111)/jdm
!      16. 08-04-86 Handle filespec errors caused by ersatz not defined (112)/jdm
!      17. 11-18-86 Fix bug if 1st 3 of 4 byte link = "]]]" (113)/jdm
!      18. 08-17-87 Fix illegal rec error possibility (114)/jdm
!      19. 04-18-88 Fix bug in truncating memos with link errors (115)/jdm
!      20. 12-12-88 Fix out-of-scope error (AlphaBasic Plus) (116)/jdm
!      21. 09-11-89 Add color support (117) /jdm
!      22. 09-11-89 Add language support for help files (118) /jdm
! Version 2.0---
!      23. 09-27-89 Use GTLANG to get language info /jdm
!      24. 10-01-89 Support IsamPlus primary, fix help bugs,
!                   various other minor bugs, improve error report, use
!		    LOGRIO.SBR for spanned blocks /jdm
!      25. 10-23-89 Clean up maps, use INMEMO save & restore area /jdm
!      26. 11-22-89 Work around bug in AMOS 2.1 RUN /jdm
!      27. 12-29-89 Move all POS vars to even boundary /jdm
!      28. 02-05-90 Remove IsamPlus rec # display & fix IsamPlus problem
!                   with handling unused records in last block of file
!                   (caused spurious illegal primary link errors) /jdm
!      29. 02-16-90 Remove inputs for help file missing messages for
!                   safer cmd file operation; better handling of file in
!                   use; fix EOF'MARK on report /jdm
!      30. 06-08-90 Fix bug if .IDX used instead of .IDA /jdm
!      31. 07-11-90 Fix bug with ISAM Plus files on extended directories:
!                   error 43 or 63 /jdm
! Version 2.1---
!      32. 09-20-90 Minor adjusments for non-AMOS /jdm
! Version 2.2---
!      33. 02-06-91 Add support for an array of links /jdm
!      34. 02-19-91 Fix problems with link array update /jdm
!      35. 12-04-91 Allow the report to be printed before deciding
!			whether to proceed with Phase 2; also, show the
!			link # if multiple links in primary record /jdm
!      36. 12-10-91 Fix non-AMOS bugs; fix bug in calculating ISAM file
!			size; also screen bug caused by 35 /jdm
!      37. 01-16-92 Fix ISAMPlus ROCK layout (must have changed); Also
!			make it smarter about ISAMPlus and check for
!			LOGRIO in advance /jdm
!      38. 01-20-92 Further corrections to rock (free pointer was wrong)/jdm
! Version 2.2A---
!      39. 02-20-92 Further adjustments for non-amos /gb
!      40. 08-11-96 Allow for 1024 rec size in IsamPlus /jdm
!      41. 05-06-97 Changes to accomodate CISAM/DISAM files.
!			To use on such a file, add the /CISAM switch
!			(e.g. .RUN ANAMMO /CISAM)
!			Then, don't enter an extension for the primary
!			data file - it will add the .DAT and assume it
!			is a CISAM/DISAM file.  Answer "Y" that it is an
!			unmodified ISAM configuration.  You do not need
!			to specify any delete or non-in-use flags.
!			Note that this version only works under A-Shell
!			version 4.2(563) or higher because it requires
!			the following features:  LOGRIO.SBR, and
!			OPTIONS=EXTFIO in the MIAME.INI file.  (EXTFIO
!			prevents a "filetype mismatch" error on opening
!			a CISAM .DAT file caused by the fact that it is
!			not a multiple of 512 bytes.)  Maximum recsize
!			supported is 2048 bytes.
!
!			Also fix bug introduced in edit [40] causing
!			 error 15 on non-ISAM reads.  Note that
!			for CISAM/DISAM, the actual rec size is 1 byte
!			larger than the declared rec size, with the extra
!			byte being the delete-or-in-use flag (but the
!			program adds the extra byte to the recsize
!			internally.)
!
!			Also, fix bug where # recs with memos shown at end
!			was including those which just had their memo links
!			zeroed out.
!
!      42. 02-08-00 (A-Shell only) Increase block size to 1024 /jdm
!      43. 06-22-05 (A-Shell only) Output linked memo info to list /jdm
!      44. 12-12-12 Fix i/o to unopened file issue /jdm
!----------------------------------------------------------------------------
!EXTERNAL ROUTINES:
!	INFLD.SBR Ver. 4-14 or higher
!	BASORT.SBR
!----------------------------------------------------------------------------

	PROGRAM ANAMMO,2.2(44)

MAP1 PHDR
	MAP2 VMAJOR,B,1,2
	MAP2 VMINOR,B,1,2
	MAP2 VSUB,B,1,0
	MAP2 VEDIT,B,1,42
	MAP2 VPATCH,B,1,0

MAP1 MEMO$,S,24
MAP1 DC1
	MAP2 DEL'REC(2)
		MAP3 DEL'MARKER,S,6
		MAP3 DEL'JNK,B,2
		MAP3 NXT'DEL2,B,2
		MAP3 NXT'DEL4X,X,4		![1]
		MAP3 DEL'FILL,S,50
MAP1 DC2,@DC1
	MAP2 DL'REC(2)
		MAP3 DL'MARKER,S,6
		MAP3 DL'JNK,B,2
		MAP3 NXT'DL2,B,2
		MAP3 NXT'DEL4B,B,4		![1]
		MAP3 DL'FILL,S,50

MAP1 NXTDL
	MAP2 NXT'DEL(2),B,4
MAP1 NXTDLX,@NXTDL
	MAP2 NXT'DELX(2),X,4

MAP1 CTL'REC
	MAP2 CTL'MARKER,S,6
	MAP2 FST'FRE2,B,2
	MAP2 FST'DEL2,B,2
	MAP2 FST'FRE4,X,4,,@FST'FRE2	![1]
	MAP2 FST'DEL4X,X,4		![1]
	MAP2 FST'DEL4B,B,4,,@FST'DEL4X	![1]
	MAP2 CTL'FILL,S,50

MAP1 FST'FRE,B,4
MAP1 FST'FREX,X,4,,@FST'FRE
MAP1 FST'DEL,B,4
MAP1 FST'DELX,X,4,,@FST'DEL

MAP1 MR1
	MAP2 MEMO'REC(2)
		MAP3 LINK'PTR4,X,4		![1]
		MAP3 MEMO'TXT,X,60
MAP1 MR2,@MR1
	MAP2 MMO'REC(2)
		MAP3 LINK'PTR2,B,2		![1]
		MAP3 LINK'JNK,X,2
		MAP3 MMO'TXT,X,60
MAP1 LNKPTR
	MAP2 LINK'PTR(2),B,4			![1]
MAP1 LNKPTRX,@LNKPTR
	MAP2 LINK'PTRX(2),X,4
MAP1 LINK,B,4					![1]
MAP1 LINKX,X,4,,@LINK				![1]
MAP1 LINKA(2),B,2,,@LINK			![1]
MAP1 BYTE'FLAG,B,1				![1]
MAP1 LNKOCC,F,6,0				![33] # of link occurances
MAP1 LNKNUM,F,6,0				![33] this occur #

MAP1 CUR'MEM'BLK,F
MAP1 CUR'MEM'REC,F
MAP1 MAX'MEM'REC,F
MAP1 LST'MEM'REC,F
MAP1 MAX'PRI'REC,F
MAP1 ZERO,X,1024				! [42]

MAP1 PRIMARY$,S,24
MAP1 BLOCK,X,1024				! [41][42]
MAP1 BLOCK2048,X,2049,@BLOCK			! [40][41]
MAP1 OFFSET,F
MAP1 REC'SIZE,F,,,@OFFSET
MAP1 LINK'POS,F
MAP1 KEY'POS,F
MAP1 KEY'SIZE,F
MAP1 KEY'STR,S,256
MAP1 LENGTH(8),F
MAP1 MAPIT1
	MAP2 EOF'POS,F
	MAP2 DEL'POS,F
	MAP2 JUNK1,X,36
MAP1 POSITION(8),F,,,@MAPIT1
MAP1 MAPIT2
	MAP2 EOF'MARK,S,12
	MAP2 EOF'MARKX,X,12,@EOF'MARK		! [41]
	MAP2 DEL'MARK,S,12
	MAP2 DEL'MARKX,X,12,@DEL'MARK		! [41]
	MAP2 JUNK2,X,72
MAP1 MARKER(8),X,12,,@MAPIT2

				! Following 3 variables may be reset to
				! match largest memo file size you use.
				! BM'SIZE must be the array size of BIT'MAP,
				! which should be the # of DISK BLOCKS in
				! the largest memo file.  Array size of BM2
				! should be 1/4 of BM'SIZE.
MAP1 BM'SIZE,F,6,8192
MAP1 BM1
	MAP2 BIT'MAP(8192),B,1
MAP1 BM2X,@BM1
	MAP2 BM2(2048),B,4

MAP1 MISC
	MAP2 CUR'PRI'BLK,F		! block #
	MAP2 REC'NO,B,4			! logical rec #
	MAP2 REC'CNT,B,2		! # records processed
	MAP2 CTL'SWITCH,B,1
	MAP2 OUT'SWITCH,B,1		! 1=error list file open
	MAP2 OUT'FILE,S,24		! error list file name
	MAP2 LD'OPEN,B,1
	MAP2 CNT'ERR(15),F		! # of errors in each category
	MAP2 FIXIT(15),B,2		! 1=fix this error
	MAP2 CNT'UN'RECS,F		! # unaccounted for recs
	MAP2 LENS,F,6			! [41]
	MAP2 STRING'VAR,S,24		! temp string operations
	MAP2 XSTRING'VAR,X,24,@STRING'VAR	! [41]
	MAP2 STRING'FMT,S,24		! [29] temp string operations
	MAP2 FI,F,6			! [29] for/next
	MAP2 FLOAT'VAR,F
	MAP2 FLOAT'VAR$,S,20		! used to input float values
	MAP2 BYTE'CHAR,X,1
	MAP2 BYTE'VAR,B,1,,@BYTE'CHAR
	MAP2 BYTE'VAR$,S,12		! used to input byte values
	MAP2 EXT'N,S,6
	MAP2 PRMPT$,S,10
	MAP2 DEL'CH'CNT,F
	MAP2 PRI'CNT,F
	MAP2 ROW,F
	MAP2 COL,F
	MAP2 EXISTS,F
	MAP2 ASC'SWITCH,B,1
	MAP2 INDEX,F
	MAP2 DEL'FLAG,B,1
	MAP2 OPEN'FLAG,B,1
	MAP2 X,F
	MAP2 I,F
	MAP2 J,F
	MAP2 K,F
	MAP2 LINES,F
	MAP2 BLK'FCTR,F			! block factor
	MAP2 TEST,F
	MAP2 UCOL,F
	MAP2 STSIZE,F
	MAP2 STATUS,F
	MAP2 IMAX,F
	MAP2 IDOT,F			! used for lifesigns
	MAP2 MDOTS,F			! "	"
	MAP2 DOTS,F			! "	"
	MAP2 ISAM'FLAG,F		! 0=random, 1=Isam
	MAP2 ISAM'SWITCH,F		! ISAM format: -1=normal,0=custom
	MAP2 ISAM'VER,F			! 0=old isam, 1=IsamPlus, 2=CISAM/DISAM [41]
	MAP2 ISAM'FREE,F		! # ISAM recs free
	MAP2 LAST'MEM'REC,F
	MAP2 SAVE'MEM'REC,F
	MAP2 BLOCK'POS,F
	MAP2 REMAINDER,F		! used in bitmap calc
	MAP2 ERR'R,F
	MAP2 EOF'FLAG,F			! set to 1 at 1st eof marked rec
	MAP2 DEL$,S,2
	MAP2 CTL'SWITCH$,S,2		! used to input ctl'switch
	MAP2 OUT'SWITCH$,S,2		! used to input out'switch
	MAP2 ISAM'SWITCH$,S,2		! "	"	isam'switch
	MAP2 TIMER$,S,8			! used to input timer value
	MAP2 IP'MAXREC,F		! [24] IsamPlus max rec #
	MAP2 BACKUP'P$,S,1		! [24]
	MAP2 BACKUP'M$,S,1		! [24]
	MAP2 A$,S,2
	MAP2 AA$,S,2
	MAP2 TMP$,S,20			! temp file name
	MAP2 LANG$,S,6			! -ext or ""
	MAP2 PW$,S,6			! time-out recovery password
	MAP2 PW2$,S,6			! "	"	"	"
    
    
MAP1 FILE'STUFF
	MAP2 FILE99,F			! used for memo
	MAP2 FILE100,F			! help data file
	MAP2 FILE101,F			! help memo file
	MAP2 MAX100,F
	MAP2 MAX101,F
	MAP2 FILE21,F			! used for isam tests
	MAP2 FILE22,F			! used for primary
	
MAP1 HELP'PARAMS
	MAP2 HELP'ROW,B,2
	MAP2 HELP'OPCODE,B,4
	MAP2 HELP'FILE'OPEN,F
	MAP2 NO'HELP'FLAG,F
	MAP2 HLINK,B,2				! [9]
	MAP2 HELP'SUB$,S,40			! [9]
	MAP2 HELP$,S,2				! [9]
	MAP2 HPOS,X,4				! [9]
	MAP2 RECSIZ100,F,6,32			! [12] MMOHLP.DAT recsize

MAP1 HELP'REC				! [12]
	MAP2 HELP'DESCR,S,30		! [12]	
	MAP2 HELP'LINK,B,2		! [12]

MAP1 HELP'DAT$,S,24,"INMEMO:HLPMMO.DAT"	! [12] HLPMMO.DAT filespec
MAP1 HELP'MMO$,S,24,"INMEMO:HLPMMO.MMO"	! [12] HLPMMO.MMO filespec


	! [24] ISAM format notes:  Both ISAM's use the same format for
	! [24] free records: 1st 4 bytes of rec gives rec # of next free
	! [24] rec (hw,lw).  IsamPlus uses filebase 0 for these #'s while
	! [24] Isam uses base 1.  Isam uses 0 to mark the end of the chain,
	! [24] while IsamPlus determines end of chain when the next free
	! [24] link is equal to the max # records in file (from rock, base 1)
	! [24] IMPORTANT: IsamPlus uses "SPAN'BLOCKS" mode, invalidating the
	! [24] the standard blocking logic so we use LOGRIO.SBR instead to
	! [24] perform logical rec io.

![37]
!MAP1 ROCK				! [6] image of ISAM rock
!	MAP2 ROCK'FILL'1,X,20
!	MAP2 IDAFRE1,B,2
!	MAP2 IDAFRE2,B,2
!	MAP2 ROCK'FILL'2,X,98           ! [24]
!	MAP2 NDAFRE1,B,2                ! [24] IsamPlus free rec (hw)
!	MAP2 NDAFRE2,B,2		! [24] "	"	 (lw)
!	MAP2 ROCK'FILL'3,X,16		! [24]
!	MAP2 IPRECCNT1,B,2		! [24] IsamPlus reccnt  (hw)
!	MAP2 IPRECCNT2,B,2		! [24] IsamPlus reccnt  (lw)
!	MAP2 IPMAXREC1,B,2		! [24] IsamPlus max rec (hw)
!	MAP2 IPMAXREC2,B,2		! [24]	"	"	(lw)
!	MAP2 ROCK'FILL'4,X,360		! [24]

![37] layout change
MAP1 ROCK				! [6] image of ISAM rock
	MAP2 ROCK'FILL'1,X,20
	MAP2 IDAFRE1,B,2
	MAP2 IDAFRE2,B,2
	MAP2 ROCK'FILL'2,X,68          	! [24]
	MAP2 IPRECSIZ1,B,2		! [37] IsamPlus rec size (hw) -92
	MAP2 IPRECSIZ2,B,2		! [37] IsamPlus rec size (lw) -94
	MAP2 ROCK'FILL2,X,4 		! [37] [38] -96	
	MAP2 NDAFRE1,B,2                ! [24] IsamPlus free rec (hw) -100
	MAP2 NDAFRE2,B,2		! [24] "	"	 (lw) -102
	MAP2 ROCK'FILL'3,X,20		! [37] -104
	MAP2 IPMAXREC1,B,2		! [37] IsamPlus max rec (hw) -124
	MAP2 IPMAXREC2,B,2		! [37]	"	"	(lw) -126
	MAP2 ROCK'FILL'4,X,384		! [24]


MAP1 ISAM'REC
	MAP2 IR1,B,2
	MAP2 IR2,B,2


MAP1 GTLANG'MAP					! [23]
	MAP2 LANG'NAME1,S,20
	MAP2 LANG'NAME2,S,20
	MAP2 LANG'EXTENSION,S,4
	MAP2 LANG'JUNK,X,310

MAP1 ERR'DSC(15),S,54				! [24]
	ERR'DSC(1)= "Primary Record After First Primary EOF Marker"
	ERR'DSC(2)= "Primary Link Contains Illegal Memo Record #"
	ERR'DSC(3)= "Primary Link Points to Not-in-use Record"
	ERR'DSC(4)= "Primary Link Points to Record Already in Use"
	ERR'DSC(5)= "Memo Link Contains Illegal Memo Record #"
	ERR'DSC(6)= "Memo Link Points to Not-in-use Record"
	ERR'DSC(7)= "Memo Link Points to Record Already In Use"
	ERR'DSC(8)= "Memo File Ctl Record Mismarked"
	ERR'DSC(9)= "Memo File Ctl Del Link Contains Illegal Memo Record #"
	ERR'DSC(10)="Memo File Ctl Del Link Points to Non-Deleted Record"
	ERR'DSC(11)="Memo File Ctl Del Link Points to Ctl Record"
	ERR'DSC(12)="Memo File Del Rec Link Contains Illegal Memo Record #"
	ERR'DSC(13)="Memo File Del Rec Link Points to Non-Deleted Record"
	ERR'DSC(14)="Memo File Del Rec Links to Previous Deleted Record"
	ERR'DSC(15)="Memo File Ctl EOF Link Incorrect"

MAP1 LOGRIO'PARAMS				! [24]
	MAP2 IO'CH,B,2,22			! [24] file channel
	MAP2 IO'READ,B,2,0			! [24] read op code
	MAP2 IO'WRT,B,2,1			! [24] write op code
	MAP2 IO'STATUS,B,2			! [24] return status
	MAP2 IO'MODE,F,6,0			! [24] 0=block, 1=log.rec.


MAP1 A2S,S,80					! [41]
MAP1 NULLFLAG,F,6				! [41]
MAP1 PHASE,F					! [41]
MAP1 FSTAT,F					! [41]
MAP1 ISMODE,F					! [41]
++include mmopar.bsi
++include mmosym.bsi
++include infpar.bsi                  ! infld maps
!------------------------------------------------------------------------

	on error goto TRAP
	filebase 1
	significance 11
	strsiz 24

     
	xcall LSTLIN,A2S
	if instr(1,ucs(A2S),"/CISAM") or instr(1,ucs(A2S),"/DISAM") then &
		ISAM'FLAG = 1 : ISAM'VER = 2	! [41]

++include getclr.bsi        ! [21] input color definitions
                                   ! [21] (& clr screen with new color)

START:
	xcall GTLANG,STATUS,GTLANG'MAP				! [23]
	if STATUS#0 or LANG'EXTENSION="USA" &
          then LANG$="" &
	  else LANG$="-"+LANG'EXTENSION 			! [23]

	call SCREEN'TITLE					! [12]
	HELP'ROW = 5						! [12]
	HELP'SUB$ = "ANAMMO-INTRO"				! [12]
	call HELP						! [12]
	HELP'ROW = 15						! [12]
	HELP'SUB$ = "ANAMMO-PASSWORD"				! [12]
	call HELP						! [12]
	HELP'ROW = 16						! [12]
	? tab(-1,11);						! [12]
S1:
	? tab(24,1);"Enter time-out period in seconds (0 to disable): ";
	TIMER=0
	xcall INFLD,24,70,5,0,"#E",TIMER$,INXCTL,1,1,EXITCODE,TIMER,CMDFLG,&
          DEFPT,MAXPT,FUNMAP,SETDEF,INFCLR                      ! [21]
	if INXCTL=2 goto EXIT2					! [12]
	TIMER = val(TIMER$)					! [12]
	if TIMER=0 goto S3					! [12]
S2:								! [12]
	? tab(24,1);tab(-1,9);"Enter your time-out recovery password: ";
	xcall INFLD,24,60,6,0,"A^]123",PW$,INXCTL,1,1,EXITCODE,TIMER,CMDFLG,&
          DEFPT,MAXPT,FUNMAP,SETDEF,INFCLR                      ! [21]

	if EXITCODE>0 goto S1   				! [12]
S3:								! [12]

	call SCREEN1

	call GET'SCREEN1
	? tab(3,1);tab(-1,10);tab(-1,12) 
	? "Phase 1:  Initial Linkage Investigation";tab(-1,11);"...";
	call OPEN'FILES
	if OUT'SWITCH=1 then call PRINT1
	if ISAM'SWITCH<>0 then ISAM'SWITCH=-1			! [6]
	PHASE = 1						! [41]
	call MAIN

	close #22						! [41]
	close #44						! [41]
	OPEN'FLAG = 0						! [14]
	PRMPT$="FIXIT?"	
	call SCREEN2
	call GET'SCREEN2
	if INXCTL=2 then goto ENDIT
	call OPEN'FILES
	call SCREEN'TITLE
	? tab(3,1);tab(-1,10);tab(-1,12)
	? "Phase 2:  Correcting Linkage Problems";tab(-1,11);"...";
	if OUT'SWITCH=1 then call PRINT1
	if LD'OPEN>=1 then call PRINT2 
	? 
	? "Initializing Bitmap";
	IMAX = int(BM'SIZE/4)
	IDOT = int(BM'SIZE/200)
	for I=1 to IMAX
		K=K+1
		if K>=IDOT then K=0 : ? ".";
		BM2(I)=0
	next I

	?
	for I=1 to 15
		CNT'ERR(I)=0
	next I
						! [6] reset free list file
	if ISAM'SWITCH<>0 then &
		ISAM'SWITCH=-1 : &
		if ISAM'VER # 2 then &
		    close #77 : &
		    open #77, TMP$, input	! [41]

	PHASE = 2				! [41]
	call MAIN
	call BLD'DEL'CHN
	PRMPT$="FIXED"
	call SCREEN2
	call DSP'END
	if LD'OPEN>=1 then call PRINT'END 
!
!
ENDIT:
        if erf(17) # -1 then close #17      ![44]

	if OPEN'FLAG<>1 then goto EXIT
	close #44
	close #22
	if ISAM'SWITCH<>0 and ISAM'VER#2 close #77		! [6][41]
!
!
!
EXIT:
	? tab (24,1);tab(-1,9);"Hit (RETURN) to Finish";
	TIMER=0
	xcall INFLD,24,61,1,0,"C",A$,INXCTL,1,INF'OPCD,EXITCODE,TIMER,CMDFLG,&
          DEFPT,MAXPT,FUNMAP,SETDEF,INFCLR                      ! [21]

EXIT2:
	? tab(-1,0);"Program End"
	? ".";
	END

BITMAP'OVERFLOW:
	? tab(22,1);tab(-1,10);tab(-1,12);
	? "Memo file too big for current ANAMMO bitmap size!"
	? "Must edit ANAMMO.BAS and expand BIT'MAP() array!"
	goto EXIT

SCREEN1:
!
!Display prompts for initial data entry
!
	call SCREEN'TITLE

	? tab(5,5); "Enter Name of Memo File: "
	? tab(6,5); "Did You Make a Backup of This File?  "
	? tab(8,5); "Enter Name of Primary File: "
	? tab(9,5); "Did You Make a Backup of This File?  "
	? tab(11,5); "Enter Record Size: "
	? tab(12,5); "First Record a Control Record? "
	? tab(13,5); "Enter Link Position Within Record: "
	? tab(14,5); "Enter Starting EOF-Record Marker Position: "
	? tab(15,5); "Enter EOF-Record Marker: "
	? tab(16,5); "Enter Starting Del-Record Marker Position: "
	? tab(17,5); "Enter Del-Record Marker: "
	? tab(18,5); "Output Detailed Account of Errors to File? "

	return
!
REPAINT1:
	if EXTERR=MME'OK goto RP1X	! [25] Screen restored by INMEMO

	call SCREEN1
	? tab(-1,12);
	? tab(5,55);MEMO$;
	? tab(6,55);BACKUP'M$;					! [24]
	? tab(8,55);PRIMARY$;
	? tab(9,55);BACKUP'P$;					! [24]
	? tab(11,55);str(REC'SIZE);
	? tab(12,55);
	if CTL'SWITCH=1 ? "Y"; else ? "N";
	? tab(13,55);str(LINK'POS);
	? tab(14,55);str(EOF'POS);
![41]	if EOF'MARK<>"" then &

	if LENGTH(1)>0 then &
		XSTRING'VAR = EOF'MARKX : LENS = LENGTH(1) :&
		call FMT'STRING : ? tab(15,55);STRING'FMT;        ! [29][41]
	? tab(16,55);str(DEL'POS);

![41]	if DEL'MARK<>"" then &

	if LENGTH(1)>0 then &
		XSTRING'VAR = DEL'MARKX : LENS = LENGTH(2) :&
		call FMT'STRING : ? tab(17,55);STRING'FMT;        ! [29][41]
RP1X:
	return
!
!
!Display 2nd screen with error statistics
!
!
SCREEN2:

	call SCREEN'TITLE
	? tab(1,62);tab(-1,10);

				! [24] Use variables for err descriptions

	? tab(2,59);                                                          "|# of |Danger|"
	? tab(3,1); "Err|             Description                              |Occ's|To Fix|";PRMPT$
	? tab(4,1); "No.|------------------------------------------------------|-----|------|------"
	? tab(5,1); " 1 |";ERR'DSC(1); tab(58);"|     |  ?   |  NO"
	? tab(6,1); " 2 |";ERR'DSC(2); tab(58);"|     | Low  |"
	? tab(7,1); " 3 |";ERR'DSC(3); tab(58);"|     | Low  |"
	? tab(8,1); " 4 |";ERR'DSC(4); tab(58);"|     | High |"
	? tab(9,1); " 5 |";ERR'DSC(5); tab(58);"|     | Low  |"
	? tab(10,1);" 6 |";ERR'DSC(6); tab(58);"|     | Low  |"
	? tab(11,1);" 7 |";ERR'DSC(7); tab(58);"|     | High |"
	? tab(12,1);" 8 |";ERR'DSC(8); tab(58);"|     | Low  |  Yes"
	? tab(13,1);" 9 |";ERR'DSC(9); tab(58);"|     | Low  |  Yes"
	? tab(14,1);"10 |";ERR'DSC(10);tab(58);"|     | Low  |  Yes"
	? tab(15,1);"11 |";ERR'DSC(11);tab(58);"|     | Low  |  Yes"
	? tab(16,1);"12 |";ERR'DSC(12);tab(58);"|     | Low  |  Yes"
	? tab(17,1);"13 |";ERR'DSC(13);tab(58);"|     | Low  |  Yes"
	? tab(18,1);"14 |";ERR'DSC(14);tab(58);"|     | Low  |  Yes"
	? tab(19,1);"15 |";ERR'DSC(15);tab(58);"|     | Low  |"
	? tab(20,1);"------------------------------------------------------------------------------"
	? tab(21,5);"The Number Of Memo Recs Unaccounted For is:"
!	
	? tab(-1,23);			! [111] enable graphics
	for ROW = 3 to 19 	
		? tab(ROW,4);tab(-1,47);
	next ROW
	? tab(20,1);
	for COL=1 to 3
		? tab(-1,46);
	next COL
	? tab(-1,45);
	for COL=5 to 58
		? tab(-1,46);
	next COL
	? tab(-1,45);
	for COL=60 to 64
		? tab(-1,46);
	next COL
	? tab(-1,45);
	for COL=66 to 71
		? tab(-1,46);
	next COL
	? tab(-1,45);
	for COL=73 to 78
		? tab(-1,46);
	next COL
	for ROW = 19 to 2 step -1
		? tab(ROW,59);tab(-1,47);
	next ROW
	for ROW = 2 to 19
		? tab(ROW,65);tab(-1,47);
	next ROW
	for ROW = 19 to 2 step -1
		? tab(ROW,72);tab(-1,47);
	next ROW
	? tab(4,4);tab(-1,44);
	for COL = 5 to 78
		? tab(-1,46);
	next COL
	? tab(4,72);tab(-1,48);tab(4,65);tab(-1,48);tab(4,59);tab(-1,48);

	? tab(-1,24);

	if PRMPT$="FIXED" then goto DSP'IT
!
	? tab(22,5);"Incorporate Unaccounted-For Recs Into New Del Chain?"
!
	if OUT'SWITCH=0 then 				&
		? tab(23,5);tab(-1,9);"Output Detailed Account of Errors to File?"
	
	if OUT'SWITCH=1 then						&
		? tab(23,5);tab(-1,9);"(Error list ";OUT'FILE;		&
			" is available for inspection - use another crt.)";:&
		OUT'SWITCH = 0	:					&
		if LD'OPEN>=0 	then					&				
			call PRINT'END 	:				&
			LD'OPEN = 0	:				&
			close #17		! [35]
!
!
!
DSP'IT:
	? tab(-1,12);
	for I=1 to 15
		? tab(I+4,60);CNT'ERR(I)
	next I

!
	? tab(21,60);CNT'UN'RECS
!
!
	return
!
!
DSP'END:
	for I=2 to 15	
		if FIXIT(I)=1 then STRING'VAR="Y" else STRING'VAR="N"
		if I<8 or I>14 then  ? tab (I+4,75);STRING'VAR;
	next I
	? tab(-1,11);
	? tab(22,5);"Length of New Deleted Chain:";
	? tab(-1,12);tab(22,60);DEL'CH'CNT
	? tab(23,5);tab(-1,11);"No. of Pri Recs With Memos Attached:";
	? tab(23,60);tab(-1,12);PRI'CNT
	return


!
!------------------------------------------------------------------------
!
GET'SCREEN1:	
!
!Get information for 1st round processing
!
!---------------------------------------------
!
!First get name of memo file and check for it.
!
	INF'OPCD=1
	STSIZE=24
	ROW=5
	COL=55
	STRING'VAR = MEMO$
	call GET'STRING
? TAB(22,1);"EXITCODE: ";EXITCODE
	if MEMO$ # STRING'VAR then BACKUP'M$ = "N"		! [29]
	if EXITCODE>0 goto ENDIT
	EXT'N=".MMO"
	call FILE'EXIST
	MEMO$=STRING'VAR
!
!Compute no of memo recs and get link size
!
	MAX'MEM'REC= abs(EXISTS)*8				! [36]
	if MAX'MEM'REC>(BM'SIZE*8) goto BITMAP'OVERFLOW

	open #99,MEMO$,random,64,FILE99					! [4]
	FILE99=1							! [4]
	read #99,CTL'REC							! [4]
	if CTL'FILL[1,1]="4" then BYTE'FLAG=4 else BYTE'FLAG=2	     ! [4][7]
	? tab(4,55);tab(-1,11);"Link size: ";tab(-1,12);BYTE'FLAG	! [4]	
	close #99							! [4]
!
!Is there a backup of the memofile?
!
	ROW=6
	DEL$=BACKUP'M$						! [29]
![29]	BACKUP'M$ = "N"  					! [24]
	call BACKUP
	if EXITCODE>0 goto GET'SCREEN1				! [24]
	BACKUP'M$ = DEL$					! [24]

!
!Now get name of primary file
!

PRI'NAM:
	ROW=8
	STRING'VAR = PRIMARY$
	HELP$ = "?"						! [24]
	call GET'STRING
	if PRIMARY$ # STRING'VAR then BACKUP'P$ = "N"		! [29]
	if EXITCODE>0 and EXITCODE<=3 then goto GET'SCREEN1
	if EXITCODE=8 then &
		HELP'SUB$="ANAMMO-PRI'FILE" : &
		call HELP'SVA : call REPAINT1 : goto PRI'NAM	! [25]

	EXT'N=".DAT"
	call FILE'EXIST
	PRIMARY$=STRING'VAR

	if instr(1,ucs(PRIMARY$),".IDA") > 0 			 &  
		call GET'ISAM'ROCK				:&
		? tab(ROW-1,65);tab(-1,11);tab(-1,9);		:&
		if ISAM'VER = 0					 &
			? "(Old ISAM)"				 &
		else if ISAM'VER = 1 				 &	
			? "(IsamPLUS)"				 &


	if ISAM'VER=2 then ? TAB(ROW-1,65);"(CISAM)"		! [37][41]

![29]	if instr(1,PRIMARY$,".IDX")>0 then &
![29]		PRIMARY$[X+3,X+3]="A" 				! [6] 
	X = instr(1,PRIMARY$,".IDX")                            ! [30]
        if X>0 then PRIMARY$[X+3,X+3]="A" 			! [30]

	if ISAM'VER # 2 then &
	    ISAM'FLAG = instr(1,PRIMARY$,".IDA")		! [6] ISAM? [41]

!
!Is there a backup?
!
	ROW=9
	DEL$=BACKUP'P$						! [29]
![29]	BACKUP'P$ = "N"  					! [24]
	call BACKUP
	if EXITCODE>0 goto PRI'NAM				! [24]
	BACKUP'P$ = DEL$					! [24]
!
!Now we get primary file record size
!

PRI'SIZ:
	ROW =11
	if ISAM'VER = 1 	REC'SIZE = IPRECSIZ2		! [37]
	if REC'SIZE>0 FLOAT'VAR$ = REC'SIZE else FLOAT'VAR$=""
	call GET'FLOAT
	? tab(24,1);tab(-1,9);
	if EXITCODE=3 then goto PRI'NAM
	REC'SIZE=FLOAT'VAR
	if REC'SIZE=0 ? chr(7); : goto PRI'SIZ
	! if REC'SIZE>=512 then IOMODE = IOMODE max 1	! [42]
!	
!Compute no of primary file records
!
        if REC'SIZE > 512 then &
	    MAX'PRI'REC= int((abs(EXISTS)*512)/REC'SIZE) &
        else	    &
	    MAX'PRI'REC= abs(EXISTS)*INT(512/REC'SIZE)		! [36][42]

	IF IO'MODE > 0 or ISAM'VER = 2 then &
		DOTS=abs(int(MAX'PRI'REC/50)) &
	else &
	        DOTS=abs(int(EXISTS/50))			! [41]

!
!Is the first record a ctl rec? Set CTL'SWITCH to 1 if so, else 0
!
PRI'CTL:
	ROW=12
	HELP$="?"							! [9]
	DEL$=CTL'SWITCH$
	if ISAM'VER >= 1 					 &
		CTL'SWITCH = 0					:&
		CTL'SWITCH$ = "N"				:&
		? tab(12,55);"N";				:&
		if EXITCODE = 3					 &
			goto PRI'SIZ				 &
		else						 &
			goto PRI'POS			! [37][41]	

	if ISAM'FLAG>0 then call NO else call YES			! [6]
	if EXITCODE=8 then &
		HELP'SUB$="ANAMMO-CONTROL REC" : &
		call HELP'SVA : call REPAINT1 : goto PRI'CTL		! [25]
	if EXITCODE=3 then goto PRI'SIZ
	CTL'SWITCH=2-INXCTL
	CTL'SWITCH$=DEL$
!
!Get link position
!
PRI'POS:
	ROW=13
	HELP$="?"							! [9]
	if LINK'POS>0 FLOAT'VAR$ = LINK'POS else FLOAT'VAR$=""
	HELP$ = HELP$ + "9"		! [33] allow home for array
	? tab(ROW,68);tab(-1,11);"HOME : array";	! [33]
							! [35]
	? tab(23,1);"If primary record contains multiple links, enter 1st position and hit HOME";

	call GET'FLOAT
	? tab(ROW,68);tab(-1,9):			! [33]
	? tab(23,1);tab(-1,9);				! [35]

	if EXITCODE=8 then &
		HELP'SUB$="ANAMMO-LINK POSITION" : &
		call HELP'SVA : call REPAINT1 : goto PRI'POS		! [25]
	if EXITCODE=3 then goto PRI'CTL
	LINK'POS=FLOAT'VAR
	if LINK'POS=0 ? chr(7); : goto PRI'POS

		! [33] give option to enter a number of link occurances
		! [33] (to support an array of links)

	if EXITCODE # 9 			&	
		LNKOCC = 1 :			&		
		goto ISAM'CONFIG			! [33]
	call LNKOCC					! [33]
	? tab(13,68);tab(-1,11);tab(-1,11);		! [33]
	if LNKOCC > 1 then 	&
		? "(x";tab(-1,12);str(LNKOCC);tab(-1,11);")"	! [32]


!PRI'LL:
!	ROW=24
!	? tab(ROW,5);"Are You Using a Two or Four Byte Link? (2 or 4): ";
!	call GET'FLOAT
!	if EXITCODE=3 then goto PRI'POS
!	if FLOAT'VAR <>2 and FLOAT'VAR<>4 then goto PRI'LL
!	BYTE'FLAG=FLOAT'VAR
!	? tab(24,1);tab(-1,9);

!
!If ISAM, ask whether to use standard ISAM file layout or other deleted
!and EOF record markers

ISAM'CONFIG:							! [6]
	if ISAM'FLAG<1 goto PRI'EOF				! [6]
!	if ISAM'VER=2 goto PRI'EOF				! [41]
	? tab(23,1);tab(-1,9);tab(-1,11);			! [6]
	? "    Is this an unmodified ISAM configuration?";	! [6]
	ROW=23							! [6]
	HELP$="?"						! [9]
	DEL$=ISAM'SWITCH$
	call YES						! [6]
	if EXITCODE=8 then &
		HELP'SUB$="ANAMMO-ISAM" : &
		call HELP'SVA : call REPAINT1 : goto ISAM'CONFIG ! [25]
	ISAM'SWITCH=2-INXCTL					! [6]
	ISAM'SWITCH$=DEL$
	? tab(23,1);tab(-1,9);tab(-1,11);			! [6]
	if ISAM'SWITCH>0 then goto OTH'NIU			! [6]
	
!
! Get starting EOF marker position
!

PRI'EOF:
	ROW=14
	if EOF'POS>0 then FLOAT'VAR$=EOF'POS else FLOAT'VAR$=""
	call GET'FLOAT
	if EXITCODE=3 then goto PRI'POS
	EOF'POS=FLOAT'VAR
	if EOF'POS=0 goto DEL'POS		! [4]
	DEL$=""
	call ASCII				!can marker be entered as
	if ASC'SWITCH=1 then goto EOF'STRING	!ASCII string?
	INDEX=1
	call UNFORM'MARK			!no, it can't
	goto DEL'POS

EOF'STRING:					!yes, it can
!	
!Get EOF string
!
	STSIZE=12
	ROW=15
	? tab(ROW,28);" (string format): "
	STRING'VAR = EOF'MARK
	call GET'STRING
	if EXITCODE=3 then goto PRI'EOF
	EOF'MARK=STRING'VAR			! [24]
	LENGTH(1)=len(EOF'MARK)

DEL'POS:
!
!Get starting Del record marker position
!
	ROW=16
	if DEL'POS>0 FLOAT'VAR$=DEL'POS else FLOAT'VAR$=""
	call GET'FLOAT
	if EXITCODE=3 then goto PRI'EOF
	if FLOAT'VAR=0 goto FIL'OUT
	DEL'POS=FLOAT'VAR
	DEL$=""
	call ASCII				!can marker be entered as
	if ASC'SWITCH=1 then goto DEL'STRING	!ASCII string?
	INDEX=2
	call UNFORM'MARK			!no, it can't
	goto OTH'NIU

DEL'STRING:					!yes, it can
!	
!Get Del string
!
	ROW=17
	? tab(ROW,28);" (string format): "
	STRING'VAR=DEL'MARK
	call GET'STRING
	if EXITCODE=3 then goto DEL'POS
	DEL'MARK=STRING'VAR
	LENGTH(2)=len(DEL'MARK)			![4]

OTH'NIU:
!	
!Other not-in use markers?
!
	? tab(23,5);"Are there other rec-not-in-use markers?";
	ROW=23
	HELP$="?"						! [9]
	DEL$=""
	call NO
	if EXITCODE=8 then &
		HELP'SUB$="ANAMMO-NIU RECS" : &
		call HELP'SVA : call REPAINT1 : goto OTH'NIU	! [25]
	if EXITCODE=3 then goto DEL'POS
	if INXCTL<>2 then call OTHERS	
!
!File output?
!
FIL'OUT:
	ROW=18
	DEL$=OUT'SWITCH$
	call NO
	if EXITCODE=3 then goto OTH'NIU
	OUT'SWITCH$=DEL$
	OUT'SWITCH=2-INXCTL
	if OUT'SWITCH<>1 then goto ND'GET'1
	call FILE'PROMPTS
	call FILE'INFO
	if EXITCODE=3 then goto FIL'OUT
	
ND'GET'1:
!
!End of call
!
	? tab(24,1);"    Any Changes? ";
	ROW=24
	COL=55
	DEL$=""
	call NO
	if DEL$="Y" goto GET'SCREEN1	



	return
!
!
!
!
GET'SCREEN2:
!
!
!Get info for second round processing.
!
!
! First, do we do second round?
!
	INF'OPCD=0

	if BACKUP'P$#"Y" or BACKUP'M$#"Y" then &
		? tab(23,1);tab(-1,9); : &
		? "Updating not allowed since backups were not made" : &
		INXCTL = 2 : &	
		goto ND'GET'SCREEN2			! [24]

	? tab(24,5);"Do You Want to Continue?(N=Quit)";
!
	BYTE'VAR=0
	ROW=24
	COL=55
	call YES'STRIP
	if INXCTL=2 goto ND'GET'SCREEN2		!INXCTL=2 means "NO" we don't
!
! Yes, we do
!
ST'GET'SCREEN2:
!
!Now, find out which problems to fix
!
	COL=75
	for I=2 to 7
		ROW=I+4
		call NO
		if EXITCODE=3 and I>2 then I=I-2 : goto NXT'FIX
		if EXITCODE=3 then I=7 :goto NXT'FIX
		if INXCTL=1 then FIXIT(I)=1 else FIXIT(I)=0
	   NXT'FIX:	
	next I
	if EXITCODE=3 then goto GET'SCREEN2
	for I=8 to 14
		FIXIT(I)=1
	next I
 SCN2'CTL'EOF:
	ROW=19
	call NO
	if EXITCODE=3 then goto ST'GET'SCREEN2
	if INXCTL=1 then FIXIT(15)=1 else FIXIT(15)=0
!
!include unexplained recs in new del ch?
!
 SCN2'DEL'INC:	
	ROW =22
	call YES
	if EXITCODE=3 then goto SCN2'CTL'EOF
	if INXCTL=1 then DEL'FLAG=1
!
!get file info if necessary
!
 SCN2'OUT:	
	if OUT'SWITCH<>0 then goto CNG'GET'SCN2
	ROW=23
	? tab(23,5);tab(-1,9);tab(-1,11);		&
		"Output Detailed Account of Errors to File?" ![36]
	call NO
	if EXITCODE=3 then goto SCN2'DEL'INC
	if INXCTL<>1 goto CNG'GET'SCN2
	COL=55
	BYTE'VAR=1
	call FILE'PROMPTS	
	call FILE'INFO
	if EXITCODE=3 then ? tab(21,1);tab(-1,10);tab(23,5);"Output to File?"; : goto SCN2'OUT

!
!any change? and set file flags.
!
CNG'GET'SCN2:
	? tab(24,5);"ANY CHANGE?";tab(-1,9);
	ROW=24	
	UCOL=COL
	COL=25
	call NO
	COL=UCOL
	if INXCTL=1 goto GET'SCREEN2
	LD'OPEN=OUT'SWITCH
	OUT'SWITCH=BYTE'VAR
	LD'OPEN=LD'OPEN or OUT'SWITCH
	INXCTL=1
!	
!
ND'GET'SCREEN2:
	return
!
!
OPEN'FILES:
!
!Open files as needed
!
8701	if OUT'SWITCH<>0 then open #17,OUT'FILE,OUTPUT 
8702	if OPEN'FLAG<>0 then goto ND'OPEN
8703		open #44,MEMO$,RANDOM,64,CUR'MEM'REC
		OPEN'FLAG=1
		if ISAM'FLAG=0 then &
		    open #22,PRIMARY$,RANDOM,1024,CUR'PRI'BLK : goto ND'OPEN ![42]

8705		if ISAM'VER=2 then &
	    	    ? " (CISAM)"; : &
		    lookup PRIMARY$,IP'MAXREC : &
		    IP'MAXREC = INT((ABS(IP'MAXREC)-1) * (512/(REC'SIZE+1))) :&
		    IO'MODE = 2 :&
	            open #22, PRIMARY$,RANDOM,REC'SIZE+1,CUR'PRI'BLK,SPAN'BLOCKS  :&
	            goto ND'OPEN
		

	! [6] For ISAM, create a sequential file containing the records
	! [6] in the free chain, and then sort it.  Later, this can be
	! [6] used to skip the deleted recs which are not otherwise marked.

		?							! [6]
		? "Scanning ISAM free chain"; 				! [6]
OF1:									! [6]
									! [6]
8706		TMP$ = "MMO"+(int(rnd(1)*999) using "###") +".TMP"	! [6]
		lookup TMP$, X						! [6]
		if abs(X) <> 0 goto OF1		![39] [6]
                if erf(77) # -1 then close #77  ![44]
8707		open #77, TMP$, output					! [6]
									! [6]
8708		call GET'ISAM'ROCK				! [37]
		X = instr(1,PRIMARY$,".")				! [6]
		PRIMARY$[X+3,X+3]="X"					! [6]
		open #21,PRIMARY$,RANDOM,512,FILE21			! [6]
		PRIMARY$[X+3,X+3]="A"					! [6]
		FILE21 = 1						! [6]
		read #21, ROCK						! [6]
		close #21						! [6]

! [24] Note that "span'blocks" will generate an error on pre 2.0 Basic,
! [24] in which case comment out the offending statements and replace
! [24] them with the lines marked ![B13] (by un-commenting them)

	if ROCK[497;15]="NEW ISAM FORMAT" then &
	ISAM'VER = 1 : ? " (IsamPlus)"; : &
		    open #22, PRIMARY$,RANDOM,REC'SIZE,FILE22,span'blocks &
		else &
		    ISAM'VER = 0 : ? " (Old Isam)"; : &
		    open #22, PRIMARY$,RANDOM,REC'SIZE,FILE22		! [24]

![B13]     if ROCK[497;15]="NEW ISAM FORMAT" then &
![B13]           ? tab(23,1);tab(-1,9); &
![B13]			"Can't work on IsamPlus files with this version of ANAMMO/Basic" : &
![B13]			END &
![B13]		else &
![B13]		    ISAM'VER = 0 : ? " (Old Isam)"; : &
![B13]		    open #22, PRIMARY$,RANDOM,REC'SIZE,FILE22		! [24]


                if ISAM'VER=0 then &
		  FILE22 = IDAFRE1*65536+IDAFRE2 : goto OF'LOOP ! next free [24]

		IO'MODE = 1			! [24] use log spanned io
                FILE22 = NDAFRE1*65536+NDAFRE2				! [24]
		IP'MAXREC = IPMAXREC1*65536+IPMAXREC2			! [24]
	? : ? "Maxrec: ";IP'MAXREC					! [6]

		! [37] if IsamPlus - check if LOGRIO in memory now by
		! [37] reading a sample rec

	if ISAM'FLAG > 0 and ISAM'VER = 1				 &
		xcall LOGRIO,IO'READ,IO'CH,BLOCK2048,2,IO'STATUS	:&
		if IO'STATUS#0 goto IO'ERR			! [37][41]


OF'LOOP:								! [6]
8731		if ISAM'VER=0 goto OF'L2
	
		   FILE22 = FILE22 + 1		! [24] convert to base 1
		   if FILE22>=IP'MAXREC then FILE22=0	! [24]
OF'L2:						! [24]
! ? FILE22;
		if FILE22=0 goto OF'SORT				! [6]
		? #77,FILE22 using "########"				! [6]
		J = J + 1 						! [6]
		if J>=DOTS J=0 : ? ".";					! [6]
		read #22, ISAM'REC					! [6]
		FILE22 = IR1*65536+IR2					! [6]
		goto OF'LOOP						! [6]
OF'SORT:								! [6]
8735		close #77						! [6]
		open #77, TMP$, input					! [6]
		? : ? "Sorting free list...";				! [6]
		xcall BASORT,77,77,8,8,1,0,0,0,0,0,0,0			! [6]
		?							! [6]
		close #77						! [6]
		open #77, TMP$, input					! [6]
		close #22						! [6]

![24]		open #22, PRIMARY$, RANDOM, 512, CUR'PRI'BLK		! [6]

8740		if ISAM'VER=1 then &
		    open #22, PRIMARY$,RANDOM,REC'SIZE,CUR'PRI'BLK,span'blocks &
		else &
		    open #22, PRIMARY$,RANDOM,1024,CUR'PRI'BLK		! [24][42]


![B13]		open #22, PRIMARY$,RANDOM,512,CUR'PRI'BLK		! [24]


ND'OPEN:
8745	return

!---------------------------------------------------------------------------
! [37] Get ISAM Rock - read rock and set ISAM'VER flag
!---------------------------------------------------------------------------
GET'ISAM'ROCK:
	if ISAM'VER = 2 then RETURN				! [41]

	X = instr(1,PRIMARY$,".")				! [6]
35100	PRIMARY$[X+3,X+3]="X"					! [6]
35110	open #21,PRIMARY$,RANDOM,512,FILE21			! [6]
35120	PRIMARY$[X+3,X+3]="A"					! [6]
	FILE21 = 1						! [6]
35130	read #21, ROCK						! [6]
35140	close #21						! [6]

![B13]	if ROCK[497;15]="NEW ISAM FORMAT" then 				 &
![B13]		? tab(23,1);tab(-1,9);"Can't work on IsamPlus files ";	 &
![B13]			"with this version of ANAMMO/Basic" 		:&
![B13]			END						 &
![B13]	else								 &
![B13]		ISAM'VER = 0

	if ROCK[497;15]="NEW ISAM FORMAT" then 		&
		ISAM'VER = 1				&
	else						&
		ISAM'VER = 0

	return
!----------------------------------------------------------------------------
!
!routines to get file info
!
FILE'PROMPTS:
	for ROW=20 to 23
		? tab(ROW,1);tab(-1,9)
	next ROW
	? tab(21,5);tab(-1,10);tab(-1,11);"Enter Output File Name:"
	? tab(22,5);"Enter Key Position:"
	? tab(23,5);"Enter Key Length:"
	return
!
!
!
FILE'INFO:
  ST'FILE'INFO:
	ROW=21
	STRING'VAR = OUT'FILE
	call GET'STRING
	if EXITCODE=3 then goto ND'FIL'IN
	OUT'FILE=STRING'VAR
	lookup OUT'FILE,EXISTS		
	if abs(EXISTS) = 0 goto CONT'FILE'INFO		! [39] adding abs
	? tab(24,1);tab(-1,9);tab(5);OUT'FILE;" already exists. Kill it? ";
	ROW=24
	DEL$=""
	call NO
	? tab(24,1);tab(-1,9);					! [24]
	if INXCTL<>1 goto ST'FILE'INFO	
CONT'FILE'INFO:
	ROW=22
	if KEY'POS>0 then FLOAT'VAR$=KEY'POS else FLOAT'VAR$=""
	call GET'FLOAT
	if EXITCODE=3 then goto ST'FILE'INFO
	KEY'POS=FLOAT'VAR
	ROW=23
	if KEY'SIZE>0 then FLOAT'VAR$=KEY'SIZE else FLOAT'VAR$=""
	call GET'FLOAT
	if EXITCODE=3 then goto CONT'FILE'INFO
	KEY'SIZE=FLOAT'VAR
	if KEY'POS+KEY'SIZE>REC'SIZE then &
          ? tab(24,1);"Length or Pos. Error"; : goto ST'FILE'INFO     ! [24]
 ND'FIL'IN:
	return
!
!get key from primary file for detailed error messages.
!
GET'KEY:
	KEY'STR=BLOCK2048[REC'CNT*OFFSET+KEY'POS;KEY'SIZE]
	return
!
!is there a backup of the file in question?
!
BACKUP:
	HELP$="1"						! [24]
	call NO
	if EXITCODE>0 return					! [24]
	if DEL$="N" call NO'BACK           			! [24]
	return	
!
!
!
NO'BACK:
![24]	? tab(-1,0);"PLEASE MAKE BACKUPS AND TRY AGAIN. THANK YOU"
![24]	? "HIT RETURN TO EXIT"
![24]	goto ENDIT
					! tell them about consequences of
					! not having a backup
	HELP'SUB$="ANAMMO-BACKUP"	! [24]-
	call HELP'SVA                   ! [25]
	call REPAINT1 
	return				! -[24]


!
!get a string from terminal at ROW,55
!

GET'STRING:	
	xcall INFLD,ROW,55,STSIZE,1,"3A^]E/"+HELP$,STRING'VAR,INXCTL,1,INF'OPCD, &
          EXITCODE,TIMER,CMDFLG,DEFPT,MAXPT,FUNMAP,SETDEF,INFCLR   ! [21]
     
	if EXITCODE=11 call TIME'OUT : goto GET'STRING
	return
!
!does the file in question exist
!
FILE'EXIST:
  ST'FILE:
	X = instr(1,STRING'VAR,".IDA")		! [31] Isam?
	if X>0 STRING'VAR[X+3;1]="X" : goto FE2	! [31] if so, chg to IDX for
						! [31] for this test to avoid
						! [31] rec size problems
	X = instr(1,STRING'VAR,".")
	if X<1 then STRING'VAR=STRING'VAR+EXT'N : ? tab(ROW,55);STRING'VAR
FE2:						! [31]
	? tab(22,1);tab(-1,10);

	lookup STRING'VAR,EXISTS
	if abs(EXISTS) # 0 goto CHK'EXCL		! [39] [36]

	? tab(23,1);"There is no such (RANDOM) file as ";STRING'VAR;". Please try again.";
	CMDFLG = 0					! [29]
	call GET'STRING
    	? tab(23,1);tab(-1,9);				! [29]
![38]	if EXITCODE>3 then goto ENDIT
	if EXITCODE>0 then goto ENDIT			! [38]
	goto ST'FILE
 
  CHK'EXCL:					! [41][29] check if in use
	PRIMARY$ = STRING'VAR
	
	if ISAM'VER=2 then &
	    X = instr(1,PRIMARY$,".")	:&
	    if X > 0 then PRIMARY$=PRIMARY$[1,X-1]	! [41]

!	if ISAM'VER=2 and instr(1,UCS(STRING'VAR),".DAT") > 0 then &
!	    open #22, PRIMARY$,INDEXED'EXCLUSIVE,CUR'PRI'BLK,FSTAT  &
!	else &
!

        open #22,STRING'VAR,RANDOM,512,CUR'PRI'BLK 	! [29] error?
35002	close #22					! [29]

  ND'FILE:
	X = instr(1,STRING'VAR,".IDX")		! [31] Isam?
	if X<1 return				! [36] no

	STRING'VAR[X+3;1]="A" 			! [31] if so, chg back to IDA
35003	lookup STRING'VAR,EXISTS		! [36] return size of IDA!

	return

GET'FLOAT:
	xcall INFLD,ROW,55,11,0,"3#]RE/"+HELP$,FLOAT'VAR$,INXCTL,1,INF'OPCD,EXITCODE,TIMER,CMDFLG,&
          DEFPT,MAXPT,FUNMAP,SETDEF,INFCLR                      ! [21]

	HELP$=""							! [9]
	if EXITCODE=11 call TIME'OUT : goto GET'FLOAT
	FLOAT'VAR=val(FLOAT'VAR$)
	return

YES:
 	xcall INFLD,ROW,COL,1,1,"3Y/"+HELP$,DEL$,INXCTL,1,INF'OPCD,EXITCODE,TIMER,CMDFLG,&
          DEFPT,MAXPT,FUNMAP,SETDEF,INFCLR                      ! [21]

	HELP$=""							! [9]
	if EXITCODE=11 call TIME'OUT : goto YES
	return	

NO:
 	xcall INFLD,ROW,COL,1,1,"3YN/"+HELP$,DEL$,INXCTL,1,INF'OPCD,EXITCODE,TIMER,CMDFLG,&
          DEFPT,MAXPT,FUNMAP,SETDEF,INFCLR                      ! [21]

	HELP$=""							! [9]
	if EXITCODE=11 call TIME'OUT : goto NO
	return	

YES'STRIP:
 	xcall INFLD,ROW,COL,1,1,"3Y0/"+HELP$,DEL$,INXCTL,1,0,EXITCODE,TIMER,NOCMD,&
          DEFPT,MAXPT,FUNMAP,SETDEF,INFCLR                      ! [21]

	HELP$=""							! [9]
	if EXITCODE=11 call TIME'OUT : goto YES'STRIP
	return	

ASCII:
	? tab(23,5);tab(-1,9);"Can Marker Be Entered as ASCII String?";
	ROW=23
	call YES
	ASC'SWITCH=2-INXCTL
	return

LNKOCC:								! [32]-
	? tab(-1,11);
	? tab(23,5);tab(-1,9);"How many link occurances (in an array): ";
	ROW=23
	FLOAT'VAR$ = LNKOCC
	INF'OPCD = 1
	HELP$ = "?"
	call GET'FLOAT
	if EXITCODE=8 then &
		HELP'SUB$="ANAMMO-LINK ARRAY"		: &
		call HELP'SVA : call REPAINT1 		: &
		: goto LNKOCC 					

	LNKOCC = FLOAT'VAR max 1
	return							! -[32]

ANYKEY:
	xcall INFLD,ROW,COL,1,0,"C3",AA$,INXCTL,1,0,EXITCODE,TIMER,CMDFLG,&
          DEFPT,MAXPT,FUNMAP,SETDEF,INFCLR                      ! [21]

	return
!
!
!routine for inputting a not in use record marker not expressable as
!an ascii string
!
UNFORM'MARK:
	INF'OPCD=0
	LENGTH(INDEX)=0
	ROW=21
	UCOL=1
	LINES=0
	? tab(23,5);tab(-1,9);					 ![41]
	? tab(24,5);"Enter 256 for null bytes, 0 to terminate";  ![41]
   ST'UNF'MARK:
	? tab(20,5);"Enter Byte #";LENGTH(INDEX)+1;" (Numeric Value): "
	call GET'BYTE
	if BYTE'VAR=0 and NULLFLAG=0 then goto ND'UNF'MARK	! [41]
![41]	if LENGTH(INDEX)>0 then  MARKER(INDEX)=MARKER(INDEX)[1,LENGTH(INDEX)]+BYTE'CHAR
![41]	if LENGTH(INDEX)=0 then MARKER(INDEX)=BYTE'CHAR
	MARKER(INDEX)[LENGTH(INDEX);1] = BYTE'CHAR		! [41]
	LENGTH(INDEX)=LENGTH(INDEX)+1
	if UCOL>70 then UCOL=1:ROW=ROW+1
	if ROW=24 then ROW=21
	? tab(20,1)
	? tab(ROW,UCOL);"Byte# ";LENGTH(INDEX);"  ";BYTE'VAR
	UCOL=UCOL+20
	if LENGTH(INDEX)<12 goto ST'UNF'MARK
ND'UNF'MARK:
	? tab(24,5);"ANY CHANGE?";tab(-1,9);
	ROW=24	
	UCOL=COL
	COL=25
	call NO
	COL=UCOL
	if INXCTL=1 goto UNFORM'MARK
	INF'OPCD=1
	return

GET'BYTE:
	NULLFLAG = 0				! [41]
	xcall INFLD,20,55,3,0,"#]RE/",BYTE'VAR$,INXCTL,1,INF'OPCD,EXITCODE,TIMER,CMDFLG,&
          DEFPT,MAXPT,FUNMAP,SETDEF,INFCLR                      ! [21]

	if EXITCODE=11 call TIME'OUT : goto GET'BYTE
	if val(BYTE'VAR$)=256 then NULLFLAG = 1	! [41]
	BYTE'VAR=val(BYTE'VAR$)

	return

!
!routine for getting not in use markers for other types of niu-recs
!than deleted and eof
!
OTHERS:
	INDEX=2
   ST'OTHERS:
	INDEX=INDEX+1
	for ROW=20 to 23
		? tab(ROW,1);TAB(-1,9)
	next ROW
    CNG'OTHERS:
	ROW=20
	INF'OPCD=0
	? tab(ROW,5);tab(-1,9);"Enter Marker Position: "
	call GET'FLOAT	
	if FLOAT'VAR<1 goto ND'OTHERS	
	POSITION(INDEX)=FLOAT'VAR
	call ASCII
	if ASC'SWITCH<>1 goto OTH'BYTES
	ROW=21
	? tab(ROW,5);"Enter Marker String:"
	call GET'STRING
	MARKER(INDEX)=STRING'VAR
	goto ST'OTHERS
    OTH'BYTES:
	call UNFORM'MARK
	goto ST'OTHERS
ND'OTHERS:
	? tab(24,5);"ANY CHANGE?";tab(-1,9);
	ROW=24	
	UCOL=COL
	COL=25
	call NO
	COL=UCOL
	if INXCTL=1 then ? tab(24,30);"Start Over With Other Markers."; : goto OTHERS
	INF'OPCD=1	
	return
!
!error trapping routine
!
TRAP:
	CMDFLG = 0			! [29] turn off cmd file processing
	if ERR(0)=1 then goto ENDIT

!	TIMER = 0		! Disable time-out to allow them to
				! study the error message.

	? tab(23,1);tab(-1,10);"----------------------------------------------------------------------------"
	? tab(23,1);
	if err(2)#0 ? "File #";err(2);"- ";			! [29]
	if ERR(0)=12 then ? "Subroutine (INFLD-BASORT-GTLANG-LOGRIO-INMEMO) not found" :&
               resume ENDIT
	if ERR(0)=16 or ERR(0)=32 then &
		? "File specification error"; : resume ENDIT
	if ERR(0)=18 then ? "Device not ready"; : resume ENDIT
	if ERR(0)=22 then ? "Illegal user code - PPN not found!"; : resume ENDIT
	if ERR(0)=23 then ? "Protection violation!"; : resume ENDIT
	if ERR(0)=24 then ? "Cannot write - disk write protected!"; : resume ENDIT
	if ERR(0)=26 then ? "Device does not exist!"; : resume ENDIT
	if ERR(0)=27 then ? "Bitmap kaput - see system operator!"; : goto ENDIT
	if ERR(0)=28 then ? "Disk not mounted!"; : goto ENDIT
	if ERR(0)=36 then ? "Improper version of INFLD.SBR"; : goto ENDIT
	if ERR(0)=37 then ? "Memo or Primary file is in use!" : goto ENDIT ![29]
	? "BASIC ERROR #";ERR(0);" LINE #";ERR(1); ![41]
	goto ENDIT

	on error goto 
	END

IO'ERR:								! [24]-
	? tab(23,1);tab(-1,9);tab(-1,12);
	if IO'STATUS=65535 &
	  ? "LOGRIO.SBR must be loaded in memory for decent performance" &
	else &
	  ? "File error #";str(IO'STATUS);" during record i/o in LOGRIO.SBR" :&
	  ? " CH: ";IO'CH;" RECNO: ";CUR'PRI'BLK; "(";REC'NO;"), MAX: ";MAX'PRI'REC  ! [41]
	goto ENDIT						! -[24]

TIME'OUT:
	? tab(24,48);tab(-1,9);tab(-1,12);"Enter Time-Out Password: ";
	xcall INFLD,24,74,6,0,"A^]E",PW2$,INXCTL,1,NODEF,EXITCODE,TIMER,CMDFLG,&
          DEFPT,MAXPT,FUNMAP,SETDEF,INFCLR                      ! [21]

	if INXCTL=2 or PW2$ <>PW$ goto ENDIT
	? tab(24,48);tab(-1,9);
	return

!
!This is the guts of this program. Here we read a primary record, and chase
!down all the links in its attached memo records, keeping track of used
!links in BIT'MAP and counting, describing or fixing errors as appropriate.
!

MAIN:
	?
	? "Scanning Primary File";
	PRI'CNT=0
	REC'NO=0
	if CTL'SWITCH=1 then REC'NO=1 :BIT'MAP(1)=1
	REC'CNT=REC'NO-1
	BLK'FCTR=int(1024/REC'SIZE)				! [42]
	if IO'MODE>=1 then BLK'FCTR = 1				! [24][41]
	CUR'PRI'BLK=1	
	EOF'FLAG=0	

9010	if IO'MODE=0 then &
		read #22,BLOCK : goto ST'MAIN			! [24]

9011	MAX'PRI'REC = IP'MAXREC					! [28]

!	if IO'MODE = 2 then &
!	    get'next #IO'CH, BLOCK2048 : goto ST'MAIN		! [41]

!	    xcall CISAM, 19, IO'CH, ISMODE, BLOCK2048, FSTAT : &
!	    goto ST'MAIN			! [41]
	    
9013	xcall LOGRIO,IO'READ,IO'CH,BLOCK2048,CUR'PRI'BLK,IO'STATUS	! [24][41]
9014	if IO'STATUS#0 goto IO'ERR				! [24]

ST'MAIN:
9015	LNKNUM = LNKNUM + 1					! [33]
	if LNKNUM > 1 and LNKNUM <= LNKOCC then &
		goto GET'LINK			&
	else					&
		LNKNUM = 1      				! [33]

	REC'NO=REC'NO+1

	REC'CNT=REC'CNT+1
	INDEX=0
	J=J+1
![33]	if J>=DOTS then J=0 : ? ".";
							! [33] show rec #
	if J>=DOTS then 			&
		J=0 : 				&
		? REC'NO using "######"; :	&
		? tab(-1,5);tab(-1,5);tab(-1,5);tab(-1,5);tab(-1,5);tab(-1,5);

     	MEMO'REC(1)=ZERO
	LINK'PTR(1)=0				![1]
	LAST'MEM'REC=0				![19]

	if REC'NO>MAX'PRI'REC  goto FND'LST'MEM

	if REC'CNT<BLK'FCTR goto CK'ISAM
	CUR'PRI'BLK=CUR'PRI'BLK+1
	REC'CNT=0
9020	if IO'MODE=0 then &
		read #22,BLOCK : goto CK'ISAM			! [24]

        if erf(17) # -1 then &
            ? #17, CUR'PRI'BLK;BLOCK2048[4;22]; " : "; &
                ASC(BLOCK2048[2,2])+ASC(BLOCK2048[3,3])*256     ! [44]

9017 	xcall LOGRIO,IO'READ,IO'CH,BLOCK2048,CUR'PRI'BLK,IO'STATUS	! [24][41]
9018	if IO'STATUS#0 goto IO'ERR					! [24]

   CK'ISAM:								! [6]
9019	IF ISAM'SWITCH=0  goto CK'PRI'REC			! [6]

	! [41] for CISAM, del flag is 1 byte past end of rec
	if ISAM'VER = 2 then &
	    if asc(BLOCK2048[REC'SIZE+1;1]) = 0 &
		goto ST'MAIN &
	    else &
		goto CK'PRI'REC


		! [6] Check if the rec about to be read is in the ISAM
		! [6] free list, and if so, skip it.

	if ISAM'SWITCH>0 goto CK2

9021		input #77, ISAM'FREE 
9022		if eof(77)=1 then ISAM'FREE=999999999			! [6]
		ISAM'SWITCH=1						! [6]

   CK2:									! [6]
	if REC'NO=ISAM'FREE then &
		ISAM'SWITCH=-1 : goto ST'MAIN				! [6]

	if REC'NO>ISAM'FREE then ISAM'SWITCH=-1		! [6] shouldn't happen!	

   CK'PRI'REC:
	INDEX=INDEX+1

	if INDEX>8 then goto GET'LINK
	if INDEX<=2 and POSITION(INDEX)=0 goto CK'PRI'REC  ! [4] no eof marker
	if POSITION(INDEX)<1 goto GET'LINK
	if MARKER(INDEX)[1,LENGTH(INDEX)]<>BLOCK2048[((REC'CNT*OFFSET)+POSITION(INDEX));LENGTH(INDEX)] goto CK'PRI'REC
	if INDEX=1 then EOF'FLAG=1 else call CK'EOF
	LNKNUM = 0					! [33]	
	goto ST'MAIN
   GET'LINK:	
	call CK'EOF					! [32] (next line)
	LINKX=BLOCK2048[((REC'CNT*OFFSET)+LINK'POS+((LNKNUM-1)*BYTE'FLAG));BYTE'FLAG]

!if LINK > 0 then 						&
!? "REC'NO :";REC'NO;" LINK: ";LINK;" POS: "; 			&
!	((REC'CNT*OFFSET)+LINK'POS+((LNKNUM-1)*BYTE'FLAG)); :	&
!stop

! [42] output memos with links
if OUT'SWITCH=1 and LINK>0 and erf(17) # -1 then &
    call GET'KEY : ? #17, KEY'STR;" links to #";LINK        ! [44]

	if LINK>MAX'MEM'REC then goto PRI'LINK'ERR	
	CUR'MEM'REC=LINK

   MEMO'CHAIN:
	if CUR'MEM'REC=0 goto ST'MAIN
	if LINK'PTR(1)=0 then PRI'CNT=PRI'CNT+1
	CUR'MEM'BLK=int(CUR'MEM'REC/8)
	if CUR'MEM'BLK>BM'SIZE goto BITMAP'OVERFLOW

	REMAINDER=CUR'MEM'REC-(CUR'MEM'BLK*8)
	CUR'MEM'BLK=CUR'MEM'BLK+1
9030	read #44,MEMO'REC(2)	
!	? "[";str(CUR'MEM'REC);"]";

	if BYTE'FLAG=2 then LINK'PTR(2)=LINK'PTR2(2) else &
		LINK'PTRX(2)=LINK'PTR4(2)[3;2]+LINK'PTR4(2)[1;2]

!![17]	BYTE'VAR=0
!![17]	for I=1 to 3
!![17]		if asc(MEMO'REC(2)[I;1])<>93 then BYTE'VAR=1
!![17]	next I
      	if MEMO'REC(2)[1,6]<>"]]]]]]" and MEMO'REC(2)[1,6]<>"]]]DEL" then &		
		goto GOOD'LINK				! [17]

!![17]	if BYTE'VAR=1 goto GOOD'LINK

	if LINK'PTR(1)=0 goto PRI'LINK'NIU

	goto MEM'LINK'NIU
   GOOD'LINK:
	TEST=BIT'MAP(CUR'MEM'BLK) and 2^REMAINDER	! is this memo in use?
	if TEST=0 goto GOOD'MEMO			! no - good
	if LINK'PTR(1)=0 goto PRI'CONV			! Prim. link error
	goto MEM'CONV					! memo link error
   GOOD'MEMO:
	BIT'MAP(CUR'MEM'BLK)=BIT'MAP(CUR'MEM'BLK)+ 2^REMAINDER
	MEMO'REC(1)=MEMO'REC(2)	
	LAST'MEM'REC=CUR'MEM'REC			! [19]
	LINK'PTR(1)=LINK'PTR(2)
	if LINK'PTR(1)>MAX'MEM'REC goto MEM'LINK'ERR
	CUR'MEM'REC=LINK'PTR(1)
!	? "GOOD MEMO"
	goto MEMO'CHAIN
!
!find the last non-eof memo record
!
FND'LST'MEM:
	?
	? "Scanning Memo File";
	MDOTS = int(MAX'MEM'REC/50) max 1
	for CUR'MEM'REC=MAX'MEM'REC to 1 step -1
		J=J+1
		if J>=MDOTS then J=0 : ? ".";
9040		read #44,DEL'REC(1)		
		CUR'MEM'BLK=int(CUR'MEM'REC/8)
		if CUR'MEM'BLK>BM'SIZE goto BITMAP'OVERFLOW
		REMAINDER=CUR'MEM'REC-(CUR'MEM'BLK*8)
		CUR'MEM'BLK=CUR'MEM'BLK+1
		if DEL'MARKER(1)="]]]]]]" then goto FND'EOF'REC
		LST'MEM'REC=CUR'MEM'REC	   ! LST'MEM'REC is last non-eof rec
		CUR'MEM'REC=1
		goto NXT'FND'LST'MEM
FND'EOF'REC:	
		BIT'MAP(CUR'MEM'BLK)=BIT'MAP(CUR'MEM'BLK)or 2^REMAINDER
NXT'FND'LST'MEM:	
	next CUR'MEM'REC
!
!
!
!
!check the deleted memo chain
!
!
   CK'MEM'DELS:
	?
	? "Checking Deleted Memos";
	DEL'REC(1)=ZERO
	NXT'DEL(1)=0
	CUR'MEM'REC=1
9050	read #44,CTL'REC
	if BYTE'FLAG=2 then FST'FRE=FST'FRE2 else &
		FST'FREX=FST'FRE4[3;2]+FST'FRE4[1;2]
	if FST'FRE=LST'MEM'REC+1 or (FST'FRE=0 and LST'MEM'REC=MAX'MEM'REC) &
		then goto OK'EOF'LINK				! [13]
	CNT'ERR(15)=CNT'ERR(15)+1	
	if FIXIT(15)<1 and CNT'ERR(7)<1 goto OK'EOF'LINK	! [13]
	FST'FRE=LST'MEM'REC+1 
	if FST'FRE>MAX'MEM'REC then FST'FRE=0			! [13]
	if BYTE'FLAG=2 then FST'FRE2=FST'FRE else &
		FST'FRE4=FST'FREX[3;2]+FST'FREX[1,2]
	write #44,CTL'REC
   OK'EOF'LINK:
	if CTL'MARKER<>"]]]DEL" call BAD'CTL		! [18] was 'goto'
	if BYTE'FLAG=2 then FST'DEL=FST'DEL2 else &
		FST'DELX=FST'DEL4X[3;2]+FST'DEL4X[1;2]
	if FST'DEL>MAX'MEM'REC goto CTL'LINK'ERR
	SAVE'MEM'REC = 1			! [18] last good rec
	CUR'MEM'REC=FST'DEL
     ST'CK'DELS:
	if CUR'MEM'REC=0 then goto ND'CK'DELS
	if CUR'MEM'REC>MAX'MEM'REC goto DEL'LINK'ERR
	CUR'MEM'BLK=int(CUR'MEM'REC/8)
	if CUR'MEM'BLK>BM'SIZE goto BITMAP'OVERFLOW
	REMAINDER=CUR'MEM'REC-(CUR'MEM'BLK*8)
	CUR'MEM'BLK=CUR'MEM'BLK+1
	J=J+1 : if J>(MDOTS/10) then ? "."; : J = 0
9060	read #44,DEL'REC(2)
	if BYTE'FLAG=2 then NXT'DEL(2)=NXT'DEL2(2) else &
		NXT'DELX(2)=NXT'DEL4X(2)[3;2]+NXT'DEL4X(2)[1;2]
	if DEL'MARKER(2)="]]]DEL" goto GD'DEL'LINK
	if NXT'DEL(1)=0 goto CTL'LINK'IU
	goto DEL'LINK'IU
     GD'DEL'LINK:
	TEST=BIT'MAP(CUR'MEM'BLK) and (2^REMAINDER)
	if TEST=0 goto GOOD'DEL
	if NXT'DEL(1)=0 goto CTL'LOOP
	goto DEL'LOOP
     GOOD'DEL:
	BIT'MAP(CUR'MEM'BLK)=BIT'MAP(CUR'MEM'BLK)+(2^REMAINDER)
	DEL'REC(1)=DEL'REC(2)
	NXT'DEL(1)=NXT'DEL(2)
	CUR'MEM'REC=NXT'DEL(1)
	SAVE'MEM'REC = CUR'MEM'REC	! [18] last good rec
	goto ST'CK'DELS
     ND'CK'DELS:
!
!Count the No of Unaccounted for Records.
!
   CNT'UN'ACC'RECS:	
	?
	? "Checking Unaccounted-For Records";
	CNT'UN'RECS=0
	for I=2 to LST'MEM'REC
		CUR'MEM'BLK=int(I/8)
		if CUR'MEM'BLK>BM'SIZE goto BITMAP'OVERFLOW
		REMAINDER=I-(CUR'MEM'BLK*8)
		CUR'MEM'BLK=CUR'MEM'BLK+1
		J=J+1
		if J>MDOTS then J=0  : ? ".";
		REC'CNT=BIT'MAP(CUR'MEM'BLK) and (2^REMAINDER)
		if REC'CNT=0 then REC'CNT=1 else REC'CNT=0
		CNT'UN'RECS=CNT'UN'RECS+REC'CNT
	
	next I
!
!
!
!end of main routine
!
!
   ND'MAIN:	
	return
!
!
!
!rebuild the deleted chain using answer to "incorporate unexplained recs"
!question
!
BLD'DEL'CHN:
	CUR'MEM'REC=1
9070	read #44,CTL'REC
	FST'DEL=0
	for CUR'MEM'REC=LST'MEM'REC to 2 step -1
9080		read #44,DEL'REC(1)	
		if DEL'MARKER(1) ="]]]DEL" then &
			call ADD'DEL'REC : &
			goto NXT'DEL'CHN			! [20]
		if DEL'FLAG<>1 then goto NXT'DEL'CHN
		CUR'MEM'BLK=int(CUR'MEM'REC/8)
		REMAINDER=CUR'MEM'REC-(CUR'MEM'BLK*8)
		CUR'MEM'BLK=CUR'MEM'BLK+1
	!	? "DEL'CH";CUR'MEM'BLK;"     ";(BIT'MAP(CUR'MEM'BLK) and (2^REMAINDER))
		if (BIT'MAP(CUR'MEM'BLK) and (2^REMAINDER)) <1 call ADD'DEL'REC ![20]
NXT'DEL'CHN:
	next CUR'MEM'REC
	CUR'MEM'REC=1
	if BYTE'FLAG=2 then FST'DEL2=FST'DEL else &
		FST'DEL4X=FST'DELX[3;2]+FST'DELX[1;2]
	write #44,CTL'REC
	if DEL'FLAG=1 then CNT'UN'RECS = 0		! [11]
	return						! [20]
!
ADD'DEL'REC:
	DEL'MARKER(1)="]]]DEL"
	NXT'DEL(1)=FST'DEL
	
	FST'DEL=CUR'MEM'REC
	if BYTE'FLAG=2 then NXT'DEL2(1)=NXT'DEL(1) else & 		
		NXT'DEL4X(1)=NXT'DELX(1)[3;2]+NXT'DELX(1)[1;2]		![10]
	write #44,DEL'REC(1)
	DEL'CH'CNT=DEL'CH'CNT+1
!	goto NXT'DEL'CHN
	return
!
!check for and keep count of non-eof primary records after first eof
!primary record
!
  CK'EOF:
	if EOF'FLAG<>1 GOTO ND'CK'EOF
	CNT'ERR(1)=CNT'ERR(1)+1
	if OUT'SWITCH=0 goto ND'CK'EOF
	call GET'KEY
        if erf(17) # -1 then            ! [44]
            ? #17 "%Primary file record (#";str(REC'NO);") ";
            if LNKOCC > 1 ? #17 "(Link #";LNKNUM;") ";		! [35]
	    ? #17 "w/ key: '";KEY'STR;"' has error #1."
        endif
   ND'CK'EOF:
	return
!!
!below are all the error handling and fixing routines
!the general plan for "fixing" errors is to cut bad chains at the last
!good link by inserting a zero link.
!
!
  PRI'LINK'ERR:
	ERR'R=2
!	? "PLE"
	call PRI'LINK
	goto ST'MAIN
!
!
!

  PRI'LINK:
	CNT'ERR(ERR'R)=CNT'ERR(ERR'R)+1
	if OUT'SWITCH=0 goto FIX'PRI'LINK
	call GET'KEY
        if erf(17) # -1 then        ! [44]
            ? #17 "%Primary file rec (#";str(REC'NO);") ";
            if LNKOCC > 1 ? #17 "(Link #";LNKNUM;") ";		! [35]
            ? #17 "w/ key: '";KEY'STR;"' has error #";str(ERR'R);"."
        endif
   FIX'PRI'LINK:
	if FIXIT(ERR'R)<>1 goto ND'PRI'LINK
![34]	BLOCK'POS=(REC'CNT*OFFSET)+LINK'POS
	BLOCK'POS=(REC'CNT*OFFSET)+LINK'POS+((LNKNUM-1)*BYTE'FLAG)   ! [34]

	LINK=0
	PRI'CNT = PRI'CNT - 1					! [41]

	BLOCK2048[BLOCK'POS;BYTE'FLAG] = LINKX[1;BYTE'FLAG]

	if IO'MODE=0 then &
		write #22,BLOCK : goto ND'PRI'LINK		! [24]

!	if IO'MODE=2 then &
!	    UPDATE'RECORD #IO'CH, BLOCK2048[1,REC'SIZE] : goto ND'PRI'LINK2	! [41]

	xcall LOGRIO,IO'WRT,IO'CH,BLOCK2048,CUR'PRI'BLK,IO'STATUS	! [24][41]
	if IO'STATUS#0 goto IO'ERR				! [24]

   ND'PRI'LINK:
	UNLOKR #IO'CH						! [41]
   ND'PRI'LINK2:						! [41]
	return
!
!
  PRI'LINK'NIU:
	ERR'R=3
!	? "PLN"
	call PRI'LINK
	goto ST'MAIN
!
!
  PRI'CONV:
	ERR'R=4
!	? "PLC"
	call PRI'LINK
	goto ST'MAIN
!
  MEM'LINK:
	CNT'ERR(ERR'R)=CNT'ERR(ERR'R)+1
	if OUT'SWITCH=0 goto FIX'MEM'LINK
	call GET'KEY
        if erf(17) # -1 then        ! [44]
            ? #17 "%Primary file record (#";str(REC'NO);") ";	
            if LNKOCC > 1 ? #17 "(Link #";LNKNUM;") ";		! [35]
            ? #17, "w/ key: '";KEY'STR;"' has error #";str(ERR'R);"."
        endif
   FIX'MEM'LINK:
	if FIXIT(ERR'R)<>1 goto ND'MEM'LINK
	if BYTE'FLAG=2 then LINK'PTR2(1)=0: goto WRITE'MEM'LINK		![1][19]
![19]	LINK'PTR(2)=0							![1]
![19]	LINK'PTR4(2)=LINK'PTRX(2)					![1]
	LINK'PTR(1)=0							! [19]
	LINK'PTR4(1)=LINK'PTRX(1)					! [19]

   WRITE'MEM'LINK:	
![19]	write #44 MEMO'REC(2)
	CUR'MEM'REC = LAST'MEM'REC					! [19]
	write #44, MEMO'REC(1)						! [19]

   ND'MEM'LINK:
	return
!
!
!
  MEM'LINK'NIU:
	ERR'R=6
	call MEM'LINK
	goto ST'MAIN
!
!
  MEM'LINK'ERR:
	ERR'R=5
	call MEM'LINK
	goto ST'MAIN
!
!
  MEM'CONV:
	ERR'R=7
	call MEM'LINK
	goto ST'MAIN
!
!
  BAD'CTL:
	ERR'R=8
	CNT'ERR(ERR'R)=CNT'ERR(ERR'R)+1
	if OUT'SWITCH=0 goto FIX'BAD'CTL
        if erf(17) # -1 then        ! [44]
            ? #17 "%Memo file control record mismarked (Error #";str(ERR'R);")."
        endif
   FIX'BAD'CTL:
	if FIXIT(ERR'R)<>1 goto ND'BAD'CTL
	CTL'MARKER="]]]DEL"
	write #44 CTL'REC

   ND'BAD'CTL:
	return
!
!
  CTL'LINK:
	CNT'ERR(ERR'R)=CNT'ERR(ERR'R)+1
	if OUT'SWITCH=0 goto FIX'CTL'LINK
        if erf(17) # -1 then        ! [44]
            ? #17 "%Memo file control record mislinked (Error #";str(ERR'R);")."
        endif
   FIX'CTL'LINK:
	if FIXIT(ERR'R)<>1 goto ND'CTL'LINK:
	if BYTE'FLAG=2 then FST'DEL2=0: goto WRITE'CTL'LINK	![1]
	FST'DEL4B=0						![1]
   WRITE'CTL'LINK:
	CUR'MEM'REC = 1             				! [18]
	write #44 CTL'REC

   ND'CTL'LINK:
	return
!
!
   CTL'LINK'ERR:
	ERR'R=9
	call CTL'LINK
	goto ND'CK'DELS
!
!
   CTL'LINK'IU:
	ERR'R=10
	call CTL'LINK
	goto ND'CK'DELS
!
!
   CTL'LOOP:
	ERR'R=11
	call CTL'LINK
	goto ND'CK'DELS
!
!

  DEL'LINK:
	CNT'ERR(ERR'R)=CNT'ERR(ERR'R)+1
	if OUT'SWITCH=0 goto FIX'DEL'LINK
        if erf(17) # -1 then        ! [44]
            ? #17 "%Memo file deleted record mislinked (Error #";str(ERR'R);")."
        endif
   FIX'DEL'LINK:
	if FIXIT(ERR'R)<>1 goto ND'DEL'LINK
	if BYTE'FLAG=2 then NXT'DEL2(1)=0: goto WRITE'DEL'LINK	! [1]
	NXT'DEL4B(1)=0						! [1]
WRITE'DEL'LINK:
	CUR'MEM'REC = SAVE'MEM'REC				! [18]
	write #44 DEL'REC(1)

   ND'DEL'LINK:
	return
!
!
   DEL'LINK'ERR:
	ERR'R=12
	call DEL'LINK
	goto ND'CK'DELS
!
!
   DEL'LINK'IU:
	ERR'R=13
	call DEL'LINK
	goto ND'CK'DELS
!
!
   DEL'LOOP:
	ERR'R=14
	call DEL'LINK
	goto ND'CK'DELS
!
!
PRINT1:
!
!Print info from initial data entry
!
	? #17  tab(5);"ANAMMO - Analyze a Memo File    Copyright (c) 1984    By MicroSabio"
	? #17 "------------------------------------------------------------------------------"	
	? #17 tab(5); "Name of Memo File: ";tab(60);MEMO$
	? #17 tab(5); "Name of Primary File: ";tab(60);PRIMARY$
	? #17 tab(5); "Record Size: ";tab(60);REC'SIZE
	if CTL'SWITCH=1 then STRING'VAR="Y" else STRING'VAR="N"
	? #17 tab(5); "First Record a Control Record? ";tab(60);STRING'VAR
	? #17 tab(5); "Link Position Within Record: "; tab(60);str(LINK'POS)
	? #17 tab(5); "Starting End of File Record Marker Position: "; &
                    tab(60);str(EOF'POS)
	XSTRING'VAR = EOF'MARKX : LENS = LENGTH(1)		![41]	! [29]
	STRING'FMT = ""						![41]
	if LENS>0 call FMT'STRING				![41]
	? #17 tab(5); "End of File Record Marker: ";tab(60);STRING'FMT	! [29]
	? #17 tab(5); "Starting Deleted Record Marker Position: "; &
                    tab(60);str(DEL'POS)

	XSTRING'VAR = DEL'MARKX : LENS = LENGTH(2)		![41]	! [29]
	STRING'FMT = ""						![41]
	if LENS>0 call FMT'STRING				![41]
	? #17 tab(5); "Deleted Record Marker: ";tab(60);STRING'FMT	! [29]
	? #17

	if POSITION(3)=0 goto PRINT1A				! [24]

	? #17 tab(5);"Other Record Not in Use Markers:"
	? #17
	? #17 tab(16);"Position";tab(45);"Marker(if Printable)"
	? #17 

	for I=3 to 8
	    if POSITION(I)<>0 then &
		? #17 tab(20);POSITION(I);TAB(50); :&
		XSTRING'VAR = MARKER(I)		   :&
		LENS = LENGTH(I)		   :&
		? #17, STRING'FMT			! [41]
![41] tab(50);MARKER(I)
	next I

PRINT1A:
	? #17
	? #17
	? #17 "Key Position Within Record:";tab(60);KEY'POS
	? #17 "Length of Key:";tab(60);KEY'SIZE                  ! [24]
	? #17
	? #17
!
				! [24] print legend of errors

	 
	? #17, "Err|     Description"
	? #17, "No.|-------------------------------------------------"
	? #17, " 1 |";ERR'DSC(1)
	? #17, " 2 |";ERR'DSC(2)
	? #17, " 3 |";ERR'DSC(3)
	? #17, " 4 |";ERR'DSC(4)
	? #17, " 5 |";ERR'DSC(5)
	? #17, " 6 |";ERR'DSC(6)
	? #17, " 7 |";ERR'DSC(7)
	? #17, " 8 |";ERR'DSC(8)
	? #17, " 9 |";ERR'DSC(9)
	? #17, "10 |";ERR'DSC(10)
	? #17, "11 |";ERR'DSC(11)
	? #17, "12 |";ERR'DSC(12)
	? #17, "13 |";ERR'DSC(13)
	? #17, "14 |";ERR'DSC(14)
	? #17, "15 |";ERR'DSC(15)
	? #17,"------------------------------------------------------"

	return
!
PRINT2:
!
!Here we print the info from start of second and third screens to output
!file if needed.
!
	? #17
	? #17
	? #17
	? #17 tab(1);"Err|             Description                              |# of |Danger|Fix it?"
	? #17 tab(1);"No.|------------------------------------------------------|Occ.s|To Fix|------"
	? #17 tab(1);" 1 |";ERR'DSC(1); tab(59);"|  ";CNT'ERR(1);tab(65);"|  ?   |  NO"
	? #17 tab(1);" 2 |";ERR'DSC(2); tab(59);"|  ";CNT'ERR(2);tab(65);"| Low  |  ";chr(78+(11*FIXIT(2)))
	? #17 tab(1);" 3 |";ERR'DSC(3); tab(59);"|  ";CNT'ERR(3);tab(65);"| Low  |  ";chr(78+(11*FIXIT(3)))
	? #17 tab(1);" 4 |";ERR'DSC(4); tab(59);"|  ";CNT'ERR(4);tab(65);"| High |  ";chr(78+(11*FIXIT(4)))
	? #17 tab(1);" 5 |";ERR'DSC(5); tab(59);"|  ";CNT'ERR(5);tab(65);"| Low  |  ";chr(78+(11*FIXIT(5)))
	? #17 tab(1);" 6 |";ERR'DSC(6); tab(59);"|  ";CNT'ERR(6);tab(65);"| Low  |  ";chr(78+(11*FIXIT(6)))
	? #17 tab(1);" 7 |";ERR'DSC(7); tab(59);"|  ";CNT'ERR(7);tab(65);"| High |  ";chr(78+(11*FIXIT(7)))
	? #17 tab(1);" 8 |";ERR'DSC(8); tab(59);"|  ";CNT'ERR(8);tab(65);"| Low  |  ";chr(78+(11*FIXIT(8)))
	? #17 tab(1);" 9 |";ERR'DSC(9); tab(59);"|  ";CNT'ERR(9);tab(65);"| Low  |  ";chr(78+(11*FIXIT(9)))
	? #17 tab(1);"10 |";ERR'DSC(10);tab(59);"|  ";CNT'ERR(10);tab(65);"| Low  |  ";chr(78+(11*FIXIT(10)))
	? #17 tab(1);"11 |";ERR'DSC(11);tab(59);"|  ";CNT'ERR(11);tab(65);"| Low  |  ";chr(78+(11*FIXIT(11)))
	? #17 tab(1);"12 |";ERR'DSC(12);tab(59);"|  ";CNT'ERR(12);tab(65);"| Low  |  ";chr(78+(11*FIXIT(12)))
	? #17 tab(1);"13 |";ERR'DSC(13);tab(59);"|  ";CNT'ERR(13);tab(65);"| Low  |  ";chr(78+(11*FIXIT(13)))
	? #17 tab(1);"14 |";ERR'DSC(14);tab(59);"|  ";CNT'ERR(14);tab(65);"| Low  |  ";chr(78+(11*FIXIT(14)))
	? #17 tab(1);"15 |";ERR'DSC(15);tab(59);"|  ";CNT'ERR(15);tab(65);"| Low  |  ";chr(78+(11*FIXIT(15)))
	? #17 tab(1);"------------------------------------------------------------------------------"
	? #17 tab(5);"The Number Of Memo Records Unaccounted For is:";tab(74);CNT'UN'RECS
!
	if DEL'FLAG=1 then STRING'VAR="Y" else STRING'VAR="N"
!
	? #17 tab(5);"Incorporate Unaccounted for Undeleted Records Into New Del Chain?";tab(74);STRING'VAR
	? #17
	? #17
	return
!
!
!
PRINT'END:
!
!Finish printing 
!
	if PRMPT$ = "FIXED"	then &
	  ? #17 tab (5);"Length of New Deleted Chain:";tab (74);DEL'CH'CNT

	? #17 tab (5);"No. of Primary Records With Memos Attached:";tab (74);PRI'CNT
	return

!-----------------------------------------------------------------------

PHDR:					! display standard program 
					! header at (2,1) in AMOSL format

	? tab(2,1);tab(-1,11);"Rev. ";
	? str(VMAJOR);".";str(VMINOR);
	if VSUB<>0 then ? chr(VSUB+96);
	if VEDIT<>0 then ? "(";str(VEDIT);")";
	if VPATCH<>0 then ? "-";str(VPATCH);
	return

!------------------------------------------------------------------------

SCREEN'TITLE:

	? tab(-1,0);
	if BASFG=0 and BASBG=0 then BASFG = 3
	? tab(-2,BASFG);tab(-3,BASBG);
	? "ANAMMO - Analyze a Memo File"
	? tab(-1,11);
	? tab(1,62);"Copyright (c) 1989";
	? tab(2,62);"By MicroSabio"
	call PHDR					! [3]
	return

!------------------------------------------------------------------------

HELP'SVA:	! [24] Entry point to display help with expressed desire
		! [24] to save and restore screen area, and wait for an
                ! [24] ESC before restoring area

	HELP'OPCODE = MMO'BDR + MMO'SVA + MMO'RSA + MMO'DPG
	goto HELP1

HELP:		! [9] Display help information if available using INMEMO.
		! [9] The help text has been pre-typed into HLPMMO.MMO.

	HELP'OPCODE = MMO'BDR

HELP1:
        HELP'SUB$ = HELP'SUB$ + LANG$                            ! [22]
	FILE100 = 0                                              ! [24]

	if HELP'FILE'OPEN=1 goto HELP2
	
	on error goto HELP'TRAP
	
	lookup HELP'DAT$,MAX100
	if abs(MAX100) = 0 goto NO'HELP			! [39] 
	MAX100 = abs(MAX100) * int(512/RECSIZ100)           ! [31]

	lookup HELP'MMO$,MAX101
	if abs(MAX101) = 0 goto NO'HELP			! [39]			
	MAX101 = abs(MAX101) * 8                            ! [31]

	open #100, HELP'DAT$, random'forced, RECSIZ100, FILE100 ! [24]
	open #101, HELP'MMO$, random'forced, 64, FILE101        ! [24]

	VWIDTH = 69						! [24]
	VROWS =  10						! [24]

	on error goto TRAP

	HELP'FILE'OPEN=1

HELP2:
	FILE100 = FILE100 + 1	
	if FILE100>MAX100 goto NO'HELP2

9090	read #100, HELP'REC
	if HELP'DESCR[1,3]="]]]" goto NO'HELP2
	if HELP'DESCR<>HELP'SUB$ goto HELP2

	xcall INMEMO,HELP'OPCODE,HELP'SUB$[8,-1],101,HELP'ROW,2,HELP'ROW+6,70, &
		HELP'LINK,HPOS,VSPEC,MMOCLR,EXTCTL         ! [21]
	
        if instr(1,HELP'SUB$,"ANAMMO-INTRO")>0 or &
           instr(1,HELP'SUB$,"ANAMMO-PASSWORD")>0 then goto HELP'EXIT ! [22]

!	? tab(24,50);tab(-1,32);"Hit RETURN to continue ";tab(-1,33);
!	xcall INFLD,24,76,1,0,"CI\",A$,INXCTL,1,NODEF,EXITCODE,TIMER,CMDFLG,&
!         DEFPT,MAXPT,FUNMAP,SETDEF,INFCLR                      ! [21]
!
!		! [25] if we asked to save memo area, try to restore now
!	
!	if (HELP'OPCODE and MMO'SVA) > 0 and EXTERR=MME'OK then &
!	  xcall INMEMO,MMO'RSA,HELP'SUB$[8,-1],101,HELP'ROW,2,HELP'ROW+6,70, &
!		HELP'LINK,HPOS,VSPEC,MMOCLR,EXTCTL &
!	else &
	if (EXTERR # MME'OK) then &
		? tab(HELP'ROW-1,1);tab(-1,10);			! [25]

HELP'EXIT:
	return

NO'HELP:
	? tab(23,1);tab(-1,9);"Sorry, help files '";HELP'MMO$;"' (or .DAT) not found   (Hit RETURN): ";
![29]	xcall INFLD,24,77,1,0,"@\",A$,INXCTL,1,NODEF,EXITCODE,TIMER,CMDFLG,&
![29]     DEFPT,MAXPT,FUNMAP,SETDEF,INFCLR                      ! [21]

	return
NO'HELP2:
	if NO'HELP'FLAG=1 return				! [112]
	? tab(22,1);tab(-1,9);"Help memo '";HELP'SUB$;"' not found. (hit RETURN):";
![29]	xcall INFLD,24,77,1,0,"@\",A$,INXCTL,1
	return

HELP'TRAP:		! [112] trap filespec errors caused by ersatz account
			! [112] 'INMEMO:' not being defined.  

	if err(0)<>16 goto TRAP		! not the error we expected!
	
	NO'HELP'FLAG=1
	resume NO'HELP

!-----------------------------------------------------------------------
! FMT'STRING [29] Convert STRING'VAR to printable STRING'FMT.  This is 
! useful for printing the DEL and EOF markers which could consist of
! printable or non-printable characters.
! [41] input: XSTRING'VAR, LENS
!-----------------------------------------------------------------------

FMT'STRING:                             ! [29]-
	STRING'FMT = ""
	for FI = 1 to LENS		! [41]
	    if asc(XSTRING'VAR[FI;1])>31 then &
		STRING'FMT = STRING'FMT+XSTRING'VAR[FI;1] &
	    else &
		STRING'FMT = STRING'FMT+"<"+str(asc(XSTRING'VAR[FI;1]))+">"
	next FI
	return

