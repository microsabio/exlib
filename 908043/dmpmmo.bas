!	DMPMMO.BAS
!
!	Copyright (c) 1984 by MicroSabio
!	Written by Jack McGregor
!-------------------------------------------------------------------------
!	Dump an INMEMO memo pad in logical memo format to either the
!	screen or a file.
!-------------------------------------------------------------------------
! USAGE:
!	.DMPMMO		(via DMPMMO.CMD:)
!
!					LOAD BAS:INFLD.SBR
!					RUN INMEMO:DMPMMO
!-------------------------------------------------------------------------
! EDIT HISTORY:
!	1.  06-29-84  Created /jdm
!	2.  11-26-84  Search Option /jdm
!	3.  05-21-85  Clarify that 'In Use' includes deleted recs /jdm
!	4.  04-14-86  Fix in-use count if all in use (not -1) /jdm
!	5.  04-21-86  [1.2] Use INFLD instead of INFLDX /jdm
!	6.  07-07-86  [1.3] Protect against illegal rec errors /jdm
!	7.  09-23-86  [1.4] Protect against subscript errors, 
!				read filename from command file /jdm
!	8.  11-18-86  [1.4a] Fix bug in displaying record #'s /jdm
!	9.  12-12-88  [1.4b] Fix illegal file spec under some op sys /jdm
!      10.  11-13-89  [2.0]  Support INMEMO 2.0 files (8 bit) /jdm
!      11.  11-13-89  [2.0]  Open file shared; wait record /jdm
!      12.  11-13-89  [2.0]  More info at end of dump /jdm
!      13.  11-22-89  [2.0]  Add colors; use include files; 
!				filebased help /jdm
!      14.  12-29-89  [2.0]  Move all POS vars to even boundary /jdm
! Version 2.1---
!      15.  09-20-90  [2.1]  Minor adjustments for non-AMOS compatibility /jdm
! Version 2.2---
!      16.  10-10-90  [2.2]  Support invisible headers /jdm
! Version 2.2a---
!      17.  02-20-92  [2.2a] Further adjustments for non-amos /gb
!      18.  07-22-19  [2.2b] Increase bitmap size /jdm
!-------------------------------------------------------------------------
! ALGORITHM NOTES:
!	1.  Ask for the name of the memo file to dump.  Lookup the 
!	    file and check which format it is.
!	2.  Create a bitmap (in memory) where each bit corresponds to a
!	    physical memo record.	
!	3.  Read the memo file and for every pointer to a physical memo
!	    record, set that record's corresponding bit.  When done,
!	    the bits corresponding to the records which are not pointed
!	    to will be zero and the others will be one.
!	4.  For every bit that is zero, scan the corresponding memo for
!	    the search pattern (if any).  If there is a match, read the
!	    corresponding logical memo into memory (as in INMEMO), 	
!	    expand compressed spaces and write out to screen or file.
!-------------------------------------------------------------------------
! [10] - [2.0 Format Notes]
! Memo files written with INMEMO 1.X use byte values 129-255 to represent
! 1-127 contiguous spaces.  INMEMO 2.X uses a lead-in byte (2) followed
! by a byte whose value is the number of spaces.  Since we aim to maintain
! the ability to read either type of file, we distinguish one format from
! the other by the use of a flag byte, (1), which is written to 2.x files
! prior to any true 8-bit values.  If a byte 129-255 is read and the (1)
! flag has not been received, then we treat it as a 1.x-style compressed
! space; otherwise we treat it as a normal 8-bit value.
!-------------------------------------------------------------------------

	PROGRAM DMPMMO,2.2b(18)

MAP1 PHDR
	MAP2 VMAJOR,B,1,2
	MAP2 VMINOR,B,1,2
	MAP2 VSUB,B,1,0
	MAP2 VEDIT,B,1,16
	MAP2 VPATCH,B,1,0



MAP1 MAX'MEMOS,F,6,1310720	! [8] Must be 8 times size of BITMAP() [22]
MAP1 BITMAP(163840),B,1		! enough for 650K bits (expand if necessary) [18]
MAP1 BUFFER,S,10000		! maximum logical memo size (change if you
				! have larger ones)

MAP1 MREC2			! 2-byte memo record layout
	MAP2 MPTR2,B,2
	MAP2 MTXT2,X,62
MAP1 DREC2,@MREC2		! 2-byte deleted record format
	MAP2 DMARK2,S,6	
	MAP2 DNXT2,B,2
	MAP2 DPTR2,B,2
	MAP2 DJNK2,X,54
MAP1 MREC4,@MREC2		! 4-byte memo record layout
	MAP2 MPTR4,B,4
	MAP2 MTXT4,X,60
MAP1 DREC4,@MREC2		! 4-byte deleted record format
	MAP2 DMARK4,S,6
	MAP2 DNXT4,B,4
	MAP2 DPTR4,B,4
	MAP2 DFLAG4,S,1
	MAP2 DJNK4,X,49
MAP1 BSWAP,B,4
MAP1 BSWPX,@BSWAP
	MAP2 BS1,B,2
	MAP2 BS2,B,2

MAP1 FILE'PARAMS
	MAP2 MFILE$,S,28
	MAP2 OFILE$,S,28
	MAP2 FILE1,F
	MAP2 SAVE'FILE1,F
	MAP2 MAXREC1,F
	MAP2 CH,F

MAP1 MISC
	MAP2 BX2,B,2
	MAP2 B1,B,1
	MAP2 B2,B,1
	MAP2 BITMAP'FLAG,F
	MAP2 DC,F
	MAP2 DX,F
	MAP2 RS,F
	MAP2 ER'FLAG,F
	MAP2 CB'FLAG,F
	MAP2 RECSTX,S,2100
	MAP2 RECSTR(30),S,70,@RECSTX
	MAP2 MAXRS,F,6,30
	MAP2 LINK2,B,2
	MAP2 LINK4,B,4
	MAP2 POS2,B,4
	MAP2 XPOS4,X,6
	MAP2 POS4,B,4,@XPOS4
	MAP2 SPC'CNT,F				! [10] space count
	MAP2 ISO'FLAG,F				! [10] =1 if 8 bit memo fmt
	MAP2 CS'FLAG,F				! [10] =1 on compressed spc
	MAP2 X,F
	MAP2 Q,F
	MAP2 R,F
	MAP2 A,F
	MAP2 I,F
	MAP2 IH'FLAG,F
	MAP2 J,F
	MAP2 A$,S,10
	MAP2 MB,F
	MAP2 ROW,F
	MAP2 MMOFMT,F			! Link size (2 or 4)
	MAP2 MMOVER,F			! 1=1.x, 2=2.x
     	MAP2 IN'USE,F
	MAP2 LABEL$,S,32
	MAP2 SLP,F
	MAP2 SRCH'CNT,F			! [12] # recs searched
	MAP2 DSP'CNT,F			! [12] # recs displayed
	MAP2 OLD'CNT,F			! [12] # 1.x format memos (old)
	MAP2 NEW'CNT,F			! [12] # 2.x format memos (new)

MAP1 HELP'REC				! [13] MMOHLP.DAT record
	MAP2 HELP'DESCR,S,30		! [13] (primitive example of a help
	MAP2 HELP'LINK,B,2		! [13] memo index file)

MAP1 MEMO'HELP				! Misc vars for help system
	MAP2 RECSIZ100,F,6,32			! [13] MMOHLP.DAT recsize
	MAP2 RECSIZ101,F,6,64
	MAP2 FILE100,F,6
	MAP2 FILE101,F,6
	MAP2 MAX100,F,6
	MAP2 MAX101,F,6
	MAP2 HELP'DAT$,S,24,"INMEMO:HLPMMO.DAT"	! [13] HLPMMO.DAT filespec
	MAP2 HELP'MMO$,S,24,"INMEMO:HLPMMO.MMO"	! [13] HLPMMO.MMO filespec
	MAP2 HLINK,B,2				! [13]
	MAP2 HELP'SUB$,S,40			! [13]
	MAP2 HELP$,S,2				! [13]
	MAP2 HPOS,X,4				! [13]
	MAP2 HDCH,F,6,100			! [13] help idx file channel
	MAP2 HMCH,F,6,101			! [13] help menu file channel
	MAP2 HELP'FLAG,F
	MAP2 HELP'SROW,B,2,17
	MAP2 HELP'SCOL,B,2,2
	MAP2 HELP'EROW,B,2,22
	MAP2 HELP'ECOL,B,2,72
	MAP2 NO'HELP'FLAG,F,6			! set if no help file
	MAP2 HELP'FILE'OPEN,F

MAP1 HVSPEC						! INMEMO support
	MAP2 HVWIDTH,B,2,65 				! (max memo size is
	MAP2 HVROWS,B,2,50				!  65 cols, 50 rows)


MAP1 SEARCH'PARAMS			! default wildcard symbols
	MAP2 S'1WILD$,S,1,"%"		! (avoid conflict with ?:help)
	MAP2 S'AST$,S,1,"*"		! * wildcard
	MAP2 S'AND$,S,1,"&"		! logical AND
	MAP2 S'OR$,S,1,"!"		! logical OR
	MAP2 S'ALP$,S,1,"@"		! 1 alpha character
	MAP2 S'NUM$,S,1,"#"		! 1 numeric character
	MAP2 S'LIMIT$,S,1,"|"		! delimiter 
	MAP2 S'FOLD$,S,1,"Y"		! searchfold

++include mmosym.bsi		! [13] symbols
++include mmopar.bsi		! [13] inmemo mappings
++include infpar.bsi             ! [13] infld maps
!-------------------------------------------------------------------------

	on error goto TRAP
	filebase 1

! ++include getclr.bsi                  ! [13] setup colors

START:
	? tab(-1,0);"DMPMMO - Logical Memo File Dump"
	? tab(-1,11);
	? tab(1,62);"Copyright (c) 1984";
	? tab(2,62);"By MicroSabio"

	call PHDR
GET'FILE:	
	? tab(6,5);tab(-1,9);tab(8,5);tab(-1,9);
	? tab(4,5);"Enter name of memo file to dump:";
	TIMER=60
	xcall INFLD,4,55,24,1,"A^E]",MFILE$,INXCTL,1,0,EXITCODE,TIMER, &
		CMDFLG,MAXPT,DEFPT,FUNMAP,SETDEF,INFCLR,HLPIDX
	? tab(24,1);tab(-1,9);
	if INXCTL=2 goto ABORT
	if EXITCODE=11 goto TIME'OUT
	X=instr(1,MFILE$,".")
	if X>0 goto LOOK

		X=instr(1,MFILE$,"[")
		if X<1 then &
			MFILE$=MFILE$+".MMO" &
		else &
			MFILE$=MFILE$[1,X-1]+".MMO"+MFILE$[X,-1]

		? tab(4,55);MFILE$
LOOK:
	lookup MFILE$,X    ! [7]
	if abs(X)=0 then &	
		? tab(24,1);"Error: ";MFILE$;" not found!"; : goto GET'FILE
	MAXREC1 = abs(X) * 8                                    ! [15]

OPEN:
	LABEL$="OPEN"						! [6]
	open #1, MFILE$, random'forced, 64, FILE1		! [11]
	FILE1 = 1
	read #1, DREC2

	if DFLAG4="4" then &
		MMOFMT = 4 : &
		MPTR4 = DNXT4 : &
		call BSWAP : &
		IN'USE = MPTR4 - 1 &
	else &
		MMOFMT = 2 : &
		IN'USE = DNXT2 - 1	

	if IN'USE=-1 then IN'USE = MAXREC1-1			! [4]
	IN'USE = IN'USE min (MAXREC1-1)				! [6]

STATS:
	? tab(5,1);tab(-1,10);
	? tab(6,5);tab(-1,11);"Statistics for ";tab(-1,12);MFILE$;tab(-1,11);
	? tab(7,10); "Link Format:"; 
	? tab(8,10);"Maximum Size:";
	? tab(9,10);"# Records in Use:";tab(9,40);"(Includes deleted records)"
	? tab(-1,12);
	? tab(7,30);str(MMOFMT);" byte";
	? tab(8,30);str(MAXREC1-1);" records"
	? tab(9,30);str(IN'USE);

	if IN'USE<MAX'MEMOS goto DESTINATION			! [6]
	? tab(21,1);tab(-1,10);tab(-1,12);chr(7)
	? "Memo file contains more records than this program's BITMAP allows."
	? "VUE the program (DMPMMO.BAS) and expand BITMAP() and MAX'MEMOS accordingly."
	END

DESTINATION:

	? tab(-1,11);
	? tab(11,5);"Enter name of output file (or CR for terminal): ";
	xcall INFLD,11,55,10,0,"A23",OFILE$,INXCTL,1,0,EXITCODE,TIMER,&
		CMDFLG,MAXPT,DEFPT,FUNMAP,SETDEF,INFCLR,HLPIDX

	if EXITCODE=11 goto TIME'OUT
	if EXITCODE>0 close #1 : goto GET'FILE

	call SEARCH
	if EXITCODE=1 close #1 : goto GET'FILE

	if OFILE$="" then CH = 0 : OFILE$="TTY.LST" else CH = 2
	open #CH, OFILE$, output

	? tab(17,1);
	? #CH
	? #CH
	? tab(-1,11);tab(17,1);
	? #CH, "Logical dump of INMEMO pad ";
	? tab(-1,12);
	? #CH, MFILE$
	? #CH
	if CH<>0 then ? : ?
	? tab(-1,11);
	if BITMAP'FLAG=1 goto PHASE2

	? "Phase 1 - building bitmap";

	DC = int(IN'USE/45)
	DX = 0
	FILE1 = 0

READ:						! read memo file and build
						! bitmap
	LABEL$="READ"				! [6]
	BITMAP'FLAG = 1
	for FILE1 = 1 to IN'USE
		DX = DX + 1	
		if DX>=DC then ? "."; : DX = 0

		read #1, MREC2
	
		if DMARK2="]]]DEL" or DMARK2="]]]]]]" goto NF1

			if MMOFMT=2 and MPTR2<>0 call MARK'BIT : goto NF1

			if MPTR4=0 goto NF1

				call BSWAP
				call MARK'BIT
NF1:	
	next FILE1


PHASE2:						! transfer logical memos
	                       			! to output file, converting
						! format in the process.  Use
    						! bitmap to identify the start
						! of each logical memo.

	LABEL$ = "PHASE2"			! [6]
	if CH<>0 then ? : ? "Phase 2 - dumping memos"

	DX = 0

	for FILE1 = 2 to IN'USE

		DX = DX + 1	
		if DX>=DC and CH<>0 then ? "."; : DX = 0

		call CHECK'BIT
		if CB'FLAG<>0 goto NP2

!		read #1, MREC2
!		if DMARK2="]]]DEL" or DMARK2="]]]]]]" goto NP2

		SRCH'CNT = SRCH'CNT + 1				! [12]

		if MMOFMT=4 goto P2F4

			if PATTERN[11,-1]="" goto NPREAD	! [6]
			LINK2 = FILE1		! see if pattern in memo
			xcall INMEMO,MMO'SCH,PATTERN,1,0,0,0,0,LINK2,POS2, &
                            HVSPEC,MMOCLR,EXTCTL

			if POS2=0 goto NP2 else goto NPREAD

P2F4:
			if PATTERN[11,-1]="" goto NPREAD	! [6]
			LINK4 = FILE1
			xcall INMEMO,MMO'SCH,PATTERN,1,0,0,0,0,LINK4,XPOS4
			if POS4=0 goto NP2 

NPREAD:
			SAVE'FILE1 = FILE1
			call READ'IN
			FILE1 = SAVE'FILE1
			if DMARK2="]]]DEL" or DMARK2="]]]]]]" goto NP2
			call DISPLAY
NP2:
	next FILE1

ENDIT:
	? #CH
	? #CH
	if SRCH'CNT#DSP'CNT then ? #CH;str(SRCH'CNT);" logical memos searched."
	? #CH;str(DSP'CNT);" logical memos dumped; ";
	? #CH;str(OLD'CNT);" old (1.x) & ";
	? #CH;str(NEW'CNT);" new (2.x) format."
	
	if CH<>0 then &
	    ? : ? : &
	    if SRCH'CNT#DSP'CNT then ? str(SRCH'CNT);" logical memos searched." :&
	    ? str(DSP'CNT);" logical memos dumped."

	close #CH
	DSP'CNT = 0
	SRCH'CNT = 0				! [12]
	OLD'CNT = 0				! [12]
	NEW'CNT = 0				! [12]

	if ER'FLAG<>0 goto END
		? 
		? tab(24,1);tab(-1,9)"Memo dump complete."; 
		? tab(24,38);"Do Another Dump/Search on this File?"; 
		xcall INFLD,24,76,1,0,"YNF",A$,INXCTL,1,0,EXITCODE,TIMER,&
			CMDFLG,MAXPT,DEFPT,FUNMAP,SETDEF,INFCLR,HLPIDX
		
		if A$="Y" goto STATS else ? tab(-1,0);
END:
	END

TRAP:
	ER'FLAG=1

	if ERR(0)=1 then goto ENDIT

	if err(0) = 37 &
	or err(0) = 38 &
	   then	print tab(24,01); tab(-1,9); "File/Record in use - Please Wait"; :&
		for SLP=1 to 10000 : next SLP :&
		   	print tab(24,01); tab(-1,9); "Checking..."; :&
			for SLP=1 to 2000 : next SLP :&
			print tab(24,01); tab(-1,09); :&
			resume					! [11]

	on error goto 						! [11]


	TIMER = 0		! Disable time-out to allow them to
				! study the error message.

	? tab(-1,12);chr(7);
	? tab(23,1);tab(-1,10);"----------------------------------------------------------------------------"
	? tab(24,1);
	if ERR(0)=12 then ? "Subroutine not found"; : goto ENDIT
	if ERR(0)=16 or ERR(0)=32 then &
		? "File specification error"; : goto ENDIT
	if ERR(0)=18 then ? "Device not ready"; : goto ENDIT
	if ERR(0)=19 then &
		? "Cannot allocate - not enough room on device!"; : goto ENDIT
	if ERR(0)=22 then ? "Illegal user code - PPN not found!"; : goto ENDIT
	if ERR(0)=23 then ? "Protection violation!"; : goto ENDIT
	if ERR(0)=24 then ? "Cannot write - disk write protected!"; : goto ENDIT
	if ERR(0)=26 then ? "Device does not exist!"; : goto ENDIT
	if ERR(0)=27 then ? "Bitmap kaput - see system operator!" : goto ENDIT
	if ERR(0)=28 then ? "Disk not mounted!"; : goto ENDIT
	if ERR(0)=36 then ? "Improper subroutine version" : goto ENDIT

	if ERR(0)<>31 goto TRAP2			! [6]
	
	? "Illegal Record in routine ";LABEL$;" (";str(FILE1);")";	! [6]
	? " - continue anyway? ";					! [6]
	xcall INFLD,24,76,1,0,"YNF",A$,INXCTL,1 			! [6]
	if A$="Y" DMARK2="]]]]]]" : ER'FLAG=0 : &
		? tab(-1,11);tab(23,1);tab(-1,10); : resume &
	else goto ENDIT							! [6]
			
TRAP2:
	on error goto 

	END
	goto ENDIT

TIME'OUT:
	? tab(24,1);tab(-1,9);"Program timed out.";

	END

ABORT:
	? tab(-1,0);"Program end."
	END
!---------------------------------------------------------------------------

MARK'BIT:					! Mark (set) the bit
						! corresponding to physical
						! memo pointed to by current
						! record.  
	
	if MMOFMT=2 then MB=MPTR2 else MB=MPTR4
	if MB>MAX'MEMOS then ? "x"; : return	! [7]
	Q = int(MB/8)
	R = MB - Q*8
	BITMAP(Q+1) = BITMAP(Q+1) or (2^R)
	return

!--------------------------------------------------------------------------

CHECK'BIT:					! Check to see if bit
						! corresponding to FILE1
						! is set - set CB'FLAG if so.
	Q = int(FILE1/8)				
	R = FILE1 - Q*8
	if FILE1>MAX'MEMOS then ? chr(7); : return	! [7]
	B1 = BITMAP(Q+1) and (2^R)
	if B1>0 then CB'FLAG=1 else CB'FLAG=0
	return


!--------------------------------------------------------------------------

READ'IN:				! Read logical memo into BUFFER
					! Starting rec = FILE1

	? #CH
	? #CH

	RECSTX = ""
	RS = 1
	BUFFER = ""
	LABEL$ = "RD'LOOP"					! [6]
RD'LOOP:

	? FILE1;chr(13);
	read #1, MREC2
	if DMARK2="]]]DEL" or DMARK2="]]]]]]" goto RD'EXIT
	if MMOFMT=2 then &
		BUFFER = BUFFER + MTXT2 : &
		X = MPTR2 &
	else &
		BUFFER = BUFFER + MTXT4 : &
		call BSWAP : &
		X = MPTR4

	if RECSTR(RS)<>"" then RECSTR(RS) = RECSTR(RS) + ", "
	if len(RECSTR(RS))>63 then RS = RS + 1
	if RS<=MAXRS then &
		RECSTR(RS) = RECSTR(RS) + str(FILE1) &
	else &
		RECSTR(MAXRS)[-3,-1]="..." : X = 0

	if X=0 goto RD'EXIT

	if X<=MAXREC1 goto NRD
		? 
		?
		?
		? tab(23,1);tab(-1,10);tab(-1,12);chr(7);
		? "Illegal memo link (";str(X);") at rec #";str(FILE1);"."
		? "Use ANAMMO to fix!";
		? " - proceed with dump anyway? ";
		xcall INFLD,24,50,1,0,"YNF",A$,INXCTL,1
		? tab(-1,11);
		if A$<>"Y" goto ENDIT
		goto RD'EXIT					! [6]
NRD:
	FILE1 = X
	goto RD'LOOP

RD'EXIT:
		! [10] determine which format the memo is (just for fun)

	if instr(1,BUFFER,chr$(2))>0 or instr(1,BUFFER,chr$(1))>0 then &
		MMOVER = 2 : goto RDX2	! [10] (any 2.x compressed spaces?)
	
	if instr(1,BUFFER," ")>0 then &
		MMOVER = 2 : goto RDX2	! [10] 1.x uses 129 for a space

	MMOVER = 1			! [10] else assume old format
RDX2:					! [10]
	return

!------------------------------------------------------------------------

DISPLAY:				! display memo in logical format,
					! expanding compressed blanks.

	X = len(BUFFER)
	ISO'FLAG = 0			! [10] Assume 7-bit fmt until we
					! [10] see the 8-bit flag

	? #CH,"Record #'s ";RECSTR(1);
	if RS=1 goto D2
	for I = 2 to (RS min MAXRS)
		? #CH
		? #CH,"           ";RECSTR(I);
	next I
D2:
	? #CH," -";
	? #CH," [";str(MMOVER);".x] -"			! [10]
	if MMOVER=1 then &
		OLD'CNT = OLD'CNT + 1 &
	else &
		NEW'CNT = NEW'CNT + 1			! [12]
	
	? tab(-1,12);
	for I = 1 to X
		A = asc(BUFFER[I,I])

                                        ! [16] print header in <brackets>
                if I=1 and A=26 then &
                    ? #CH,"<"; : IH'FLAG=1 : goto NXI           ! [16]

                if IH'FLAG=1 and A=26 then &
                    ? #CH,">" : IH'FLAG=0 : goto NXI            ! [16]

		if CS'FLAG=1 then SPC'CNT = A : goto EXSPC	! [10] 

		if A=13 then ? #CH : goto NXI

                if A=1 then ISO'FLAG = 1 : goto NXI    ! [10] 8-bit memo
                if A=2 then CS'FLAG = 1 : goto NXI     ! [10] compressed space
          
		if A<128 or ISO'FLAG=1 then ? #CH, chr(A); : goto NXI  ! [10]
		if A=128 goto NXI

		  SPC'CNT = A - 128			! [10] old fmt spaces
EXSPC:							! [10] expand spaces
		  for J = 1 to SPC'CNT			! [10]
			? #CH," ";
		  next J
		  CS'FLAG = 0			! [10] clr comp space flag
NXI:
	next I

	DSP'CNT = DSP'CNT + 1

	? tab(-1,11);
	return

!-------------------------------------------------------------------------

BSWAP:
	BSWAP = MPTR4		! swap 4-byte binary
	BX2 = BS1		! from AM100L format
	BS1 = BS2		! to BASIC format					
	BS2 = BX2
	MPTR4 = BSWAP
	return

!-----------------------------------------------------------------------

PHDR:					! display standard program 
					! header at (2,1) in AMOSL format

	? tab(2,1);tab(-1,11);"Rev. ";
	? str(VMAJOR);".";str(VMINOR);
	if VSUB<>0 then ? "-";chr(VSUB+96);
	if VEDIT<>0 then ? "(";VEDIT;")";
	if VPATCH<>0 then ? "-";str(VPATCH);
	return

!-----------------------------------------------------------------------

SEARCH:		! Allow input of a search pattern.

	? tab(13,5);tab(-1,12);"Search Parameters: ";tab(-1,11);
	? tab(13,25);"'?' Char:    '*' Char:    AND Char:     OR Char: "
	? tab(14,25);"'@' Char:    '#' Char:    Delimiter:    Fold?"
	S'1WILD$ = "%"			! (avoid conflict with ?:help)
	S'AST$ = "*"
	S'AND$ = "&"
	S'OR$ = "!"
	S'ALP$ = "@"
	S'NUM$ = "#"
	S'LIMIT$ = "|"
	S'FOLD$ = "Y"
	if PATTERN<>"" then PATTERN=PATTERN[11,-1]
	
SR1:
	xcall INFLD,13,35,1,1,"*156?]",S'1WILD$,INXCTL,1,1,EXITCODE,TIMER,&
		CMDFLG,MAXPT,DEFPT,FUNMAP,SETDEF,INFCLR,HLPIDX

	if EXITCODE=8 call HLP1 : goto SR1
	if HELP'FLAG=1 then call CLEAR'HELP 
	if EXITCODE=5 goto SR9
	if EXITCODE=6 PATTERN="" : goto SR10
	if EXITCODE=1 return
SR2:
	xcall INFLD,13,48,1,1,"*26?]",S'AST$,INXCTL,1,1,EXITCODE,TIMER,&
		CMDFLG,MAXPT,DEFPT,FUNMAP,SETDEF,INFCLR,HLPIDX

	if EXITCODE=8 call HLP2 : goto SR2
	if HELP'FLAG=1 then call CLEAR'HELP 
	if EXITCODE=6 goto SR9
	if EXITCODE=2 goto SR1	
SR3:
	xcall INFLD,13,61,1,1,"*26?]",S'AND$,INXCTL,1,1,EXITCODE,TIMER,&
		CMDFLG,MAXPT,DEFPT,FUNMAP,SETDEF,INFCLR,HLPIDX
	
	if EXITCODE=8 call HLP3 : goto SR3
	if HELP'FLAG=1 then call CLEAR'HELP 
	if EXITCODE=6 goto SR9
	if EXITCODE=2 goto SR2	
SR4:
	xcall INFLD,13,74,1,1,"*26?]",S'OR$,INXCTL,1,1,EXITCODE,TIMER,&
		CMDFLG,MAXPT,DEFPT,FUNMAP,SETDEF,INFCLR,HLPIDX

	if EXITCODE=8 call HLP4 : goto SR4
	if HELP'FLAG=1 then call CLEAR'HELP 
	if EXITCODE=6 goto SR9
	if EXITCODE=2 goto SR3	
SR5:
	xcall INFLD,14,35,1,1,"*26?]",S'ALP$,INXCTL,1,1,EXITCODE,TIMER,&
		CMDFLG,MAXPT,DEFPT,FUNMAP,SETDEF,INFCLR,HLPIDX

	if EXITCODE=8 call HLP5 : goto SR5
	if HELP'FLAG=1 then call CLEAR'HELP 
	if EXITCODE=6 goto SR9
	if EXITCODE=2 goto SR4	
SR6:
	xcall INFLD,14,48,1,1,"*26?]",S'NUM$,INXCTL,1,1,EXITCODE,TIMER,&
		CMDFLG,MAXPT,DEFPT,FUNMAP,SETDEF,INFCLR,HLPIDX

	if EXITCODE=8 call HLP6 : goto SR6
	if HELP'FLAG=1 then call CLEAR'HELP 
	if EXITCODE=6 goto SR9
	if EXITCODE=2 goto SR5	
SR7:
	xcall INFLD,14,61,1,1,"*26?]",S'LIMIT$,INXCTL,1,1,EXITCODE,TIMER,&
		CMDFLG,MAXPT,DEFPT,FUNMAP,SETDEF,INFCLR,HLPIDX

	if EXITCODE=8 call HLP7 : goto SR7
	if HELP'FLAG=1 then call CLEAR'HELP 
	if EXITCODE=6 goto SR9
	if EXITCODE=2 goto SR6	
SR8:
	xcall INFLD,14,74,1,1,"*26?]",S'FOLD$,INXCTL,1,1,EXITCODE,TIMER,&
		CMDFLG,MAXPT,DEFPT,FUNMAP,SETDEF,INFCLR,HLPIDX

	if EXITCODE=8 call HLP8 : goto SR8
	if HELP'FLAG=1 then call CLEAR'HELP 
	if EXITCODE=6 goto SR9
	if EXITCODE=2 goto SR7	
SR9:

	? tab(15,5);tab(-1,11);"Enter search pattern: ";
	xcall INFLD,15,30,45,0,"A23?]",PATTERN,INXCTL,1,1,EXITCODE,TIMER,&
		CMDFLG,MAXPT,DEFPT,FUNMAP,SETDEF,INFCLR,HLPIDX

	if EXITCODE=2 or EXITCODE=3 goto SR1
	if EXITCODE=8 call HLP9 : goto SR9
	if HELP'FLAG=1 then call CLEAR'HELP 
SR10:
 	if S'FOLD$="Y" PATTERN = "^" + PATTERN else PATTERN = " " + PATTERN
	PATTERN = S'ALP$ + S'NUM$ + "  " + PATTERN		! [10]
	PATTERN = S'1WILD$ + S'AST$ + S'LIMIT$ + S'AND$ + S'OR$ + PATTERN

	return

!--------------------------------------------------------------------------

CLEAR'HELP:
	for ROW = HELP'SROW-1 TO HELP'EROW+1
		? tab(ROW,1);tab(-1,9);
	next ROW
	HELP'FLAG = 0
	return


HLP1:				
	HELP'SUB$ = "DMPMMO '?' Wildcard"
	call HELP
	goto CRCONT

HLP2:
	HELP'SUB$ = "DMPMMO '*' Wildcard"
	call HELP
  	goto CRCONT	

HLP3:
	HELP'SUB$ = "DMPMMO AND Operator"
	call HELP
 	goto CRCONT	

HLP4:
	HELP'SUB$ = "DMPMMO OR Operator"
	call HELP
	goto CRCONT
HLP5:
	HELP'SUB$ = "DMPMMO '@' Wildcard"
	call HELP
	goto CRCONT	
HLP6:
	HELP'SUB$ = "DMPMMO '#' Wildcard"
	call HELP
	goto CRCONT	
HLP7:
	HELP'SUB$ = "DMPMMO Delimiter"
	call HELP
	goto CRCONT	

HLP8:
	HELP'SUB$ = "DMPMMO Searchfold"
	call HELP
	goto CRCONT	

HLP9:
	HELP'SUB$ = "DMPMMO Search Pattern"
	call HELP
	goto CRCONT	

CRCONT:
	HELP'FLAG=1
	return

!-----------------------------------------------------------------------

HELP:		! [12] Display help information if available using INMEMO.
		! [12] The help text has been pre-typed into HLPMMO.MMO.

	if HELP'FILE'OPEN=0 call OPEN'MEMO'IDX

	FILE100 = 0

HELP2:
	FILE100 = FILE100 + 1	
	if FILE100>MAX100 goto NO'HELP2

	read #100, HELP'REC
	if HELP'DESCR[1,3]="]]]" goto NO'HELP2
	if ucs(HELP'DESCR)<>ucs(HELP'SUB$) goto HELP2

	xcall INMEMO,MMO'DPG+MMO'BDR,HELP'SUB$[8,-1],HMCH,HELP'SROW, &
		HELP'SCOL,HELP'EROW,HELP'ECOL,HELP'LINK,HPOS,HVSPEC,MMOCLR,EXTCTL
	return

NO'HELP2:
	if NO'HELP'FLAG=1 return				! [112]
	? tab(24,1);tab(-1,9);"Help memo '";HELP'SUB$;"' not found. (hit RETURN):";
	xcall INFLD,24,77,1,0,"@\",A$,INXCTL,1
	return


!--------------------------------------------------------------------------

OPEN'MEMO'IDX:
	on error goto HELP'TRAP
	
	lookup HELP'DAT$,MAX100
	if abs(MAX100)= 0 goto NO'HELP			! [17]
	MAX100 = abs(MAX100) * int(512/RECSIZ100)           ! [15]

	lookup HELP'MMO$,MAX101
	if abs(MAX101) = 0 goto NO'HELP			! [17]				
	MAX101 = abs(MAX101) * 8                            ! [15]

	open #HDCH, HELP'DAT$, random'forced, RECSIZ100, FILE100
	open #HMCH, HELP'MMO$, random'forced, 64, FILE101

	on error goto TRAP

	HELP'FILE'OPEN=1
	return

NO'HELP:
	? tab(24,1);tab(-1,9);"Sorry, help files '";HELP'MMO$;"' (or .DAT) not found   (Hit RETURN): ";
	xcall INFLD,24,77,1,0,"@\",A$,INXCTL,1
	return


HELP'TRAP:		! [112] trap filespec errors caused by ersatz account
			! [112] 'INMEMO:' not being defined.  

	if err(0)<>16 goto TRAP		! not the error we expected!
	
	NO'HELP'FLAG=1
	resume NO'HELP


