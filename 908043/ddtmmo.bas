!	DDTMMO.BAS
!
!	Copyright (c) MicroSabio 1983,1984
!	Written by Jack McGregor
!------------------------------------------------------------------------
!	Program to allow direct editing of a memo by its physical link.
!------------------------------------------------------------------------
! USAGE:
!	.DDTMMO		via DDTMMO.CMD:
!
!					LOAD BAS:INFLD.SBR
!					RUN INMEMO:DDTMMO
!-------------------------------------------------------------------------
! EDIT HISTORY:
!	0.  01-07-91  [2.0]  Created /jdm
! Version 2.2A
!	1.  02-26-92  [2.2A] Adjustments for non-AMOS /gb
!------------------------------------------------------------------------
! EXTERNAL SUBROUTINES:	
!	INFLD.SBR  
!-------------------------------------------------------------------------		
! ALGORITHM NOTES:
!	1.  Ask for the name of the memo file.  
!	2.  Lookup file to determine maximum # recs, link size.
!	3.  Input a link to edit (or 0 to add)
!	4.  Call INMEMO in MMO'EDT mode (edit)
!	5.  Display updated link
!	6.  goto back to 3
!-------------------------------------------------------------------------

	PROGRAM DDTMMO,2.0
	
MAP1 PHDR
	MAP2 VMAJOR,B,1,2
	MAP2 VMINOR,B,1,0
	MAP2 VSUB,B,1,0
	MAP2 VEDIT,B,1,0
	MAP2 VPATCH,B,1,0

MAP1 CTL'REC2
	MAP2 MARKER2,S,6
	MAP2 NXT'FRE2,B,2
	MAP2 NXT'DEL2,B,2
	MAP2 FILL2,S,54
MAP1 CTL'REC4,@CTL'REC2
	MAP2 MARKER4,S,6	
	MAP2 NXT'FRE4,B,4
	MAP2 NXT'DEL4,B,4
	MAP2 FLAG4,S,1
	MAP2 FILL4,S,49

MAP1 BRACKS,S,64,"]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]"

MAP1 MEMO$,S,24

MAP1 BSWAP,B,4
MAP1 BSWPX,@BSWAP
	MAP2 BS1,B,2
	MAP2 BS2,B,2
MAP1 BX2,B,2

MAP1 MISC
	MAP2 OPCODE,F,6			! [1]
	MAP2 DEL'CNT,F,6
	MAP2 EXISTS,F,6
	MAP2 X,F,6
	MAP2 DOTCNT,F,6
	MAP2 DC,F,6
	MAP2 USE'CNT,F,6
	MAP2 FREE'CNT,F,6
	MAP2 RECCNT,F,6
	MAP2 LFMT,F,6
	MAP2 FILE1,F,6
	MAP2 A$,S,10
	MAP2 FREE'PTR,F,6
	MAP2 LINK$,S,10
	MAP2 ODD,B,1
	MAP2 LINK2,B,2
	MAP2 LINK4,B,4
	MAP2 CH,F,6,1
	MAP2 EXTFLT,F,6
	MAP2 POSFLT,F,6
	MAP2 TITLE$,S,30

++include infpar.bsi		! infld parameters
++include mmopar.bsi		! inmemo parameters
++include mmosym.bsi		

	VWIDTH = 132			! max size of memo to edit
	VROWS = 100

	TIMER = 90			! 90 sec timeout
	CMDFLG = 1			! input from command file
!------------------------------------------------------------------------

	on error goto TRAP
	filebase 1

! ++include getclr.bsi		! [7] setup color

START:
	? tab(-1,0);"DDTMMO - Direct entry/editing of Memos by Link";
	? tab(-1,11);
	? tab(1,62);"Copyright (c) 1991";
	? tab(2,62);"By MicroSabio"

	call PHDR

	? tab(4,5); "Enter Name of Memo File: "

	? tab(5,1);tab(-1,10);
F1:	
	DEL'CNT = 0
	xcall INFLD,4,45,24,1,"A^]E\",MEMO$,INXCTL,1,1,EXITCODE,TIMER, &
		CMDFLG,DEFPT,MAXPT,FUNMAP,SETDEF,INFCLR,HLPIDX
	if EXITCODE=11 goto TIME'OUT
	if INXCTL=2 goto EXIT

	X = instr(1,MEMO$,".")
	if X<1 then MEMO$=MEMO$+".MMO" : ? tab(4,45);MEMO$

	? tab(5,5);tab(-1,9);

	? tab(-1,11);
	? tab(6,5); "Memo Link Format: ";
	? tab(6,35); "# of Records in Use:";

	lookup MEMO$,EXISTS
	if abs(EXISTS)<>0 goto READ'MEMO	! [1]

	? tab(24,1);MEMO$;" not found!";
	goto F1

READ'MEMO:
	RECCNT = abs(EXISTS) * 8                       

	open #CH, MEMO$, random'forced, 64, FILE1
	FILE1 = 1
	read #CH, CTL'REC2

	if FLAG4="4" then LFMT=4 else LFMT=2
	? tab(6,23);tab(-1,12);LFMT;tab(-1,11);"byte";tab(-1,12)

	if LFMT=2 then 			&
		FREE'PTR = NXT'FRE2 	&
	else 				&
		BSWAP = NXT'FRE4 : 	&
		call BSWAP : 		&
		FREE'PTR = BSWAP

	if FREE'PTR=0 then FREE'PTR = RECCNT + 1	
	? tab(6,59);FREE'PTR 

GET'LINK:
	? tab(8,5);tab(-1,11);"Enter link # to edit (0 to add, ESC to exit): ";
	xcall INFLD,8,60,6,0,"#E",LINK$,INXCTL,1,1,EXITCODE,TIMER,CMDFLG,&
		DEFPT,MAXPT,FUNMAP,SETDEF,INFCLR,HLPIDX

	if EXITCODE>0 goto DONE

	? tab(10,1);tab(-1,10);

	TITLE$ = "Memo"
	if val(LINK$)>0 then 	&
		TITLE$ = TITLE$ + " (Link = "+str(LINK$)+")"
	TITLE$ = TITLE$ + "~"
	POS = 0
	EXTERR = 0			! reset (in case set before)

	if LFMT = 2 then 	&
		LINK2 = LINK$ :	&
		call INMEMO'2 :	&
		LINK$ = LINK2	&
	else			&
		LINK4 = LINK$ :	&
		call INMEMO'4 : &
		LINK$ = LINK4


	? tab(22,1);tab(-1,11);"LINK: ";tab(-1,12);LINK$;
	POSFLT = POS
	if POSFLT > 99999 then POSFLT = POSFLT - (2^32) ! convert to signed
	? tab(-1,11);" POS:";tab(-1,12);POSFLT;
	EXTFLT = EXTCOD
	if EXTFLT > 32767 then EXTFLT = EXTFLT - (2^16)	! convert to signed
	? tab(-1,11);"EXTCOD:";tab(-1,12);EXTFLT;
	? tab(-1,11);"EXTERR:";tab(-1,12);EXTERR;
	? tab(-1,11);"ROW:";tab(-1,12);EXTROW;
	? tab(-1,11);"COL:";tab(-1,12);EXTCOL;
	? tab(-1,11);"Size:";tab(-1,12);EXTHIT;
	? tab(-1,11);"x";tab(-1,12);EXTWID;
	? tab(-1,11);"(";tab(-1,12);str(EXTBYT);tab(-1,11);" bytes)"

	goto GET'LINK

DONE:
	close #CH
	goto F1


TRAP:
	if ERR(0)=1 then goto EXIT
	? tab(23,1);tab(-1,10);"----------------------------------------------------------------------------"
	? tab(24,1);
	TIMER=0			! Turn time out off to make sure they have
				! time to study the error message.

	if ERR(0)=12 then ? "INFLD.SBR not found"; : goto ENDIT
	if ERR(0)=16 or ERR(0)=32 then &
		? "File specification error"; : resume F1
	if ERR(0)=18 then ? "Device not ready"; : resume F1
	if ERR(0)=22 then ? "Illegal user code - PPN not found!"; : resume F1
	if ERR(0)=23 then ? "Protection violation!"; : resume DONE
	if ERR(0)=26 then ? "Device does not exist!"; : resume F1
	if ERR(0)=27 then ? "Bitmap kaput - see system operator!"; : goto F1
	if ERR(0)=28 then ? "Disk not mounted!"; : goto F1
	if ERR(0)=31 then ? "Illegal link in deleted record chain!"; : goto DONE
	if ERR(0)=36 then ? "Improper version of INFLD.SBR"; : goto ENDIT
	on error goto 
	END



EXIT:
	? tab(-1,0);"Program end"

ENDIT:
	END

TIME'OUT:
	? tab(-1,0);"Program timed out."
	goto ENDIT

!-------------------------------------------------------------------------

PHDR:					! display standard program 
					! header at (2,1) in AMOSL format

	? tab(2,1);tab(-1,11);"Rev. ";
	? str(VMAJOR);".";str(VMINOR);
	if VSUB<>0 then ? "-";chr(VSUB+96);
	if VEDIT<>0 then ? "(";VEDIT;")";
	if VPATCH<>0 then ? "-";str(VPATCH);
	return

!--------------------------------------------------------------------------
! INMEMO'2 - xcall to inmemo with 2 byte links
! INPUT -  LINK2, CH
! OUTPUT - POS, LINK2, EXTCTL
!-------------------------------------------------------------------------

INMEMO'2:
     OPCODE = MMO'BDR + MMO'LID + MMO'EDT         ![1] 

     xcall INMEMO,MMO'BDR+MMO'LID+MMO'EDT,TITLE$,CH,11,2,20,78, &
		 LINK2,XPOS,VSPEC,MMOCLR,EXTCTL 

    if EXTERR = MME'ERR then	&
	call MMO'ERROR
     return

!--------------------------------------------------------------------------
! INMEMO'4 - xcall to inmemo with 4 byte links
! INPUT -  LINK4, CH
! OUTPUT - POS, LINK4, EXTCTL
!-------------------------------------------------------------------------

INMEMO'4:
     xcall INMEMO,MMO'BDR+MMO'LID+MMO'EDT,TITLE$,CH,11,2,20,78, &
		 LINK4,XPOS,VSPEC,MMOCLR,EXTCTL 
     if EXTERR = MME'ERR then	&
	call MMO'ERROR
     return

!--------------------------------------------------------------------------
! MMO'ERROR - Generalized error handling
! INPUT - EXTCTL, POS
! OUTPUT- messages on the screen
!-------------------------------------------------------------------------
MMO'ERROR:

     ? tab(24,1);tab(-1,9);tab(-1,21);"INMEMO Error: ";
     if POS = MMO'FUL then                             &
          ? "Memo File Full - Updates NOT Saved!";     &
     else if POS = MMO'LKE then                        &
          ? "Link Error! ";            	       	       &
     else                                              &
          ? "Memo Error: ";POS;

     ? chr(7);tab(-1,22);
     return

!-----------------------------------------------------------------------

BSWAP:
	BX2 = BS1		! from AM100L format
	BS1 = BS2		! to BASIC format					
	BS2 = BX2
	return

