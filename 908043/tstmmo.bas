!   	TSTMMO.BAS
!
!	07/30/85
!-----------------------------------------------------------------------
!	This program demonstrates the use of INMEMO.SBR on a simple
!	file.  The program was originally used to maintain a product
!	type file; INMEMO calls were added later.  Note that all of the
!	AlphaACCOUNTING subroutines have been replaced with equivalent
!	code using INFLD.SBR.  Contact MicroSabio for more information
!	on the use of INFLD.SBR.  If you don't have INFLD, rename the
!	routine INFLDX (provided in the INMEMO release) to INFLD; note
!	however, that it will only work with the existing INMEMO utilities.
!	Image mode is set using XCALL NOEKO - equivalent to XCALL NOECHO.
!-----------------------------------------------------------------------
! EDIT HISTORY
!	1.  04/09/84 Use OPCODE 128 to avoid redrawing the border /jdm
!	2.  04/30/84 changes to open file routine: "OPEN" to test 
!		LOKSER performance./bb
!	3.  04/30/84 Redisplay Memo Link After Update/bb
!	4.  05/09/84 Zero PLINK on link error /jdm
!	5.  08-20-84 Modify for access to 1.4 features /jdm
!	6.  11/13/84 Modify to test search opcode /jdm
!	7.  02/10/85 Fix bug in expand routine /jdm
!	8.  02/21/85 Optional horizontal scrolling; NOEKO /jdm
!	9.  03/15/85 Trap POS=2 error also; no hor.scroll in add mode/jdm
!	10. 08/07/85 Remove ref. to POS=2, use new POS,BADLINK /jdm
!	11. 04/21/86 Minor changes for 1.6C release  [1.6(111)]
!	12. 11/19/86 Add new help system,symbols, display-scroll [1.7(112)]
!	13. 03/15/87 Add short version of preload [1.7(113)]
!	14. 08/16/87 Optimize border redisplay [1.7(114)]
!	15. 11/22/89 Redo record;use menus/tables,color; cleanup [2.0(115)]
!	16. 12/29/89 Show EXTCOD using Floating point for signed;
!                        fix odd address of HPOS; add maps to allow
!                        compil/m [2.0(116)] /jdm
! Version 2.1---
!       17. 09-20-90 Minor corrections for non-AMOS compatibility /jdm
!       18. 06-04-08 Add some comments to source regarding how to 
!                       disable/replace MMO'DPG message /jdm
!-----------------------------------------------------------------------
! NOTES ON INMEMO OPTIONS
!	This program demonstrates the use of a particular set of INMEMO
!	options:
!		2 Byte Links
!		Graphics Border
!		Virtual Paging
!		Edit w/o redisplay of text or border  (Change mode)
!		Smart Insert-Delete (if supported on terminal)
!		Pre-load of memo text	
!
!	You may wish to experiment with different options.  We suggest
!	that you make a copy of this program as shipped before you start
!	changing it.
!
!	THIS PROGRAM USES THE 2-BYTE LINK FORMAT.  TO CONVERT TO 4-BYTE,
!	ERASE THE DATA FILE (INMEMO.DAT), CHANGE THE SIZE OF PTDSCR TO
!	26 BYTES (FROM 2), THE SIZE OF PLINK TO 4 BYTES (FROM 2), AND
!	THE SIZE OF XPOS TO 6 BYTES (FROM 4).
!	THEN RECOMPILE AND RUN - IT WILL ASK YOU ABOUT CREATING A NEW
!	PRIMARY FILE.
!--------------------------------------------------------------------------

	PROGRAM TSTMMO,2.1(117)

++include mmosym.bsi
++include mmopar.bsi
++include infpar.bsi

MAP1 PHDR
	MAP2 VMAJOR,B,1,2
	MAP2 VMINOR,B,1,0
	MAP2 VSUB,B,1,0
	MAP2 VEDIT,B,1,115
	MAP2 VPATCH,B,1,0

					! PRIREC is a sample of a primary
					! file record with a memo link
MAP1 PRIREC
	MAP2 PTDSCR,S,28			! description
	MAP2 PTDEPT,B,1				! department #
	MAP2 PTAX,S,1				! category
	MAP2 OLINK,B,2				! INMEMO: link to memopad

MAP1 MISC'FILE
	MAP2 RECSIZ16,F,6,32
	MAP2 MAXREC16,F,6
	MAP2 FILE16,F,6
	MAP2 FILE2,F,6
	MAP2 SIZE,F,6
	MAP2 EXISTS,F,6
	
MAP1 I'O
	MAP2 ENTRY,S,20
	MAP2 MESAG,S,60
	MAP2 WHATNO,F,6
	MAP2 A$,S,1
	MAP2 SC$,S,1
	MAP2 I,F,6
	MAP2 J,F,6
	MAP2 ROW,F,6
	MAP2 X,F,6
	MAP2 SCOUNT,F,6
	MAP2 FLAG,F,6
	MAP2 SPOOL,F,6

MAP1 ODDSTUFF
    MAP2 UNF
	MAP3 ODD2,B,1
	MAP3 PLINK,B,2

MAP1 ZPOS,B,4

MAP1 PREV'MNU$,S,10

MAP1 PROG'BLOCK
	MAP2 PROG'TITLE,S,73,"INMEMO DEMONSTRATION PROGRAM"+SPACE(73)
	MAP2 PROG'NAME,S,6,"TSTMMO"

MAP1 BRACKS,S,32,"]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]"

MAP1 MASKS
	MAP2 MASK1,S,11,"$###,###.##"
	MAP2 MASK2,S,5,"##.##"

MAP1 OLDFIL$,S,24
MAP1 NEWFIL$,S,24

MAP1 MISC'MEMO						! INMEMO support vars
	MAP2 PROMPT$,S,60," 4.  Comments"		! INMEMO support
	MAP2 MCH,B,1,2					! INMEMO file #
	MAP2 PLINE,S,100				! INMEMO support
	MAP2 DEFTXT,S,3000				! INMEMO def. text
	MAP2 STR2$,S,60					! INMEMO
	MAP2 STRW,B,2,15    				! INMEMO: start row
	MAP2 STCL,B,2,9    				! INMEMO: start col
	MAP2 ENDRW,B,2,21    				! INMEMO: end row
	MAP2 ENDCL,B,2,73    				! INMEMO: end col
	MAP2 OPCODE,F,6					! INMEMO: opcode

MAP1 HELP'REC				! [12] MMOHLP.DAT record
	MAP2 HELP'DESCR,S,30		! [12] (primitive example of a help
	MAP2 HELP'LINK,B,2		! [12] memo index file)

MAP1 MEMO'HELP				! Misc vars for help system
	MAP2 RECSIZ100,F,6,32			! [12] HLPMMO.DAT recsize
	MAP2 NEWMAX,F,6
	MAP2 NEWMAXREC16,F,6
	MAP2 FILE100,F,6
	MAP2 MAX100,F,6
	MAP2 FILE101,F,6
	MAP2 MAX101,F,6
	MAP2 HELP'DAT$,S,24,"HLPMMO.DAT"	! [12] HLPMMO.DAT filespec
	MAP2 HELP'MMO$,S,24,"HLPMMO.MMO"	! [12] HLPMMO.MMO filespec
	MAP2 HLINK,B,2				! [12]
	MAP2 HELP'SUB$,S,40			! [12]
	MAP2 HELP$,S,2				! [12][16] (make HPOS even)
	MAP2 HPOS,B,4				! [12]
	MAP2 HDCH,F,6,100			! help idx file channel
	MAP2 HMCH,F,6,101			! help menu file channel
	MAP2 HELP'ROW,B,2
	MAP2 HELP'COL,B,2
	MAP2 HELP'EROW,B,2
	MAP2 HELP'FLAG,F,6
	MAP2 NO'HELP'FLAG,F,6
	MAP2 HELP'FILE'OPEN,F,6
	MAP2 WHELP'FLAG,F,6


MAP1 MENU'STUFF                         ! Vars used for memo menus & tables
	MAP2 DMNU'TTL$,S,20,"Department"
	MAP2 MNU'SROW,B,2,6
	MAP2 MNU'SCOL,B,2,60
	MAP2 MNU'EROW,B,2,12
	MAP2 MNU'ECOL,B,2,78
	MAP2 MNU'TTL$,S,30
	MAP2 MNU'SEL$,S,100
        MAP2 ETX$,S,1,chr$(3)           ! lead-in for menu defaults
	MAP2 MNU'LINK,B,2
	MAP2 SELECT,F

MAP1 HVSPEC						! INMEMO support
	MAP2 HVWIDTH,B,2,65 				! (max memo size is
	MAP2 HVROWS,B,2,50				!  65 cols, 50 rows)



MAP1 SEARCH'PARAMS			! default wildcard symbols
	MAP2 S'1WILD$,S,1,"%"		! (avoid conflict with ?:help)
	MAP2 S'AST$,S,1,"*"		! * wildcard
	MAP2 S'AND$,S,1,"&"		! logical AND
	MAP2 S'OR$,S,1,"!"		! logical OR
	MAP2 S'ALP$,S,1,"@"		! 1 alpha character
	MAP2 S'NUM$,S,1,"#"		! 1 numeric character
	MAP2 S'LIMIT$,S,1,"|"		! delimiter 
	MAP2 S'FOLD$,S,1,"Y"		! searchfold



MAP1 EXTFLT,F,6						! [16]

!------------------------------------------------------------------------

	ON ERROR GOTO TRAP
	SIGNIFICANCE 11
	FILEBASE 1
!	XCALL NOEKO				! set image mode


!++include getclr.bsi                  	! set up color

				! See if file exists.  If not, give 
				! option to initialize a new file.

! ?tab(-3,2);tab(-2,1);

	LOOKUP "INMEMO.DAT",EXISTS
	IF EXISTS=0 GOTO NO'FILE
	
	CALL OPEN
	
	VWIDTH = 65 				! (max memo size is
	VROWS = 51 				!  65 cols, 50 rows)
!	SPC'CNT = int(((ENDCL-STCL)-len(PROMPT$))/2) max 0	
!	PROMPT$ = space(SPC'CNT) + PROMPT$		! center prompt


			! Look the two menu/tables and save their links now

	MNU'TTL$ = DMNU'TTL$
	call GET'MENU
	if HELP'LINK = 0 END			! crude abort
	MNU'LINK = HELP'LINK

START:
	xcall MIAMEX,65,1		! append screen snapshot to jobnam.BUF
	? TAB(-1,0);PROG'BLOCK
	call PHDR					! [12]
	? TAB(6,5);"PLEASE ENTER YOUR SELECTION: "
	? TAB(8,10); "1.  ADD SAMPLE RECORDS"
	? TAB(10,10);"2.  EDIT SAMPLE RECORDS"
	? TAB(12,10);"3.  DELETE SAMPLE RECORDS"
	? TAB(14,10);"4.  PRINT SAMPLE RECORDS"
	? TAB(16,10);"5.  EXPAND FILE"
	? TAB(18,10);"6.  SEARCH MEMO FOR PATTERN"
	
OPTION:	
	XCALL INFLD,6,40,1,0,"#EW",ENTRY,INXCTL,1,0,EXITCODE,TIMER,&
		CMDFLG,DEFPT,MAXPT,FUNMAP,SETDEF,INFCLR,HLPIDX

	IF INXCTL=2 CALL CLOSE : GOTO PREV'MENU
	SELECT=ENTRY
	ON SELECT CALL ADD,CHANGE,DELETE,PRINT,EXPAND,SEARCH
	GOTO START

!-----------------------------------------------------------------------

TRAP:
	? TAB(23,1);"Error #";ERR(0);
	if ERR(2)>0 then ? "  Last file used: ";ERR(2);
	if ERR(0)=31 then &
		? "Bad link was: ";PLINK; : &
		OLINK = 0 : &
		WRITE #16, PRIREC	! [4]
	MESAG = ""
	CALL MESAG
	GOTO PREV'MENU

!----------------------------------------------------------------------

OPEN:
	MAXREC16 = INT(512/RECSIZ16) * abs(EXISTS)          ! [17]

	OPEN #16, "INMEMO.DAT", RANDOM'FORCED, RECSIZ16, FILE16
	OPEN #MCH,"INMEMO.MMO", RANDOM'FORCED, 64, FILE2

	RETURN

!----------------------------------------------------------------------

CLOSE:
	CLOSE #16
	CLOSE #MCH					! Close INMEMO file	
	RETURN

!-----------------------------------------------------------------------

PREV'MENU:
	END

!-----------------------------------------------------------------------

NO'FILE:
	? tab(3,1);tab(-1,10);
	? TAB(5,5);"The sample data file (";
	? "INMEMO.DAT) does not exist."
	? TAB(7,5);"Do you want to create one? ";
	xcall INFLD,7,50,1,1,"YY",ENTRY,INXCTL,1
	if ENTRY<>"Y" goto PREV'MENU

	? TAB(9,5);"How many data records do you want allow? ";
	xcall INFLD,9,50,3,1,"#E",ENTRY,INXCTL,1
	on INXCTL goto PREV'MENU,NO'FILE

!	CNGCTL=2
!	xcall ANYCN,CNGCTL,WHATNO
!	on (CNGCTL+2) goto NO'FILE,NF2,NO'FILE

	? tab(24,1);tab(-1,9);tab(-1,12);"ANY CHANGE? ";
	XCALL INFLD,24,20,1,0,"YN",A$,INXCTL,1
	IF A$="Y" goto NO'FILE

NF2:
	SIZE = INT(VAL(ENTRY) / INT(512/RECSIZ16)) + 1
	MAXREC16 = SIZE * INT(512/RECSIZ16)
	? tab(3,1);"ALLOCATING FILE";
	ALLOCATE "INMEMO.DAT",SIZE
	LOOKUP "INMEMO.DAT",EXISTS
	CALL OPEN
	FOR FILE16=1 TO MAXREC16
		WRITEL #16, BRACKS
		? ".";
	NEXT FILE16
	? TAB(3,1);TAB(-1,9);
	GOTO START

!-----------------------------------------------------------------------

ADD:
	xcall MIAMEX,65,1		! append screen snapshot to jobnam.BUF

	? TAB(2,1);TAB(-1,10);"ADD   "

	CALL SCREEN
ADD2:
	CALL CLEAR'DATA
	CALL WHICH : ON INXCTL GOTO START,START
	IF PRIREC[1,3]<>"]]]" THEN &
		MESAG = "TABLE "+FILE16+" ALREADY IN USE" : &
		CALL MESAG : &
		GOTO ADD2

	PTDSCR = ""
	PTDEPT = ""
	PTAX = ""
	PLINK = 0
	
A1:	CALL PT1 : ON EXITCODE GOTO START,START
A2:	CALL PT2 : ON EXITCODE GOTO START,A1
A3:	CALL PT3 : ON EXITCODE GOTO START,A2,A2
	? tab(24,1);tab(-1,9);"Do you want to see demo of default memo text? ";
	xcall INFLD,24,60,1,1,"YN<F",A$,INXCTL,1
	if A$="N" goto A3N

	? tab(24,1);tab(-1,9);"Do you want the short version? ";	! [113]
	xcall INFLD,24,60,1,1,"YN<F",A$,INXCTL,1			! [113]
	if A$="N" goto A3X						! [113]

		DEFTXT = "~"+chr(19)+chr(8)+"Invisible header"+chr(19)+ &
				"This is a short example of" + chr(13) + &
				"preloading text.  Append an ESC to have" + &
				chr(13) + "it exit automatically"  &
				+ "%7E." ! + chr(27)
		goto A4
A3X:
		DEFTXT="~T~h~i~s~ ~i~s~ ~a~n~ ~e~x~a~m~p~l~e~ ~o~f~" + &	
			chr(13)+" ~s~l~o~w~ ~m~o~t~i~o~n~ ~d~e~f~a~u~l~t~" + &
			" ~m~e~m~o~ ~t~e~x~t~."+chr(13)+chr(13)+ &
			"This is normal speed."
		DEFTXT=DEFTXT+chr(13)+"Now for some vertical scrolling..."
		DEFTXT=DEFTXT+"~~~~~~~~~~~~~~"			! pause
		DEFTXT=DEFTXT+chr(13)+"~~~~~~~~"+chr(13)+"~~~~~~~"
		DEFTXT=DEFTXT+chr(13)+"~~~~~~~~"+chr(13)+"~~~~~~~"
		DEFTXT=DEFTXT+"Now H~o~r~i~z~o~n~t~a~l~ scrolling.."
		DEFTXT=DEFTXT+".~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~"
		DEFTXT=DEFTXT+"~/~"+chr(11)+"~/~"+chr(11)+"~/~"+chr(11)+"~~~^~~~"
		DEFTXT=DEFTXT+chr(10)+"\"+chr(10)+"\"+chr(10)+"\..."
		DEFTXT=DEFTXT+".~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~"
		DEFTXT=DEFTXT+chr(10)
		for I = 1 to 15
			DEFTXT=DEFTXT+chr(8)
		next I
		DEFTXT=DEFTXT+"Notice the border arrows?~~~~~~~~~~~~~~~~~~~ "
		DEFTXT=DEFTXT+chr(1)+chr(1)+chr(1)+chr(10)
		DEFTXT=DEFTXT+"Watch as we scroll up...~~~~~~~~~~~"
		DEFTXT=DEFTXT+chr(11)+"~~~~~"+chr(11)+"~~~~"+chr(11)+"~~~~~"
		DEFTXT=DEFTXT+chr(11)+"~~~~~"+chr(11)+"~~~~"+chr(11)+"~~~~~"
		DEFTXT=DEFTXT+chr(11)+"~~~~~"+chr(11)+"~~~~"+chr(11)+"~~~~~"
		DEFTXT=DEFTXT+chr(11)+"~~~~~"+chr(11)+"~~~~"+chr(11)+"~~~~~"
		DEFTXT=DEFTXT+"Now left"
		STR2$ = "(watch the arrows)"             
		J = len(STR2$)
		for I=1 to 90
			if J>0 and I>13 then DEFTXT=DEFTXT+STR2$[J,J]+chr(8) : J=J-1
			DEFTXT=DEFTXT+chr(8)+"~"
		next I
		DEFTXT=DEFTXT+chr(21)+chr(2)+"~~~~"+chr(2)+"~~~~"
		DEFTXT=DEFTXT+"Let's insert a few lines..."+chr(10)
		DEFTXT=DEFTXT+chr(2)+"~~~~"+chr(2)+"~~~~"+chr(11)
		DEFTXT=DEFTXT+"Now i~t~'~s~ ~~y~~o~~u~~r~~ ~~~t~~~u~~~r~~~n! "
		VWIDTH = 100
		GOTO A4
A3N:
	? tab(24,1);tab(-1,9);"Disable horizontal scrolling for better word wrap? ";
	xcall INFLD,24,54,1,1,"YYF<",SC$,INXCTL,1
	if SC$="Y" VWIDTH=65 else VWIDTH=100
	? tab(24,1);tab(-1,9);

A4:
	? tab(23,1);tab(-1,9); : input "EXTPRW: ",EXTPRW

	CALL PT4


ADDC:
!	XCALL ANYCN,CNGCTL,WHATNO
!	IF CNGCTL<1 GOTO ADDX

	? tab(24,1);tab(-1,9);"ANY CHANGE? ";		! This code replaces
	XCALL INFLD,24,20,1,0,"YN",A$,INXCTL,1		! the action of 
	IF A$="N" GOTO ADDX				! ANYCN.SBR which
							! could not be
	? tab(24,25);"WHICH? ";				! distributed due
	XCALL INFLD,24,35,1,0,"#E",A$,INXCTL,1		! to licensing
	if INXCTL=2 goto ADDC				! restrictions with
	WHATNO = val(A$)				! AlphaMicro

		ON WHATNO CALL PT1,PT2,PT3,PT4
		GOTO ADDC

ADDX:
	OLINK = PLINK
	WRITE #16, PRIREC
	GOTO ADD2

!----------------------------------------------------------------------

CHANGE:

	xcall MIAMEX,65,1		! append screen snapshot to jobnam.BUF

	? TAB(2,1);TAB(-1,10);"CHANGE"
	CALL SCREEN
CNG2:
	CALL CLEAR'DATA
	CALL WHICH : ON INXCTL GOTO START,START
	IF PRIREC[1,3]="]]]" THEN &
		MESAG = "TABLE "+FILE16+" NOT WRITTEN YET" : &
		CALL MESAG : &
		GOTO CNG2

	CALL DISPLAY'DATA
CNGC:

        A$ = "N"
	? tab(24,1);tab(-1,9);tab(-1,12);"ANY CHANGE? ";! This code replaces
	XCALL INFLD,24,20,1,0,"a^#",A$,INXCTL,1,1	! the action of 
	IF A$="N" GOTO CNGX 				! ANYCN.SBR which
	if val(A$)>0 goto CNGC2				! could not be
	? tab(24,25);"WHICH? ";				! distributed due
	XCALL INFLD,24,35,1,0,"#E",A$,INXCTL,1		! to licensing
	if INXCTL=2 goto CNGC				! restrictions with
CNGC2:
	WHATNO = val(A$)				! AlphaMicro

if WHATNO#4 then goto CNGX0

? tab(23,1);tab(-1,9); : input "EXTPRW: ",EXTPRW
? TAB(23,16); : input "EXTPCL: ",EXTPCL
? TAB(23,27); : input "Enter DEFTXT: ",DEFTXT
if DEFTXT#"" and DEFTXT[1,1]#"~" then DEFTXT="~"+DEFTXT

! Scan DEFTXT for ^ and replace with control chars
DFTLOOP:
	X = instr(1,DEFTXT,"^")
	if X > 0 then &
		DEFTXT = DEFTXT[1,X-1] + chr(asc(DEFTXT[X+1;1])-64) &
			+ DEFTXT[X+2,-1] : goto DFTLOOP

	VWIDTH = 0
CNGX0:
		ON WHATNO CALL PT1,PT2,PT3,PT4
		GOTO CNGC

CNGX:
	OLINK = PLINK
	WRITE #16, PRIREC
	GOTO CNG2
	
!-------------------------------------------------------------------------

DELETE:
	xcall MIAMEX,65,1		! append screen snapshot to jobnam.BUF

	? TAB(2,1);TAB(-1,10);"DELETE"

	CALL SCREEN
DEL2:
	CALL CLEAR'DATA
	CALL WHICH : ON INXCTL GOTO START,START
	IF PRIREC[1,3]="]]]" THEN &
		MESAG = "TABLE "+FILE16+" NOT WRITTEN YET" : &
		CALL MESAG : &
		GOTO DEL2

	CALL DISPLAY'DATA

DELX:
	? TAB(24,1);"DELETE THIS DATA RECORD? ";
	XCALL INFLD,24,40,1,1,"YN",ENTRY,INXCTL,1
	IF ENTRY<>"Y" RETURN

DELY:
	? TAB(24,50);"SURE? ";
	XCALL INFLD,24,60,1,1,"YN",ENTRY,INXCTL,1
	IF ENTRY<>"Y" RETURN

	XCALL INMEMO,MMO'DEL,"",MCH,0,0,0,0,PLINK,XPOS		! Delete memo pad
	if POS<>0 call SHOW'POS					! [9]
	? tab(23,1);"Returned link = ";PLINK
	WRITE #16, BRACKS
	MESAG = "TABLE "+FILE16+" DELETED "
	CALL MESAG
	GOTO DEL2

!------------------------------------------------------------------------

WHICH:
	? tab(23,1);tab(-1,9);
	XCALL INFLD,6,30,3,1,"#E?",ENTRY,INXCTL,1,0,EXITCODE,TIMER,&
		CMDFLG,DEFPT,MAXPT,FUNMAP,SETDEF,INFCLR,HLPIDX

	IF INXCTL>1 RETURN

	HELP'ROW = 15
	HELP'COL = 9

	if EXITCODE=8 then &
		WHELP'FLAG=1 : HELP'SUB$="TSTMMO Rec #" : call HELP &
		: goto WHICH

	FILE16 = ENTRY
	IF FILE16<1 OR FILE16>MAXREC16 THEN &
		MESAG = "MUST BE BETWEEN 1 AND "+MAXREC16 : &
		CALL MESAG : &
		GOTO WHICH

	READL #16, PRIREC
	PLINK = OLINK
	RETURN

!-----------------------------------------------------------------------

SCREEN:

	? TAB(6,5);TAB(-1,10);
	? TAB(6,5); "Record # ";
	? TAB(8,10); "1.  Description:"
	? TAB(10,10);"2.  Department "
	? TAB(12,10);"3.  Taxable? (Y/N)"
	? TAB(23,1);"Link:"

			! display just the a border (no text)		! [14]
	OPCODE=MMO'BDR+MMO'LID		! LID flag speeds up clear op too
	PLINK=0								! [14]

	XCALL INMEMO,OPCODE,PROMPT$,MCH,STRW,STCL,ENDRW,ENDCL, &
		PLINK,XPOS,VSPEC,MMOCLR,EXTCTL				! [14]
	RETURN

!------------------------------------------------------------------------

CLEAR'DATA:
	? TAB(6,30);TAB(-1,9);
	FOR ROW = 8 TO 12 STEP 2
		? TAB(ROW,40);TAB(-1,9);
	NEXT ROW	
	RETURN

!------------------------------------------------------------------------

DISPLAY'DATA:
!	? tab(STRW-1,1);tab(-1,10);
	? TAB(8,40);PTDSCR
	? TAB(10,40);str(PTDEPT);tab(-1,9);

	MNU'SEL$=""
	if PTDEPT>0 then MNU'SEL$ = PTDEPT using "##Z"
	if MNU'SEL$#"" call DO'TABLE		! look-up selection
	? tab(10,44);MNU'SEL$		        ! display description

	? TAB(12,40);PTAX
	? TAB(23,1);"Link: ";PLINK

	VWIDTH = 100

			! 
 	OPCODE = MMO'DSP+MMO'BDR+MMO'NBR ! [14] don't redisplay border
!	OPCODE = MMO'DSP+MMO'BDR+MMO'AAH
	if WHELP'FLAG=1 OPCODE = OPCODE-MMO'NBR

![18] Notes on border and prompts: 
![18] MMO'BDR+MMO'NBR tells INMEMO there is a border, but don't redraw it
![18] However, MMO'DPG wants to display a standard prompt on the bottom
![18] ("Use arrows to scroll, ESC to exit, etc.")
![18] You can't complete eliminate that message but you can replace it
![18] (including with a single space) by supplying a prompt with the 
![18] desired new message following a chr(13). 
![18] Example:
     OPCODE = OPCODE + MMO'DPG + MMO'DBM ! + MMO'NOA
      PROMPT$ = "Top"+chr(13)+" ~"  ! Replace bottom prompt with " "
![18] (Note: "top" won't appear because of MMO'NBR
! OPCODE = OPCODE AND NOT MMO'BDR

	XCALL INMEMO,OPCODE,PROMPT$,MCH,STRW,STCL,ENDRW,ENDCL, &
		PLINK,XPOS,VSPEC,MMOCLR,EXTCTL
	if POS<>0 call SHOW'POS						! [9]
	WHELP'FLAG = 0
	RETURN							

!---------------------------------------------------------------------------

PT1:
	XCALL INFLD,8,40,28,0,"A2",PTDSCR,INXCTL,1,1,EXITCODE,TIMER,&
		CMDFLG,DEFPT,MAXPT,FUNMAP,SETDEF,INFCLR,HLPIDX

	RETURN

PT2:
	MNU'TTL$ = DMNU'TTL$
	MNU'SEL$ = PTDEPT using "#ZZ"		! preload default
	if PTDEPT=0 then MNU'SEL$=""	        ! no default

	call DO'MENU				! display/select from menu

	PTDEPT = val(MNU'SEL$)			! save selection
	? tab(10,40);tab(-1,9);MNU'SEL$		! display selection
	if MNU'SEL$#"" call DO'TABLE		! re-look-up selection
	? tab(10,44);MNU'SEL$		        ! display description
	RETURN

PT3:
	XCALL INFLD,12,40,1,1,"YY23",PTAX,INXCTL,1,1,EXITCODE,TIMER,&
		CMDFLG,DEFPT,MAXPT,FUNMAP,SETDEF,INFCLR,HLPIDX
	RETURN

PT4:
	OPCODE=MMO'EDT+MMO'BDR+MMO'LID	!+MMO'AAH
!	OPCODE=OPCODE + MMO'CXY
!	EXTROW = 3
!	EXTCOL = 5

PT4A:

!OPCODE=MMO'EDT+MMO'SIL
!	if SELECT<>1 then &
!		OPCODE = OPCODE + MMO'NMR		! no border or text
	
							! redisplay on edit
!	DEFTXT = PROMPT$ + chr(13) + "Custom bottom prompt~>>" + DEFTXT
        EXTMAP = -1                               ! [16] enable all fun keys

!? tab (16,2);"***TEXT = ";PROMPT$;
!? tab(20,3);"STROW , STCL, ENDRW, ENDCL = ";STRW;STCL;ENDRW;ENDCL;
!? tab(21,3);"VWIDTH, VROWS =";VWIDTH;VROWS;
!stop
!VWIDTH = 1
!VROWS = 1
!EXTOTH=-1	! enable alternate exit codes

TFCLR = 0
TBCLR = 1
!OPCODE = OPCODE + MMO'SVA+MMO'RSA
!TPRINT TAB(STRW+2,1);"Can you see me now?"
?tab(-1,121);  ! steady block
	XCALL INMEMO,OPCODE,DEFTXT,MCH,STRW,STCL,ENDRW,ENDCL,PLINK, &
		ZPOS,VSPEC,MMOCLR,EXTCTL
!? tab(20,3);"POS = ";POS
!stop
	if POS<>0 call SHOW'POS						! [9]

	! Convert binary EXTOD to signed floating pt [16]
	if EXTCOD>30000 then EXTFLT = EXTCOD - 65536 else EXTFLT = EXTCOD

	? TAB(23,1);"LINK:";PLINK;" POS:";POS;"EXTCOD:";EXTFLT;
	? "EXTERR:";EXTERR;"ROW:";EXTROW;"COL:";EXTCOL;"Size:";
	? EXTHIT;"x";EXTWID;"(";str(EXTBYT);" bytes)"
	DEFTXT=""				! eliminate default text

	RETURN	

!-----------------------------------------------------------------------

PRINT:

	? TAB(2,1);TAB(-1,10);"PRINT "
	
	OPEN #14,"INMEMO.PRT",OUTPUT
	? TAB(9,5);"Printing";
	  ? #14,TAB(5);"PRODUCT DEFINITION FILE LISTING"
	  ? #14
	  ? #14
	  ? #14

	FOR FILE16 = 1 TO MAXREC16

	  READL #16, PRIREC
	  IF PRIREC[1,3]="]]]" THEN GOTO PNXT

	  PLINK = OLINK
	  ? TAB(9,16);FILE16
	  ? #14,TAB(5);FILE16 USING "##.";"  ";PTDSCR;TAB(45);
	  ? #14,"DEPARTMENT: ";PTDEPT;TAB(65);	  
	  IF PTAX="N" THEN ? #14,"NON-";
	  ? #14,"TAXABLE"
	  ? #14
	  ? #14, TAB(10);"Comments: "
	  ? #14  	  		
	  POS=0
INMEMO'LOOP:
	  XCALL INMEMO,MMO'LIN,PLINE,MCH,0,0,0,0,PLINK,XPOS,VSPEC,MMOCLR,EXTCTL
	  IF POS=0 GOTO PNXT
		if POS=MMO'LKE PLINE = PLINE + "[Illegal Link]"
IF EXTERR>0 ? "EXTERR: ";EXTERR : STOP
		? #14, TAB(10);PLINE
		GOTO INMEMO'LOOP

PNXT:
	  ? #14
	NEXT FILE16

	CLOSE #14
!	XCALL SPOOL,"INMEMO.PRT"
	XCALL EZTYP,"INMEMO.PRT"
	RETURN

!-----------------------------------------------------------------------

MESAG:
	MESAG = MESAG + "  "
	? TAB(24,1);TAB(-1,9);
!	XCALL MESAG,MESAG,2
	? chr(7);
	? tab(24,1);tab(-1,9);tab(-1,12);MESAG;tab(24,57);
	? "Hit RETURN to recover:";
	xcall INFLD,24,79,1,0,"C",A$,INXCTL,1
	? tab(24,1);tab(-1,9);
	RETURN

!----------------------------------------------------------------------

EXPAND:

	? TAB(2,1);TAB(-1,10);"EXPAND";

	? tab(5,5);"File currently has ";MAXREC16;" records"
	? tab(7,5);"How many do you want to expand it to? "
	XCALL INFLD,7,60,3,1,"#E",ENTRY,INXCTL,1
	IF INXCTL>0 RETURN

	NEWMAX = ENTRY
	IF NEWMAX<MAXREC16 THEN &
		MESAG = "Shrinking the file may cause some recs to be lost" : &
		CALL MESAG

!	CNGCTL=2
!	XCALL ANYCN,CNGCTL,WHATNO

	? tab(24,1);tab(-1,9);tab(-1,12);"ANY CHANGE? ";
	XCALL INFLD,24,20,1,0,"YN",A$,INXCTL,1
	IF A$="Y" GOTO EXPAND

	LOOKUP "INMEMO.TMP",EXISTS
	IF EXISTS<>0 KILL "INMEMO.TMP"


	SIZE = INT(NEWMAX / INT(512/RECSIZ16)) + 1
	NEWMAXREC16 = SIZE * INT(512/RECSIZ16)
	? tab(10,5);"PROCESSING";
	ALLOCATE "INMEMO.TMP",SIZE
	OPEN #11,"INMEMO.TMP",RANDOM,RECSIZ16,FILE16

	FOR FILE16=1 TO NEWMAXREC16
		IF FILE16<=MAXREC16 THEN &
			READL #16, PRIREC &
		ELSE &
			PRIREC = BRACKS

		WRITE #11, PRIREC
		? ".";
	NEXT FILE16

	CLOSE #11
	CLOSE #16
	OLDFIL$="INMEMO.DAT"
	NEWFIL$="INMEMO.OLD"
	LOOKUP NEWFIL$,EXISTS
	IF EXISTS<>0 KILL NEWFIL$
	!XCALL RENAM,OLDFIL$,NEWFIL$,FLAG
	NEWFIL$="INMEMO.TMP"
	!XCALL RENAM,NEWFIL$,OLDFIL$,FLAG
	IF FLAG<>0 THEN &
		MESAG = "CANNOT COMPLETE RENAME OPERATION" : &
		CALL MESAG : &
		NEWFIL$="INMEMO.OLD" : &
		!XCALL RENAM,NEWFIL$,OLDFIL$,FLAG 

	CLOSE #MCH					! [7]
	LOOKUP "INMEMO.DAT",EXISTS
	CALL OPEN
	RETURN	
		
!----------------------------------------------------------------------

SEARCH:		! This routine demonstates a search of the entire memo
		! file using the standard wildcards and searchfold.

	? tab(2,1);tab(-1,10);"SEARCH";tab(-1,11);

	? tab(4,5);"'?' Char:    '*' Char:    AND Char:    OR Char:    Delimiter:     Fold?"
	? tab(5,5);"'#' Char:    '@' Char:"
	S'1WILD$ = "?"
	S'AST$ = "*"
	S'AND$ = "&"
	S'OR$ = "!"
	S'LIMIT$ = "|"
	S'FOLD$ = "Y"
	S'ALP$ = "@"
	S'NUM$ = "#"

	? tab(23,1);"Use ^T to accept existing defaults, back arrow to back up."
	
SR1:
	xcall INFLD,4,15,1,1,"*16?]",S'1WILD$,INXCTL,1,1,EXITCODE
	if EXITCODE=8 call HLP1 : goto SR1
	if HELP'FLAG=1 then call CLEAR'HELP 
	if EXITCODE=6 goto SR7
	if EXITCODE=1 return
SR2:
	xcall INFLD,4,28,1,1,"*26?]",S'AST$,INXCTL,1,1,EXITCODE
	if EXITCODE=8 call HLP2 : goto SR2
	if HELP'FLAG=1 then call CLEAR'HELP 
	if EXITCODE=6 goto SR7
	if EXITCODE=2 goto SR1	
SR3:
	xcall INFLD,4,41,1,1,"*26?]",S'AND$,INXCTL,1,1,EXITCODE
	if EXITCODE=8 call HLP3 : goto SR3
	if HELP'FLAG=1 then call CLEAR'HELP 
	if EXITCODE=6 goto SR7
	if EXITCODE=2 goto SR2	
SR4:
	xcall INFLD,4,53,1,1,"*26?]",S'OR$,INXCTL,1,1,EXITCODE
	if EXITCODE=8 call HLP4 : goto SR4
	if HELP'FLAG=1 then call CLEAR'HELP 
	if EXITCODE=6 goto SR7
	if EXITCODE=2 goto SR3	
SR5:
	xcall INFLD,4,67,1,1,"*26?]",S'LIMIT$,INXCTL,1,1,EXITCODE
	if EXITCODE=8 call HLP5 : goto SR5
	if HELP'FLAG=1 then call CLEAR'HELP 
	if EXITCODE=6 goto SR7
	if EXITCODE=2 goto SR4	
SR6:
	xcall INFLD,4,78,1,1,"*26?]",S'FOLD$,INXCTL,1,1,EXITCODE
	if EXITCODE=8 call HLP6 : goto SR6
	if HELP'FLAG=1 then call CLEAR'HELP 
	if EXITCODE=6 goto SR7
	if EXITCODE=2 goto SR5	
SR7:
	xcall INFLD,5,15,1,1,"*26?]",S'ALP$,INXCTL,1,1,EXITCODE
	if EXITCODE=8 call HLP7 : goto SR7
	if HELP'FLAG=1 then call CLEAR'HELP 
	if EXITCODE=6 goto SR8
	if EXITCODE=2 goto SR6
SR8:
	xcall INFLD,5,28,1,1,"*26?]",S'NUM$,INXCTL,1,1,EXITCODE
	if EXITCODE=8 call HLP8 : goto SR8
	if HELP'FLAG=1 then call CLEAR'HELP 
	if EXITCODE=6 goto SR8
	if EXITCODE=2 goto SR7	
SR9:
	? tab(6,5);tab(-1,11);"Enter search pattern: ";
	xcall INFLD,6,40,30,0,"A23?]",PATTERN,INXCTL,1,1,EXITCODE
	if EXITCODE=2 or EXITCODE=3 goto SR1
	if EXITCODE=8 call HLP9 : goto SR9
	if HELP'FLAG=1 then call CLEAR'HELP 

	? tab(7,1);tab(-1,9);"Searching for '";PATTERN;"'...";tab(-1,10);
	?
 	if S'FOLD$="Y" PATTERN = "^" + PATTERN else PATTERN = " " + PATTERN
	PATTERN = "  " + PATTERN			! reserved chars
	PATTERN = S'1WILD$+S'AST$+S'LIMIT$+S'AND$+S'OR$+S'ALP$+S'NUM$+PATTERN
	SCOUNT = 0
SLOOP:	

	FOR FILE16 = 1 TO MAXREC16

	  READL #16, PRIREC
	  IF PRIREC[1,3]="]]]" THEN GOTO SNXT
	  PLINK = OLINK
	  ? chr(13);
	  ? FILE16;tab(-1,9);" ";
	  XCALL INMEMO,MMO'SCH,PATTERN,MCH,0,0,0,0,PLINK,XPOS
	  IF POS=0 ? chr(13);tab(-1,9); : GOTO SNXT

		? PTDSCR
		SCOUNT = SCOUNT + 1

SNXT:
		UNLOKR #16
	NEXT FILE16

	?
	?
	? SCOUNT;"occurances located.  Hit any key to recover ";
	xcall INFLD,0,50,1,0,"@",A$,INXCTL,1
	PATTERN=""

	return

!--------------------------------------------------------------------------

HLP1:				
	HELP'SUB$ = "DMPMMO '?' Wildcard"
	HELP'ROW = 8
	HELP'COL = 2
	call HELP
	goto CRCONT

HLP2:
	HELP'SUB$ = "DMPMMO '*' Wildcard"
	HELP'ROW = 10
	HELP'COL = 4
	call HELP

  	goto CRCONT	

HLP3:
	HELP'SUB$ = "DMPMMO AND Operator"
	HELP'ROW = 12
	HELP'COL = 6
	call HELP

 	goto CRCONT	

HLP4:
	HELP'SUB$ = "DMPMMO OR Operator"
	HELP'ROW = 13
	HELP'COL = 8
	call HELP

	goto CRCONT	

HLP5:
	HELP'SUB$ = "DMPMMO Delimiter"
	HELP'ROW = 14
	HELP'COL = 10
	call HELP

	goto CRCONT	

HLP6:
	HELP'SUB$ = "DMPMMO Searchfold"
	HELP'ROW = 15
	HELP'COL = 12
	call HELP

	goto CRCONT	

HLP7:
	HELP'SUB$ = "DMPMMO '@' Wildcard"
	HELP'ROW = 16
	HELP'COL = 14
	call HELP

	goto CRCONT	

HLP8:
	HELP'SUB$ = "DMPMMO '#' Wildcard"
	HELP'ROW = 17
	HELP'COL = 16
	call HELP

	goto CRCONT	
HLP9:
	HELP'SUB$ = "DMPMMO Search Pattern"
	HELP'ROW = 18
	HELP'COL = 18
	call HELP

	goto CRCONT	

CLEAR'HELP:
	![12] Leave help on screen just for the hell of it	
![12]	for ROW = 15 to 22
![12]		? tab(ROW,1);tab(-1,9);
![12]	next ROW
	HELP'FLAG = 0
	return

CRCONT:
	HELP'FLAG=1
	return

SHOW'POS:							! [9]
	if POS=MMO'FUL then &
		MESAG = "(POS="+str(POS)+") Use '.EXPMMO' to expand memo file!" : &
		call MESAG : &
		END	
	if POS=MMO'LKE then &
		MESAG = "(POS="+str(POS)+") INMEMO Link Error!" : &
		call MESAG : &
		return

		MESAG = "(POS="+str(POS)+") Illegal return value of POS!"
		call MESAG
		END

!-----------------------------------------------------------------------

HELP:		! [12] Display help information if available using INMEMO.
		! [12] The help text has been pre-typed into HLPMMO.MMO.

	if HELP'FILE'OPEN=0 call OPEN'MEMO'IDX

	FILE100 = 0

HELP2:
	FILE100 = FILE100 + 1	
	if FILE100>MAX100 goto NO'HELP2

	read #100, HELP'REC
	if HELP'DESCR[1,3]="]]]" goto NO'HELP2
	if ucs(HELP'DESCR)<>ucs(HELP'SUB$) goto HELP2
	HELP'EROW = (HELP'ROW+4) min 21

        HPOS=0
	xcall INMEMO,MMO'DPG+MMO'BDR+MMO'IPG,HELP'SUB$[8,-1],HMCH,HELP'ROW,HELP'COL, &
		HELP'EROW,HELP'COL+60,HELP'LINK,HPOS,HVSPEC,MMOCLR,EXTCTL
	
	if HPOS # 0 then &
		POS = HPOS : call SHOW'POS 		! [16]
	return

NO'HELP2:
	if NO'HELP'FLAG=1 return				! [112]
	? tab(24,1);tab(-1,9);"Help memo '";HELP'SUB$;"' not found. (hit RETURN):";
	xcall INFLD,24,77,1,0,"@\",A$,INXCTL,1
	return


!--------------------------------------------------------------------------

PHDR:					! display standard program
					! header at (2,1) in AMOSL format

	? tab(2,1);tab(-1,11);"Rev. ";
	? str(VMAJOR);".";str(VMINOR);
	if VSUB<>0 then ? "-";chr(VSUB+96);
	if VEDIT<>0 then ? "(";str(VEDIT);")";
	if VPATCH<>0 then ? "-";str(VPATCH);
	return

!-------------------------------------------------------------------------

OPEN'MEMO'IDX:
	on error goto HELP'TRAP
	
	lookup HELP'DAT$,MAX100
	if MAX100=0 goto NO'HELP
	MAX100 = abs(MAX100) * int(512/RECSIZ100)               ! [17]

	lookup HELP'MMO$,MAX101
	if MAX101=0 goto NO'HELP				
	MAX101 = abs(MAX101) * 8                                 ! [17]

	open #HDCH, HELP'DAT$, random'forced, RECSIZ100, FILE100
	open #HMCH, HELP'MMO$, random'forced, 64, FILE101

	on error goto TRAP

	HELP'FILE'OPEN=1
	return

NO'HELP:
	? tab(24,1);tab(-1,9);"Sorry, help files '";HELP'MMO$;"' (or .DAT) not found   (Hit RETURN): ";
	xcall INFLD,24,77,1,0,"@\",A$,INXCTL,1
	return


HELP'TRAP:		! [112] trap filespec errors caused by ersatz account
			! [112] 'INMEMO:' not being defined.  

	if err(0)<>16 goto TRAP		! not the error we expected!
	
	NO'HELP'FLAG=1
	resume NO'HELP


!-------------------------------------------------------------------------

GET'MENU:			! display a pop-up menu
				! Input: MNU'TTL$; Output: HELP'LINK

	HELP'LINK = 0
	if HELP'FILE'OPEN=1 goto MENU'START
	
	call OPEN'MEMO'IDX

MENU'START:

	FILE100 = 45		! we happen to know this already

MENU2:
	FILE100 = FILE100 + 1	
	if FILE100>MAX100 goto NO'MENU2

	read #100, HELP'REC
	if HELP'DESCR[1,3]="]]]" goto NO'MENU2
	if ucs(HELP'DESCR)<>ucs(MNU'TTL$) goto MENU2


	
MENU'EXIT:
	return

NO'MENU:
	? tab(24,1);tab(-1,9);"Sorry, menu files '";HELP'MMO$;"' (or .DAT) not found   (Hit RETURN): ";
	xcall INFLD,24,77,1,0,"@\",A$,INXCTL,1
	return
NO'MENU2:
	if NO'HELP'FLAG=1 return				! [112]
	? tab(24,1);tab(-1,9);"Help menu '";MNU'TTL$;"' not found. (hit RETURN):";
	xcall INFLD,24,77,1,0,"@\",A$,INXCTL,1
	return

!-----------------------------------------------------------------------
! This routine displays a memo-based menu and inputs a selection.  
! (Call GET'MENU to get the link from the menu name)
! Inputs: MNU'TTL$ = menu title
!	  MNU'SEL$ = default abreviated menu selection 
! Outputs:MNU'SEL$ = the new abbreviated menu sel (null if ESC)
!-----------------------------------------------------------------------

DO'MENU:			

	? tab(24,1);tab(-1,9);"ttl: ";MNU'TTL$;"/  sel: ";MNU'SEL$;
	MNU'SEL$ = MNU'TTL$+"~"+ETX$+MNU'SEL$

	xcall INMEMO,MMO'MNU+MMO'BDR,MNU'SEL$,HMCH,MNU'SROW,MNU'SCOL, &
		MNU'EROW,MNU'ECOL,MNU'LINK,HPOS,HVSPEC,MMOCLR,EXTCTL

	X = instr(1,MNU'SEL$,"-")
	if X>0 then MNU'SEL$ = MNU'SEL$[1,X-1]	! return just the key

		! Clear the menu from the screen
	
	for I = MNU'SROW-1 to MNU'EROW+1
		? tab(I,MNU'SCOL-1);tab(-1,9);
	next I
	return

!-----------------------------------------------------------------------
! This routine takes a menu selection abbreviation and looks up and returns
! the associated description.
! Inputs: MNU'SEL$ = menu abbreviated selection
! Outputs:MNU'SEL$ = expanded selection (description) (or blank)
!-----------------------------------------------------------------------

DO'TABLE:
               ! Note that the MMO'TBL and MMO'SCH opcodes require the
               ! lookup pattern to be preceded by 10 special characters
               ! which define various wildcards, etc.  Since we aren't
               ! using these, we set them all to the same character (\)
               ! except for #8 which is the delimiter separating 
               ! logical items (we are using the end of line as our
               ! item delimiter.)  Our table entries have the format:
               !    key-description
               ! so we append a dash to the end of the key
               ! to make sure we don't find the key in the middle of 
               ! a description.

	MNU'SEL$ = "\\\\\\\  \"+MNU'SEL$+"-"

	xcall INMEMO,MMO'TBL+MMO'SCH,MNU'SEL$,HMCH,MNU'SROW,MNU'SCOL, &
		MNU'EROW,MNU'ECOL,MNU'LINK,HPOS
               ! [16] HPOS > 0 is normal for this call, but we should
               ! [16] still check for link errors
	if HPOS = MMO'LKE then &
          POS=HPOS : call SHOW'POS : MNU'SEL$="<Table Lookup Error>" ! [16]
	return

