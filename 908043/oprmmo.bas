!	OPRMMO.BAS
!
!	Copyright (c) MicroSabio 1983,1984
!	Written by Jack McGregor
!------------------------------------------------------------------------
!	Program to display usage information about a memo file (within
!	the INMEMO system)
!------------------------------------------------------------------------
! USAGE:
!	.OPRMMO		(via OPRMMO.CMD:
!
!					LOAD BAS:INFLD.SBR
!					RUN INMEMO:OPRMMO
!-------------------------------------------------------------------------
! EDIT HISTORY:
!	1.  12-20-83  [1.0]  Created /jdm
!	2.  04-13-84  [1.0a] Include deleted recs in total # free recs /jdm
!	3.  07-01-84  [1.1]  Handle both 2-byte & 4-byte format /jdm
!	5.  09-24-86  [1.2]  Read file name from command file /jdm
!	4.  04-21-86  [1.3]  Use INFLD instead of INFLD, PROGRAM stmt/jdm
!	5.  11-18-86  [1.3a] Report illegal del link error /jdm
!	6.  02-10-89  [1.3b] Allow larger numbers in mask /jdm
!	7.  11-22-89  [2.0]  Add color /jdm
!       8.  07-24-90  [2.1]  Minor adjustments for non-AMOS /jdm
!	9.  01-07-91  [2.1]  Fix param error in xcall infld /jdm
!      10.  02-19-92  [2.2a] More adjustments for non-amos /gb
!------------------------------------------------------------------------
! EXTERNAL SUBROUTINES:	
!	INFLD.SBR
!-------------------------------------------------------------------------		
! ALGORITHM NOTES:
!	1.  Ask for the name of the memo file.
!	2.  Lookup file to determine maximum # recs.
!	3.  Read control record to determine format.
!	4.  Scan delete chain to determine # of deleted records.
!-------------------------------------------------------------------------

	PROGRAM OPRMMO,2.2a(10)
	
MAP1 PHDR
	MAP2 VMAJOR,B,1,2
	MAP2 VMINOR,B,1,0
	MAP2 VSUB,B,1,0
	MAP2 VEDIT,B,1,9
	MAP2 VPATCH,B,1,0

MAP1 CTL'REC2
	MAP2 MARKER2,S,6
	MAP2 NXT'FRE2,B,2
	MAP2 NXT'DEL2,B,2
	MAP2 FILL2,S,54
MAP1 CTL'REC4,@CTL'REC2
	MAP2 MARKER4,S,6	
	MAP2 NXT'FRE4,B,4
	MAP2 NXT'DEL4,B,4
	MAP2 FLAG4,S,1
	MAP2 FILL4,S,49

MAP1 BRACKS,S,64,"]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]"

MAP1 MEMO$,S,24

MAP1 BSWAP,B,4
MAP1 BSWPX,@BSWAP
	MAP2 BS1,B,2
	MAP2 BS2,B,2
MAP1 BX2,B,2

MAP1 MISC
	MAP2 DEL'CNT,F,6
	MAP2 EXISTS,F,6
	MAP2 X,F,6
	MAP2 DOTCNT,F,6
	MAP2 DC,F,6
	MAP2 USE'CNT,F,6
	MAP2 FREE'CNT,F,6
	MAP2 RECCNT,F,6
	MAP2 LFMT,F,6
	MAP2 FILE1,F,6
	MAP2 A$,S,10
	MAP2 FREE'PTR,F,6
++include infpar.bsi		! [7] infld parameters
++include mmopar.bsi		! [7] inmemo parameters

	TIMER = 90			! 90 sec timeout
	CMDFLG = 1			! input from command file
!------------------------------------------------------------------------

	on error goto TRAP
	filebase 1

++include getclr.bsi		! [7] setup color

START:
	? tab(-1,0);"OPRMMO - Show Memo File Usage Information";
	? tab(-1,11);
	? tab(1,62);"Copyright (c) 1984";
	? tab(2,62);"By MicroSabio"

	call PHDR

	? tab(6,5); "Enter Name of Memo File: "

	? tab(10,5); "Memo Link Format: "
	? tab(11,5); "Total # of Records in Memo File: "
	? tab(12,5); "Total # of Records in Use:"
	? tab(13,5); "Total # of Deleted Records:"
	? tab(14,5); "Total # of Free Records:"

F1:	
	DEL'CNT = 0
	xcall INFLD,6,45,24,1,"A^]E",MEMO$,INXCTL,1,1,EXITCODE,TIMER, &
		CMDFLG,DEFPT,MAXPT,FUNMAP,SETDEF,INFCLR,HLPIDX
	if EXITCODE=11 goto TIME'OUT
	if INXCTL=2 goto EXIT

	X = instr(1,MEMO$,".")
	if X<1 then MEMO$=MEMO$+".MMO" : ? tab(6,45);MEMO$

	? tab(8,5);tab(-1,9);
	? tab(22,1);tab(-1,10);

	lookup MEMO$,EXISTS
	if abs(EXISTS) <> 0 goto READ'MEMO	! [10]

	? tab(8,5);MEMO$;" not found!"
	goto F1

READ'MEMO:
	RECCNT = abs(EXISTS) * 8                       ![8]
	? tab(11,44);RECCNT using "######";		! [6]

	open #1, MEMO$, random, 64, FILE1
	FILE1 = 1
	read #1, CTL'REC2

	if FLAG4="4" then LFMT=4 else LFMT=2
	? tab(10,48);tab(-1,12);LFMT;tab(-1,11);"byte";tab(-1,12)

	if LFMT=2 then &
		FREE'PTR = NXT'FRE2 &
	else &
		BSWAP = NXT'FRE4 : &
		call BSWAP : &
		FREE'PTR = BSWAP

	if FREE'PTR=0 then FREE'PTR = RECCNT + 1	

READ'DEL:	
	? tab(22,1);tab(-1,9);"Reading deleted chain";

	DOTCNT = FREE'PTR/100

DEL'LOOP:
	if LFMT=2 then &
		FILE1 = NXT'DEL2 &
	else &
		BSWAP = NXT'DEL4 : &
		call BSWAP : &
		FILE1 = BSWAP

	if FILE1=0 goto SHOW'STATS
		
		read #1, CTL'REC2
		if MARKER2<>"]]]DEL" then goto ILLEGAL'DEL'LINK	  	! [5]

		DC = DC + 1
		if DC>DOTCNT then DC=0 : ? ".";
		DEL'CNT = DEL'CNT + 1
		goto DEL'LOOP

SHOW'STATS:

	? tab(22,1);tab(-1,9);

	USE'CNT = FREE'PTR - 1 - DEL'CNT
	FREE'CNT = RECCNT - USE'CNT		! - DEL'CNT [1]

	? tab(12,44);USE'CNT using "######";
	? tab(13,44);DEL'CNT using "######";
	? tab(14,44);FREE'CNT using "######";

!	MEMO$ = ""			! clear file name default
AGAIN:
	close #1
	goto F1

ILLEGAL'DEL'LINK:					! [5]
	? tab(23,1);tab(-1,10);"--------------------------------------------------------------------"	
	? "Non deleted record in delete chain!  - Use ANAMMO or FIXMMO!! - Hit CR: ";
	CMDFLG = 0					! no cmd file input
	xcall INFLD,24,78,1,0,"C\",A$,INXCTL,1,0,EXITCODE,TIMER,CMDFLG,&
		DEFPT,MAXPT,FUNMAP,SETDEF,INFCLR,HLPIDX
	goto SHOW'STATS


TRAP:
	if ERR(0)=1 then goto EXIT
	? tab(23,1);tab(-1,10);"----------------------------------------------------------------------------"
	? tab(24,1);
	TIMER=0			! Turn time out off to make sure they have
				! time to study the error message.
? "ERROR #";ERR(0);"      "
	if ERR(0)=12 then ? "INFLD.SBR not found"; : goto ENDIT
	if ERR(0)=16 or ERR(0)=32 then &
		? "File specification error"; : resume F1
	if ERR(0)=18 then ? "Device not ready"; : resume F1
	if ERR(0)=22 then ? "Illegal user code - PPN not found!"; : resume F1
	if ERR(0)=23 then ? "Protection violation!"; : resume AGAIN
	if ERR(0)=26 then ? "Device does not exist!"; : resume F1
	if ERR(0)=27 then ? "Bitmap kaput - see system operator!"; : goto F1
	if ERR(0)=28 then ? "Disk not mounted!"; : goto F1
	if ERR(0)=31 then ? "Illegal link in deleted record chain!"; : goto AGAIN
	if ERR(0)=36 then ? "Improper version of INFLD.SBR"; : goto ENDIT
	on error goto
	END



EXIT:
	? tab(-1,0);"Program end"

ENDIT:
	END

TIME'OUT:
	? tab(-1,0);"Program timed out."
	goto ENDIT

!-------------------------------------------------------------------------

BSWAP:
	BX2 = BS1		! from AM100L format
	BS1 = BS2		! to BASIC format					
	BS2 = BX2
	return

!-----------------------------------------------------------------------

PHDR:					! display standard program
					! header at (2,1) in AMOSL format

	? tab(2,1);tab(-1,11);"Rev. ";
	? str(VMAJOR);".";str(VMINOR);
	if VSUB<>0 then ? "-";chr(VSUB+96);
	if VEDIT<>0 then ? "(";VEDIT;")";
	if VPATCH<>0 then ? "-";str(VPATCH);
	return



