!	FIXMMO.BAS
!
!	Copyright (c) 1984 by MicroSabio
!	Written by Jack McGregor
!-------------------------------------------------------------------------
!	Rebuild the deleted rec chain & reset ctl rec for a memo pad
!-------------------------------------------------------------------------
! USAGE:
!	.FIXMMO		(via FIXMMO.CMD:)
!
!					LOAD INFLD.SBR
!					RUN FIXMMO
!-------------------------------------------------------------------------
! EDIT HISTORY:
!	1.  07-27-84  Created /jdm
!	3.  09-24-86  Read file name from command file /jdm
!	2.  04-21-86  Use INFLD instead of INFLDX /jdm
!	4.  11-22-89  Add color /jdm
! Version 2.1---
!       5.  09-20-90  Minor adjustment for non-AMOS /jdm
! Version 2.2A
!	6.  02-21-92  Further adjustments for non-AMOS /gb
!-------------------------------------------------------------------------
! EXTERNAL SUBROUTINES:	
!	INFLD.SBR  
! ALGORITHM NOTES:
!	1.  Ask for the name of the memo file to rebuild. 
!	2.  Reset the ctl rec with deleted ptr = 0
!	3.  Read file backwards from the end until the first non-]]]]]]
!	    rec found; then set ctl rec EOF pointer.	
!	4.  Continue reading backwards up to rec #1, deleting any ]]]]]]
!	    or ]]]DEL recs, and linking chain with ctl rec.
!-------------------------------------------------------------------------

	PROGRAM FIXMMO,2.1(5)

MAP1 PHDR
	MAP2 VMAJOR,B,1,2
	MAP2 VMINOR,B,1,0
	MAP2 VSUB,B,1
	MAP2 VEDIT,B,1,5 
	MAP2 VPATCH,B,1,0

MAP1 CTL'REC2
	MAP2 MARKER2,S,6
	MAP2 NXT'FRE2,B,2
	MAP2 NXT'DEL2,B,2
	MAP2 FILL2,S,54
MAP1 CTL'REC4,@CTL'REC2
	MAP2 MARKER4,S,6	
	MAP2 NXT'FRE4,B,4
	MAP2 NXT'DEL4,B,4
	MAP2 FLAG4,S,1
	MAP2 FILL4,S,49

MAP1 BRACKS,S,64,"]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]"

MAP1 MEMO$,S,24

MAP1 BSWAP,B,4
MAP1 BSWPX,@BSWAP
	MAP2 BS1,B,2
	MAP2 BS2,B,2
MAP1 BX2,B,2

MAP1 MISC
     MAP2 X,F
     MAP2 I,F
     MAP2 XX,F
     MAP2 EXISTS,F
     MAP2 DC,F
     MAP2 DX,F
     MAP2 MMOFMT,F
     MAP2 RECCNT,F
     MAP2 FILE1,F
     MAP2 SAVE'FILE1,F
     MAP2 A$,S,10
     MAP2 DEL'CNT,F

++include infpar.bsi
++include mmopar.bsi

	TIMER = 90			! 90 sec timeout
	CMDFLG = 1			! input from command file
!-------------------------------------------------------------------------

	filebase 1

++include getclr.bsi

START:
	? tab(-1,0);"FIXMMO - Rebuild deleted rec chain & reset control rec";
	? tab(-1,11);
	? tab(1,62);"Copyright (c) 1984";
	? tab(2,62);"By MicroSabio"
	call PHDR

	? tab(6,5); "Enter Name of Memo File: "

	? tab(10,5); "Memo Link Format: "

F1:	
	DEL'CNT = 0
	xcall INFLD,6,45,24,1,"A^]E",MEMO$,INXCTL,1,1,EXITCODE,TIMER,CMDFLG,&
		DEFPT,MAXPT,FUNMAP,SETDEF,INFCLR,HLPIDX
	if EXITCODE=11 goto TIME'OUT
	if INXCTL=2 goto EXIT

	X = instr(1,MEMO$,".")
	if X<1 then MEMO$=MEMO$+".MMO" : ? tab(6,45);MEMO$

	? tab(8,5);tab(-1,9);
	? tab(22,1);tab(-1,10);

	lookup MEMO$,EXISTS
	if abs(EXISTS) <> 0 goto READ'MEMO	! [6] 

	? tab(8,5);MEMO$;" not found!"
	goto F1

READ'MEMO:
	RECCNT = abs(EXISTS) * 8                            ! [5]

	open #1, MEMO$, random, 64, FILE1
	FILE1 = 1
	read #1, CTL'REC2

	if FLAG4="4" then MMOFMT=4 else MMOFMT=2
	? tab(10,48);tab(-1,12);MMOFMT;tab(-1,11);"byte";tab(-1,12)



REBUILD:				! Rebuild the deleted record chain
					! and reset control rec

	? tab(14,10);"Searching for EOF ";		

	FILE1 = RECCNT+1
	DC = int(RECCNT/40)+1
RB'LOOP:				! located 1st EOF rec

	DX = DX + 1				
	if DX>=DC then ? "."; : DX = 0

	FILE1 = FILE1 - 1
	if FILE1 = 1 goto RB'SET
	read #1, CTL'REC4
	if MARKER4="]]]]]]" goto RB'LOOP
RB'SET:
	SAVE'FILE1 = FILE1		! Store EOF pointer in ctl rec
	if SAVE'FILE1=RECCNT then SAVE'FILE1 = -1
	? " (";str(SAVE'FILE1+1);")"
	FILE1 = 1
	read #1, CTL'REC4
	if MMOFMT = 2 then &
		NXT'FRE2 = SAVE'FILE1 + 1 : &
		NXT'DEL2 = 0 &
	else &
		BSWAP = SAVE'FILE1+1 : &
		call BSWAP : &
		NXT'FRE4 = BSWAP : &
		NXT'DEL4 = 0	

	MARKER4 = "]]]DEL"
	write #1, CTL'REC4

	FILE1 = SAVE'FILE1+1		! now rebuild del chain
	if FILE1=0 then FILE1 = RECCNT+1
	? tab(15,10);"Rebuilding Deleted Chain ";
RB'LP2:
	DX = DX + 1				
	if DX>=DC then ? "."; : DX = 0

	FILE1 = FILE1 - 1
	if FILE1 = 1 goto RB'DONE
	read #1, CTL'REC4
	if MARKER4<>"]]]]]]" and MARKER4<>"]]]DEL" goto RB'LP2

	SAVE'FILE1 = FILE1		! delete record
	FILE1 = 1
	read #1, CTL'REC4
	if MMOFMT=2 then &
		XX = NXT'DEL2 : &
		NXT'DEL2 = SAVE'FILE1 &
	else &
		XX = NXT'DEL4 : &
		BSWAP = SAVE'FILE1 : &
		call BSWAP : &
		NXT'DEL4 = BSWAP

	write #1, CTL'REC4

	FILE1 = SAVE'FILE1		! Make rec just deleted point
	read #1, CTL'REC4		! to previous start of del chain
	if MMOFMT = 2 then &
		NXT'DEL2 = XX &
	else &
		NXT'DEL4 = XX 

	MARKER4 = "]]]DEL"
	write #1, CTL'REC4

	goto RB'LP2


RB'DONE:
EXIT:
	? tab(24,1);"Process Complete  -  Press any key to continue: ";
	xcall INFLD,24,60,1,0,"A",A$,INXCTL,1

	? tab(-1,0);
	end

TIME'OUT:
	? tab(24,1);tab(-1,9);"Program timed out.";
	end


!-------------------------------------------------------------------------

BSWAP:
	BX2 = BS1		! from AM100L format
	BS1 = BS2		! to BASIC format					
	BS2 = BX2
	return

!-----------------------------------------------------------------------

PHDR:					! display standard program 
					! header at (2,1) in AMOSL format

	? tab(2,1);tab(-1,11);"Rev. ";
	? str(VMAJOR);".";str(VMINOR);
	if VSUB<>0 then ? "-";chr(VSUB+96);
	if VEDIT<>0 then ? "(";VEDIT;")";
	if VPATCH<>0 then ? "-";str(VPATCH);
	return


