! 	EXPMMO.BAS
!
!	Copyright (c) MicroSabio 1983,1984
!	Written by Jack McGregor
!------------------------------------------------------------------------
!	Program to expand or contract memo files (for use with INMEMO.SBR)
!------------------------------------------------------------------------
! USAGE:
!	.EXPMMO		(via EXPMMO.CMD:
!
 !					LOAD BAS:INFLD.SBR
!					LOAD BAS:RENAM.SBR
!					LOAD BAS:JOBNAM.SBR
!					RUN INMEMO:EXPMMO
!-------------------------------------------------------------------------
! EDIT HISTORY:
!	1. 12-16-83  [1.0]  Created /jdm
!	2. 04/05/84  [1.0a] Check that new size < 64K records /jdm
!	3. 07/03/84  [1.1]  Handle either 2 or 4 byte link format /jdm
!	4. 11/12/84  [1.1a] Fix mysterious retrogression /jdm
!	5. 04/21/86  [1.2]  Use INFLD instead of INFLDX/jdm
!	6. 09/26/86  [1.3]  Get file name from command file /jdm
!	7. 11/19/86  [1.3a] Fix bug in 4 byte expand ctl record /jdm
!	8. 11/22/89  [2.0]  Add color /jdm
!	9. 09/20/90  [2.1]  Minor adjustments for non-AMOS /jdm
!      10. 02/26/92  [2.2a] More adjustments for non-AMOS /gb
!------------------------------------------------------------------------
! EXTERNAL SUBROUTINES:	
!	INFLD.SBR  (stripped down version of INFLD.SBR which only works
!		     with the INMEMO utilities, although INFLD will also
!		     work fine.)	
!	RENAM.SBR   (Renames a disk file; equivalent to the AlphaACCOUNTING
!		     routine RENAME.SBR)	
!	JOBNAM.SBR  (Returns the user's AMOSL job name.)	
!-----------------------------------------------------------------------
! ALGORITHM NOTES:
!	This programs requires a block of contiguous disk space large
!	enough for the new file size (whether bigger or smaller than
!	the original).
!
!	If the file is expanded, all data is preserved.  If the old
!	file had no unallocated space, then the new control record is
!	updated to indicate the new start of end-of-file area.
!
!	There are two cases of file contraction: when the new file is
!	larger than the number of allocated records in the original file,
!	and when the new file is smaller.  In the first case, all data
!	is preserved and the control record remains unchanged.  In the
!	second case, some data is lost (although it is possible that they
!	are all deleted records).  In this case, the new end of file pointer
!	is set to zero, as is the new delete chain pointer if the old delete
!	chain pointer was greater than the new file size.  A message is	
!	displayed at the end warning user that delete chain must be rebuilt
!	using the ANAMMO program.
!------------------------------------------------------------------------

	PROGRAM EXPMMO,2.1(9)

MAP1 PHDR
	MAP2 VMAJOR,B,1,2
	MAP2 VMINOR,B,1,0
	MAP2 VSUB,B,1,0
	MAP2 VEDIT,B,1,8
	MAP2 VPATCH,B,1,0

MAP1 CTL'REC2
	MAP2 MARKER2,S,6
	MAP2 NXT'FRE2,B,2
	MAP2 NXT'DEL2,B,2
	MAP2 FILL2,S,54
MAP1 CTL'REC4,@CTL'REC2
	MAP2 MARKER4,S,6
	MAP2 NXT'FRE4,B,4
	MAP2 NXT'DEL4,B,4
	MAP2 FLAG4,S,1
	MAP2 FILL4,S,49

MAP1 BRACKS,S,64,"]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]"

MAP1 MEMO'REC,X,64

MAP1 MISC
	MAP2 MEMO$,S,24
	MAP2 TMPFIL$,S,24
	MAP2 NEWBLKS,F,6
	MAP2 DOTCNT,F,6
	MAP2 DC,F,6
	MAP2 I,F,6
	MAP2 X,F,6
	MAP2 A$,S,10
	MAP2 JOB$,S,10
	MAP2 FLAG,F,6
	MAP2 NEW'SIZE,F,6
	MAP2 OLD'SIZE,F,6
	MAP2 XFER'SIZE,F,6
	MAP2 ALC'SIZE,F,6
	MAP2 EXISTS,F,6
	MAP2 FILE1,F,6
	MAP2 LFMT,F,6
     	MAP2 SIZE$,S,10
	MAP2 OLD'NXT'FRE,F,6
MAP1 BSWAP,B,4
MAP1 BSWPX,@BSWAP
	MAP2 BS1,B,2
	MAP2 BS2,B,2
MAP1 BX2,B,2

++include infpar.bsi
++include mmopar.bsi

	TIMER = 30			! 30 sec timeout
	CMDFLG = 1			! read from command file

!------------------------------------------------------------------------

	on error goto TRAP
	filebase 1

++include getclr.bsi		! [8] Set up colors

START:
	? tab(-1,0);"EXPMMO - Expand or Contract a Memo File"
	? tab(-1,11);
	? tab(1,62);"Copyright (c) 1984";
	? tab(2,62);"By MicroSabio"

	call PHDR

	? tab(6,5);"Enter name of memo file:";

F1:	
	xcall INFLD,6,54,24,1,"A^]E\",MEMO$,INXCTL,1,1,EXITCODE,TIMER, &
		CMDFLG,DEFPT,MAXPT,FUNMAP,SETDEF,INFCLR,HLPIDX
	if EXITCODE=11 goto TIME'OUT
	if INXCTL=2 goto EXIT

	X = instr(1,MEMO$,".")
	if X<1 then MEMO$=MEMO$+".MMO" : ? tab(6,54);MEMO$

	? tab(8,5);tab(-1,10);

	lookup MEMO$,EXISTS
	if abs(EXISTS)<>0 goto SIZE	! [10]

	? tab(8,5);MEMO$;" not found!"
	goto F1		

SIZE:
	? tab(8,5);tab(-1,11);"Current memo file size is: ";tab(-1,12);
	OLD'SIZE = abs(EXISTS) * 8				! [9]
	? tab(8,44);OLD'SIZE using "#####";
	? tab(-1,11);tab(8,54);"Link format: ";tab(-1,12);
	? tab(9,5);tab(-1,11);"Size of allocated portion of file: ";tab(-1,12);
	open #1, MEMO$, random, 64, FILE1
	FILE1 = 1
	read #1, CTL'REC2
	if FLAG4<>"4" then &
		LFMT=2 : ? tab(8,68);"2 byte" &
	else &
		LFMT=4 : ? tab(8,68);"4 byte"

	if LFMT=2 then &
		if NXT'FRE2>0 then &
			ALC'SIZE = NXT'FRE2 -1 &
		else &
			ALC'SIZE = OLD'SIZE &
	else &
		if NXT'FRE4>0 then &
			BSWAP = NXT'FRE4 : &
			call BSWAP : &
			ALC'SIZE = BSWAP - 1 &
		else &
			ALC'SIZE = OLD'SIZE

	? tab(9,44);ALC'SIZE using "#####"

	if LFMT=2 then &
		OLD'NXT'FRE=NXT'FRE2 &
	else &
		BSWAP = NXT'FRE4 : &
		call BSWAP : &
		OLD'NXT'FRE=BSWAP
			
	close #1

NEWSIZE:

	? tab(11,5);tab(-1,11);"Enter new number of records desired: ";

	CMDFLG = 0			! don't read from cmd file anymore
	xcall INFLD,11,54,8,1,"#123R\",SIZE$,INXCTL,1,1,EXITCODE,TIMER,&
		CMDFLG,DEFPT,MAXPT,FUNMAP,SETDEF,INFCLR,HLPIDX

	if EXITCODE=11 goto TIME'OUT
	if EXITCODE>0 goto F1
	if val(SIZE$)>65535 and LFMT=2 then &
		? tab(24,1);"Maximum size for 2-byte link format is 65535!"; : &
		goto NEWSIZE

WARNING:
	? tab(13,5);tab(-1,11);"This means you are ";tab(-1,12);
	NEW'SIZE = val(SIZE$)
	if NEW'SIZE>OLD'SIZE then &
		? "expanding"; &
	else if NEW'SIZE<OLD'SIZE then &
		? "contracting"; &
	else ? "doing nothing! " : goto F1

	? tab(-1,11);" the file.  Right? ";

	if NEW'SIZE<ALC'SIZE then &
		? tab(14,5);tab(-1,12); : &
		? "*** THIS WILL CAUSE SOME DATA TO BE LOST! ***"

	xcall INFLD,13,54,1,0,"YN23",A$,INXCTL,1,0,EXITCODE,TIMER,&
		CMDFLG,DEFPT,MAXPT,FUNMAP,SETDEF,INFCLR,HLPIDX

	if EXITCODE=11 goto TIME'OUT
	if EXITCODE=2 or EXITCODE=3 or A$="N" goto F1

ALLOCATE:

! [10]	xcall JOBNAM,JOB$			! get job name
	JOB$ = "EXPMMO"			! [10]
	TMPFIL$ = JOB$+".TMP"
	lookup TMPFIL$, EXISTS
	if abs(EXISTS)<>0 kill TMPFIL$		![10]

	NEWBLKS = int(NEW'SIZE/8)+1
	? tab(21,1);"Allocating ";TMPFIL$;"...";
	allocate TMPFIL$, NEWBLKS
	for I=1 to 5000 : next I
	? tab(21,1);tab(-1,9);


TRANSFER:


	? tab(21,1);"Transferring";
	open #1, MEMO$, random, 64, FILE1
	open #2, TMPFIL$, random, 64, FILE1

	NEW'SIZE = NEWBLKS * 8	
	DOTCNT = int(NEW'SIZE/40)			! lifesigns

	XFER'SIZE = OLD'NXT'FRE-1
	if XFER'SIZE<0 then XFER'SIZE=OLD'SIZE		! # recs to xfer
	XFER'SIZE = XFER'SIZE min NEW'SIZE		! (make sure not
							! bigger than new
							! file size

!			? tab(15,40);"OLD'SIZE: ";OLD'SIZE
!			? tab(16,40);"NEW'SIZE: ";NEW'SIZE
!			? tab(17,40);"XFER'SIZE: ";XFER'SIZE
!			? tab(18,40);"DOTCNT : ";DOTCNT
!			? tab(21,20);

	for FILE1 = 1 to XFER'SIZE
		DC=DC+1	
		if DC>=DOTCNT then DC=0 : ? ".";
		read #1, MEMO'REC
		write #2, MEMO'REC
	next FILE1

	if NEW'SIZE>XFER'SIZE goto CLEAR	! If new file bigger, clear
						! remainder


		FILE1 = 1			! If new file size is smaller
		read #2, CTL'REC2		! than the original link to
		if LFMT=2 then NXT'FRE2=0 else NXT'FRE4=0 ! first end-of-file record,
		                 		! then we need to zero that
		           			! link in the new file.
		if LFMT=2 then &
			if NXT'DEL2>NEW'SIZE then NXT'DEL2=0
		if LFMT=4 then &
			BSWAP=NXT'DEL4 : &
			call BSWAP : &
			if BSWAP>NEW'SIZE then NXT'DEL4=0

		write #2, CTL'REC2
		goto RENAME

CLEAR:
	for FILE1 = (XFER'SIZE+1) to NEW'SIZE
		DC=DC+1
		if DC>=DOTCNT then DC=0 : ? "]";
		write #2, BRACKS
	next FILE1

					! If old file had no free records,
					! then update free record ptr
					! in new file

	if OLD'NXT'FRE>0 then goto RENAME

		FILE1 = 1		
		read #2, CTL'REC2
		if LFMT=2 then &
			NXT'FRE2 = ALC'SIZE + 1 &
		else &
			BSWAP = ALC'SIZE + 1 : &
			call BSWAP : &
			NXT'FRE4 = BSWAP
						! [7]
		write #2, CTL'REC2

RENAME:

	close #1
	close #2
	? tab(21,1);tab(-1,9);"Deleting old memo file ";MEMO$;"...";
	kill MEMO$
	for I = 1 to 5000 : next I
	? tab(21,1);tab(-1,9);"Renaming ";TMPFIL$;" to ";MEMO$;"...";
	xcall RENAME,TMPFIL$,MEMO$,FLAG		! [10]
	if FLAG=0 goto DONE

	? tab(24,1);"Cannot rename file!  See system operator!";
	goto ENDIT

DONE:
	? tab(24,1);"Process Complete - Hit any key to continue: ";
	xcall INFLD,24,50,1,1,"@",A$,INXCTL,1
	if NEW'SIZE<ALC'SIZE goto REBUILD'MSSG else goto EXIT


TRAP:
	if ERR(0)=1 then goto EXIT

	TIMER = 0		! Disable time-out to allow them to
				! study error messages

	? tab(22,1);tab(-1,10);"----------------------------------------------------------------------------"
	? tab(23,1);
	if ERR(0)=12 then &
		? "Subroutine not found (INFLD.SBR, JOBNAM.SBR or RENAM.SBR)"; &
		: goto ENDIT
	if ERR(0)=16 or ERR(0)=32 then &
		? "File specification error"; : resume F1
	if ERR(0)=18 then ? "Device not ready"; : resume F1
	if ERR(0)=19 then &
		? "Cannot allocate - not enough room on device!"; &
		: resume NEWSIZE
	if ERR(0)=22 then ? "Illegal user code - PPN not found!"; : resume F1
	if ERR(0)=23 then ? "Protection violation!"; : resume F1
	if ERR(0)=24 then ? "Cannot write - disk write protected!"; : resume F1
	if ERR(0)=26 then ? "Device does not exist!"; : resume F1
	if ERR(0)=27 then ? "Bitmap kaput - see system operator!"; : goto ENDIT
	if ERR(0)=28 then ? "Disk not mounted!"; : goto F1
	if ERR(0)=36 then ? "Improper version of INFLD.SBR"; : goto ENDIT
	? tab(22,1);
	on error goto
	END
EXIT:
	? tab(-1,0);"Program end"

ENDIT:
	END

TIME'OUT:
	? tab(-1,0);"Program timed out."
	goto ENDIT

REBUILD'MSSG:
	? tab(24,1);tab(-1,9);tab(-1,12);
	? chr(7);
	? "Need to rebuild memo file - use ANAMMO ";
	goto ENDIT

BSWAP:
	BX2 = BS1
	BS1 = BS2
	BS2 = BX2
	return

!-----------------------------------------------------------------------

PHDR:					! display standard program
					! header at (2,1) in AMOSL format

	? tab(2,1);tab(-1,11);"Rev. ";
	? str(VMAJOR);".";str(VMINOR);
	if VSUB<>0 then ? chr(VSUB+96);
	if VEDIT<>0 then ? "(";str(VEDIT);")";
	if VPATCH<>0 then ? "-";str(VPATCH);
	return


