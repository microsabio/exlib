:<(INMEMO) Samples & utilities relating to INMEMO
    ANAMMO - Analyze memo file
    DDTMMO - Direct editing of memo file by link #
    DMPMMO - Search/dump memo file
    EXPMMO - Expand memo file
    FIXMMO - Rebuild memo file
    MAKMMO - Make (create) a memo file
    OPRMMO - Display memo file control info
    TSTMMO - Sample program using various INMEMO calls

Also see XTXMMO in [908,40] (uses XTEXT to edit INMEMO files)
>
 
