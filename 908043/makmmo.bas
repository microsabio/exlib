!	MAKMMO.BAS
!
!	Copyright (c) MicroSabio 1983,1984
!	Written by Jack McGregor
!------------------------------------------------------------------------
!	Program to create and initialize a memo file to be used with
!	INMEMO.SBR
!------------------------------------------------------------------------
! USAGE:
!	.MAKMMO		(via MAKMMO.CMD:
!
!					LOAD BAS:INFLD.SBR
!					RUN INMEMO:MAKMMO
!------------------------------------------------------------------------
! EDIT HISTORY:
!	1.  12-14-83  [1.0] Created /jdm
!	2.  07-03-84  [1.1] Allow either 2 or 4-byte link format /jdm
!	3.  04-21-86  [1.2] Use INFLD instead of INFLDX /jdm
!	4.  11-22-89  [2.0] Add color /jdm
!       5.  09-20-90  [2.1] Chg version only to match other utilities /jdm
!	6.  02-19-92  [2.2A] Minor adjustment for non-amos /gb
!------------------------------------------------------------------------
! EXTERNAL SUBROUTINES:	
!	INFLD.SBR
!-------------------------------------------------------------------------		
! ALGORITHM NOTES:
!	1.  Ask for the name of the memo file to create.
!	2.  Lookup to see if file exists.  If so, ask if it should be
!	    deleted.  (If not, abort.)
!	3.  Ask for link format & # of records.
!	4.  Create a file; initialize it by writing brackets to entire
!	    file and setting control record to the proper format.	
!-------------------------------------------------------------------------


	PROGRAM MAKMMO,2.2A(6)

MAP1 PHDR
	MAP2 VMAJOR,B,1,2
	MAP2 VMINOR,B,1,0
	MAP2 VSUB,B,1,0
	MAP2 VEDIT,B,1,4
	MAP2 VPATCH,B,1,0

MAP1 CTL'REC2
	MAP2 MARKER2,S,6
	MAP2 NXT'FRE2,B,2
	MAP2 NXT'DEL2,B,2
	MAP2 FILL2,S,54
MAP1 CTL'REC4,@CTL'REC2
	MAP2 MARKER4,S,6
	MAP2 NXT'FRE4,B,4
	MAP2 NXT'DEL4,B,4
	MAP2 FLAG4,S,1
	MAP2 FILL4,S,49

MAP1 BRACKS,S,64,"]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]"

MAP1 MEMO$,S,24

MAP1 BSWAP,B,4
MAP1 BSWPX,@BSWAP
	MAP2 BS1,B,2
	MAP2 BS2,B,2
MAP1 BX2,B,2

MAP1 MISC
     MAP2 I,F
     MAP2 X,F
     MAP2 RECCNT,F
     MAP2 DOTCNT,F
     MAP2 DC,F
     MAP2 EXISTS,F
     MAP2 DEL$,S,2
     MAP2 SIZE,F
     MAP2 SIZE$,S,10
     MAP2 LFMT$,S,2
     MAP2 FILE1,F

++include infpar.bsi
++include mmopar.bsi

	TIMER = 30			! 30 second timeout
	CMDFLG = 1			! read from command file

!------------------------------------------------------------------------

	on error goto TRAP
	filebase 1

++include getclr.bsi		! [4] setup color

START:
	? tab(-1,0);"MAKMMO - Initialize a Memo File"
	? tab(-1,11);
	? tab(1,62);"Copyright (c) 1984";
	? tab(2,62);"By MicroSabio"

	call PHDR

	? tab(6,5); "Enter Name of Memo File: "
	? tab(10,5);"Enter Link Size (2 or 4 bytes): "
	? tab(12,5);"Enter Number of 64-Byte Records Desired: "

F1:	
	? tab(8,5);tab(-1,9);
	xcall INFLD,6,54,24,1,"A^]E",MEMO$,INXCTL,1,1,EXITCODE,TIMER,&
		CMDFLG,DEFPT,MAXPT,FUNMAP,SETDEF,INFCLR,HLPIDX
	if EXITCODE=11 goto TIME'OUT
	if INXCTL=2 goto EXIT

	X = instr(1,MEMO$,".")
	if X<1 then MEMO$=MEMO$+".MMO" : ? tab(6,54);MEMO$

	? tab(22,1);tab(-1,10);

	lookup MEMO$,EXISTS
	if abs(EXISTS)=0 goto FORMAT	! [6]

	? tab(8,5);MEMO$;" already exists!  Do you wish to delete?"
	xcall INFLD,8,70,1,1,"YN",DEL$,INXCTL,1,0,EXITCODE,TIMER,&
		CMDFLG,DEFPT,MAXPT,FUNMAP,SETDEF,INFCLR,HLPIDX

	if EXITCODE=11 goto TIME'OUT
	if EXITCODE=1 or DEL$="N" ? tab(8,5);tab(-1,9) : goto F1

	? tab(-1,11);tab(8,72);"Sure? ";
	xcall INFLD,8,78,1,1,"YN",DEL$,INXCTL,1,0,EXITCODE,TIMER,&
		CMDFLG,DEFPT,MAXPT,FUNMAP,SETDEF,INFCLR,HLPIDX

	if EXITCODE=11 goto TIME'OUT
	if EXITCODE=1 or DEL$="N" ? tab(8,5);tab(-1,9) : goto F1

DELETE:
	? tab(22,1);"Deleting ";MEMO$;"..."
	kill MEMO$
	for I = 1 to 2000 : next I
	? tab(22,1);tab(-1,9);

FORMAT:
	if LFMT$="" then LFMT$="4"
	xcall INFLD,10,70,1,1,"#23\",LFMT$,INXCTL,1,1,EXITCODE,TIMER,&
		CMDFLG,DEFPT,MAXPT,FUNMAP,SETDEF,INFCLR,HLPIDX

	if EXITCODE=11 goto TIME'OUT
	if EXITCODE=2 or EXITCODE=3 goto F1
	if LFMT$<>"2" and LFMT$<>"4" goto FORMAT

SIZE:
	xcall INFLD,12,70,7,1,"#23\",SIZE$,INXCTL,1,1,EXITCODE,TIMER,&
		CMDFLG,DEFPT,MAXPT,FUNMAP,SETDEF,INFCLR,HLPIDX

	if EXITCODE=11 goto TIME'OUT
	if EXITCODE=2 or EXITCODE=3 goto FORMAT
	if LFMT$="2" and val(SIZE$)>65535 then &
		? tab(24,1);tab(-1,11); &
		"Maximum size with 2-byte link format is 65535"; : goto SIZE

CREATE:
	SIZE = int(val(SIZE$) / 8)+1
	? tab(22,1);"Allocating ";MEMO$;" (";SIZE;"blocks)..."
	allocate MEMO$,SIZE
	for I = 1 to 2000 : next I
	? tab(22,1);tab(-1,9);	

INIT:
	? tab(22,1);"Initializing ";MEMO$;
	open #1, MEMO$, random, 64, FILE1	
	RECCNT = SIZE * 8
	DOTCNT = int(RECCNT/40)
	for FILE1 = 2 to RECCNT
		DC=DC+1	
		if DC>=DOTCNT then DC=0 : ? ".";
		write #1, BRACKS
	next FILE1

	FILE1 = 1
	MARKER2="]]]DEL"	
	if LFMT$="2" then &
		NXT'FRE2=2 : &
		NXT'DEL2=0 : &
		FILL2=BRACKS : &
		write #1, CTL'REC2 &
	else &
		NXT'FRE4=2 : &
		BSWAP=NXT'FRE4 : &
		call BSWAP : &
		NXT'FRE4=BSWAP : &
		NXT'DEL4=0 : &
		FILL4=BRACKS : &
		FLAG4="4" : &
		write #1, CTL'REC4

	close #1
	? tab(22,1);tab(-1,9);
	goto START


TRAP:
	if ERR(0)=1 then goto EXIT

	TIMER = 0		! Disable time-out to allow them to
				! study the error message.

	? tab(23,1);tab(-1,10);"----------------------------------------------------------------------------"
	? tab(24,1);
	if ERR(0)=12 then ? "INFLD.SBR not found"; : goto ENDIT
	if ERR(0)=16 or ERR(0)=32 then &
		? "File specification error"; : resume F1
	if ERR(0)=18 then ? "Device not ready"; : resume F1
	if ERR(0)=19 then &
		? "Cannot allocate - not enough room on device!"; : resume SIZE
	if ERR(0)=22 then ? "Illegal user code - PPN not found!"; : resume F1
	if ERR(0)=23 then ? "Protection violation!"; : resume F1
	if ERR(0)=24 then ? "Cannot write - disk write protected!"; : resume F1
	if ERR(0)=26 then ? "Device does not exist!"; : resume F1
	if ERR(0)=27 then ? "Bitmap kaput - see system operator!" : goto ENDIT
	if ERR(0)=28 then ? "Disk not mounted!"; : goto F1
	if ERR(0)=36 then ? "Improper version of INFLD.SBR" : goto ENDIT
	on error goto 
	END
EXIT:
	? tab(-1,0);"Program end"

ENDIT:
	END

TIME'OUT:
	? tab(-1,0);"Program timed out."
	goto ENDIT

BSWAP:
	BX2 = BS1
	BS1 = BS2
	BS2 = BX2
	return

!-----------------------------------------------------------------------

PHDR:					! display standard program 
					! header at (2,1) in AMOSL format

	? tab(2,1);tab(-1,11);"Rev. ";
	? str(VMAJOR);".";str(VMINOR);
	if VSUB<>0 then ? "-";chr(VSUB+96);
	if VEDIT<>0 then ? "(";str(VEDIT);")";
	if VPATCH<>0 then ? "-";str(VPATCH);
	return


