//;test the Info -> DocInfo conversions
//PDFX,Info.Ask,0
//PDFX,Info.Creator,MicroSabio 
//PDFX,Info.Author,Jack
//PDFX,Info.Subject,Info Metadata
//PDFX,Info.Title,Test v3 Info to v5 DocInfo conversion
//PDFX,Info.Keywords,bisou,charm,lieben,zulu
//PDFX,Save.App.Run,1
//PDFX,Save.ShowSaveDialog,0
//;PDFX,Save.FullFileName,"sos61:[907,20]INFO.PDF"
//;PDFX,Save.FullFileName,"\\prdpdfappshare\ashare\temp\info.pdf"
//PDFX,Save.FullFileName,"ASHTST:"
//;PDFX,Save.WhenExists,Overwrite
//PDFX,Save.WhenExists,AutoNumber
//PDFX,Save.AutoNumber.NumDigits,1

Test Info properties (and auto-conversion to v5 DocInfo)

Check the properties of this document to verify that it contains
the following:

Creator (application) = MicroSabio
Author = Jack
Subject = Info Metadata
Title = Test v3 Info to v5 DocInfo conversion
Keywords = bisou,charm,lieben,zulu

