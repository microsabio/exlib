:<(PDFX5) PDFX5 Samples (experimental)

    INFO.TXT - Test Info (and conversion to DocInfo)
    DOCINFO.TXT - Test v5 DocInfo native properties
    WATER1.TXT - Sample text watermark
    WATER2.TXT - Sample image watermark
    PASSWORD.TXT - Sample password protected doc
    NUP1.TXT - Sample using n-UP printing 
    BOOKLET1.TXT - Sample of booklet printing

Also see [908,45] for PDFX v3 examples 

>
 
