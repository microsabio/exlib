:<(GDIPRT) GDI Printing 
    (Note: PRINT PROMPT=xxxxxx.TXT to test)
    TSTGDI.TXT     - Sample printfile with GDI commands
    SETPEN.TXT     - Sample printfile showing new //SETPENEX function
    *.EMF          - Sample metafiles
    MMOGDI.BP      - Sample program using //TEXTRECTANGLE, MX_GDICALC, XTEXT
    ANGLE.TXT      - Sample of angular text
    SETFONT.TXT    - Sample of various //SETFONT
    TSTWID.TXT     - Sample showing use of width param to //SETFONT
    FIXWID.TXT     - Sample showing granularity of fixed pitch width in SETFONT
    MONOWT.TXT     - Sample testing if weight and style affect width
    POLYGON*.TXT   - Samples showing //POLYGONs
    TEXTRECTR.TXT  - Sample showing //TEXTRECTANGLEs with rotation
    TEXTRECTR2.TXT - Sample showing //TEXTRECTANGLEs with various alignments
    REXTRECTR3.TXT - Mirrored image using //SETTRANSFORM
    GDICALC.BP - Sample program using MX_GDICALC 
    WTOGDI.BP - Convert Unicode 16 file to GDI
>
 
