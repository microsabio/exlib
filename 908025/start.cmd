:<(TCP) TCPX.SBR / HTTP.SBR sample/test routines
    TCPTST - Prompts for all args for easy testing
    TCPXFR - Working file transfer program
    TSTSVR - Sample program launching TCPXFR as a server via HOSTEX
    TSTCLI - Sample program launching TCPXFR as a client to send file to svr
    TCPPAR - Sample parent for parent/child model (UNIX)
    TCPCH1 - Sample child for "   "    "   "
    TCPSV1 - Sample single-threaded general purpose server
    TCPCLX - SBX to encapsulate a client connect/request/response 
                (to TCPSV1-style server)
    TCPCL1 - Sample program using XCALL TCPCLX
    TCPCON - SBX to encapsulate just the client connect and close operations
    TCPCO1 - Sample program using XCALL TCPCON
    TCPSRV?- Alternate/more sophisticated version of TCPSV1
    HTTPGET- Retrieve a file from an HTTP server (using TCPX)
    HTTPSGET- Retrieve a file from an HTTP server (HTTP.SBR; supp SSL)
    HTTPJPG- Retrieve a file (e.g. JPG) from HTTP server (using HTTP)
    HTTPPNGPDF - Download file (google map), create PDF 
    HTTP1  - Post a request (file) to an HTTP server (using HTTP)
    AIRPORT- Example using HTTP to query web service about airport info
    TCPSV3 - Variation of TSTSV1 which accepts HTTP POST requests
    GOOGXLAT - Sample using HTTP with Google API translation services
    GENPPKEY - Generate Public/Private key pair (for SSH login)
    SOAPCALC - Simple SOAP example (service adds two numbers)
>
