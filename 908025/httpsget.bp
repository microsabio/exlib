program HTTPSGETMC,1.0(101)  ! test HTTP GET using XCALL HTTP
!------------------------------------------------------------
!EDIT HISTORY
! [100] 30-Sep-21 / jdm / created
! [101] 28-Jun-22 / jdm / switch to REQGETX with GETRAW option

!------------------------------------------------------------
!Notes:
!
!------------------------------------------------------------
significance 11
++include ashinc:ashell.def
++include ashinc:http.def

map1 MISC
    map2 FLAGS,B,4
    map2 URL$,S,0
    map2 RESPONSE$,S,20,"get.rsp"
    map2 STATUS,F
    map2 REQUEST$,S,0
    map2 DBG,B,1
    map2 CH,B,2,77
    map2 CUSTOM,B,1        
    map2 PROPERTIES$,S,200 
    map2 OP,B,1
    
    ? tab(-1,0);"Test HTTP.SBR GETX"

    FLAGS = 0
    input "Enter 0)simple get, 1) GETX ",OP
    if OP then
        FLAGS |= XHTTPF_REQGETX
        OP = 0
        input "Enter 0) plain text, 1) raw ",OP
        if OP then
            FLAGS |= XHTTPF_REQGETRAW
        endif
    else
        FLAGS |= XHTTPF_REQGET
    endif
    
    FLAGS = FLAGS or XHTTPF_FILERESP 

    input "Enter 1 for debug mode (creates DEBUG.REQ copy of request) [0] ",DBG
    if DBG then     
        FLAGS |= XHTTPF_DEBUG
    endif  
    ? "Flags = ";FLAGS

    input "URL: ",URL$

    if lookup(RESPONSE$) then kill RESPONSE$

!>!    input "Enter 1 to try adding a couple of custom headers [0]", CUSTOM
!>!    if CUSTOM then
!>!        FLAGS = FLAGS or XHTTPF_HDRBODY     ! use REQUEST$ to supply custom headers
!>!        REQUEST$ = "Referer: http://www.microsabio.com" 
!>!        REQUEST$ += chr(13) + "User-Agent:A-Shell/ashnet"       ! separate headers with chr(13)
!>!        REQUEST$ += chr(13) + "Accept-Encoding: "! text"           ! this will overwrite an existing header
!>!    endif
    
    xcall HTTP,1,STATUS,FLAGS,URL$,REQUEST$,RESPONSE$,PROPERTIES$

    ? "Status : ";STATUS;

    if STATUS = 0 then
        ? " [OK]"
        xcall SIZE, RESPONSE$, STATUS
        ? "Downloaded ";RESPONSE$;" is ";STATUS;" bytes"
    else
        if STATUS > 0 then
            ? " [HTTP response code] "
        else
            ? "[";Fn'Print'Http'Error'Msg$(STATUS);"]"
        endif
    endif
    if FLAGS and XHTTPF_DEBUG then
        ? "Also see DEBUG.REQ (copy of formulated request sent to server)"
    endif
    end

Function Fn'Print'Http'Error'Msg$(status as f6) as s100

    switch status
    case HTTPERR_TOOFEW
        Fn'Print'Http'Error'Msg$ = "Too few params"
        exit
    case HTTPERR_NOMEM
        Fn'Print'Http'Error'Msg$ = "Can't alloc needed memory"
        exit
    case HTTPERR_BADOPCODE
        Fn'Print'Http'Error'Msg$ = "Bad opcode"
        exit
    case HTTPERR_PERR
        Fn'Print'Http'Error'Msg$ = "General parameter error (or unsupported options)"
        exit
    case HTTPERR_LIBLINK
        Fn'Print'Http'Error'Msg$ = "Error loading/linking ASHNET.DLL functions"
        exit
    case HTTPERR_LICENSE
        Fn'Print'Http'Error'Msg$ = "License failure"
        exit
    case HTTPERR_ADDFILE
        Fn'Print'Http'Error'Msg$ = "Error adding file to upload list"
        exit
    case HTTPERR_SYNCREQ
        Fn'Print'Http'Error'Msg$ = "Error during synchronous request"
        exit
    case HTTPERR_POSTFILE
        Fn'Print'Http'Error'Msg$ = "Error processing file for POST"
        exit
    case HTTPERR_STRALC
        Fn'Print'Http'Error'Msg$ = "Error allocating string buffer"
        exit
    case HTTPERR_PUTREQ
        Fn'Print'Http'Error'Msg$ = "Error during PUT"
        exit
    case HTTPERR_DOWNLOAD
        Fn'Print'Http'Error'Msg$ = "Error during download"
        exit
    default
        Fn'Print'Http'Error'Msg$ = "Unknown error"
        exit

    endswitch
EndFunction
