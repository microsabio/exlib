program TCPCLX,1.0(103)		! SBX to send a TCP request to TCPSV1 server
!--------------------------------------------------------------------------
!NOTES
! This is sample subroutine which encapsulates the details of sending a
! request to the TCPSV1 server and receiving a response.  The idea is
! that it could serve as a model for a subroutine that could be used
! by a variety of client programs that needed to make a TCP request to
! a server similar to TCPSV1.   See the program TCPCL1 for a sample
! program calling this subroutine.
!
!USAGE:
!	xcall TCPCLX,SERVER,REQ,TIMER,STATUS,RSP{,FLAGS,SOCKET}
!
!WHERE:
!	SERVER (string) is the IP address or domain of the server
!           It should have :<port> appended to specify the port,e.g.:
!               SERVER="99.33.123.44:39876"
!               SERVER="myserver.com:39876"
!
!   REQ (string) is the request string
!           Format is arbitrary but should obviously be something that
!           the server understands, and within the size limits imposed
!           by the server.  (See configuration section below)
!
!   TIMER (numeric) is the number of MILLISECONDS you want to wait for a
!       a response before giving up.  (Default = 10000 = 10 seconds)
!
!   STATUS (float) returns 0 for timeout (RSP=""), <0 for TCP error
!          If RSP="" and STATUS>0, then STATUS = Basic error
!
!   RSP (string) is the response string (returned)
!	       In the case of a TCP error, we put msg in RSP
!
!   FLAGS (numeric) optional flags                      [102]
!       0x0001 = leave socket open, return in SOCKET    [102]
!
!   SOCKET (numeric) returned socket (if FLAG and 1)    [102]
!       
!
!DEBUGGING NOTES:
!   If you use SET DEBUG prior to running the program which calls this
!   routine (assuming it is running on a real terminal and not a web
!   server), then we output various messages to the screen
!--------------------------------------------------------------------------
!{Edit History}
!
! 08-09-2022 --- 103 --- Jack McGregor
! Increase packet size

! 03-01-2008 --- 102 --- Jack McGregor
! Add FLAGS, SOCKET args
!
! 08-09-2007 --- 101 --- Jack McGregor
! If TIMER < 1000, don't retry initial connection.
!
![100] April 18, 2006 09:42 PM          Edited by Jack
!	Created (based on TCPSV1)
!------------------------------------------------------------------------

++pragma SBX
++pragma ERROR_IF_NOT_MAPPED "ON"

++include ASHINC:ASHELL.DEF
++include ASHINC:XCALL.BSI		! picks up param list


! configurable parameters (may need to match server)
define REQ_TERMINATOR$ = "|"         ! Request/Response terminator
! define RSP_BUF_SIZE    = 5000        ! Max length of entire response
! define REQ_BUF_SIZE    = 10000       ! Max length of entire request
define RSP_BUF_SIZE    = 100000       ! [103] Max length of entire request was 5000
define REQ_BUF_SIZE    = 100000       ! [103] Max length of entire request was 10000

! TCP-related definitions
define TCP_PORT        = 39876       ! default port (if not specified)
define TCP_BUF_SIZE    = 10000       ! Size of TCP read/write buffer [103] was 2048
define TCP_DEFAULT_TIMER = 10000     ! default send/recv timeout

![102] define FLAGS
define CLXF_LEAVE_OPEN = 1           ! [102] leave socket open

map1 PARAMS
    map2 SERVER,S,100
    map2 REQ'BUF,S,REQ_BUF_SIZE      ! buffer to hold entire request
    map2 RSP'BUF,S,RSP_BUF_SIZE      ! buffer to hold entire response
    map2 TIMER,F
    map2 FLAGS,B,4                   ! [102] see CLFX_xxx

map1 TCP'PARAMS
    map2 TCP'PORT,B,2	         ! Port to connect on
    map2 TCP'BUF,S,TCP_BUF_SIZE  ! buffer for reading/writing to socket
    map2 TCP'STATUS,F
    map2 TCP'SOCKPORT,F
    map2 TCP'OP,F
    map2 TCP'FLAGS,F,6
    map2 TCP'TIMER,B,4
    map2 TCP'HOSTNAME,S,100

map1 MISC
    map2 X,F
    map2 BYTES'LEFT,F
    map2 T1,F

BEGIN:
    on error goto TRAP

    if (XCBCNT < 5) then
        ? "Insufficient parameters (";XCBCNT;") passed to TCPCLX"
        stop
        ! end   ! (this would return to caller)
        xcall MIAMEX,82	! (this would abort to dot prompt)
    endif

    ! pick up the three input params in one shot
    xgetargs SERVER,REQ'BUF,TIMER

    xgetarg 6,FLAGS                 ! [102] pick up optional flags

    if TIMER = 0 then
        TIMER = TCP_DEFAULT_TIMER
    endif

    ! parse SERVER to get port
    xcall strip,SERVER
    X = instr(1,SERVER,":")
    if X < 1 then
        TCP'PORT = TCP_PORT         ! no port specified; use default
        TCP'HOSTNAME = SERVER
    else
        TCP'PORT = SERVER[X+1,-1]
        TCP'HOSTNAME = SERVER[1,X-1]
    endif

    if DEBUG then
        ? "TCPCLX Debug: TCP'PORT=";TCP'PORT;" TCP'HOSTNAME=";TCP'HOSTNAME
    endif

    call CONNECT'TO'SERVER
    if TCP'STATUS < 0 goto RETURN'TO'CALLER

    ! as a convenience, append the request/response terminator
    ! character to response if it is missing
    xcall strip,REQ'BUF
    if REQ'BUF[-1,-1] # REQ_TERMINATOR$ then
        REQ'BUF = REQ'BUF + REQ_TERMINATOR$
    endif

    call SEND'REQUEST
    if TCP'STATUS <= 0 goto RETURN'TO'CALLER

    call RECV'RESPONSE


RETURN'TO'CALLER:
    if TCP'SOCKPORT > 0 and ((FLAGS and CLXF_LEAVE_OPEN)=0) then  ![102]
        call CLOSE'SOCKET
    endif

    ! return parameters
    xputarg 4,TCP'STATUS	! return arg 4 from TCP'STATUS

    ! if TCP error, put error msg in response
    if TCP'STATUS < 0 then
        xcall TCPX,TCPOP_ERRMSG,abs(TCP'STATUS),RSP'BUF,0, 0
    endif

    xputarg 5,RSP'BUF  	    ! return arg 5 from RSP'BUF

    if (FLAGS and CLXF_LEAVE_OPEN) then   ! [102]
        xputarg 7, TCP'SOCKPORT           ! [102]   
    endif                                 ! [102] 

    End		! return to caller

TRAP:
    ? "Error in TCPCLX.SBX: Err # ";err(0)
    xcall SLEEP,5           ! make sure we see it (remove for web server)
    RSP'BUF = ""
    TCP'STATUS = err(0)     ! return Basic error in TCP'STATUS
    goto RETURN'TO'CALLER

!----------------------------------------------------------------------------
! Establish connection with server
! Inputs:
!   TCP'HOSTNAME
!   TCP'PORT
!   TIMER [101] (if < 1000, don't retry connection)
! Outputs:
!   TCP'STATUS < 0 for error
!   TCP'SOCKPORT (data socket)
! Notes:
!   Retry for up to 3 seconds if we fail
!----------------------------------------------------------------------------

CONNECT'TO'SERVER:
    T1 = TIME
TRY'AGAIN:
    TCP'SOCKPORT = TCP'PORT
	TCP'OP = TCPOP_CONNECT
    TCP'FLAGS = TCPXFLG_BLOCK             ! blocking connection
    TCP'TIMER = 0                         ! this doesn't matter here
    TCP'BUF = ""                          ! MUST BE "" !!!
 	call TCPX

    if TIMER > 1000 then                 ! [101] don't retry if wait not > 1000
     	if TCP'STATUS < 0 and TIME-T1 < 3 then
     	  xcall SLEEP,0.5
     	  goto TRY'AGAIN
        endif
    endif
    return

!----------------------------------------------------------------------------
! Send Request
! Inputs:
!   TCP'SOCKPORT
!   REQ'BUF (request string)
!   TCP_BUF_SIZE
!   TIMER
! Outputs:
!   TCP'STATUS < 0 for error, 0 for timeout
! Uses:
!   BYTES'LEFT,X
! Notes:
!   We may need to break the request up if it is bigger than
!   our TCP buffer
!----------------------------------------------------------------------------

SEND'REQUEST:
    BYTES'LEFT = len(REQ'BUF)
    X = 1
    TCP'STATUS = 1
    do while BYTES'LEFT > 0 and TCP'STATUS > 0
        if BYTES'LEFT > TCP_BUF_SIZE then
            TCP'FLAGS = TCP_BUF_SIZE
            TCP'BUF = REQ'BUF[X;TCP'FLAGS]
            X = X + TCP'FLAGS
        else
            TCP'BUF = REQ'BUF[X;BYTES'LEFT]
            TCP'FLAGS = BYTES'LEFT
        endif

        TCP'OP = TCPOP_WRITE
        TCP'TIMER = TIMER
        call TCPX
        if TCP'STATUS > 0 then
            BYTES'LEFT = BYTES'LEFT - TCP'STATUS
        endif
    loop
    return

!----------------------------------------------------------------------------
! Read response
! Inputs:
!   TCP'SOCKPORT
!   TIMER
! Outputs:
!   TCP'STATUS < 0 for error, 0 for timeout
!   RSP'BUF contains complete response
! Notes:
!   Response must be terminated by REQ_TERMINATOR$ character!!!!
!   (If not, we will time out waiting for it)
!----------------------------------------------------------------------------

RECV'RESPONSE:

    RSP'BUF = ""        ! initialize response buffer
    T1 = TIME

RECV'LOOP:
    TCP'OP = TCPOP_READ
    TCP'FLAGS = 0       ! read whatever is available
    TCP'TIMER = TIMER
    TCP'BUF = ""
    call TCPX

    ! following logic handles an obscure situation where
    ! some kind of event in the TCP channel caused the
    ! read to return immediately with no data
    if TCP'STATUS = 0 and RSP'BUF = "" and TIME-T1 < 2 then
        goto RECV'LOOP
    endif

    if TCP'STATUS <= 0 then
        return          ! abort
    endif

    ! append packet to the response buffer
    RSP'BUF = RSP'BUF + TCP'BUF

    ! check if we got a terminator
    X = instr(1,TCP'BUF,REQ_TERMINATOR$)
    if X < 1 then goto RECV'LOOP    ! no, so keep reading

    if X < len(TCP'BUF) then
        ? "*** WARNING: TERMINATOR NOT LAST CHARACTER IN PACKET! ***"
    endif
    return

!----------------------------------------------------------------------------
! Close socket
! Inputs:
!   TCP'SOCKPORT
! Outputs:
!   TCP'STATUS < 0 for error
!   TCP'SOCKPORT = 0
!----------------------------------------------------------------------------

CLOSE'SOCKET:
    if TCP'SOCKPORT <> 0 then
    	TCP'OP = TCPOP_SHUTDOWN
        call TCPX

    	TCP'OP = TCPOP_CLOSE
        call TCPX
        TCP'SOCKPORT = 0
    endif
    return

!----------------------------------------------------------------------------
! Wrapper for the TCPX routine (in client mode)
! Inputs:
!   TCP'OP
!   TCP'SOCKPORT
!   TCP'BUF (for send)
!   TCP'TIMER
!   TCP'FLAGS
! Outputs:
!   TCP'STATUS  (<0 for error)
!   TCP'BUF (for recv)
! Notes:
!
!----------------------------------------------------------------------------
TCPX:
    if DEBUG then
        ? "TCPCLX Debug: TCPX Op:";TCP'OP;" Flags:";TCP'FLAGS;
        if TCP'OP=TCPOP_CONNECT then
            ? " Hostname: ";TCP'HOSTNAME;" Port: ";TCP'SOCKPORT;
        endif
        if TCP'OP=TCPOP_CLOSE then ? " Socket:";TCP'SOCKPORT;
        ? "...";
    endif

	xcall TCPX,TCP'OP,TCP'STATUS,TCP'BUF,TCP'SOCKPORT,TCP'FLAGS, &
	       TCP'TIMER,TCP'HOSTNAME
	       
    if DEBUG then
        ? 
        ? "TCPCLX Debug:  Status:";TCP'STATUS;
        IF TCP'OP = TCPOP_READ then ? "[";TCP'BUF;"]";
        IF TCP'OP = TCPOP_CONNECT then ? " Socket:";TCP'SOCKPORT;
        ?
    endif        	           
    return
    
