:R
:<JSON

   Test/demo programs illustrating SOSFUNC:FNMJSON.BSI functions

   MJ1.BP - Simple Load (deserialize) and output (serialize) test
   MJ2.BP - Load, retrieve/set named values, traverse
   MJ3.BP - Convert ordmap to JSON string, deserialize, re-serialize
   MJ4.BP - Build a simple invoice JSON doc from scratch
   MJ5.BP - Pre-programmed retrieval and update of MJ4.JSO

>

