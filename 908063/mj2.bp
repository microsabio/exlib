program mj2, 1.0(106)  ! test fnmjson.bsi functions
!------------------------------------------------------------------------
!EDIT HISTORY
! [106] 05-Aug-19 / jdm / Add Fn'MJ'CountSubElements
! [105] 03-Aug-19 / jdm / Convert from mlistjson2.bp; use fnmjson.bsi
! [104] 01-Oct-18 / jdm / renamed fnmlistjsn.bsi to mlistjson.bsi
! [103] 06-Jul-18 / jdm / add Fn'MJ'TraverseToMap() test; tinker with
!                           Fn'Traverse'CB1() routine
! [102] 04-Jul-18 / jdm / add Fn'MJ'Traverse()
! [101] 30-Jun-18 / jdm / add Fn'MJ'SetNamedValue$; merge name,path
! [100] 05-Jun-18 / jdm / created to test Fn'MJ'GetNamedValue$()
!------------------------------------------------------------------------
!REQUIREMENTS
!   6.5.1633.0+
!NOTES
!   This program allows various kinds of interactive testing of the
!   get and set value functions, plus load, serialize, traverse, count.
!   Also see:
!       MJ1 for basic load and serialize
!       MJ3 for load from and/or map
!       MJ4 for building a simple JSON document from scratch (created MJ4.JSO)
!       MJ5 for pre-programmed query/change/deletion operations on MJ4.JSO
!------------------------------------------------------------------------

++include'once ashinc:ashell.def
++include'once ashinc:types.def
++include'once sosfunc:fnmjson.bsi      ! [105]
define MLIST_FEATURE_LEVEL = 3          ! we want the Mlist'Multi'Level'Display()
++include'once mlistdemo.bsi

define TRAVERSE_CALLBACK_LIST$ = "mj2.lst"   ! output file used by sample traverse callback

dimx $JSON, MLIST(varstr)

map1 GLOBALS
    map2 OP,B,1
    map2 FSPEC$,s,0     ! needs to be dynamic for serialize-to-string option
    
   
    ? "Test FNMJSON.BSI search, set, traverse routines"
    
    input line "JSON file to parse: ",FSPEC$
    if Fn'MJ'LoadDocFromFile($JSON(),FSPEC$) > 0 then
        OP = 0
        input "1) to display, 0) to proceed: ",OP
        if OP then
            call Mlist'Multi'Level'Display($JSON(),FSPEC$+" as MLIST()...")
        endif
    
        do
            OP = 0
            input "1) Get value, 2) Set value, 3) Traverse, 4) Traverse to Map, 0) Quit: ",OP
            
            switch OP
                case 1
                    call Test'MJ'NamedValue($JSON(),.FALSE)
                    exit
                case 2
                    call Test'MJ'NamedValue($JSON(),.TRUE)
                    exit
                case 3
                    call Test'MJ'Traverse($JSON())
                    exit
                case 4                                      ! [103]
                    call Test'MJ'TraverseToMap($JSON())
                    exit

            endswitch
        loop until OP = 0
    endif

    end
    
!---------------------------------------------------------------------
!Procedure:
!   Simple test/demo of Fn'MJ'GetNamedValue$() and Fn'MJ'SetNamedValue$()
!   and Fn'MJ'SetNamedValueX()     [105]
!Params:
!   $json  (mlist) [byref] - JSON document stored as MLIST
!   bset (BOOLEAN) [in]  - if .TRUE, test Fn'MJ'SetNamedValue$()
!Globals:
!Notes:
!---------------------------------------------------------------------
Procedure Test'MJ'NamedValue($json() as mlist(varstr), bset as BOOLEAN:inputonly)

    map1 locals
        map2 pnamepath$,s,0
        map2 pvalue$,s,0
        map2 nvalue$,s,0
        map2 fspec$,s,0     ! needs to be dynamic for serialize-to-string option
        map2 indent,b,2
        map2 status,i,4
        map2 subelements,i,4    ! [106]

    do
        pnamepath$ = ""
        input "Enter path to search for (blank to quit): ",pnamepath$
        if pnamepath$ # "" then
            pvalue$ = Fn'MJ'GetNamedValue$($json(), pnamepath$) 
            
            if pvalue$ # "" then
                ? "Value: ";pvalue$
                ? "Found at: ";pnamepath$
                
                ![106] count and display the # of subelements (just because)
                subelements = Fn'MJ'CountSubElements($json(),pnamepath$)
                ? "(Element has ";
                if subelements > 0 then
                    ? subelements;
                else
                    ? "no";
                endif
                ? " subelements)"
                
                if bset then        ! if testing Set option...
                    nvalue$ = ""
                    ? "New value (ENTER to keep, * to delete, scalar, object, or array) " ! [105]
                    input line "",nvalue$
                    if nvalue$ = "*" then           ! [105] special flag for delete
                        nvalue$ = .NULL
                    endif

                    ![105] pvalue$ = Fn'MJ'SetNamedValue$($json(), pnamepath$, nvalue$) 
                    status = Fn'MJ'SetNamedValueX($json(), pnamepath$, nvalue$)    ! [105]
                    ? "Status: ";status;
                    switch status
                        case 0
                            ? " [OK]" 
                            exit
                        case MJERR_BAD_JSON
                            ? " (New value does not look like valid JSON)"
                            exit
                        case MJERR_ASH_VER
                            ? " (A-Shell version too low)"
                            exit
                        case MJERR_BAD_OBJECT
                            ? " (Object has mismatched { ... } braces)"
                            exit
                        case MJERR_BAD_ARRAY
                            ? " (Array has mismatched [ ... ] brackets)"
                            exit
                        default
                            ? " (Unknown error)"
                            exit
                    endswitch
                           
                    ? "Old value: ";pvalue$
                    ? "Replaced with: ";nvalue$
                    
                    fspec$ = ""
                    input line "Enter file to re-serialize it to (blank for display): ",fspec$
                    if fspec$ = "" then
                        call Mlist'Multi'Level'Display($json(),"Updated MLIST()...")
                    else
                        input "Enter indent (0 for compact stream, else # spaces per level): ",indent
                        status = Fn'MJ'Serialize($json(), fspec$, indent)
                        ? "Return status: ";status;" (bytes output)"
                    endif
                endif
            else
                ? "NOT FOUND!"
                repeat
            endif
        endif
    loop until pnamepath$ = ""

EndProcedure

!---------------------------------------------------------------------
!Procedure:
!   simple demo/test of Fn'MJ'Traverse()
!Params:
!   $json  (mlist) [byref] - JSON document stored as MLIST
!Globals:
!Notes:
!   The callback in this case just outputs a list showing the
!   parameters passed to the callback.
!---------------------------------------------------------------------
Procedure Test'MJ'Traverse($json() as mlist(varstr))

    map1 locals
        map2 pnamepath$,s,0
        map2 count,i,4
        map2 op,i,2
        
    pnamepath$ = ""
    input "Enter path to start traverse at (blank for everything): ",pnamepath$
    
    call Fn'Traverse'CB1()      ! initialize the callback routine
    
    count = Fn'MJ'Traverse($json(), pnamepath$, @Fn'Traverse'CB1())
    ? "Items processed: ";count
    if count > 0 then
        if lookup(TRAVERSE_CALLBACK_LIST$) then
            op = 1
            input "1) view file created by callback, 0) end ", op
            if op then
                xcall EZTYP, TRAVERSE_CALLBACK_LIST$
            endif
        else
            ? "Possible error: callback list file (";TRAVERSE_CALLBACK_LIST$;") not found"
        endif
    endif
    
EndProcedure

!---------------------------------------------------------------------
!Procedure:
!   simple demo/test of Fn'MJ'TraverseToMap()
!Params:
!   $json  (mlist) [byref] - JSON document stored as MLIST
!Globals:
!Notes:
!   The callback in this case is a standard one included within the
!   fnmlistjsn.bsi module.
!---------------------------------------------------------------------
Procedure Test'MJ'TraverseToMap($json() as mlist(varstr))

    map1 locals
        map2 pnamepath$,s,0
        map2 count,i,4
        map2 op,i,2
        map2 flags,b,4
        
    dimx $map, ordmap(varstr;varstr)    
        
    pnamepath$ = ""
    input "Enter path to start traverse at (blank for everything): ",pnamepath$
    input "Flags: +1 (horz), +2 (vert), +4 (include path in map index): ",flags
    
    count = Fn'MJ'TraverseToMap($json(), pnamepath$, $map(), flags)
    ? "Items processed: ";count
    
    foreach $$i in $map()
        ? .key($$i);"  ==>  ";$$i
    next $$i
    
EndProcedure


!---------------------------------------------------------------------
!Function:
!   Sample callback function for Fn'MJ'Traverse()
!Params:
!   curpath$  (str) [in] - current path (parent)
!   item$ (str) [in]     - current item (within the curpath)
!Returns:
!   1 to count the item 'processed'  (always in this example)
!   0 to count the item 'skipped'
!   -1 to terminate the traverse
!Globals:
!   TRAVERSE_CALLBACK_LIST$  (name of file to output to)
!Notes:
!   Call with no arguments to initialize.
!   First call after that will establish the base path (curpath$)
!   From that point forward, until the base path is no longer
!   the base of the incoming curpath, we just append lines to 
!   the file TRAVERSE_CALLBACK_LIST$ showing the args passed.
! 
!   (A more real-world version would surely do something more useful)
!---------------------------------------------------------------------
!++pragma trace_begin (0,"","$T")
Function Fn'Traverse'CB1(curpath$ as s0:inputonly, item$ as s0:inputonly) as i2

    static map1 basepath$,s,0
    map1 newpath$,s,0
    
    if .argcnt = 0 then     ! initialize
        open #3737, TRAVERSE_CALLBACK_LIST$, output
        close #3737
        basepath$ = ""
    else
        newpath$ = curpath$ ![103] + Fn'MJ'Item'Name$(item$)
        if basepath$ = "" then      ! first found path establishes the base 
            basepath$ = newpath$ 
        endif
        if basepath$ = "" or instr(1,newpath$,basepath$) = 1 then   ! [103] "" basepath matches everything
            open #3737, TRAVERSE_CALLBACK_LIST$, append
            ? #3737, curpath$;"   ";item$
            close #3737
            Fn'Traverse'CB1 = 1     ! indicate we "processed" the item
        else
            Fn'Traverse'CB1 = -1    ! we're done
        endif
    endif
EndFunction
