:<(REGEX) REGEX.SBR Samples
    REGEX.BP  - Simplest possible test program
    REGEX2.BP - Similar to REGEX but compares pattern to every line in file
    REGEX.SEQ - Sample file
    REGEML.BP - Sample using a REGEX to validate an email address format
    REGEX4.BP - Example with array of fixed length submatches
    REGEX5.BP - Example with array of dynamic submatches
    REGEX6.BP - Example of search/replace
>
 
