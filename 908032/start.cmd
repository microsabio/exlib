:<(IMAGE) Imaging samples (most require A-Shell Windows Imaging Add-on)
    IMAGE - Sample/demo for XCALL AUI,AUI_IMAGE
    SAMPLE.* - Sample image files
    ATEIMG - Sample/demo using ATECCH.SBX to first xfer file to ATECACHEDIR
    IMGDSX - Useful image display utility
    IMGTST - Sample showing various ways to launch external image viewer
    STIMGT - Sample using AUI_CONTROL with MBF_STATIC to display image/icon.

Also see [907,33] for SBX to xfer image to ATE cache
>
