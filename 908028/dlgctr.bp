program DLGCTR,1.0(101)      ! demonstrate centering a dialog on screen
!---------------------------------------------------------------------------
!EDIT HISTORY
![101] 01-Jun-20 / jdm / add a centered text output to test bgc issue (fixed 1681.3)
![100] 22-Sep-06 / jdm / Created to test using new  AUIWIN feature (966.2) to get screen
!                           size in dlg units, then create a dialog centered and taking up 
!                           nearly the entire screen, with the main window hidden. 
!---------------------------------------------------------------------------

++include ashinc:ashell.def

MAP1 WIN'PARAMS
    MAP2 LFT,B,2
    MAP2 TOP,B,2
    MAP2 RGT,B,2
    MAP2 BTM,B,2
    MAP2 SHOFLG,B,2,1
    MAP2 ROWS,B,2
    MAP2 COLS,B,2
    MAP2 TOPSTS,B,1
    MAP2 BOTSTS,B,1
    MAP2 CID,B,2            ! ctlid (0=main win, -1=rtn screen res)
    MAP2 HGRID,B,2          ! horz grid 
    MAP2 VGRID,B,2          ! vert grid 
    MAP2 HRES,B,2           ! horz screen resolution (in pixels)
    MAP2 VRES,B,2           ! vert screen resolution (in pixels)
    MAP2 HGRIDPIX,B,2       ! dialog grid unit horz (in pixels)
    MAP2 VGRIDPIX,B,2       ! dialog grid unit vert (in pixels)
    MAP2 HALTRES,B,2        ! horz screen size in dialog (altpos) units
    MAP2 VALTRES,B,2        ! vert screen size in dialog (altpos) units
    MAP2 VDLGOVERHEAD,B,2   ! dlg vert overhead for caption (pixels) 
    MAP2 HDLGOVERHEAD,B,2   ! dlg horz overhead

MAP1 CTL'PARAMS
    MAP2 SROW,F
    MAP2 EROW,F
    MAP2 SCOL,F
    MAP2 ECOL,F
    MAP2 STATUS,F
    MAP2 DLGID,B,2
    MAP2 PARENTID,B,2
    MAP2 CTLID,B,2
    MAP2 EXITCODE,F

MAP1 MISC
    MAP2 A,B,1
    MAP2 A$,S,1

    ? tab(-1,0);

    trace.print "Test creating a centered dialog with no hidden main window"
    trace.print "First we hide the main window..."

    xcall AUI,AUI_WINDOW,0      ! hide main window

    trace.print
    trace.print "Now get screen dimensions..."

    xcall AUI,AUI_WINDOW,SW_QUERY,HGRIDPIX,VGRIDPIX,HALTRES,VALTRES, &
        HDLGOVERHEAD,VDLGOVERHEAD,TOPSTS,BOTSTS,-1,HRES,VRES  ! [102]

    trace.print "Screen Res: "+HRES+"x"+VRES+" (pixels)"
    trace.print "    "+HALTRES+"x"+VALTRES+" (dialog altpos units; cols x rows)" 
    trace.print "Dlg unit size: "+HGRIDPIX+"x"+VGRIDPIX+" (pixels)" 
    trace.print "Dlg overhead for caption: " + VDLGOVERHEAD + " (pixels) "

    trace.print
    trace.print "Now create a dialog nearly size of screen"

    ! leave a margin of 1 dialog units all around
    SROW = 2
    SCOL = 2
    EROW = VALTRES - 3
    ECOL = HALTRES - 2
    PARENTID = 0

    trace.print "Dialog coordinates (MBF_ALTPOS) = " &
        +SROW+","+SCOL+" - "+EROW+","+ECOL

    xcall AUI, AUI_CONTROL, CTLOP_ADD, DLGID, "Centered Dialog",  &
        MBST_ENABLE, MBF_DIALOG+MBF_ALTPOS+MBF_DLGNOCREEP, &
        NUL_CMD$, NUL_FUNC$, STATUS, &
        SROW, SCOL, EROW, ECOL, NUL_FGC, &h00daab90, &
        NUL_FONTATTR, NUL_FONTSCALE, NUL_FONTFACE$, NUL_TOOLTIP$, &
        PARENTID

    trace.print "Dialog ID = " + DLGID
    
    if DLGID = 0 then
        trace.print "!!!ERROR: Unable to create dialog!!!"
        end
    endif

    ! query the dialog dimensions just to see how they compare to what we
    ! asked for

    trace.print "Querying dialog to check actual dimensions..."
    xcall AUI,AUI_WINDOW,-1,LFT,TOP,RGT,BTM, &
        ROWS,COLS,TOPSTS,BOTSTS,DLGID,HGRID,VGRID  

    trace.print " Universal units (10000x10000): "
    trace.print "   TOP="+TOP+",LFT="+LFT+",BTM="+BTM+",RGT="+RGT &
        + " ,VGRID="+VGRID+",HGRID="+HGRID

    trace.print " Pixels: "
    trace.print "   " &
        + " TOP="+int(TOP*VRES/10000) &
        + ",LFT="+int(LFT*HRES/10000) &
        + ",BTM="+int(BTM*VRES/10000) &
        + ",RGT="+int(RGT*HRES/10000) &
        + ",VGRID="+VGRIDPIX &
        + ",HGRID="+HGRIDPIX 

    trace.print " Dialog (altpos) Units: "
    trace.print "   " &
        + " TOP="+int(TOP/VGRID) &
        + ",LFT="+int(LFT/HGRID) &
        + ",BTM="+int(BTM/VGRID) &
        + ",RGT="+int(RGT/HGRID)


    ! add an OK button
    xcall AUI, AUI_CONTROL, CTLOP_ADD, NUL_CTLID, "OK", &
         MBST_ENABLE, MBF_BUTTON+MBF_KBD, &
        "VK_ESC", NUL_FUNC$, NUL_CSTATUS, &
        EROW-4, int(ECOL/2)-5, EROW-3, int(ECOL/2)+5, NUL_FGC, NUL_BGC, &
        NUL_FONTATTR, NUL_FONTSCALE, NUL_FONTFACE$, NUL_TOOLTIP$, &
        DLGID

    tprint tab(3,5);"This dialog should be almost entire size of screen"
    tprint tab(4,7);"with a small border around the outside."

    tprint tab(6,5);"Click the OK button to restore original window."

    xcall AUI, AUI_CONTROL, CTLOP_ADD, "txtCenterTest", "(This should be centered)", MBST_ENABLE, &
        MBF_STATIC+MBF_CENTER, NUL_CMD$, NUL_FUNC$, NUL_CSTATUS, &
        8,SCOL,10,ECOL, NUL_FGC, NUL_BGC, &
        NUL_FONTATTR, NUL_FONTSCALE, NUL_FONTFACE$, NUL_TOOLTIP$, &
        NUL_PARENTID, NUL_WINCLASS$, NUL_WINSTYLE, NUL_WINSTYLEX, &
        NUL_CTYPE2

    trace.print
    trace.print "Waiting for an event..."

    CTLID = 0
    xcall AUI, AUI_EVENTWAIT, DLGID, CTLID, EXITCODE, EVW_NEXT

    trace.print "Event: EXITCODE="+ EXITCODE + ", CTLID=" + CTLID

    ! (we don't care what the event was, just finish up)

    trace.print "Closing dialog..."
    xcall AUI, AUI_CONTROL, CTLOP_DEL, DLGID

    trace.print "Restore original main window..."

    xcall AUI, AUI_WINDOW, 1

    trace.print "OK, all done"

    end


