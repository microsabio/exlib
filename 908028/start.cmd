:<(UI) Window, Terminal, Monitor  characteristics
    AUIWIN    - Test querying, changing, resetting window characteristics
    DLGCTR    - Test/demo using AUIWIN to get info needed to position a 
                parent-less dialog as desired relative to the desktop.
    DLGNEST   - Test various nested dialog scenarios.
    DLGCTR3   - Test creating different kinds of dialogs on dual monitors
    TRMCHR    - Test querying terminal / dialog characteristics
    FLDTST    - Test for field emulation vs mode emulation
    RSTCOLORS - Reset color palette; optionally save to .ash
    DLGNOCAP  - Experiments with positioning a DLGNOCAP dialog
    DLGPOS2   - Position a child dialog relative to control in parent dlg
    DLGSCRL   - Example of dialog with scroll bar
    DLGSCALE  - Dialog for experimenting with scale factors
>
