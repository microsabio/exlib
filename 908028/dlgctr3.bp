program DLGCTR3,1.0(101)      ! nested dialog centering test
!---------------------------------------------------------------------------
!EDIT HISTORY
![100] October 27, 2012 08:43 AM        Edited by jack
!   Created from DLGCTR2
!---------------------------------------------------------------------------

++include ashinc:ashell.def

MAP1 CTL'PARAMS
    MAP2 SROW,F
    MAP2 EROW,F
    MAP2 SCOL,F
    MAP2 ECOL,F
    MAP2 SROW2,F
    MAP2 EROW2,F
    MAP2 SCOL2,F
    MAP2 ECOL2,F
    MAP2 STATUS,F
    MAP2 DLGID1$,S,20,"dlgMain"
    MAP2 DLGID2$,S,20,"dlgCenter2"
    MAP2 PARENTID1$,s,20
    MAP2 PARENTID2$,s,20
    MAP2 OKID$,s,20,"btnOK"
    MAP2 OPENID$,S,20,"btnOpen"
    MAP2 CLOSEID$,S,20,"btnClose"
    MAP2 CTLID$,S,20
    MAP2 EXITCODE,F
    MAP2 UNITS,B,1
    MAP2 CTYPE,B,4
    MAP2 HIDE,B,1
    MAP2 OPTIONS1,B,2
    MAP2 OPTIONS2,B,2
    MAP2 MSG$,S,100

    ? tab(-1,0);"Nested dialog centering test (ENTER to all prompts for defaults)"
    ? "First dialog configuration: "
    input "  0) Use main window grid units, 1) Use altpos units:",UNITS

    ? "  Enter srow,scol,erow,ecol (";
    if UNITS then
        ? "altpos units) ";
        CTYPE = MBF_ALTPOS
    else
        ? "main window grid units) ";
    endif

    input "",SROW,SCOL,EROW,ECOL
    if SROW=0 then
        SROW = 1
        SCOL = 1    
        EROW = 10
        ECOL = 50
    endif

    input "  Options (sum) +1) DLGNOCREEP, +2) DLGNOPARENT: ",OPTIONS1
    input "  0) Visible main window, 1) hidden main window: ",HIDE    

    ? "Second dialog configuration (size slightly smaller than first dlg)"
    input "  Options (sum) +1) DLGNOCREEP, +2) DLGNOPARENT: ",OPTIONS2 
    input "  Parent: 0) main window or desktop, 1) first dialog: ",PARENTID2$
    
    if HIDE then                    
        xcall AUI,AUI_WINDOW,0      ! hide main window
    endif

    CTYPE = CTYPE or MBF_DIALOG 
    if OPTIONS1 and 1 then                   
        CTYPE = CTYPE or MBF_DLGNOCREEP     
    endif                                   
    if OPTIONS1 and 2 then                   
        CTYPE = CTYPE or MBF_DLGNOPARENT    
    endif                                   

    PARENTID1$ = "0"  ! (main window)
    xcall AUI, AUI_CONTROL, CTLOP_ADD, DLGID1$, "First Centered Dialog",  &
        MBST_ENABLE+MBST_CENTER, CTYPE, &
        NUL_CMD$, NUL_FUNC$, STATUS, &
        SROW, SCOL, EROW, ECOL, NUL_FGC, NUL_BGC, &
        NUL_FONTATTR, NUL_FONTSCALE, NUL_FONTFACE$, NUL_TOOLTIP$, &
        PARENTID1$

    ! add some text with info about the dialog
    MSG$ = "Coords: "+SROW+","+SCOL+","+EROW+","+ECOL
    if CTYPE and MBF_DLGNOCREEP then
        MSG$ += "  MBF_DLGNOCREEP"
    endif
    if CTYPE and MBF_DLGNOPARENT then
        MSG$ += "  MBF_DLGNOPARENT"
    endif
    MSG$ += " ParId: "+PARENTID1$

    xcall AUI, AUI_CONTROL, CTLOP_ADD, NUL_CTLID, MSG$, &
        MBST_ENABLE, MBF_STATIC, NUL_CMD$, NUL_FUNC$, NUL_CSTATUS, &
        2, 1, 3, ECOL, NUL_FGC, NUL_BGC, &
        NUL_FONTATTR, NUL_FONTSCALE, NUL_FONTFACE$, NUL_TOOLTIP$, &
        NUL_PARENTID, NUL_WINCLASS$, NUL_WINSTYLE, NUL_WINSTYLEX, &
        NUL_CTYPE2
    
    ! add an Open button 
    xcall AUI, AUI_CONTROL, CTLOP_ADD, OPENID$, "Open", &
         MBST_ENABLE, MBF_BUTTON+MBF_KBD, &
        "VK_xF101", NUL_FUNC$, NUL_CSTATUS, &
        EROW-SROW-3, (ECOL-SCOL)/2-8, EROW-SROW-2, (ECOL-SCOL)/2-2, &
        NUL_FGC, NUL_BGC, NUL_FONTATTR, NUL_FONTSCALE, NUL_FONTFACE$, &
        "Try moving this dialog before clicking open",DLGID1$

    ! add a Close button 
    xcall AUI, AUI_CONTROL, CTLOP_ADD, CLOSEID$, "Close", &
         MBST_ENABLE, MBF_BUTTON+MBF_KBD, &
        "VK_ESC", NUL_FUNC$, NUL_CSTATUS, &
        EROW-SROW-3, (ECOL-SCOL)/2+2, EROW-SROW-2, (ECOL-SCOL)/2+8, &
        NUL_FGC, NUL_BGC, NUL_FONTATTR, NUL_FONTSCALE, NUL_FONTFACE$, &
        NUL_TOOLTIP$, DLGID1$

    ! wait for event
    CTLID$ = OPENID$  ! (start with focus on OPEN button)
    do
        xcall AUI, AUI_EVENTWAIT,  DLGID1$, CTLID$, EXITCODE, 0

        if EXITCODE = -101 then     ! open 2nd dialog   

            CTYPE = CTYPE or MBF_DIALOG 
            if OPTIONS2 and 1 then                   
                CTYPE = CTYPE or MBF_DLGNOCREEP     
            endif                                   
            if OPTIONS2 and 2 then                   
                CTYPE = CTYPE or MBF_DLGNOPARENT    
            endif                                   

            if val(PARENTID2$) # 0 then
                PARENTID2$ = DLGID1$
            else
                PARENTID2$ = "0"
            endif

            ! make 2nd dialog slightly smaller than first
            SROW2 = SROW + 1
            SCOL2 = SCOL + 1
            EROW2 = EROW - 1
            ECOL2 = ECOL -1
            xcall AUI, AUI_CONTROL, CTLOP_ADD, DLGID2$, "Second Centered Dialog",  &
                MBST_ENABLE+MBST_CENTER, CTYPE, &
                NUL_CMD$, NUL_FUNC$, STATUS, &
                SROW2, SCOL2, EROW2, ECOL2, NUL_FGC, NUL_BGC, &
                NUL_FONTATTR, NUL_FONTSCALE, NUL_FONTFACE$, NUL_TOOLTIP$, &
                PARENTID2$

            ! add some text with info about the dialog
            MSG$ = "Coords: "+SROW2+","+SCOL2+","+EROW2+","+ECOL2
            if CTYPE and MBF_DLGNOCREEP then
                MSG$ += "  MBF_DLGNOCREEP"
            endif
            if CTYPE and MBF_DLGNOPARENT then
                MSG$ += "  MBF_DLGNOPARENT"
            endif
            MSG$ += " ParId: "+PARENTID2$
        
            xcall AUI, AUI_CONTROL, CTLOP_ADD, NUL_CTLID, MSG$, &
                MBST_ENABLE, MBF_STATIC, NUL_CMD$, NUL_FUNC$, NUL_CSTATUS, &
                2, 1, 3, ECOL, NUL_FGC, NUL_BGC, &
                NUL_FONTATTR, NUL_FONTSCALE, NUL_FONTFACE$, NUL_TOOLTIP$, &
                NUL_PARENTID, NUL_WINCLASS$, NUL_WINSTYLE, NUL_WINSTYLEX, &
                NUL_CTYPE2

            ! add an OK button (horizontally centered)
            xcall AUI, AUI_CONTROL, CTLOP_ADD, OKID$, "OK", &
                 MBST_ENABLE+MBST_HCENTER, MBF_BUTTON+MBF_KBD, &
                "VK_ESC", NUL_FUNC$, NUL_CSTATUS, &
                EROW-SROW-3, 1, EROW-SROW-2, 6, &
                NUL_FGC, NUL_BGC, NUL_FONTATTR, NUL_FONTSCALE, NUL_FONTFACE$, &
                NUL_TOOLTIP$, DLGID2$

            CTLID$ = OKID$  ! (start with focus on OK button)
            xcall AUI, AUI_EVENTWAIT,  DLGID2$, CTLID$, EXITCODE, 0
            ! treat any event as close

            xcall AUI, AUI_CONTROL, CTLOP_DEL, DLGID2$  ! delete 2nd dialog  
            
            CTLID$ = CLOSEID$   ! reset focus to close button on 1st dlg
            EXITCODE = 0
        endif
    loop until EXITCODE = 1

    xcall AUI, AUI_CONTROL, CTLOP_DEL, DLGID1$  ! delete main dialog  

    if HIDE then                    
        xcall AUI, AUI_WINDOW, 1    ! restore main window
    endif

    end


