program xtra21,1.0(100)   ! demo of indexed PopupMenus 
!---------------------------------------------------------------------------
!EDIT HISTORY
![100] 08-Jan-17 17:16       Edited by Jack
!    Created   
!---------------------------------------------------------------------------
!NOTES
!   Requires 6.3.1542+
!
!   Presents a list of cities to visit, with the context menu listing possible
!   activities in that city (and thus varying by city, i.e. by row)
!
!   Indexed PopupMenus are PopupMenus that are associated with index 
!   characters A-Z rather than with columns.  See the following A-Shell
!   Reference topic for details...
!
!   "Indexed Popup Menus" 
!   http://www.microsabio.net/dist/doc/conref/00ashref.htm#!indexedpopupmenus.htm
!---------------------------------------------------------------------------

++include ashinc:ashell.def
++include ashinc:xtree.sdf
++include ashinc:xtree.def    

defstruct ST_XTROW      ! row struct definition
    map2 PMXREF,S,15    ! popupmenu cross reference column
    map2 CITY,S,30      ! city to visit
endstruct

dimx XTROW(1),ST_XTROW,auto_extend
map1 XTROW$,S,sizeof(ST_XTROW)

map1 MISC
    map2 EXITCODE,F
    map2 TOT'ITEMS,B,4
    map2 I,F
    map2 ANSWER,B,4

map1 XPARAMS
    map2 COLDEF$,S,0
    map2 XTR,XTRCTL
    map2 MMOCLR,MMOCLR
    map2 XTFLAGS,ST_XFLAGS

    ? tab(-1,0);"Test XTREE indexed popupmenus"
    
    COLDEF$ = ""
    
    ! PopupMenu B for beach activities
    COLDEF$ += "0~0~ ~H~PopupMenu:B=Beach Activities,[SUB];" &
                + "Bodysurfing,VK_xF101;Sunbathing,VK_xF102;Beach Volleyball,VK_xF103;" &
                + "[ENDSUB],;~~"
                
    ! PopupMenu C for city tours"
    COLDEF$ += "0~0~ ~H~PopupMenu:C=City Tours,[SUB];" &
                + "Walking Tour,VK_xF201;Bus Tour,VK_xF202;Segway Tour,VK_xF203;" &
                + "[ENDSUB],;~~"
                
    ! PopupMenu M for museums
    COLDEF$ += "0~0~ ~H~PopupMenu:M=Museum Visits,[SUB];" &
                + "Art Museum,VK_xF301;History Museum,VK_xF302;" &
                + "[ENDSUB],;~~"
                
    ! PopupMenu S for separator
    COLDEF$ += "0~0~ ~H~PopupMenu:S=-------,;~~"
    
    ! PopupMenu P for special Paris activities
    COLDEF$ += "0~0~ ~H~PopupMenu:P=" &
                + "Louvre,VK_xF401;" &
                + "Muse� D'Orsay,VK_xF402;" &
                + "Batau Mouche,VK_xF403;" &
                + "Tour Eiffel,VK_xF404;~~"
                
    ! PopupMenu L for special LA activities
    COLDEF$ += "0~0~ ~H~PopupMenu:E=" &
                + "Hollywood Walk of Stars,VK_xF501;" &
                + "Universal Studios,VK_xF502;" &
                + "Getty Museum,VK_xF503;" &
                + "MicroSabio Headquarters,VK_xF504;~~"
                
    ! PopupMenu H for Horseback riding
    COLDEF$ += "0~0~ ~H~PopupMenu:H=Horseback Riding,VK_xF801;~~"

    ! PopupMenu N for Night Life
    COLDEF$ += "0~0~ ~H~PopupMenu:N=Night life,VK_xF601;~~"

    ! PopupMenu T for Tabernacle
    COLDEF$ += "0~0~ ~H~PopupMenu:T=Tabernacle,VK_xF602;~~"
    
    ! PopupMenu D for dining (open ended sub-menu) - must be followed
    ! by one or more self-contained menu items and a standalone ENDSUB
    COLDEF$ += "0~0~ ~H~PopupMenu:D=Dining,[SUB];~~"&
    
    ! PopupMenu E for ENDSUB
    COLDEF$ += "0~0~ ~H~PopupMenu:E=[ENDSUB],;~~"
    
    ! PopupMenus G,V,W,X,Y,Z for different dining options
    COLDEF$ += "0~0~ ~H~PopupMenu:V=Pancake House,VK_xF701;~~"
    COLDEF$ += "0~0~ ~H~PopupMenu:W=Barbecue,VK_xF702;~~"
    COLDEF$ += "0~0~ ~H~PopupMenu:X=Haute Cuisine,VK_xF703;~~"
    COLDEF$ += "0~0~ ~H~PopupMenu:Y=Seafood,VK_xF704;~~"
    COLDEF$ += "0~0~ ~H~PopupMenu:Z=Spicy/Ethnic,VK_xF705;~~"
    COLDEF$ += "0~0~ ~H~PopupMenu:G=Grub,VK_xF706;~~"

    ! Define a Global menu that will be included with every popup menu
    COLDEF$ += "0~0~ ~H~PopupMenu=Book It!,VK_xF100;------,;~~"

    ! Finally, define the real columns...

    ! To attach a row-specific menu to this column, we use the PopupMenu=##
    ! clause...
    COLDEF$ += "16~30~City~S~PopupMenu=2~~"

    ! Note that we could also have attached the PopupMenu=## clause to the
    ! zero column so as to make it available to every display column, with...
    ! COLDEF$ += "0~0~ ~H~PopupMenu=2~~"

    ! (But, in that case, we'd need to remove the other zero-column PopupMenu
    ! definition (PopupMenu=Book It!,VK_xF100;...) since the two types of 
    ! PopupMenu clauses cannot be combined for one column)
    
    ! define the city column, with popup menu cross-reference to col 1
    COLDEF$ += "16~30~City~S~~"  ! PopupMenu=2~~"


    ! define the cross-reference column (normally hidden but visible here)
    COLDEF$ += "1~15~(menu xref)~S~Dspwid=4~RGBbg=200,200,200~~" 
    
    XTFLAGS.FLAGS = 0
    XTFLAGS.FLAGS = XTFLAGS.FLAGS or XTF_XYXY
    XTFLAGS.FLAGS = XTFLAGS.FLAGS or XTF_COLDFX
    XTFLAGS.FLAGS = XTFLAGS.FLAGS or XTF_MODELESS

    XTFLAGS.FLAGS2 = 0
    XTFLAGS.FLAGS2 = XTFLAGS.FLAGS2 or XTF2_AUTOEXPCOL

    XTR.OPCODE = XTROP_CREATE
    XTR.SHOWGRID = 1
    XTR.GRIDSTYLE = XTRGRID_BOTH
    XTR.CTLNO = -1
    XTR.CLOSEDENDED = 1

!     1234567890123456789012345678901234567890
        ! beach,city,museum,dining (no bbq),nightlife
data "+BCMDXYZESN    Miami"
        ! city,museum,dining (only bbq),nightlife
data "CMDWESN        Kansas City"
        ! city,museum,special paris stuff,all dining (exc bbq),nightlife
data "+CMSPDXYZESN   Paris"         
        ! beach,city,museum,special LA stuff,all dining (exc bbq),nightlife
data "+BCMSLDXYZESN  Los Angeles"   
        ! city,museum,tabernacle
data "+CMST          Salt Lake City"
        ! city,seafood
data "+CDVYE         Mobile"      
        ! seafood
data "+DGEH          Virginia City"
data ""

    I = 0
    do
        read XTROW$
        if XTROW$ # "" then
            I += 1
            XTROW(I).PMXREF = XTROW$[1,15]
            XTROW(I).CITY = XTROW$[16,-1]
        else
            exit
        endif
    loop
    
    TOT'ITEMS = I

    do
        xcall XTREE, 5, 20, ANSWER, XTROW(1), TOT'ITEMS, COLDEF$, &
            EXITCODE, 10, 50, XTFLAGS, "", MMOCLR, XTR

        ? tab(12,1);"Exitcode: ";EXITCODE

        ? tab(14,10);
        I = 0
        input "1 to try again, 0 to quit: ",I
        if I = 0 then exit
            
    loop 
    
    ? tab(-1,0);"End"
    end

