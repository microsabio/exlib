program XTRA7ML,2.0(203)     ! modeless variation of XTRA7
!--------------------------------------------------------------------
!    Program builds a list of images in a specified directory, then
!    uses that list to create an XTREE for displaying them with
!    following information:
!
!       <image>,name,size,date,editable description
!
!   Similar to XTRA7, it allows you click on a thumbnail in the tree
!   and display a larger version.  But unlike XTRA7, it allows you
!   to leave the enlarged image dialog(s) visible while returning to the 
!   tree.
!
! NOTES:
!   Program illustrates following techniques/routines:
!   - XTREE editable (multiline) text
!   - XTREE image display
!   - CSV reading/writing
!   - Open File dialog with multi-select
!   - Use of progress dialog during XTREE loading, which XTREE deletes
!   - Modeless dialogs
!   - AUI_IMAGE
!   - MX_AUTOPARENT
!--------------------------------------------------------------------
! EDIT HISTORY
! Version 2.0---
! [200] 27-Aug-23 / jdm / created from XTRA7
! [201] 02-Sep-23 / jdm / switch from IMAGE to AUI_CONTROL for the expanded picture dialog
! [202] 02-Sep-23 / jdm / adjust the Windows vs Linux stuff
! [203] 19-Sep-23 / jdm / support images in local directory with ./ prefix, handle icons (in Windows)
! 
! Version 1.0---
! [106] 11-Aug-23 / jdm / minor tinkering, minor modernization; fix ico handling
! [105] 30-Nov-12 / jdm / Image dialog was keying off of XTR.XROW instead of XTR.TARGETROW
! [104] 13-Aug-07 / jdm / Add option to edit the image cell; add BG color to image cell
! [103] 24-Aug-07 / jdm / Add options to test XTROP_REPLACE, XTROP_RESELECT
! [100] 16-Apr-06 / jdm / Created
! [102] 24-Oct-06 / jdm / Change XTR.SELECTAREA so to allow click anywhere on row
! [101] 23-Apr-06 / jdm / Add clickexit for image column, display text in cell
!                           with image
!--------------------------------------------------------------------
++pragma ERROR_IF_NOT_MAPPED "ON" ! (avoid stupid unmapped symbol errors)

++include'once ashinc:xtree.sdf     ! [106]
++include'once ashinc:xtree.def
++include'once ashinc:ashell.def
++include'once ashinc:image.def
++include'once ashinc:evtmsg.def
++include'once ashinc:scrsts.def
++include'once ashinc:gtlang.sdf    ! [106] 
++include'once ashinc:types.def     ! [200]
++include'once sosfunc:fnameext.bsi ! [201]
++include'once sosfunc:fnauienv.bsi ! [203]

! global configuration options
define MAX_IMGARY = 100           ! max # of images
define IMG_CSV_FILE = "XTRA7.CSV"   ! file we store our list of images in

map1 IMG'ARRAY(MAX_IMGARY)
    map2 IMG'COLOR1,S,1         ! [104] cell BG color for fname cell
    map2 IMG'FNAMETN,S,39       ! image fname.ext,description      2-40 (thumbnail) [106]
    map2 IMG'FBYTES,S,9         ! image size in bytes 41-49
    map2 IMG'FMDATE,S,9         ! image modify date   50-58
    map2 IMG'FNAME,S,300        ! full size image name   69-358 [106] 

![104] this version used if only the description is editable...
map1 ANSWERX
    map2 ANSARY(MAX_IMGARY)
        map3 ANS'TEXT,S,300      ! updated description

![104] this version used if the image filename is editable too
map1 ANSWERX2,@ANSWERX
    map2 ANSARY2(MAX_IMGARY)
        map3 ANS2'IMGCOLOR1,S,1 ! BG color
        map3 ANS2'IMGTEXT,S,39  ! text inside image cell
        map3 ANS2'TEXT,S,300    ! separate image description cell


map1 ANSWER,B,4,@ANSWERX    ! or single selection
map1 ANSWER$,S,400,@ANSWERX ! (convenient string format for display)

map1 XTR, XTRCTL         ! [106] 
map1 LANG, ST_GTLANG     ! [106]

map1 MISC
    map2 EXITCODE,F
    map2 SROW,F
    map2 SCOL,F
    map2 EROW,F
    map2 ECOL,F
    map2 PROMPT,S,120
    map2 ID,B,2
    map2 FLAGS,F,6
    map2 MLVL$,S,1
    map2 DLG$,S,1
    map2 MSEL$,S,1
    map2 VALIDATE$,S,1
    map2 SHOW3D$,S,1
    map2 IMGEDIT$,S,1                       ! [104] (allow editing of image cell text)
    map2 A$,S,1
    map2 STATUS,F
    map2 COLDEF,S,400                       ! column definitions
    map2 DLGID,B,2
    map2 PGRID,B,2
    map2 DSROW,F
    map2 DSCOL,F
    map2 DEROW,F
    map2 DECOL,F
    map2 BID,B,2
    map2 CNO,B,1
    map2 A,F
    map2 COLFMT$,S,20           !
    map2 STATE,F                !
    map2 IMGPATH$,S,1000
    map2 IMGDIR$,S,140
    map2 FSPEC$,S,200
    map2 UDATE,B,4
    map2 FBYTES,F
    map2 CREATE'CSV$,S,1
    map2 BAKNAME$,S,40
    map2 FILTER$,S,100
    map2 TITLE$,S,100
    map2 X,F
    map2 IMGSIZ$,S,100
    map2 VARY$,S,1
    map2 IMGCOUNT,F
    map2 TIMEFMT$,S,32
    map2 TEXTPLUSIMAGE$,S,1     ! [101]
    map2 IMGDLGID,B,2           ! [101]
    map2 HANDLE,F,6             ! [101]
    map2 AB,B,1                 ! [101]
    map2 RC,B,1                 ! [103]
    map2 I,I,2
    map2 BDONE,BOOLEAN          ! [200]
    map2 IMGCTL,B,1             ! [201] 

!Of following colors, only TFCLR/TBCLR (text) and PFCLR/PBCLR (prompt/titles)
!are relevant
!Colors: 0=blk,1=wht,2=blu,3=mag,4=red,5=yel,6=grn,7=cyan
map1 MMOCLR                             ! HELP memo colors
    map2 BFCLR,B,1,3                ! border fg
    map2 BBCLR,B,1,2                ! border bg
    map2 TFCLR,B,1,0                ! text fg (black text)
    map2 TBCLR,B,1,1                ! text bg (white bg)
    map2 AFCLR,B,1,3                ! arrows fg
    map2 ABCLR,B,1,2                ! arrows bg
    map2 PFCLR,B,1,-1               ! prompt fg (column titles) (dfltclrs)
    map2 PBCLR,B,1,-1               ! prompt bg
    map2 WFCLR,B,1,5                ! warnings & messages fg
    map2 WBCLR,B,1,0                ! "    "    "     bg
    map2 SFCLR,B,1,-1               ! orig. status line fg
    map2 SBCLR,B,1,-1               ! orig. status line bg
    map2 RFCLR,B,1,-1               ! ruler/reserved fg
    map2 RBCLR,B,1,-1               ! ruler/reserved bg


        xcall GTLANG,STATUS,LANG       ! [106] 

        MLVL$ = LANG.NO'CHAR                ! multi-level
        DLG$ = LANG.YES'CHAR                !  use dialog?
        SHOW3D$ = LANG.NO'CHAR
        VALIDATE$ = LANG.NO'CHAR
        TEXTPLUSIMAGE$ = LANG.NO'CHAR       ! [101] text w/ image in cell
        IMGEDIT$ = LANG.NO'CHAR             ! [104]

        ? tab(-1,0);"Test XCALL XTREE with images"

        ! see if we have a data file
        if lookup(IMG_CSV_FILE) = 0 then
            CREATE'CSV$ = LANG.YES'CHAR
        else
            ? IMG_CSV_FILE;" exists - enter ";LANG.YES'CHAR;" to discard and create a new image list: ";
            input "",CREATE'CSV$
        endif

        ! [101]
        if CREATE'CSV$ = LANG.YES'CHAR then
            ? "Show example of text in same cell as image [";TEXTPLUSIMAGE$;"] ?";
            input "",TEXTPLUSIMAGE$
            if TEXTPLUSIMAGE$ = "" then TEXTPLUSIMAGE$ = LANG.NO'CHAR
        endif

        ! build a new list of images
        if CREATE'CSV$ = LANG.YES'CHAR then
            ! use open file dialog routine
            FILTER$ = "BMP files|*.BMP|JPEG files|*.JPG;*.JPEG" &
                + "|TIFF files|*.TIF|PCX files|*.PCX|GIF files|*.GIF"
            TITLE$ = "Select multiple images to display in XTREE"
            xcall MIAMEX,MX_GETOFD,IMGPATH$,FILTER$,TITLE$, &
                OFN_ALLOWMULTISELECT+OFN_FILEMUSTEXIST+OFN_EXPLORER

            ! IMGPATH$ will come back <dir><LF><file1.ext><LF><file2.ext><LF>...
            if IMGPATH$ = "" then
                ? "No files selected, so we end here"
                END
            endif
            X = instr(1,IMGPATH$,chr(10))
            if X < 1 then
                ? "You need to select at least 2 files"
                END
            endif
            IMGDIR$ = IMGPATH$[1,X-1]
            IMGPATH$ = IMGPATH$[X+1,-1]
            I = 0                       ! array index
            do while IMGPATH$ # ""
                I = I + 1
                ? "."; tab(-1,254);
                if I < MAX_IMGARY then
                    X = instr(1,IMGPATH$,chr(10))
                    if X > 0 then
                        IMG'FNAMETN(I) = IMGPATH$[1,X-1]         ! [106]
                        IMGPATH$ = IMGPATH$[X+1,-1]
                    else
                        IMG'FNAMETN(I) = IMGPATH$               ! [106] 
                        IMGPATH$ = ""
                    endif
                    ? "FNAME = ";IMG'FNAME(I)
                    ! get additional info about file
                    FSPEC$ = IMGDIR$ + "\" + IMG'FNAMETN(I)     ! [106] 

                    xcall MIAMEX,MX_FILESTATS,"R",FSPEC$,FBYTES,UDATE
                    IMG'FBYTES(I) = FBYTES using "#########"
                    xcall MIAMEX,MX_FTFORMAT,UDATE,TIMEFMT$
                    !TIMEFMT$ "Wed Dec dd hh:mm:ss ccyy"
                    IMG'FMDATE(I) = TIMEFMT$[9,10]+"-"+TIMEFMT$[5,7]+"-"+TIMEFMT$[23,24]
                    IMG'FMDATE(I) = IMG'FMDATE(I) + space(9)
                    IMG'FNAME(I) = IMG'FNAMETN(I)               ! [106] 

                    ! [101] illustrate option of showing text in cell
                    ! [101] with image by adding ,text
                    if ucs(TEXTPLUSIMAGE$) = LANG.YES'CHAR then
                        IMG'FNAMETN(I) = IMG'FNAMETN(I) + ",(sample text)"
                    endif

                else
                    ? "Exceeded our arbitrary limit of 100 images; ignoring rest"
                    IMGPATH$ = ""
                endif
            loop
            IMGCOUNT = I

            ! now save the data to a CSV
            ! first rename old copy if present
            if lookup(IMG_CSV_FILE) # 0 then
                ? "Enter name to save existing data file (blank for none): ",BAKNAME$
                if BAKNAME$#"" and instr(1,BAKNAME$,".") < 1 then
                    BAKNAME$ = BAKNAME$ + ".csv"
                endif
                if BAKNAME$ # "" then
                    xcall RENAME,IMG_CSV_FILE,BAKNAME$,STATUS
                    if STATUS # 0 then
                        ? "Error #";STATUS;" while renaming"
                    endif
                endif
            endif

            call SAVE'ARRAY'TO'CSV

        endif

        ! input the data from the CSV

        open #1, IMG_CSV_FILE, input
        I = 0
        input line #1, IMGDIR$
        do while eof(1) # 1
            if (I < MAX_IMGARY) then
                I = I + 1
                input csv #1, IMG'FNAMETN(I),IMG'FBYTES(I),IMG'FMDATE(I),IMG'FNAME(I)
                if IMG'FNAMETN(I)="" then I = I - 1  ! loaded a null line
            else
                ? "Image array truncated at ";I
            endif
        loop
        close #1
        IMGCOUNT = I
        ? IMGCOUNT;"images loaded"
        ? "ImgSiz= arg format: maxwid,maxht,bpp{,fit}{,stretch}{,scaleq}"
        ? "  (Default: 100,75,24,fit,scaleq)"
        input line "Enter ImgSiz= param list: ",IMGSIZ$
        if IMGSIZ$ = "" then IMGSIZ$ = "100,75,24,fit,scaleq"
        ? "Using ImgSiz=";IMGSIZ$
        ? "Variable height rows? [";LANG.YES'CHAR;"] ";
        VARY$ = LANG.YES'CHAR
        input "",VARY$
        if VARY$ = "" then VARY$ = LANG.YES'CHAR

        ? "Allow editing the image cell text? [";IMGEDIT$;"] ";     ! [104]
        input "",IMGEDIT$
        if IMGEDIT$ = "" then IMGEDIT$ = LANG.NO'CHAR

        ! [201] choice of AUI_IMAGE or AUI_CONTROL
        ! [202] choice only valid in Windows
        ! [203] if OSNAME$[1,2] = "WIN" then
        if Fn'Is'Windows() then             ! [203] 
            input "0) Use AUI_IMAGE, 1) using AUI_CONTROL [0]: ",IMGCTL     ! [201] 
        else
            IMGCTL = 1                                  ! [202]
            ? "(Using AUI_CONTROL for image handling)"  ! [202] 
        endif

        ! Initialize our editable data
        for I = 1 to MAX_IMGARY
            ANS'TEXT(I) = IMG'FNAME(I) + space(300)  ! must be space-filled
            if ucs(IMGEDIT$) = LANG.YES'CHAR then       ! [104]
                ANS2'TEXT(I) = ANS'TEXT(I)
                ! only the text (not the image fname) is editable...
                X = instr(1,IMG'FNAMETN(I),",")
                ANS2'IMGTEXT(I) = IMG'FNAMETN(I)[X+1,-1]
            endif

            ! [104] Also, initialize the color column...
            if I = 1 then IMG'COLOR1(I) = "A"   ! Use "A" RGBbg def for first image row
            if I = 2 then IMG'COLOR1(I) = "B"   ! Use "B" RGBbg def for second image row
            if I > 2 then IMG'COLOR1(I) = " "   ! Use no color for the rest
        next I

        call SETUP'XTREE'PARAMETERS

        ! [105] if dialog option, load a dialog first, then put xtree in it
        if ucs(DLG$)=LANG.YES'CHAR then

            ! Create a nice big dialog
            DSROW = 1
            DSCOL = 1
            DEROW = 22
            DECOL = 75
            SROW = 2        ! adjust xtree coords to be relative to dialog
            SCOL = 3
            EROW = (DEROW-DSROW-4)
            ECOL = (DECOL-DSCOL-2)

            ! and add a couple of buttons just for fun

            xcall AUI,AUI_CONTROL,CTLOP_ADD,DLGID,"XTREE with images ("+IMGDIR$+")",MBST_ENABLE, &
                MBF_DIALOG+MBF_SYSMENU,"","",STATUS,DSROW,DSCOL,DEROW,DECOL

            ! and add a couple of buttons just for fun
            xcall AUI,AUI_CONTROL,CTLOP_ADD,BID,"&OK",MBST_ENABLE, &
                MBF_KBD,"%VK_ENTER%","",STATUS,EROW+2,3,EROW+3,12
                
            xcall AUI,AUI_CONTROL,CTLOP_ADD,BID,"&Cancel",MBST_ENABLE, &
                MBF_KBD,"%VK_ESCAPE%","",STATUS,EROW+2,16,EROW+3,25

        endif

        ! since it may take awhile to load the images into the XTREE,
        ! let's display a little animated AVI and plug it's ID into
        ! XTR.DELCTLID so XTREE will automatically remove it

       xcall AUI,AUI_CONTROL,CTLOP_ADD,XTR.DELCTLID,"Loading XTREE...",MBST_ENABLE, &
           MBF_DIALOG+MBF_MODELESS,"","",STATUS,8,15,13,30,-2,-2,0,0,"","",DLGID

       xcall AUI,AUI_CONTROL,CTLOP_ADD,PGRID,"cogs.avi", MBST_ENABLE, &
               0, "","",STATUS,1,1,5,15,-2,-2,0,0,"","",XTR.DELCTLID, &
               "SysAnimate32",ACS_CENTER+ACS_TRANSPARENT+ACS_AUTOPLAY,0

        do      ! [200] 
            
            ! [104] we need separate xcall statements to deal with the different editable
            ! [104] cell possibilities...
            if ucs(IMGEDIT$)=LANG.YES'CHAR then     ! image cell contains editable text
                xcall XTREE,SROW,SCOL,ANSWERX,IMG'ARRAY(1),IMGCOUNT,COLDEF,EXITCODE, &
                    EROW,ECOL,FLAGS,"",MMOCLR,XTR
            else
                xcall XTREE,SROW,SCOL,ANSWERX,IMG'ARRAY(1),IMGCOUNT,COLDEF,EXITCODE, &
                    EROW,ECOL,FLAGS,"",MMOCLR,XTR
            endif

            trace.print EXITCODE, XTR.XROW, XTR.XCOL, XTR.TARGETROW, XTR.TARGETCOL
            if XTR.TARGETROW then
                trace.print IMG'FNAMETN(XTR.TARGETROW)
            endif

            switch EXITCODE             ! [200] 
                case 0                  ! [200] ok
                case 1                  ! [200] Cancel
                    BDONE = .true       ! [200] either one closes the app
                    exit
                    
                case -48                ! [104] deal with validation on image cell text
                    if XTR.XROW > 0 and XTR.XCOL > 0 then
                        trace.print "Validating cell "+XTR.XROW+","+XTR.XCOL+" ..."
                        ! note that answer array only has the ",text" - not the image filename
                        ! (so let's remove it from our was,is display)
                        X = instr(1,IMG'FNAMETN(XTR.XROW),",")
                        trace.print "Was ["+IMG'FNAMETN(XTR.XROW)[X+1,-1]+"], is [" + ANS2'IMGTEXT(XTR.XROW)+"]"
                        XTR.OPCODE = XTROP_RESELECT
                    endif
                    exit


                case -55            ! [101] (see ClickExit=) enlarge the image in separate dialog
                    if XTR.TARGETROW > 0 then
                        !call IMAGE'DIALOG
                        trace.print XTR.TARGETROW, IMGDIR$, IMG'FNAME(XTR.TARGETROW)
                        FSPEC$ = IMG'FNAME(XTR.TARGETROW)       ! [105] was XTR.XROW
                        X = instr(1,FSPEC$,",")
                        if X > 0 then FSPEC$ = FSPEC$[1,X-1]    ! strip off the description
                        if instr(1,FSPEC$,"/\",INSTRF_ANY) = 0 and IMGDIR$ # "" then    ! [106]
                            if instr(1,IMGDIR$,"::") then                               ! [106] default ::icondll
                                FSPEC$ = FSPEC$ + IMGDIR$                   
                            elseif IMGDIR$[1,4] = "http" then       ! [106]
                                FSPEC$ = IMGDIR$ + "/" + FSPEC$
                            elseif FSPEC$[1,1] # "." then           ! [203] fully qualified
                                FSPEC$ = IMGDIR$ + "\" + FSPEC$
                            endif
                        endif
                        trace.print FSPEC$
                        call Fn'Image'Dialog(imagespec$=FSPEC$,imgctl=IMGCTL)   ! [201] pass AUI_IMAGE vs AUI_CONTROL param
                        XTR.OPCODE = XTROP_RESELECT
                    endif
                    exit

                case -120...-101       ! [200] clicked OK on image dialog
                    call Fn'Image'Dialog(exitcode=EXITCODE)
                    exit
                    
                default
                    
                    xcall EVTMSG,"Unexpected exitcode: exitcode=%d, XTR.XROW=%d", &
                        EXITCODE,XTR.XROW
                    exit
            endswitch
            
        loop until BDONE        ! [200]


        ! Note that clicking on dialog X to close it will
        ! send an ESC which will force XTREE to exit, so we don't
        ! need any special test for ESC.

        xcall AUI,AUI_CONTROL,CTLOP_DEL,DLGID

        ? "Saving image list and updated descriptions to ";IMG_CSV_FILE;"..."
        call SAVE'ARRAY'TO'CSV

        ? TAB(-1,0);   ! [200] easy way to clear any of the other image dialog(s)

        END


!--------------------------------------------------------------------------
! Setup up columns, flags, and XTRCTL options
! Inputs:
!   MLVL$ = LANG.YES'CHAR for multi-level
!--------------------------------------------------------------------------
SETUP'XTREE'PARAMETERS:

    ! Define the columns.  Note that you don't have to match the layout
    ! of the data, and should be able to eliminate a column just by
    ! commenting out the corresponding line below
    !  123456789012345678901234567890123456789012345678901234567890
    ! "13PAN6    Elephant leg            30.00  00010 One size (mis)fits all"
    COLDEF = ""
    CNO = 0

    ![104] Define some RGB colors...
    COLDEF = COLDEF +"0~0~ ~H~RGBbg=240,230,220,A~RGBbg=205,215,235,B~~"

    if ucs(MLVL$)=LANG.YES'CHAR then         ! Multi-level
        COLDEF = COLDEF + "1~1~x~@H~~"   ! Special column with dummy "x" title
        CNO = CNO + 1
    endif

    ! Image Column
    CNO = CNO + 1

    if ucs(IMGEDIT$) = LANG.YES'CHAR then
!       COLDEF = COLDEF + "1~40~Image~IEXB~ImgDir="+IMGDIR$ &
!         + "~ImgSiz=" + IMGSIZ$ &
!         + "~ImgCount=" + str(1) + ",0" &
!         + "~ClickExit=0,-55~~"  ! [104] set exitcode -51 on RIGHT click (no left click)

        COLDEF = COLDEF + "1~40~Image~IEXB\~ImgDir="+IMGDIR$ &
          + "~ImgSiz=" + IMGSIZ$ &
          + "~ImgCount=1,1" &
          + "~ClickExit=0,-55~~"  ! [104] set exitcode -55 on RIGHT click (no left click)


    else
        COLDEF = COLDEF + "1~40~Image~IB\~ImgDir="+IMGDIR$ &
          + "~ImgSiz=" + IMGSIZ$ &
          + "~ImgCount=" + str(1) + ",0" &
          + "~ClickExit=-55~~"  ! [101] set exitcode -55 on left click
    endif

    ! Image file name (this is actually a dup of first column
    CNO = CNO + 1
    COLDEF = COLDEF + "1~40~Thumbnail~S~~"

    ! Image file size
    CNO = CNO + 1
    COLDEF = COLDEF + "41~9~Bytes~#>~~"

    ! Image file modify date
    CNO = CNO + 1
    COLDEF = COLDEF + "50~9~Date~D~~"

    ! Editable description
    CNO = CNO + 1
    COLDEF = COLDEF + "59~300~Full Sized Image File~SEWm~~"

	! add a popup menu...
!    COLDEF = COLDEF + "0~0~ ~S~PopupMenu=" &
!        + "Try On,%VK_xF501%;Backorder,%VK_xF502%;Discontinue,%VK_xF503%;" &
!        + "-----,;Shoplift,%VK_xF504%~~"    ! [116] add separator

    ! Set up (hard-code) flags
    FLAGS = 0

    ! These first three are mandatory for this program...
    FLAGS |= XTF_XYXY           ! alt coords (srow,scol,erow,ecol)
    FLAGS |= XTF_COLDFX         ! complex syntax in COLDEF
    FLAGS |= XTF_EDITABLE       ! needed for editable cb or text [110]

    ! Remaining flags are optional - you can experiment with them
    FLAGS |= XTF_FKEY           ! Allow F1-F16
    FLAGS |= XTF_LEFT           ! Left arrow (Exitcode -40)
    FLAGS |= XTF_RIGHT          ! Right arrow (Exitcode -41)
    FLAGS |= XTF_UP             ! Up arrow (Exitcode -42)
!   FLAGS |= XTF_TAB            ! TAB (Exitcode -44)
    FLAGS |= XTF_HOME           ! Right arrow (Exitcode -45)
    FLAGS |= XTF_END            ! Right arrow (Exitcode -46)
    FLAGS |= XTF_MODELESS       ! Leave box on screen after exit
    FLAGS |= XTF_TIMOUT         ! 200 sec timeout (auto ESC)
    FLAGS |= XTF_SORT           ! Allow column sorting
    FLAGS |= XTF_REORD          ! Allow reordering of columns
    if (ucs(VARY$) = LANG.YES'CHAR) then
        FLAGS |= XTF_VARY       ! This affects image internal storage
    endif

    if ucs(MSEL$)=LANG.YES'CHAR then 
        FLAGS |= XTF_MSEL       ! [106] multi-select
    endif
    
!If this flag set, ESC within an editable cell updates the cell (instead
!of discarding changes) and then exits XTREE (instead of just exiting
!from cell editing mode).  Also, hitting ENTER in the last editable cell
!exits XTREE
    FLAGS |= XTF_ENTESC     ! [117]

    ! Set up XTRCTL options...
    XTR.ITEMLINES = 6        ! (max) # lines per row to show (see XTF_VARY)
    XTR.SHOWGRID = 1         ! show grid lines
    XTR.GRIDSTYLE = 2        ! solid vertical & horizontal
                             ! 0=vert solid, 1=horz solid, 2=both,
                             ! 3=vert dotted, 4=horz dotted, 5=both
![102]  XTR.SELECTAREA = 3 + (3*16)   ! click celltext only, highlight first cell
    XTR.SELECTAREA = XTRSEL_AREA_ANY + XTRSEL_STY_CELL1 ! [102]
    XTR.SCROLLTIPS = 1       ! show scroll tips
    XTR.FLYBY = 1            ! show flyby highlighting
    XTR.TRUNCATED = 1        ! show truncation indicators
    if ucs(SHOW3D$)=LANG.YES'CHAR then
        XTR.SHOW3D = 1           ! [108] try 3D
    else
        XTR.SHOW3D = 0
    endif

    XTR.OPCODE = XTROP_CREATE

!XTR.HIDEHEADER = 1

!   XTR.SHOWBUTTONS0 = 1     ! show exp/collapse buttons (level 0)
!   XTR.SHOWBUTTONS = 1      ! show exp/collapse buttons (level 1+)

    XTR.USETHEMES = 1

    return


!----------------------------------------------------------------------------
! Save array to CSV (Comma Separated Values) text file
! Input:
!   IMGDIR$
!   IMG'ARRAY(1-IMGCOUNT)
!   IMG_CSV_FILE
! Output
!   data written to file
! Uses/Destroys:
!   I
!----------------------------------------------------------------------------
SAVE'ARRAY'TO'CSV:
    ! output array to CSV
    open #1, IMG_CSV_FILE, output
    print #1, IMGDIR$
    for I = 1 to IMGCOUNT
        WRITECD #1, IMG'FNAMETN(I),IMG'FBYTES(I),IMG'FMDATE(I),ANS'TEXT(I)
    next I
    close #1
    return



!---------------------------------------------------------------------
!Function:
!   Manage creation and shutdown of multiple image enlargement dialogs
!Params:
!   imagespec$ (str) [in] - image to enlarge 
!   exitcode (num) [in] - exit code to process (for closing dialogs)
!   imgctl (num) [in] - 0) use AUI_IMAGE, 1) use AUI_CONTROL [201] 
!Returns:
!Globals:
!Notes:
!   Each image will have its own modeless diaog with a unique 
!   exitcode (-(100 + index) into images array) 
!---------------------------------------------------------------------
Function Fn'Image'Dialog(imagespec$="" as s100:inputonly, exitcode=0 as i4:inputonly, &
                            imgctl=0 as b1:inputonly) as i4

defstruct ST_IMAGE_ENLARGEMENT
    map2 fspec$,s,100
    map2 handle,i,2
    map2 dlgid$,s,30
endstruct

    map1 locals
        map2 ix,i,2         ! index into array
        map2 status,i,4
        map2 ixavail,i,2
        map2 dsrow,i,2,3    ! dialog starting row
        map2 dscol,i,2,3    ! dialog starting col
        map2 derow,i,2,15   ! dialog ending row
        map2 decol,i,2,50   ! dialog ending col
        map2 info$,s,100
        map2 cflags,b,4     ! [203]
 
        map2 imginfo            ! image info
            map3 img'owidth,f
            map3 img'olength,f
            map3 img'dwidth,f
            map3 img'dlength,f
            map3 img'bpp,f

        
    static dimx images(0), ST_IMAGE_ENLARGEMENT, auto_extend  ! array of images displayed
        
    if exitcode then    ! close a dialog
        ix = abs(exitcode) - 100
        if ix > 0 and ix <= .extent(images()) then
            if images(ix).handle then
                xcall AUI, AUI_IMAGE,IMGOP_CLOSE,images(ix).handle,status    ! close image
            endif

            xcall AUI,AUI_CONTROL,CTLOP_DEL,images(ix).dlgid$
            images(ix).fspec$ = ""
            images(ix).handle = 0
            images(ix).dlgid$ = ""
            .fn = ix
        endif
        
    elseif imagespec$ # "" then     ! open a new enlargement dialog
        ! check if we already have one
        for ix = 1 to .extent(images())
            if images(ix).fspec$ = imagespec$ then
                ! refresh the window (move it to the top)
                xcall AUI, AUI_WINDOW, SW_SHOWNORMAL, 0,0,0,0, 0,0, -1,-1, images(ix).dlgid$
                .fn = ix            ! return it's index
                exitfunction
            elseif ixavail = 0 and  images(ix).fspec$ = "" then
                ixavail = ix
            endif
        next ix    
        if ixavail = 0 then                 ! if no empty slot, add one
            ix = .extent(images()) + 1
        else
            ix = ixavail                    ! else re-use slot
        endif
        
        images(ix).fspec$ = imagespec$
        images(ix).dlgid$ = "imagedlg" + ix

        exitcode = 100 + ix
        xcall AUI,AUI_CONTROL,CTLOP_ADD,images(ix).dlgid$,"Image Enlargement ("+images(ix).fspec$+")",MBST_ENABLE, &
            MBF_DIALOG+MBF_SYSMENU+MBF_MODELESS,"VK_xF"+exitcode,NUL_FUNC$,NUL_CSTATUS, dsrow, dscol, derow, decol

        if imgctl = 0 then      ! [201] use AUI_IMAGE
            xcall MIAMEX, MX_AUTOPARENT, images(ix).dlgid$   ! force modeless dialog to be parent...   

            xcall AUI, AUI_IMAGE,IMGOP_LOADDISP,images(ix).handle,status,1,2,derow-dsrow-2,decol-dscol-2, IMGF_SCALEQ, images(ix).fspec$
            
            if status # 0 then
                xcall EVTMSG,"Unable to display image %s with AUI_IMAGE, status=%d",images(ix).fspec$,status
            else
                ! display some image info
                xcall AUI,AUI_IMAGE,IMGOP_INFO,images(ix).handle,status,imginfo

                info$ = "Image size: " + str(img'owidth) + " x " &
                + str(img'olength) + ", BPP: " +str(img'bpp)

                xcall AUI,AUI_CONTROL,CTLOP_ADD,0,info$,MBST_ENABLE, &
                    MBF_STATIC,"","","", &
                    derow-dsrow,3,derow-dsrow,decol-dscol-2
            endif
        
        else        ! [201] use AUI_CONTROL for the image (no autoparent needed)
            
            xcall AUI, AUI_CONTROL, CTLOP_ADD, NUL_CTLID, images(ix).fspec$, MBST_ENABLE, &
                MBF_STATIC+MBF_BITMAP, NUL_CMD$, NUL_FUNC$, NUL_CSTATUS, 1, 2, derow-dsrow-2, decol-dscol-2, NUL_FGC, NUL_BGC, &
                NUL_FONTATTR, NUL_FONTSCALE, NUL_FONTFACE$, NUL_TOOLTIP$, images(ix).dlgid$     ! [201] note explicit parent dlg

            ! AUI_CONTROL doesn't have an option to extract the image details from the image file, so 
            ! instead we'll just display the image name underneath it.
            cflags = MBF_STATIC
            if instr(1,lcs(images(ix).fspec$),".ico") then      ! [203] 
                cflags |= MBF_ICON
            endif
            xcall AUI,AUI_CONTROL,CTLOP_ADD,0,Fn'Name'Ext$(images(ix).fspec$),MBST_ENABLE, &
                MBF_STATIC,"","","", derow-dsrow,3,derow-dsrow,decol-dscol-2, NUL_FGC, NUL_BGC, &
                NUL_FONTATTR, NUL_FONTSCALE, NUL_FONTFACE$, NUL_TOOLTIP$, images(ix).dlgid$     ! [201] note explicit parent dlg
        endif

        .fn = ix
    endif
EndFunction


