! Program Name:   xtra9
! Description:    XTREE example using choosable cell colors with
!                   editable cells; use MX_CHOOSECOLOR and DIMX.
! Author:         Jack McGregor
! Date:           06-Nov-2006
!---------------------------------------------------------------------
!Notes:
! Based on Franks appt type routine which allows the user to define
! appointment types and assign them custom colors, which then get
! applied in another XTREE app, the appt scheduler.
! We maintain a text file (XTRA9.SEQ) of the definitions, as follows:
!
!   1234567890123456789012345678901234567890
!   ##cddddddddddddddddddddddrrrrrrrrrrr
!
! where:
!   ## is a 2 digit editable field for the type number
!   c  is the bg color column for the type description
!   dddddddddddddddddddddd  is the type description
!   rrrrrrrrrrr is a hidden column storing the rgb (rrr,ggg,bbb)
!--------------------------------------------------------------------
!Edit History
!VEDIT=102
![100] 06-Nov-06 / jdm / Created
![101] 25-Jan-11 / jdm / Update flags arg, experiment with closedended
![102] 05-Aug-23 / jdm / Modernization
!---------------------------------------------------------------------
!{end history}

++pragma ERROR_IF_NOT_MAPPED "TRUE"

++include'once ashinc:ashell.def
++include'once ashinc:xtree.def
++include'once ashinc:xtree.map
++include'once ashinc:xtree.sdf         ! [101]  
++include'once ashinc:types.def         ! [102]
++include'once sosfunc:fnexplode.bsi    ! [102]

! misc definitions
define MAX_TYPES = 15           ! max # of xtree rows
define TYPE_FSPEC$ = "XTRA9.SEQ"! file containing the save type defs

! layout of the data array (and answer array, and data file)
defstruct ST_XTRA9              ! layout of both data and answer arrays
    map2 TYPE,S,2               ! ## (type #)
    map2 DESCRX                 ! combined color + description
        map3 BGCLR,S,1          ! c (color code)
        map3 DESCR,S,22         ! ddddd (description)
    map2 RGB,S,11               ! rrr,ggg,bbb
endstruct

defstruct ST_RGB
    map2 R,B,1
    map2 G,B,1
    map2 B,B,1
endstruct

MAP1 XTREE'PARAMS
    map2 COLDEF$,S,0
    map2 MAXCNT,F
    map2 XFLG,ST_XFLAGS         ! [101]   
    map2 XTR,XTRCTL             ! [102] 
    map2 XANS, ST_XTRA9         ! [102]
    map2 XDATA, ST_XTRA9        ! [102]

dimx XANS(0),ST_XTRA9,auto_extend       ! [102]
dimx XDATA(0),ST_XTRA9,auto_extend      ! [102] 
map1 XDATA$,S,sizeof(ST_XTRA9)          ! [102] string version for input line

map1 MISC
    map2 X,F
    map2 Y,F
    map2 CH,F,6,9
    map2 I,F
    map2 DLG'ROWS,B,2,10
    map2 DLG'COLS,B,2,30
    map2 DLG'SROW,B,2,4
    map2 DLG'SCOL,B,2,4
    map2 DLG'ID,B,2
    map2 DLG'TITLE$,S,40,"XTREE Editable Colored Cells Demo"
    map2 OK'ID,B,2
    map2 COLOR'ID,B,2
    map2 CANCEL'ID,B,2
    map2 CSTATUS,F
    map2 STATUS,F
    map2 EXITCODE,F
    map2 RGB$,S,11      ! ###,###,###
    map2 NEED'SETUP,BOOLEAN
    map2 DONE,BOOLEAN
    map2 XRGB,ST_RGB
    map2 RGB,B,3,@XRGB  ! B3 version of RGB struct
    
    
    if lookup(TYPE_FSPEC$) = 0 then
        open #CH, TYPE_FSPEC$, output
        ? #CH, "01BPurple                128,128,255"
        ? #CH, "04DYellow                255,255,0"
        ? #CH, "06GTeal                  53,187,147"
        close #CH
    endif
        
    ! now load the data array
    I = 0               ! items loaded
    if lookup(TYPE_FSPEC$) then
        open #CH, TYPE_FSPEC$, input
        do while eof(CH) = 0
            input line #CH, XDATA$
            trace.print (99,"xtra9") XDATA$
            if XDATA$ # "" and XDATA$[1,1] # ";" then
                ! make sure lines are padded with spaces
                XDATA$ = pad$(XDATA$,sizeof(ST_XTRA9))
                I += 1
                XDATA(I) = XDATA$
                XANS(I) = XDATA(I)  ! answer array same layout
                trace.print (99,"xtra9") I, XDATA.TYPE, XDATA.BGCLR, XDATA.DESCR, XDATA.RGB
            endif
        loop 
        close #CH
    endif
    MAXCNT = I 
    trace.print (99,"xtra9") MAXCNT
    if MAXCNT = 0 then
        ? "No data in ";TYPE_FSPEC$
        end
    endif
    
    ! create a dialog...
    call CREATE'XTREE'DIALOG
    NEED'SETUP = .TRUE
    
!XTREE'LOAD:
    do while not DONE
        if NEED'SETUP then
            call Fn'Setup'XTREE(xflg=XFLG, xtr=XTR, coldef$=COLDEF$, xans()=XANS())    ! define the rest of the XTREE options...
            NEED'SETUP = .FALSE
            XTR.OPCODE = XTROP_CREATE       ! use this to force reprocessing of coldef
            for I= 1 to .extent(XANS())     ! copy answer back to data since CREATE will reload from there
                XDATA(I) = XANS(I)
                trace.print (99,"xtra9") I, XANS(I).DESCR, XDATA(I).DESCR, XANS(I).RGB, XDATA(I).RGB
            next
        endif
    
        xcall XTREE,2,3,XANS(1),XDATA(1),MAXCNT,COLDEF$, &
            EXITCODE,DLG'ROWS-2,DLG'COLS-3,XFLG,"",0,XTR

        XTR.OPCODE = XTROP_RESELECT
        trace.print (99,"xtra9") EXITCODE, XTR.XROW

        switch EXITCODE
            
            case -3                          ! color button
                XANS = XANS(XTR.XROW)       ! get a working copy of row
                trace.print (99,"xtra9") XTR.XROW, XANS.TYPE, XANS.BGCLR, XANS.DESCR, XANS.RGB, XANS(XTR.XROW).RGB, XDATA(XTR.XROW).RGB
                ! extract the current RGB def, if any...
                if XANS.RGB # "" then
                    STATUS = Fn'ExplodeEx(XANS.RGB,",",0,XRGB.R,XRGB.G,XRGB.B) ! [102]
!>!                    X = instr(1,XANS.RGB,",")
!>!                    if X > 0 then
!>!                        RGB.R = XANS.RGB[1,X-1]
!>!                        Y += 1
!>!                        X = instr(Y,XANS.RGB,",")
!>!                        if X > 0 then
!>!                            RGB.G = XANS.RGB[Y,X-1]
!>!                            RGB.B = XANS.RGB[X+1,-1]
!>!                        endif
!>!                    endif
                endif
                trace.print (99,"xtra9") XANS.RGB, XRGB.R, XRGB.G, XRGB.B, RGB

                
                ! display color selection dialog...
                xcall MIAMEX,MX_CHOOSECOLOR, RGB, STATUS
                trace.print (99,"xtra9") STATUS, XRGB.R, XRGB.G, XRGB.B
                
                if STATUS = 0 then      ! color chosen
                    ![102] XT'ANSWER'LAYOUT = XT'ANSWER(XTR.XROW)
                    ! format returned RGB value
                    RGB$ = str(XRGB.R) + "," + str(XRGB.G) + "," + str(XRGB.B)
                    ! put it into the data array
                    XANS.RGB = RGB$ + space(11)
                    XANS(XTR.XROW) = XANS  
                    trace.print (99,"xtra9") XANS.DESCR, XANS.RGB

                    XTR.OPCODE = XTROP_DELETE   ! delete old tree and recreate
                    xcall XTREE,2,3,XANS(1),XDATA(1),MAXCNT,COLDEF$, &
                        EXITCODE,DLG'ROWS-2,DLG'COLS-3,XFLG,"",0,XTR

                    NEED'SETUP = .TRUE
                else                            ! else just re-enter
                    XTR.OPCODE = XTROP_RESELECT
                    XFLG.FLAGS |= XTF_NOREDRAW  ! (optimization)
                endif
                trace.print (99,"xtra9") XANS(XTR.XROW).DESCR, XDATA(XTR.XROW).DESCR, XANS(XTR.XROW).RGB, XDATA(XTR.XROW).RGB
                exit
                
            case 0                              ! ok
            case -2                             ! save

                open #CH, TYPE_FSPEC$, output

                for I = 1 to MAXCNT
                    XANS = XANS(I)
                    XDATA = XDATA(I)
                    trace.print (99,"xtra9") I, XANS.TYPE, XDATA.TYPE, XANS.BGCLR, XDATA.BGCLR, XANS.DESCR, XDATA.DESCR, XANS.RGB, XDATA.RGB
                    if val(XANS.TYPE) > 0 and XANS.DESCR # "" then  ! skip empty rows
                        ? #CH, XANS(I)
                    endif
                next I
                close #CH
                exit
                DONE = .TRUE
                
            case 1                  ! ESC
                DONE = .TRUE
                
        endswitch
    loop

    ! close the dialog
    xcall AUI, AUI_CONTROL, CTLOP_DEL, DLG'ID

    END

TRAP:
    END


!---------------------------------------------------------------------------
! Global subroutine: SETUP'XTREE
! Purpose:  Set up the XFLGLAGS, COLDEF$, and XTRCTL fields
! Inputs:
!   XA'RGB()    (rgb values)
! Outputs:
!   XA'CLRBG()  (rgb code corresponding to RGB)
!   XFLGLAGS, COLDEF$, XTRCTL
! Local vars
!   i,lastidx,clridx$
!---------------------------------------------------------------------------

!---------------------------------------------------------------------
!Function:
!   Set up the XTREE config
!Params:
!   xflg (ST_XFLAGS) [out] - flags
!   xtr (XTRCTL) [out]
!   coldef$ (S) [out] - column defs
!   xans() (ST_XTRA9) [in,byref] - answer array
!Returns:
!Globals:
!Notes:
!---------------------------------------------------------------------
Function Fn'Setup'XTREE(xflg as ST_XFLAGS:outputonly, xtr as XTRCTL:outputonly, &
                        coldef$ as s0:outputonly, xans() as ST_XTRA9) as i4

    map1 locals
        map2 i,f
        map2 clridx$,s,1

    xflg.FLAGS = 0
    xflg.FLAGS2 = 0     ! [101]   

    ! these are mandatory for this program...
    xflg.FLAGS |= XTF_XYXY           ! alt coords (srow,scol,erow,ecol)
    xflg.FLAGS |= XTF_COLDFX         ! complex syntax in COLDEF
    xflg.FLAGS |= XTF_EDITABLE       ! needed for editable cb or text
    xflg.FLAGS |= XTF_MODELESS       ! Leave box on screen after exit
    xflg.FLAGS |= XTF_FKEY           ! Allow Fx codes!!

    ! Remaining xflg.FLAGS are optional - you can experiment with them

!    xflg.FLAGS = xflg.FLAGS or XTF_LEFT           ! Left arrow (Exitcode -40)
!    xflg.FLAGS = xflg.FLAGS or XTF_RIGHT          ! Right arrow (Exitcode -41)
!    xflg.FLAGS = xflg.FLAGS or XTF_UP             ! Up arrow (Exitcode -42)

!   xflg.FLAGS = xflg.FLAGS or XTF_TAB            ! TAB (Exitcode -44)
!   xflg.FLAGS = xflg.FLAGS or XTF_HOME           ! Right arrow (Exitcode -45)
!   xflg.FLAGS = xflg.FLAGS or XTF_END            ! Right arrow (Exitcode -46)

!   xflg.FLAGS = xflg.FLAGS or XTF_VARY           ! Vari. ht rows (see xtr.ITEMLINES)

    xflg.FLAGS |= XTF_SORT           ! Allow column sorting
    xflg.FLAGS |= XTF_REORD          ! Allow reordering of columns

    xflg.FLAGS2 |= XTF2_AUTOEXPCOL    ! [101]
    xflg.FLAGS2 |= XTF2_ANSEQDATA     ! [102] note: this shouldn't make a difference since
                                      ! [102] the answer array is made up of all editable cells
    xflg.FLAGS2 |= XTF2_AUTOFILTER    ! [102] 
    xflg.FLAGS2 |= XTF2_NOAUTOFILTER  ! [102] this would override XTF2_AUTOFILTER

    ! setup COLDEF

    coldef$ = ""

    ! add a dummy PopupMenu...
    ! (this was necessary to get even the automatic context menu
    ! to display up thru 1736.1)
    !coldef$ = coldef$ + "0~0~x~H~PopupMenu=----,;~~"

    ! Column: 2 digit type number (using #Z mask)
    coldef$ += .offsiz$(ST_XTRA9.TYPE) + "~Type#~#ES~Mask=#Z~~" ! [102] 1~2

    ! Column: 23 character description (including bg color byte)
    coldef$ += .offsiz$(ST_XTRA9.DESCRX) + "~Description~SBE~~" ! [102] 3~23

    ! Column: 11 character rrr,ggg,bbb
    !   note: code HU, for hidden, update-only
    coldef$ += .offsiz$(ST_XTRA9.RGB) + "~RGB~SHU~~"            ! [102] 26~11

    ! Define BG colors based on the RGB values in the data array
    ! (put them all in a pseudo-column definition)
    coldef$ += "0~0~x~H~"
    ! start with an explicit white color...
    coldef$ += "RGBbg=255,255,255,A~"


    trace.print (99,"xtra9") coldef$, .extent(xans())

    ! add RGBbg= definitions for any other defined colors
    clridx$ = "A"
    for i = 1 to .extent(xans())
        if xans(i).RGB # "" then
            clridx$ = chr(asc(clridx$)+1)
            coldef$ += "RGBbg=" + strip(xans(i).RGB) + ","+ clridx$ + "~"
            xans(i).BGCLR = clridx$
            trace.print (99,"xtra9") i, xans(i).RGB, xans(i).BGCLR, xans(i).DESCR
        endif
    next i
    coldef$ = coldef$ + "~"     ! add the trailing ~

    ! setup XTRCTL
    xtr.OPCODE = XTROP_CREATE
    xtr.CTLNO = -1                      ! auto-select xtree #
    xtr.ITEMLINES = 1
    xtr.TREELINESTYLE = 0               ! n/a
    xtr.SHOWBUTTONS0 = 0                ! n/a
    xtr.SHOWBUTTONS = 0                 ! n/a
    xtr.SHOWGRID = 1                    ! grid lines (yes)
    xtr.GRIDSTYLE = 2                   ! solid horz & vert
    xtr.TRUNCATED = 1                   ! show dots if truncated
    xtr.SELECTAREA = XTRSEL_AREA_CELL1 + XTRSEL_STY_CELL1
    xtr.FLYBY = 1                       ! Fly by highlighting (0=no, 1=yes)
    xtr.SCROLLTIPS = 1                  ! Show scroll tips (0=no, 1=yes)
    xtr.KBDSTR = "VK_xF3"               ! kbd click string
    xtr.USETHEMES = 1                   ! 1=use XP themes (if available)
    !xtr.PARENTID = dlgid   (let this default)              ! ID of parent control
    xtr.SHOW3D = 0                      ! 1=use 3D style
    xtr.HIDEHEADER = 0                  ! 1=hide header
    xtr.EXPANDLEVEL = 0                 ! n/a
    xtr.SKEY = ""                       ! n/a
    xtr.TIMEOUT = 0                     ! timeout (ms) (exitcode=11)
    xtr.LEFTPANECOLS = 0                ! (XTF_SPLIT) # cols in left pane
    xtr.LEFTPANEWIDTH = 0               ! (XTF_SPLIT) width of left pane
                                        !   0=optimize, -1=50/50, else col units
    xtr.CLOSEDENDED = 1                 ! last col open ended [101]

    xputarg @xtr 
    xputarg @xflg
    xputarg @coldef$
    
EndFunction


!---------------------------------------------------------------------------
! Global subroutine: CREATE'XTREE'DIALOG
! Purpose:  Create a dialog to hold the XTREE
! Inputs:
!   DLG'ROWS, DLG'COLS, DLG'SROW, DLG'SCOL, DLG'TITLE$
! Outputs:
!   DLG'ID
! Notes:
!   Dialog has following buttons:
!       SAVE
!       COLOR
!       CANCEL
!---------------------------------------------------------------------------

CREATE'XTREE'DIALOG:
    ! create a dialog
    xcall AUI, AUI_CONTROL, CTLOP_ADD, DLG'ID, DLG'TITLE$, MBST_ENABLE, &
        MBF_DIALOG+MBF_ALTPOS+MBF_SYSMENU, NUL_CMD$, NUL_FUNC$, CSTATUS, &
        DLG'SROW, DLG'SCOL, DLG'ROWS+DLG'SROW-1, DLG'COLS+DLG'SCOL-1, &
        NUL_FGC, NUL_BGC, NUL_FONTATTR, NUL_FONTSCALE, NUL_FONTFACE$, &
        NUL_TOOLTIP$, NUL_PARENTID, NUL_WINCLASS$, NUL_WINSTYLE, &
        NUL_WINSTYLEX

    ! put [save] [color] [cancel] buttons in dialog (use icon buttons)

    ! [save] button
    xcall AUI, AUI_CONTROL, CTLOP_ADD, OK'ID, "&Save", MBST_ENABLE, &
        MBF_BUTTON+MBF_KBD, "VK_xF2", NUL_FUNC$, CSTATUS, &
        DLG'ROWS-1, DLG'COLS-24, DLG'ROWS-1, DLG'COLS-19, &
        NUL_FGC, NUL_BGC, NUL_FONTATTR, NUL_FONTSCALE, NUL_FONTFACE$, &
        "Save and Exit", DLG'ID, NUL_WINCLASS$, NUL_WINSTYLE, &
        NUL_WINSTYLEX

    ! [color] button
    xcall AUI, AUI_CONTROL, CTLOP_ADD, COLOR'ID, "Co&lor", MBST_ENABLE, &
        MBF_BUTTON+MBF_KBD, "VK_xF3", NUL_FUNC$, CSTATUS, &
        DLG'ROWS-1, DLG'COLS-17, DLG'ROWS-1, DLG'COLS-12, &
        NUL_FGC, NUL_BGC, NUL_FONTATTR, NUL_FONTSCALE, NUL_FONTFACE$, &
        "Choose Color", DLG'ID, NUL_WINCLASS$, NUL_WINSTYLE, &
        NUL_WINSTYLEX

    ! [cancel] button
    xcall AUI, AUI_CONTROL, CTLOP_ADD, CANCEL'ID, "&Cancel", MBST_ENABLE, &
        MBF_BUTTON+MBF_KBD, "VK_ESC", NUL_FUNC$, CSTATUS, &
        DLG'ROWS-1, DLG'COLS-10, DLG'ROWS-1, DLG'COLS-5, &
        NUL_FGC, NUL_BGC, NUL_FONTATTR, NUL_FONTSCALE, NUL_FONTFACE$, &
        "Quit without Saving", DLG'ID, NUL_WINCLASS$, NUL_WINSTYLE, &
        NUL_WINSTYLEX

    return
