program PCKMNU,1.0(104)	! 2-level PCKLST menu (works in both text & gui)
!----------------------------------------------------------------------
! Variation of a 2-level menu system, using 2 side-by-side PCKLST 
! controls (major selections on the left, minor on the right). 
!
! This is logically similar to a single multi-level XTREE menu, except
! that only the minor options related to the currently highlighted
! major option are visible at one time. A primary advantage of this
! design is that it works in both text and GUI modes (relying on 
! PCKLST's ability to switch modes), and supports both intuitive 
! keyboard and mouse navigation.
!
!    +--------------+ +-------------------------------------------+
!    | A) INVENTORY | | 01) Inventory file maintenance            |
!    | B) Receivable| | 02) Receiving                             |
!    | C) Payable   | | 03) Purchase orders                       |
!    | D) Payroll   | | 04) Turnover report                       |
!    | ...          | | 05) Backorders                            |
!    |              | | ...                                       |
!    +--------------+ +-------------------------------------------+
!   
! The program illustrates a new feature in 5.1.1220.0 - the ability
! to specify an XTREE-style coldef definition string in place of 
! the normal PCKLST-style "prompt" parameter. This allows the GUI
! version (which effectively uses XTREE via PCKLST redirection)
! to take advantage of some XTREE-only features (like Exitchars)
! without interfering with the text mode PCKLST operation. 5.1.1220.0
! also implemented support for the SelChgExit=### feature, previously
! only available in XTREE, to force PCKLST to exit whenever the 
! selection changes. (This is critical to update the minor menu 
! to match the currently selected major menu choice).
!
! There are literally no "IF GUI..." conditionals needed 
! in this program to handle the differences between the text and
! GUI versions. Based on whether the environment supports GUI, 
! and whether the SBR=PCKLST_GUI statement is in the MIAME.INI,
! PCKLST will operate in native (text) mode or call XTREE to 
! operate in GUI mode. 
!
! As a convenience, the program allows switching modes internally
! (by querying and optionally changing the SBR=PCKLST_GUI flag).
!
! Notes on differences between the text and GUI modes:
!
! a) PCKLST (text) recognizes the MMOCLR parameter to set the colors
!    of the various parts of the control.  XTREE (gui) ignores MMOCLR
!    and uses a standard Windows color scheme, which can be overridden
!    by Advanced Coldef options (e.g. RGBfgSel, RGBfgEven, etc.) (These
!    will be ignored by PCKLST/text, so you can configure both modes
!    without having to detect which mode is operational.
!
! b) XTREE supports an option to exit whenever specified characters 
!    are typed (Exitchars). This could be used to allow cross-selection,
!    i.e. to select an option from the left menu (via the A-P letters)
!    while the right menu has the focus. PCKLST doesn't support that,
!    but it's not a big loss because the user can just use the left/right
!    arrow keys to jump between the menus (which works in GUI mode too).
!    Directly selecting a 2 digit choice (01-16) from the right menu 
!    while the left menu has the focus would require exiting on "0" or 
!    "1" and then using a timed input routine to get the next character.
!    (The program actually supports this in GUI mode - see INPUT'SEL)
! 
! c) In PCKLST mode, the font grid is fixed, so we know exactly what
!    is visible (based on the coordinates of the menu boxes).  In XTREE
!    mode, proportional fonts are used, whose size can be adjusted 
!    by CTRL+ and CTRL-, or by Font= and Scale= advanced coldef options,
!    as well as by the Misc Settings menu font scaling options. This
!    doesn't affect the operation of the program though, as horizontal
!    and vertical scroll bars will appear automatically as needed if
!    the choices are too large to fit in the box. Note that although
!    the program currently limits the number of items to 16, since
!    vertical scrolling is supported by both PCKLST and XTREE, you 
!    could allow any number of items in each menu.
!----------------------------------------------------------------------
! Edit History:
![100] June 08, 2011 09:10 PM           Edited by jack
!   Created, based on a design spec from Enzo
![101] June 27, 2011 08:54 AM           Edited by jack
!   Added XTF_UP, XTF2_DOWN
![102] June 28, 2011 03:36 PM           Edited by jack
!   Use Caption for macro menu, header for minor
![103] November 10, 2011 08:41 AM       Edited by jack
!   Illustrate way to handle literal \
![104] December 27, 2011 02:42 PM       Edited by jack
!   Remove the inoperative restore for text mode
!----------------------------------------------------------------------

++include ashinc:ashell.def         
++include ashinc:xtree.def
++include ashinc:xtree.sdf
++include ashinc:trmchr.map     ! 
++include sosfunc:fnminasver.bsi    ! Fn'MinAshVer() check for req version

define MAX_MAJOR = 16           ! # rows in first list 
define MAX_MINOR = 16           ! # rows in second list

define XMODE_DISPLAY = 0        ! app flag for XTREE display-only
define XMODE_SELECT = 1         ! app flag for XTREE selection 

map1 MAJOR                      ! major menu variables
    map2 M1'ARRAY(MAX_MAJOR),S,14 ! 
    map2 M1'SELECTION,F
    map2 M1'SROW,B,2,4
    map2 M1'SCOL,B,2,3          ! note: PCKLST border OUTSIDE this
    map2 M1'EROW,B,2,19
    map2 M1'ECOL,B,2,17         ! note: PCKLST border OUTSIDE this
    map2 M1'XFLAGS,ST_XFLAGS
    map2 M1'XCTL,XTRCTL
    map2 M1'MMOCLR,MMOCLR       ! (MMOCLR structure defined in xtree.sdf) 
map1 M1'COLDEF,S,0         

map1 MINOR                      ! minor menu variables
    map2 M2'ARRAY(MAX_MINOR),S,50 ! 
    map2 M2'SELECTION,F
    map2 M2'SROW,B,2,4
    map2 M2'SCOL,B,2,20         ! must be >= M1'ECOL+3 for PCKLST
    map2 M2'EROW,B,2,19
    map2 M2'ECOL,B,2,75
    map2 M2'XFLAGS,ST_XFLAGS
    map2 M2'XCTL,XTRCTL
    map2 M2'XMODE,B,1
    map2 M2'MMOCLR,MMOCLR       ! (MMOCLR structure defined in xtree.sdf) 
map1 M2'COLDEF,S,0

map1 MISC
    map2 EXITCODE,F
    map2 I,F
    map2 MENU'SEL,I,2           ! was B,2
    map2 GET'BUF$,S,1
    map2 GET'RCVD,B,1
    map2 C,B,1
    map2 STATUS,F               
    map2 SBRFLAGS,B,4           
    map2 M2'SAVE'FLAG,B,1       ! [102] set after menu2 saved
!----------------------------------------------------------------------

    ? tab(-1,0);"PCKMNU - Menu using PCKLST.SBR"

! ? tab(-10,AG_IATIMEOUT);"1,10";chr(127); 
! trace.print "Timeout set to 10 secs"

    ! check if we have at least 5.1.1220.0 (required to support the 
    ! coldef syntax for the prompt parameter in pcklst)

    if Fn'MinAshVer(5,1,1220,0) < 0 then
        ? "Sorry, this program requires 5.1.1220.0 or higher"
        end
    endif

    ! retrieve current screen colors (useful only in text mode to
    ! select an appropriate color for the inactive menu selection bar -
    ! see M1'MMOCLR.R?CLR assignments in Load'Menu'Major)

    xcall TRMCHR,STATUS,TRMCHR'MAP     

    ! We don't need to know whether we are in GUI or text mode, but
    ! for convenience of testing, we'll check for SBR=PCKLST_GUI and 
    ! offer to switch modes

    xcall MIAMEX, MX_SBRFLG, MXOP_GET, SBRFLAGS
    ? "Current mode is: ";
    if SBRFLAGS and SBRF_PCKLST_GUI then
        ? "GUI"
    else
        ? "text"
    endif
    input "Enter 1=text mode, 2=GUI (else no change): ",C

    if C = 1 or C = 2 then
        ? tab(2,1);tab(-1,10);
        if C = 1 then
            SBRFLAGS = SBRFLAGS and not SBRF_PCKLST_GUI
            ? "[text mode]"
        elseif C = 2 then
            SBRFLAGS = SBRFLAGS or SBRF_PCKLST_GUI
            ? "[GUI mode]"
        endif
        xcall MIAMEX, MX_SBRFLG, MXOP_SET, SBRFLAGS
    endif

    ? tab(3,1);tab(-1,10);

    ! Display a prompt/banner at the bottom with instructions
    ? tab(23,1);"ESC to exit; Left/Right to switch menus, ENTER to select"
    ? tab(24,1);"F1 for help";

    call Load'Menu'Major
    M1'SELECTION = 1

    ! main loop - repeat until we exit or make a selection
    do while EXITCODE # 1
        call Do'Menu'Major
        if MENU'SEL <> 0 exit       
    loop 

    
    if EXITCODE = 1 or MENU'SEL < 0 goto ENDIT  

CHAIN'OUT:
    ? tab(23,1);tab(-1,10);"Chaining to program #";MENU'SEL
    end

ENDIT:
    ? tab(23,1);tab(-1,10);"End"
    END


!-----------------------------------------------------------------
! Load the MAJOR menu items and set the XTREE options
! Inputs:
!   TRMCHR'FORE (from XCALL TRMCHR)
! Outputs:
!   M1'ARRAY(1-16)
!   M1'MMOCLR
!   M1'XCTL   (XTREE XTRCTL options)
!   M1'FLAGS
!-----------------------------------------------------------------
Load'Menu'Major:

    M1'ARRAY(1)  = "A) Argentina"
    M1'ARRAY(2)  = "B) Australia"
    M1'ARRAY(3)  = "C) Canada"
    M1'ARRAY(4)  = "D) Chile"
    M1'ARRAY(5)  = "E) France"
    M1'ARRAY(6)  = "F) Germany"
    M1'ARRAY(7)  = "G) Italy"
    M1'ARRAY(8)  = "H) Mexico"
    M1'ARRAY(9)  = "I) New Zealand"
    M1'ARRAY(10) = "J) Portugal"
    M1'ARRAY(11) = "K) Spain"
    M1'ARRAY(12) = ""
    M1'ARRAY(13) = "M) California"
    M1'ARRAY(14) = "N) Oregon"
    M1'ARRAY(15) = "O) Washington"
    M1'ARRAY(16) = ""

    M1'SELECTION = 1
    M1'XCTL.OPCODE = XTROP_CREATE

    ! Set color scheme here (mostly we'll use defaults, but
    ! a new trick in 5.1.1220 uses the RFCLR/RBCLR values
    ! to update the menu bar on exit; let's try it)

    M1'MMOCLR.BFCLR = -1       ! border
    M1'MMOCLR.BBCLR = -1
    M1'MMOCLR.TFCLR = -1       ! text
    M1'MMOCLR.TBCLR = -1
    M1'MMOCLR.AFCLR = -1       ! arrows
    M1'MMOCLR.ABCLR = -1
    M1'MMOCLR.PFCLR = -1       ! prompt/title
    M1'MMOCLR.PBCLR = -1
    M1'MMOCLR.WFCLR = -1       ! warnings
    M1'MMOCLR.WBCLR = -1       ! 
    M1'MMOCLR.SFCLR = -1       ! status line
    M1'MMOCLR.SBCLR = -1       ! 

    ! the RFCLR/RBCLR values (as of 5.1.1220) are used to update the 
    ! menu bar on exit; since this will be the current colors (because
    ! of T?CLR = -1), we'll try setting RFCLR to the dim version of it

    M1'MMOCLR.RFCLR = TRMCHR'FORE + 8
    M1'MMOCLR.RBCLR = -1

    M1'XFLAGS.FLAGS = XTF_FKEY or XTF_XYXY
    M1'XFLAGS.FLAGS = M1'XFLAGS.FLAGS or XTF_RIGHT      ! (-41)
    M1'XFLAGS.FLAGS = M1'XFLAGS.FLAGS or XTF_LEFT       ! (-40)

    M1'XFLAGS.FLAGS = M1'XFLAGS.FLAGS or XTF_FKEY       ! (-1..-12)
    M1'XFLAGS.FLAGS = M1'XFLAGS.FLAGS or XTF_TAB        ! (-44)
    M1'XFLAGS.FLAGS = M1'XFLAGS.FLAGS or XTF_MODELESS
    M1'XFLAGS.FLAGS = M1'XFLAGS.FLAGS or XTF_COLDFX 

    M1'XFLAGS.FLAGS2 = XTF2_POPUP                       ! [102]

    M1'COLDEF = "1~14~ ~S~DefaultScale=120~SelChgExit=103~ExitChars=01~" &
        + "Caption=Countries~~"     ! [102]   

    return

!----------------------------------------------------------------------
! Set up the XTREE options for the MAJOR menu and call it. This 
! routine also handles calling the MINOR menu as needed.
! Returns:
!   EXITCODE
!   MENU'SEL:  >0: then we've completed the selection; 
!              -1: abort prog 
! Notes:
!----------------------------------------------------------------------

Do'Menu'Major:

    do                          ! keep calling menu until done
        MENU'SEL = 0

        call Load'Menu'Minor        ! load minor menu accordingly
        M2'XMODE = XMODE_DISPLAY    ! display minor menu
        call Do'Menu'Minor

        M1'XCTL.CTLNO = 1
        M1'XCTL.HIDEHEADER = 1      ! no header
        M1'XCTL.GRIDSTYLE = XTRGRID_BOTH
        M1'XCTL.SHOWGRID = 1
        M1'XCTL.KBDSTR = "VK_xF101" ! click on 1st menu gives EXITCODE -101

        ! set the target row based on the current M1'SELECTION
        M1'XCTL.TARGETROW = M1'SELECTION
    
        ? tab(22,1);tab(-1,9);		! remove prior debug msg

        xcall PCKLST,M1'SROW,M1'SCOL,M1'SELECTION,M1'ARRAY(1),MAX_MAJOR, &
            M1'COLDEF,EXITCODE,M1'EROW,M1'ECOL,M1'XFLAGS,"", &
            M1'MMOCLR,M1'XCTL  

        ? tab(22,1);"Selection=";M1'SELECTION;", exitcode=";EXITCODE

        ! process the exitcodes..

        if EXITCODE = 1 or M1'SELECTION = 0 then        ! ESCAPE
          call REMOVE'MNU   ! [104]
          MENU'SEL = -1
          EXITCODE = 1      ! [104] (gets reset by REMOVE'MNU)
          exit            
        endif

        if EXITCODE = -40 then          ! left cursor
            MENU'SEL = -1
            exit
        endif

        ! If the user enters "0" or "1" (indicating an attempt to 
        ! selection from the minor menu while focused on the major menu)
        ! we'll try to accommodate them by calling a timed input routine
        ! to wait for a second digit and then check to see if they make
        ! a valid ## (01-16). Note: requires ExitChars=01 (works in GUI only)
        if EXITCODE = -51 then          ! "0" or "1"
           call INPUT'SEL    
           if MENU'SEL > 0 exit         ! exit if we got a valid ##
        endif

        ! RIGHT, TAB, ENTER or click on MINOR menu ...
        if EXITCODE = -41 or EXITCODE = -102 &
        or EXITCODE = -44 or EXITCODE = 0 then  
            M2'XMODE = XMODE_SELECT         ! allow selection in 2nd menu
            call Do'Menu'Minor
            if MENU'SEL > 0 then
                call REMOVE'MNU   ! [104]
                exit
            endif
        endif
    
        if EXITCODE <= -1 and EXITCODE >= -12 then  ! F1-F12
            call Process'Fkey                       ! 
            if EXITCODE = 1 exit                    ! 
        endif

        ! [101] on down arrow exit from minor menu, move major sel down
        if EXITCODE = -43 and M1'SELECTION < MAX_MAJOR then
            M1'SELECTION = M1'SELECTION + 1
        endif

        ! [101] likewise on up arrow
        if EXITCODE = -42 and M1'SELECTION > 1 then 
            M1'SELECTION = M1'SELECTION - 1
        endif

        M1'XCTL.OPCODE = XTROP_RESELECT     ! re-enter major menu
    loop

    return

!------------------------------------------------------------------
! Load minor menu items, based on current M1'SELECTION
!
! Inputs:
!       M1'SELECTION (currently selected item in major menu)
! Outputs:
!       M2'ARRAY(1-16)
!       M2'MMOCLR (possibly set differently based on selection)
!       M2'XFLAGS
!       M2'COLDEF
!       M2'XCTL
!------------------------------------------------------------------
Load'Menu'Minor:
    
    for I = 1 to 16
        M2'ARRAY(I) = ""
    next I

    switch M1'SELECTION
        case 1          ! (Argentina)
            M2'ARRAY(1)  = " \01 Benmarco\Malbec Mendoza"   ! [103] 
            M2'ARRAY(2)  = "02 Pascual Toso Malbec Maipu Valley"
            M2'ARRAY(3)  = "03 Kalinda Malbec Mendoza"
            M2'ARRAY(4)  = "04 Dominio del Plata ""Crios de Susana Balbo"""
            M2'ARRAY(5)  = "05 Elsa Malbec Mendoza"
            M2'ARRAY(6)  = "06 Lamadrid Bonarda Reserva Mendoza"
            M2'ARRAY(7)  = "07 Elsa Torrontes Mendoza"
            M2'ARRAY(8)  = ""
            M2'ARRAY(9)  = "09 Bodega Weinert ""Carrascal"" Lujan de Cuyo"
            M2'ARRAY(10) = "10 Salentein Malbec Reserva Valle de Uco"
            M2'ARRAY(11) = ""
            M2'ARRAY(12) = "12 Valle Perdido Pinot Noir Neuquen"
            exit

        case 2          ! (Australia)
            M2'ARRAY(1) = "01 Tintar Shiraz McLaren Vale"
            M2'ARRAY(2) = "02 Leeuwin Estate ""Prelude"" Margaret River"
            M2'ARRAY(3) = "03 Jacob's Creek Steingarten Riesling Barossa"
            M2'ARRAY(4) = ""
            M2'ARRAY(5) = "05 Green Point Shiraz Victoria"
            M2'ARRAY(6) = ""
            M2'ARRAY(7) = "07 Shaw and Smith ""M3 Vineyard"" Chardonnay"
            M2'ARRAY(8) = "08 Two Hands ""Gnarly Dudes"" Shiraz Barossa"
            M2'ARRAY(9) = ""
            M2'ARRAY(10) = ""
            M2'ARRAY(11) = "11 Coopers Sparkling Ale"
            M2'ARRAY(12) = "12 Jim Barry ""Lodge Hill"" Shiraz Clare Valley"
            M2'ARRAY(13) = ""
            M2'ARRAY(14) = ""
            M2'ARRAY(15) = "15 Gemtree ""Cadenzia"" Grenache-Tempranillo"
            M2'ARRAY(16) = ""
            exit

        case 3          ! (Canada)
            M2'ARRAY(1)  = "01 Whistle Pig Straight Rye Whiskey"
            M2'ARRAY(2)  = ""
            M2'ARRAY(3)  = "03 Henry of Pelham Riesling Ice Wine Niagra"
            M2'ARRAY(4)  = "04 Henry of Pelham Baco Noir"
            M2'ARRAY(5)  = "05 Henry of Pelham Cabernet Franc Ice Wine"
            M2'ARRAY(6)  = ""
            M2'ARRAY(7)  = "07 Brasserie Dieu du Ciel ""P�che Mort�l"""
            M2'ARRAY(8)  = "08 Brasserie Dieu du Ciel ""Aphrodite"""
            M2'ARRAY(9)  = "09 Brasserie Dieu du Ciel ""Corne du Diable"""
            M2'ARRAY(10) = ""
            M2'ARRAY(11) = "11 Crystal Head Vodka"
            M2'ARRAY(12) = ""
            M2'ARRAY(13) = "13 Inniskillin Cabernet Franc Ice wine"
            exit

        case 4          ! (Chile)
            M2'ARRAY(1)  = "01 Veramonte Sauvignon Blanc Reserva"
            M2'ARRAY(2)  = "02 Cousi�o Macul ""Antiguas Reservas"" Maipu"
            M2'ARRAY(3)  = "03 Casa Lapostelle ""Cuv�e Alexandre"""
            M2'ARRAY(4)  = ""
            M2'ARRAY(5)  = "05 Santa Ema Cabernet Sauvignon Reserve Maipu"
            M2'ARRAY(6)  = "06 Vi�a Von Siebental Parcela #7"
            M2'ARRAY(7)  = ""
            M2'ARRAY(8)  = "08 Vi�a Falernia Syrah Reserva Elqui Valley"
            M2'ARRAY(9)  = "09 Santa Rita ""Medalla Real"" Maipu Valley"
            M2'ARRAY(10) = "10 Tabali Pinot Noir Reserva Limari Valley"
            M2'ARRAY(11) = ""
            M2'ARRAY(12) = "12 Casillero del Diablo Reserve Privada"
            M2'ARRAY(16) = "16 Carma Carm�n�re Colchagua Valley"
            exit

        case 5      ! (France)
            M2'ARRAY(1)  = "01 La Bienfaisance, St-Emilion"
            M2'ARRAY(2)  = "02 Grand Bateau Rouge, Bordeaux"
            M2'ARRAY(3)  = "03 St-Jean-des-Graves, Graves"
            M2'ARRAY(4)  = "04 Lanessan, Haut-M�doc"
            M2'ARRAY(5)  = ""
            M2'ARRAY(6)  = "06 Domaine de la Roman�e Conti Richebourg"
            M2'ARRAY(7)  = "07 Bouchard Pere et Fils Montrachet Grand Cru"
            M2'ARRAY(8)  = "08 Domaine Hubert Lignier Clos de la Roche"
            M2'ARRAY(9)  = ""
            M2'ARRAY(10) = "10 E. Guigal ""La Turque"" C�te-R�tie"
            M2'ARRAY(11) = "11 Clos des Papes Ch�teauneuf-du-Pape"
            M2'ARRAY(12) = "12 M. Chaptoutier ""Le Pavillon"" Ermitage"
            M2'ARRAY(13) = ""
            M2'ARRAY(14) = "14 Krug ""Clos Ambonnay"" Brut"
            M2'ARRAY(15) = "15 Roederer ""Cistral"" Brut"
            M2'ARRAY(16) = "16 Billecart-Salmon Le Clos Saint-Hilaire"
            exit

        case 6      ! (Germany) 
            M2'ARRAY(1)  = "01 Kalinda Rheingau Riesling Qba"
            M2'ARRAY(2)  = "02 Schloss Saarstein Pinot Blanc"
            M2'ARRAY(3)  = ""
            M2'ARRAY(4)  = "04 Josef Leitz Rudesheimer Drachenstein"
            M2'ARRAY(5)  = "05 Josef Rosch Leiwener Klostergarten Riesling"
            M2'ARRAY(6)  = "06 Franz Karl Schmitt Niersteiner Pettenthal"
            M2'ARRAY(7)  = ""
            M2'ARRAY(8)  = ""
            M2'ARRAY(9)  = "09 Dr. Pauly Bergweiler Bernkasterler"
            M2'ARRAY(10) = "10 Dr. Heyden Oppenheimer Sacktrager Spatlase"
            M2'ARRAY(11) = ""
            M2'ARRAY(12) = ""
            M2'ARRAY(13) = "13 Weihenstephaner HefeWeis"
            M2'ARRAY(14) = "14 Reissdorf Kolsch"
            exit

        case 7      ! (Italy)
            M2'ARRAY(1)  = "01 Bruno Giacosa Barbaresco Falleto ""Rabaja"""
            M2'ARRAY(2)  = "02 Giacosa Barbaresco Santa Stefano Riserva"
            M2'ARRAY(3)  = ""
            M2'ARRAY(4)  = "04 Tenuta dell'Ornellaia Ornellaia"
            M2'ARRAY(5)  = "05 Bertani Amarone della Valpolicella"
            M2'ARRAY(6)  = "06 Tommoso Bussola Amarone ""Vigneto Alto"""
            M2'ARRAY(7)  = ""
            M2'ARRAY(8)  = "08 Marchesi di Barolo Barolo"
            M2'ARRAY(9)  = "09 Vietti Barolo ""Brunate"""
            M2'ARRAY(10) = "10 Vietti Barolo ""Lazzarito"""
            M2'ARRAY(11) = ""
            M2'ARRAY(12) = "12 Casanova de Neri Brunello de Montalcino"
            M2'ARRAY(13) = "13 Valdicava Brunello de Montalcino"
            M2'ARRAY(14) = "14 Banfi Brunello de Montalcino Reserva"
            M2'ARRAY(15) = ""
            M2'ARRAY(16) = "16 Fattoria Galardi Terra di Lavoro"
            exit

        case 8      ! (Mexico) 
            M2'ARRAY(1) = "01 Don Julio 1942 Anejo Tequila"
            M2'ARRAY(2) = "02 Don Julio Reposado Tequila"
            M2'ARRAY(3) = "03 Espolon Reposado Tequila"
            M2'ARRAY(4) = "04 Espolon Silver Tequila"
            M2'ARRAY(5) = ""
            M2'ARRAY(6) = "06 Embajador Blanco Mescal de Oaxaca"
            M2'ARRAY(7) = "07 Del Maguey Chichicapa Mezcal"
            exit

        case 9      ! (New Zealand)
            M2'ARRAY(1) = "01 Dashwood Sauvignon Blanc Marlborough"
            M2'ARRAY(2) = ""
            M2'ARRAY(3) = "03 Quartz Reef Pinot Noir Central Otago"
            M2'ARRAY(4) = "04 Mountford ""Village"" Pinot Noir Waipara"
            M2'ARRAY(5) = ""
            M2'ARRAY(6) = "06 Kim Crawford Sauvignon Blanc Marlborough"
            M2'ARRAY(7) = "07 Starborough Sauvignon Blanc Marlborough"
            M2'ARRAY(8) = "08 Oyster Bay Sauvignon Blanc Marlborough"
            M2'ARRAY(9) = ""
            M2'ARRAY(10) = "10 Te Awa Syrah Hawkes Bay"
            M2'ARRAY(11) = "11 Neudorf ""Moutere"" Pinot Noir Nelson"
            M2'ARRAY(12) = ""
            M2'ARRAY(13) = "13 Man O' War Pinot Gris Ponui Island"
            M2'ARRAY(14) = ""
            M2'ARRAY(15) = "15 Cobblestone ""Te Muna"" Pinot Noir"
            M2'ARRAY(16) = ""
            exit

        case 10     ! (Portugal) 
            M2'ARRAY(1) = "01 Adega Cooperativo Regional de Mon�ao "
            M2'ARRAY(2) = "02 Casal Garcia Vinho Verde Ros� "
            M2'ARRAY(3) = ""
            M2'ARRAY(4) = "04 Quinta dos Roques Reserva Dao"
            M2'ARRAY(5) = "05 Quinta de Cabriz Colheita Seleccionada Dao"
            M2'ARRAY(6) = ""
            M2'ARRAY(7) = "07 Lavradores de Feitoria ""Tres Bagos"" Douro"
            M2'ARRAY(8) = "08 Quinta do Vallado Douro"
            M2'ARRAY(9) = "09 Chyrseia Post Scriptum Douro"
            M2'ARRAY(10) = "10 Lemos & Van Zeller Casa de Casal de Loivos"
            M2'ARRAY(11) = "" 
            M2'ARRAY(12) = "12 Niepoort Colheita 1988"
            M2'ARRAY(13) = "13 Niepoort Vintage 1997"
            M2'ARRAY(14) = ""
            M2'ARRAY(15) = "15 Barbeito Malvasia Colheita 2000"
            M2'ARRAY(16) = ""
            exit

        case 11     ! (Spain)
            M2'ARRAY(1) = "01 Sorry, no Spanish wines in stock"
            exit

        case 12
            exit

        case 13     ! (California)
            M2'ARRAY(1)  = "01 Franciscan Napa Valley Cabernet Sauvignon"
            M2'ARRAY(2)  = "02 Urbanite Cellars ""Amplio"" Cab. Sauv."
            M2'ARRAY(3)  = "03 Caymus Napa Valley Cabernet Sauvignon"
            M2'ARRAY(4)  = ""
            M2'ARRAY(5)  = "05 MacRostie Carneros Pinot Noir"
            M2'ARRAY(6)  = "06 Varner ""Spring Ridge"" Santa Cruz Pinot"
            M2'ARRAY(7)  = "07 Au Bon Climat Santa Barbara Pinot Noir"
            M2'ARRAY(8)  = ""
            M2'ARRAY(9)  = "09 Rombauer Carneros Chardonnay"
            M2'ARRAY(10) = "10 Edna Valley Vineyards ""Paragon"" Chard."
            M2'ARRAY(11) = "11 Chalone Estate Chardonnay"
            M2'ARRAY(12) = ""
            M2'ARRAY(13) = "13 Ravenswood ""Barricia"" Sonoma Zinfandel"
            M2'ARRAY(14) = ""
            M2'ARRAY(15) = "15 Ambullneo ""Howling"" Santa Maria Syrah"
            M2'ARRAY(16) = "16 Melville ""Estate-Verna's"" Sta. Rita Syrah"
            exit

        case 14     ! (Oregon)
            M2'ARRAY(1)  = "01 Domaine Serene ""Yamhill Cuvee"" Willamette"
            M2'ARRAY(2)  = "02 Chehalem ""3 Vineyards"" Willamette Pinot"
            M2'ARRAY(3)  = "03 Cristom ""Mt. Jefferson Cuvee"" Willamette"
            M2'ARRAY(4)  = "04 Argyle ""Nuthouse"" Willamette Pinot Noir"
            M2'ARRAY(5)  = ""
            M2'ARRAY(6)  = "06 A to Z Oregon Pinot Noir"
            M2'ARRAY(7)  = "07 Beaux Fr�res ""Beaux Freres Vineyard"" Will."
            M2'ARRAY(8)  = "08 Penner Ash Willamette Valley Pinot Noir"
            M2'ARRAY(9)  = ""
            M2'ARRAY(10) = "10 Ponzi Willamette Valley Pinot Gris"
            M2'ARRAY(11) = "11 King Estate ""Acrobat"" Oregon Pinot Gris"
            exit

        case 15     ! (Washington) 
            M2'ARRAY(1)  = "01 Chateau Ste. Michelle Columbia Valley Cab"
            M2'ARRAY(2)  = ""
            M2'ARRAY(3)  = "03 Northstart Columbia Valley Merlot"
            M2'ARRAY(4)  = "04 L'Ecole No 41 Columbia Valley Merlot"
            M2'ARRAY(5)  = "05 Owen Roe ""Abott's Table"" Columbia Red"
            M2'ARRAY(6)  = ""
            M2'ARRAY(7)  = "07 Leonetti Walla Walla Valley Merlot"
            M2'ARRAY(8)  = ""
            M2'ARRAY(9)  = "09 Hogue ""Genesis"" Washington Chardonnay"
            M2'ARRAY(10) = ""
            M2'ARRAY(11) = "11 Tamarack Cellars Columbia Valley Cab Franc"
            M2'ARRAY(12) = ""
            M2'ARRAY(13) = ""
            M2'ARRAY(14) = ""
            M2'ARRAY(15) = "15 Mark Ryan Winery ""Wild Eyed"" Red Mtn Syrah"
            exit

        case 16
        default
            exit
    endswitch

    ! Set color scheme here (in case we want to adjust it
    ! for each submenu)

    M2'MMOCLR.BFCLR = 5        ! border
    M2'MMOCLR.BBCLR = 0 
    M2'MMOCLR.TFCLR = 5        ! text
    M2'MMOCLR.TBCLR = 0
    M2'MMOCLR.AFCLR = 5        ! arrows
    M2'MMOCLR.ABCLR = 0 
    M2'MMOCLR.PFCLR = 5        ! prompt/title
    M2'MMOCLR.PBCLR = 0 
    M2'MMOCLR.WFCLR = 4        ! warnings 
    M2'MMOCLR.WBCLR = 0        ! 
    M2'MMOCLR.SFCLR = 4        ! status line
    M2'MMOCLR.SBCLR = 0        ! 
    M2'MMOCLR.RFCLR = -1       ! ruler
    M2'MMOCLR.RBCLR = -1

    M2'XFLAGS.FLAGS = XTF_FKEY or XTF_RIGHT or XTF_XYXY
    M2'XFLAGS.FLAGS = M2'XFLAGS.FLAGS or XTF_MODELESS
    M2'XFLAGS.FLAGS = M2'XFLAGS.FLAGS or XTF_COLDFX 
    M2'XFLAGS.FLAGS = M2'XFLAGS.FLAGS or XTF_LEFT
    M2'XFLAGS.FLAGS = M1'XFLAGS.FLAGS or XTF_UP         ! [101] (-42) 

    M2'XFLAGS.FLAGS2 = XTF2_DOWN                        ! [101] (-43) 
    M2'XFLAGS.FLAGS2 = M2'XFLAGS.FLAGS2 or XTF2_POPUP   ! [102]
    if M2'SAVE'FLAG = 0 then                                ! [104]
        M2'XFLAGS.FLAGS2 = M2'XFLAGS.FLAGS2 or XTF2_SAVRES  ! [104]
    endif

![103] With "\", backslash is just a literal character
![103] M2'COLDEF = "1~50~Wines in Stock~S\~"         ! [102] add title [103] \
![103] without it, backslash marks hidden text
    M2'COLDEF = "1~50~Wines in Stock~S~"             ! [102] add title 

    M2'COLDEF = M2'COLDEF + "DefaultScale=120~HdrScale=140~" &
        + "ExitChars=ABCDEFGHIJKLMNOP~RGBbgOdd=220,255,230~~"  

    M2'XCTL.OPCODE = XTROP_CREATE
    M2'XCTL.CTLNO = 2
![102]    M2'XCTL.HIDEHEADER = 1            ! no header
    M2'XCTL.GRIDSTYLE = XTRGRID_BOTH
    M2'XCTL.SHOWGRID = 1
    M2'XCTL.KBDSTR = "VK_xF102" ! click on 1st menu gives EXITCODE -102

    return

!----------------------------------------------------------------
! Display or select from the minor menu
! Input:
!   M2'XMODE (XMODE_DISPLAY or XMODE_SELECT)
!   M2'MMOCLR
!   M2'COLDEF
!   M2'XFLAGS
! Output:
!   MENU'SEL > 0 if an item was selected
!   M1'SELECTION (if they enter A-P)
!----------------------------------------------------------------
Do'Menu'Minor:
       
    if M2'XMODE = XMODE_DISPLAY then
        M2'XFLAGS.FLAGS = M2'XFLAGS.FLAGS or XTF_NOSEL  ! display only
    else
        M2'XFLAGS.FLAGS = M2'XFLAGS.FLAGS and not XTF_NOSEL ! select
    endif

    M2'SELECTION = 1                   ! start minor menu at
    M2'XCTL.TARGETROW = M2'SELECTION   ! pos 1 (but click will override)

debug.print "PCKLST: FLAGS2="+M2'XFLAGS.FLAGS2

    xcall PCKLST,M2'SROW,M2'SCOL,M2'SELECTION,M2'ARRAY(1),MAX_MAJOR, &
            M2'COLDEF,EXITCODE,M2'EROW,M2'ECOL,M2'XFLAGS,"", &
            M2'MMOCLR,M2'XCTL  

    ? tab(22,1);tab(-1,9);"Selection=";M2'SELECTION;", exitcode=";EXITCODE

    if M2'SELECTION > 0 and EXITCODE = 0 then
        MENU'SEL = M2'SELECTION
    else
        MENU'SEL = 0
    endif

    ! if then enter A-P, we'll set the M1'SELECTION accordingly
    ! (this only works in GUI mode)
    if EXITCODE = -51 then      ! "A" - "P"
        M1'SELECTION = M2'XCTL.XNAVCOD - asc("A") + 1
    endif

    ! [104] remove the SAVRES flag (only use it once)
    if M2'XFLAGS.FLAGS2 and XTF2_SAVRES then
        M2'SAVE'FLAG = 1        
        M2'XFLAGS.FLAGS2 = M2'XFLAGS.FLAGS2 and not XTF2_SAVRES
    endif    
    return


!------------------------------------------------------------------
! come here if they enter digit 0 or 1
! allow up to 1.25 seconds to input the 2nd digit
! Inputs:
!   digit entered in XTREE is in M1'XTR.NAVCOD
! Returns:
!   MENU'SEL > 0 if valid ##
!------------------------------------------------------------------
INPUT'SEL:
    
    xcall GET,GET'BUF$,0,1,GET'RCVD,1250

    ! if a character entered and it is a digit, combine it with
    ! the previous one (in M1'XCTL.NAVCOD), and if it makes a number
    ! between 1 and 16, chain to the corresponding program
    if GET'RCVD = 1 then
        if GET'BUF$>="0" and GET'BUF$<="9" then
            MENU'SEL = val(chr(M1'XCTL.XNAVCOD)) * 10 + val(GET'BUF$)
            if MENU'SEL <= 0 or MENU'SEL > 16 then
                 MENU'SEL = 0   ! invalid selection
            endif
        endif
    endif
    return



!----------------------------------------------------------------
! Handle F1-F12
! Input:
!   EXITCODE (-1 ... -12)
! Output:
!   EXITCODE (set to 1 to abort program; else we go back to menu)
!----------------------------------------------------------------

Process'Fkey:

    ! for F1 display a help message...
    if EXITCODE = -1 then
        xcall MSGBOX, "This isn't a very helpful message", &
            "PCKMNU help", MBTN_OK, MBICON_ICON, MBMISC_TASKMODAL
    else            ! else display the exitcode and exit
        ? tab(22,50);"F";str(-EXITCODE);" - hit any key: ";
        C = getkey(-1)
        EXITCODE = 1    ! (to abort program)
    endif
    return


REMOVE'MNU:

!	M1'XCTL.OPCODE = XTROP_DELETE
!       M1'XFLAGS.FLAGS2 = M1'XFLAGS.FLAGS2 or XTF2_SAVRES  
!	xcall PCKLST,M1'SROW,M1'SCOL,POP'INDEX,M1'ARRAY(1),16, &
!          M1'TITLE,VAR1,M1'EROW,M1'ECOL,M1'XFLAGS,"", &
!          M1'MMOCLR,M1'XCTL

	M2'XCTL.OPCODE = XTROP_DELETE
        M2'XFLAGS.FLAGS2 = M2'XFLAGS.FLAGS2 or XTF2_SAVRES  

	xcall PCKLST,M2'SROW,M2'SCOL,M2'SELECTION,M2'ARRAY(1),MAX_MAJOR, &
            M2'COLDEF,EXITCODE,M2'EROW,M2'ECOL,M2'XFLAGS,"", &
            M2'MMOCLR,M2'XCTL
	RETURN

