:<(UI/IO) XTREE.SBR Sample programs
    XTRARY   - Simple array sample    
    XTRA2    - Array sample using a dialog, multiple selection, popup menu
    XTRA2A   - Variation of XTRA2 with 4 levels, merge, expandlevel
    XTRA2S   - Variation of XTRA2 using split mode, dialog menus
    XTRA3    - More sophisticated array sample - prompts for all options
    XTRA3D   - Version of XTRA3 with XTF_DRAGDROP support.
    XTRA4    - Shopping cart example (transferring items from one tree to 
               another using various methods including drag/drop)
    XTRA5    - Array w/ chkboxes, editable cells, validation, popups, split mode
    XTRA5C   - Variation of XTRA5 using CellList, dialog menus
    XTRA5M   - Variation of XTRA5 with multi-selection
    XTRA6    - Radio buttons, multi-line editable cells, 
               enable/disable individual radio buttons during validation
    XTRA7    - Image display, use of progress dialog while loading
    XTRA7C   - (variation with checkboxes, option to set XTR'ITEMLINES)
    XTRA8    - Spreadsheet (every column but first is editable)
    XTRA9    - Editable cells and user-definable individual cell colors
               (also see [908,57] for another variation from UmZero)
    XTRA10   - Primitive example of dueling editable xtrees
    XTRA11   - Steve Evan's sample editable xtree/dialog
    XTRA13   - Monthly planner sample (all cells editable, cell colrs)
    XSTEVE   - Multi-level/colorful file-based example from Steve Evans
    XTRA15   - Property sheet example
    XTRFIL   - Simple file sample
    XTRCSV   - Simple CSV example
    XVMNU2   - Sample 2-level text/gui menu 
    XUTF8A   - Example with XTF2_UTF8
    XUTF8B   - Another example with XTF2_UTF8
    XTRAFGBG - Example of RGBbgfg
    XTRADP   - Example date picker
Also see XTR32BITS in [907,13] and [907,12] (reusable procedure example).
>
 
