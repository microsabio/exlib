:<(INFLD) INFLD Test Programs

    TSTCBZ - Checkboxes (GUI), both via INFLD and via AUI_CONTROL
    TSTCBZ2 -Variation allowing choice of main window or dialog
    INFMUL - Multiline INFLD (GUI)
    INFBTN - Simple example of clickable INFLD fields and labels
    INFRO  - Variation testing read only behavior
    INFAC1 - Simple example of auto-complete mode (||c)
    INFAC2 - Better example of auto-complete (US cities)
    CMBMNU - Sample of 1-line 2-level menu scheme using INFLD combos
    INF1   - Test various styles for handling 1 character gui controls
    QRYCB  - Simple example of CTLOP_QRYCB
>
