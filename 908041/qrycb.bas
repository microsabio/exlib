program qrycb,1.0(100)  ! simple example of querying checkboxes
!---------------------------------------------------------------
!EDIT HISTORY
![100] December 16, 2013 09:26 AM       Edited by jack
!   created (for doc)
!---------------------------------------------------------------

++include ashinc:ashell.def

define MAX_CB = 7       ! # of checkboxes
define DLG_ID$ = "dlg1" ! id of main dialog

map1 cb(MAX_CB)     
    map2 cb'id$,s,20    ! control id
    map2 cb'dsc$,s,20   ! display text
    map2 cb'val,b,1     ! checkbox value

map1 misc
    map2 i,f
    map2 row,b,4
    map2 col,b,4
    map2 evw'parentid$,s,24
    map2 evw'ctlid$,s,24
    map2 evw'flags,b,4
    map2 exitcode,f
    map2 status,f

data "cbAPEX",      "APEX"
data "cbASQL",      "ASQL"
data "cbATE",       "ATE"
data "cbATS",       "ATS"
data "cbISAMA",     "ISAM-A"
data "cbPDFX",      "PDFX"
data "cbPolyShell", "PolyShell"


    ! create the dialog
    xcall AUI, AUI_CONTROL, CTLOP_ADD, DLG_ID$, "A-Shell Options", &
        MBST_ENABLE, MBF_DIALOG + MBF_ALTPOS + MBF_SYSMENU, &
        NUL_CMD$, NUL_FUNC$, NUL_CSTATUS, 2000, 5000, 8000, 25000

    ! create the checkbox controls
    for i = 1 to 7
        read cb'id$(i), cb'dsc$(i)

        ! divide into two columns
        if i <= 4 then
            row = (i + 1) * 1000  ! millirows
            col = 3000
        else
            row = (i - 3) * 1000
            col = 12000
        endif

        cb'val(2) = 1   ! (initialize one of the checkboxes to checked)

        xcall AUI, AUI_CONTROL, CTLOP_ADD, cb'id$(i), cb'dsc$(i), &
            MBST_ENABLE, MBF_CHKBOX + MBF_LFJUST + MBF_TABSTOP, &
            NUL_CMD$, cb'val(i), NUL_CSTATUS, row, col, row+1000, col+7000, &
            NUL_FGC, NUL_BGC, NUL_FONTATTR, NUL_FONTSCALE, NUL_FONTFACE$, &
            NUL_TOOLTIP$, DLG_ID$
    next i

    ! create qry/exit buttons
    xcall AUI, AUI_CONTROL, CTLOP_ADD, "btnQry", "checks::ashico1", &
        MBST_ENABLE, MBF_BUTTON + MBF_ICON + MBF_KBD, &
        "VK_xF101", NUL_FUNC$, NUL_CSTATUS, 5500, 14000, 6500, 16000, &
        NUL_FGC, NUL_BGC, NUL_FONTATTR, NUL_FONTSCALE, NUL_FONTFACE$, &
        "Query the current checkbox values", DLG_ID$

    xcall AUI, AUI_CONTROL, CTLOP_ADD, "btnExit", "delete::ashico1", &
        MBST_ENABLE, MBF_BUTTON + MBF_ICON + MBF_KBD, &
        "VK_ESC", NUL_FUNC$, NUL_CSTATUS, 5500, 17000, 6500, 19000, &
        NUL_FGC, NUL_BGC, NUL_FONTATTR, NUL_FONTSCALE, NUL_FONTFACE$, &
        "Exit dialog", DLG_ID$
    
    ! wait for user to click Query or Exit
    evw'parentid$ = "dlg1"
    evw'ctlid$ = cb'id$(1)
    evw'flags = 0
    do
        xcall AUI, AUI_EVENTWAIT, evw'parentid$, evw'ctlid$, exitcode, evw'flags
    
        switch exitcode
            case -101            ! clicked ok
                trace.print "Querying checkboxes..."
                for i = 1 to MAX_CB
                    xcall AUI, AUI_CONTROL, CTLOP_QRYCB, cb'id$(i), NUL_CTEXT$, &
                        NUL_CSTATE, NUL_CTYPE, NUL_CMD$, NUL_FUNC$, status
                    cb'val(i) = status
                    trace.print "CB #"+i+" : "+cb'dsc$(i)+" = "+cb'val(i)
                next i
                exit
        endswitch
    loop until exitcode = 1     ! (escape)

    xcall AUI, AUI_CONTROL, CTLOP_DEL, DLG_ID$      ! close dialog
end
