program MOUSEC,1.0(100) ! test mouse cursor reporting
!*! Created on 1997-05-24 at 3:20 PM by James A. Jarboe I V; edit time: 0:17:4 
!                                                                              
! Must have NOECH0.SBR and ACCEPT.SBR included with this source                  
!                                                                              
map1 CHAR, F                                                                   
                                                                               
! Turn Echo off                                                                
!                                                                              
        call CLEAR                                                             
                                                                               
! Turn mouse off.                                                              
!                                                                              
        ? tab(-1,159);                                                         
                                                                               
! Turn mouse on just like ESP does.                                            
!                                                                              
        ? tab(-1,158);"C";chr$(34);chr$(27);chr$(30);                          
        xcall NOECHO                                                           
TEMP:                                                                          
        XCALL ACCEPT, CHAR                       ! Get character input.         
        if CHAR = ASC("c") then call CLEAR : GOTO TEMP                         
        if CHAR = ASC("C") then call CLEAR : GOTO TEMP                         
        if CHAR = ASC("Q") then goto endit      ! Quit if "Q" Pressed.         
        ? (CHAR)                                ! Print value of char.         
         goto TEMP                               ! Again.                       
                                                                                
 endit:                                                                         
         ? tab(-1,159);                                                         
         end                                                                    
                                                                                
CLEAR:                                                                         
         ? TAB(-1,0);"* <-Left or right double click on star"                   
         ? TAB( 3,6);"The return sequence should be"                            
         ? TAB( 4,6);"27 <- Lead-in"                                            
         ? TAB( 5,6);"30 <- Lead-in"                                            
         ? TAB( 6,6);"32 <- Row 1"                                              
         ? TAB( 7,6);"32 <- Col 1"                                              
         ? TAB( 8,6);"34 <- Down click left    33 = Down click right"           
         ? TAB( 9,6);"27 <- Lead-in"                                            
         ? TAB(10,6);"30 <- Lead-in"                                            
         ? TAB(11,6);"32 <- Row 1"                                              
         ? TAB(12,6);"32 <- Col 1"                                              
         ? TAB(13,6);"32 <- Terminator stops here for single click"             
         ? TAB(14,6);"27 <- Lead-in for double click"                           
         ? TAB(15,6);"30 <- Lead-in for double click"                           
         ? TAB(16,6);"32 <- Row 1"                                              
         ? TAB(17,6);"32 <- Col 1"                                              
         ? TAB(18,6);"32 <- Double click terminator"                            
                                                                     
        ? TAB(24,1);"Press Q to Quit, Press c to refresh screen.";   
        ? TAB(3,1)                                                   
        return                                                       
