program AMOUSE,1.0(4)   ! test mouse hot-spot tcrt calls
!------------------------------------------------------------------------
! Note: This is based on the MOUSE.BAS in the AM72/AM75/InSight documentation,
! and uses the same MOUSE.MAP and MOUSE.BSI files, but this main module
! is quite different.
!
! EDIT HISTORY
! [1] 01-Jul-96 Created /jdm
! [2] 23-Jul-96 Added status lines, minor cleanup /jdm
! [3] 12-Jun-02 Recompile with adjusted mouse.bsi for 4.7(805)+ /jdm
! [4] 05-Sep-10 Minor tweaking for AMOS compatibility /jdm
!-------------------------------------------------------------------------

++include ashinc:mouse.map

	on error goto EXIT

MAP1 A$,S,20
MAP1 INXCTL,F
MAP1 EXITCODE,F
MAP1 TIMER,F
MAP1 CMDFLG,F
MAP1 DEFPT,F,6,-1
MAP1 MAXPT,F,6,-1
MAP1 FUNMAP,B,4,-1
MAP1 CURSOR,F,6

        xcall NOECHO    ! [4] needed for AMOS (MESAG.SBR)

	? tab(-1,0);"AMOUSE.BAS - Test Mouse TCRT's 159, 160, and 162"
	? "Check if hot spots supported...";
 	call init'hot'spot
	if hotspt = 1 ? "Yes" else ? "No"


	? tab(4,1);"Hot spots have NOT yet been activated.  To activate them, select"
	? tab(5,1);"the appropriate menu option(s). When activated, cursor"
	? tab(6,1);"should change from the I-Beam to an arrow or other pointer"

	print tab(08,10);"1. Activate Hot Spots"
	print tab(10,10);"2. Disable Hot Spots"
	print tab(12,10);"3. Set Cursor to Hourglass for 3 seconds"
	print tab(14,10);"4. Cycle through Watch cursors for 12 seconds"
	print tab(16,10);"5. Set Cursor to pointing finger"
	print tab(18,10);"6. Set Cursor to arrow"
	print tab(20,10);"7. Computerized sex"

	! display a help "button"
	xcall MSBOXX,8,65,8,70,3
	? tab(8,66);"Help";

LOOP:
	? tab(22,5);"Selection (ESC to exit): ";
	xcall INFLD,0,0,6,0,"AE?h",A$,INXCTL,1,0,EXITCODE,TIMER,CMDFLG,&
		DEFPT,MAXPT,FUNMAP
	IF INXCTL=2 GOTO EXIT
	IF A$="1" THEN CALL INIT'HOT'SPOTS : GOTO LOOP
	IF EXITCODE=-2 OR A$="2" THEN CALL DISABLE'HOT'SPOTS : GOTO LOOP
	IF EXITCODE=-3 OR A$="3" THEN CALL HOUR : GOTO LOOP
	IF EXITCODE=-4 OR A$="4" THEN CALL WATCHES : GOTO LOOP
	IF EXITCODE=-5 OR A$="5" THEN CALL FINGER : GOTO LOOP
	IF EXITCODE=-6 OR A$="6" THEN CALL ARROW : GOTO LOOP
	IF EXITCODE=-7 OR A$="7" THEN CALL SEX : GOTO LOOP
	IF EXITCODE=8 THEN CALL HELP : GOTO LOOP
	IF EXITCODE=-8 THEN CALL TSTATUS : GOTO LOOP
	IF EXITCODE=-9 THEN CALL BSTATUS : GOTO LOOP

	! Note, MESAG will display as a dialog box if PCTDVG driver used
	XCALL MESAG,"You entered an invalid option!",2
	GOTO LOOP

WATCHES:
	? TAB(24,1);TAB(-1,9);"Please Wait...";
	? TAB(-1,160);CHR$(41);		! chg to watch cursor
	XCALL SLEEP,1
	? TAB(-1,160);CHR$(42);		! chg to watch cursor
	XCALL SLEEP,1
	? TAB(-1,160);CHR$(43);		! chg to watch cursor
	XCALL SLEEP,1
	? TAB(-1,160);CHR$(44);		! chg to watch cursor
	XCALL SLEEP,1
	? TAB(-1,160);CHR$(45);		! chg to watch cursor
	XCALL SLEEP,1
	? TAB(-1,160);CHR$(46);		! chg to watch cursor
	XCALL SLEEP,1
	? TAB(-1,160);CHR$(47);		! chg to watch cursor
	XCALL SLEEP,1
	IF CURSOR=0 THEN CURSOR=32
	? TAB(-1,160);CHR$(CURSOR);	! back to pointer
	? TAB(24,1);TAB(-1,9);		! remove "please wait"
	RETURN

HOUR:
	? TAB(24,1);TAB(-1,10);"Please Wait...";
	? CHR$(8);  			! (This will flush buffer)
	? TAB(-1,160);CHR$(40);		! chg to hourglass cursor
	XCALL SLEEP,3
	IF CURSOR=0 THEN CURSOR=32	! default to arrow
	? TAB(-1,160);CHR$(CURSOR);	! back to previous
	? TAB(24,1);TAB(-1,9);
	RETURN			

FINGER:
	? TAB(-1,160);CHR$(35);		! set pointing hand
	CURSOR = 35			! 35=finger
	RETURN
ARROW:
	? TAB(-1,160);CHR$(32);		! back to arrow
	CURSOR = 32			! 32=arrow
	RETURN
SEX:
	XCALL MESAG,"Sorry, that option requires the 'Professional Version'",2
	RETURN

INIT'HOT'SPOTS:
! 	Define our hot spots
!
!	hot'spot(x) is formatted as XXYYLLSS~SS
!
!	XX = strow,  YY = stcol, LL = len, SS~SS = string to return

	hot'spot(1) = "101020" + chr$(7)+"2"	! F2 (^G2)
	hot'spot(2) = "121040" + chr$(7)+"3"	! F3 (^G3)
	hot'spot(3) = "141045" + chr$(7)+"4"	! F4 (^G4)
	hot'spot(4) = "161032" + chr$(7)+"5"	! F5 (^G5)
	hot'spot(5) = "181022" + chr$(7)+"6"	! F6 (^G6)
 	hot'spot(6) = "201019" + "7" + chr$(13) ! "7" + CR
 	hot'spot(7) = "086506" + "?"		! help key
 	hot'spot(8) = "000120" + chr$(7)+"8"	! top status
 	hot'spot(9) = "250123" + chr$(7)+"9"	! bottom status


	CALL 	set'hot'spot		! mouse.bsi
 	XCALL MESAG,"Hot spots have been enabled",2
	? tab(4,1);tab(-1,9);"Hot spots are now ENABLED"
	? tab(5,1);tab(-1,9);		
	? tab(6,1);tab(-1,9);

	? TAB(-1,128);"Top status hot spot";tab(-1,129);	
	? TAB(-1,130);"Bottom status hot spot";tab(-1,129);	


	RETURN

DISABLE'HOT'SPOTS:
	CALL	clear'hot'spot		! mouse.bsi
	XCALL MESAG,"Hot spots are now disabled",2	
	? tab(4,1);tab(-1,9);"Hot spots are not DISABLED"
	? tab(-1,128);space(20);tab(-1,129);
	? tab(-1,130);space(23);tab(-1,129);
	RETURN

HELP:
	XCALL MESAG,"This is not much of a help message",2
	RETURN

TSTATUS:
	XCALL MESAG,"Top Status Line",2
	RETURN
BSTATUS:
	XCALL MESAG,"Bottom Status Line",2
	RETURN


EXIT:
	call	clear'hot'spot
        xcall ECHO              ! [4]
	END

++include ashinc:mouse.bsi
