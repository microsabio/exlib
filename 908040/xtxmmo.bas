program XTXMMO,1.0(103)	! Joe's XTEXT editor of INMEMO files
!---------------------------------------------------------------------------------
! Program sent to me from Joe Leibel, Nov 19 2006, which illustrates using
! XTEXT as an editor and INMEMO as a filing system
!---------------------------------------------------------------------------------
!{Edit History}
! 10-12-2007 --- 103 --- Jack McGregor
! Fix option to create new; expand sizes, use MMO'UWE to write to memo,
! better tracing, etc.
!
! 19-12-2006 --- 102 --- Jack McGregor
! Fix two problems: shortening text was not fully removing leftovers;
! wraps marked by CR in INMEMO need to be expanded to CRLF in XTEXT.
!
! 19-11-2006 --- 101 --- Jack McGregor
! Tidy up, add ++includes, comments
!
!---------------------------------------------------------------------------------

++include ashinc:ashell.def
++include ashinc:xtext.def
define MAX_MMO_LEN = 8000       ! [103] max size of a memo

map1 INMEMO
        map2 MMO'OPCODE         ,B,4
        map2 MMO'CHANNEL        ,B,2
        map2 MMO'START'ROW      ,B,2
        map2 MMO'START'COL      ,B,2
        map2 MMO'END'ROW        ,B,2
        map2 MMO'END'COL        ,B,2
        map2 MMO'LINK           ,B,2
        map2 MMO'LINK4          ,B,4
        map2 MMO'XPOS           ,X,6    ! (specify MMO'XPOS in xcall)
        map2 MMO'POS,B,4,@MMO'XPOS      ! (use MMO'POS in tests & assignments)


        map2 MMO'SPEC                   ! (leave 0 to disable scrolling)
                map3 MMO'WIDTH  ,B,2    ! scrolling width
                map3 MMO'ROWS   ,B,2    ! scrolling rows 23-Nov-94 [103]


map2 MMO'CLR                             ! HELP memo colors (optional)
        map3 BFCLR,B,1,-1               ! border fg
        map3 BBCLR,B,1,-1               ! border bg
        map3 TFCLR,B,1,-1               ! text fg
        map3 TBCLR,B,1,-1               ! text bg
        map3 AFCLR,B,1,-1               ! arrows fg
        map3 ABCLR,B,1,-1               ! arrows bg
        map3 PFCLR,B,1,-1               ! prompt fg
        map3 PBCLR,B,1,-1               ! prompt bg
        map3 WFCLR,B,1,-1               ! warnings & messages fg
        map3 WBCLR,B,1,-1               ! "     "             bg
        map3 SFCLR,B,1,-1               ! orig. status line fg
        map3 SBCLR,B,1,-1               ! orig. status line bg
        map3 RFCLR,B,1,-1               ! ruler/reserved fg
        map3 RBCLR,B,1,-1               ! ruler/reserved bg

map2 MMO'CTL
        map3 EXTCOD,B,2                 ! exit code
        map3 EXTERR,B,2                 ! exit error codes
        map3 EXTMAP,B,4                 ! exit code enable bitmap
        map3 EXTROW,B,2                 ! cursor row
        map3 EXTCOL,B,2                 ! cursor col
        map3 EXTBYT,B,4                 ! # bytes in phys. memo
        map3 EXTHIT,B,2                 ! # rows
        map3 EXTWID,B,2                 ! # cols
        map3 EXTPRW,B,2                 ! # protected rows
        map3 EXTSMI,S,1                 ! start menu item char
        map3 EXTEMI,S,1                 ! end menu item char
        map3 EXTMRW,B,2                 ! error message row (column 1)
        map3 EXTTOP,B,2                 ! top window offset
        map3 EXTLFT,B,2                 ! left window offset
        map3 EXTPCL,B,2                 ! # protected columns
        map3 EXTTIM,B,2                 ! Inter Char Time-out
        map3 EXTLIN,B,2                 ! Inmemo Line Max
        map3 EXTMGN,B,2                 ! Inmemo Line Margin
        map3 EXTOTH,B,2                 ! Other Menu Exit Keys
        map3 EXTJNK,X,14                ! space for expansion


map1 MMO'SEARCH'PARAMS$                 ! default wildcard symbols
        map2 S'1WILD$,S,1,"?"           ! Matches and one character
        map2 S'AST$,S,1,"*"             ! * wildcard
        map2 S'PLIMIT$,S,1,"|"          ! Pattern delimiter
        map2 S'AND$,S,1,"&"             ! logical AND
        map2 S'OR$,S,1,"!"              ! logical OR
        map2 S'ALP$,S,1,"@"             ! 1 alpha character
        map2 S'NUM$,S,1,"#"             ! 1 numeric character
        map2 S'RESERVED$,S,1," "        ! Reserved
        map2 S'RTERM$,S,1," "           ! Record Terminater
        map2 S'FOLD$,S,1,"^"            ! searchfold


map1 MMO'OPCODES
        map2 MMO'DSP,B,2,0              ! Display only
        map2 MMO'EDT,B,2,1              ! Edit
        map2 MMO'BDR,B,2,2              ! Use a window border
        map2 MMO'LID,B,2,4              ! Smart line insert & delete
        map2 MMO'DEL,B,2,8              ! Delete memo
        map2 MMO'LIN,B,2,16             ! Return 1 logical line
        map2 MMO'OPN,B,2,32             ! Open file
        map2 MMO'CLS,B,2,64             ! Close file
        map2 MMO'NBR,B,2,128            ! No border redisplay
        map2 MMO'NMR,B,2,256            ! No memo or border redisplay
        map2 MMO'SCH,B,2,512            ! Search
        map2 MMO'SIL,B,2,1024           ! Silent mode (no terminal output)
        map2 MMO'DPG,B,2,2048           ! Display only, with paging
        map2 MMO'MNU,B,2,4096           ! Menu bar mode (vertical menus)
        map2 MMO'TBL,B,2,8192           ! Table lookup (only with MMO'SCH)
        map2 MMO'FST,B,2,16384          ! Fast Mode (only with MMO'MNU)
        map2 MMO'EWU,B,2,32768          ! Update w/o edit
        map2 MMO'UWE,B,3,65536          ! Edit w/o update
        map2 MMO'SVA,B,3,131072         ! (2^17) Save memo screen area
        map2 MMO'RSA,B,3,262144         ! (2^18) Restore memo screen area
        map2 MMO'CXY,B,3,524288         ! (2^19) Specify start ROW,COL
        map2 MMO'NOA,B,3,1048576        ! (2^20) Omit arrows
        map2 MMO'AAH,B,3,2097152        ! (2^21) Auto adjust window height
        map2 MMO'IPG,B,3,4194304   ![319]!(2^22) Intelligent version of DPG
        map2 MMO'DBM,B,3,8388608   ![320]! (2^23) Start disp. at bottom
        map2 MMO'ISL,B,4,16777216  ![321]! (2^24) Insert into Sorted List
        map2 MMO'APS,B,4,33554432  ![323]! (2^25) Alternate Prompt Style
        map2 MMO'NAF,B,4,67108864  ![332]! (2^26) No auto format on pre-load
        map2 MMO'OTX,B,4,134217728 ![342]! (2^27) Output to TEXT param.
        map2 MMO'FFM,B,4,268435456 ![373]! (2^28) Free form menu mode
        map2 MMO'OPT,B,4,536870912 ![395]! (2^29) OPTimized disk output
        map2 MMO'RET,B,4,1073741824     ! RETURN on bottom line exits memo
        map2 MMO'HDR,B,4,2147483648     ! Don't convert chr(26) on MMO'LIN


map1 MMO'ERRORS
        map2 MMO'LKE,B,4,-1             ! Link error
        map2 MMO'FUL,B,4,1              ! file full
        map2 MMO'VTS,B,4,2              ! memo full (VSPEC Too Small)


map1 EXT'CODES                          ! (returned in EXTCOD)
        map2 MMX'VTS,B,2,2              ! (no update) VSPEC Too Small
        map2 MMX'QUI,B,2,1              ! (no update) (control-c)
        map2 MMX'OK,B,2,0               ! normal exit (updated)
                                        ! negative numbers are command keys
        map2 MMX'SUA,B,2,-40            ! shifted up arrow
        map2 MMX'SDA,B,2,-41            ! shifted down arrow
        map2 MMX'SLA,B,2,-42            ! shifted left arrow
        map2 MMX'SRA,B,2,-43            ! shifted right arrow


map1 EXT'ERRORS                         ! (returned in EXTERR)
        map2 MME'ERR,B,2,2              ! abnormal exit - see POS
        map2 MME'SAF,B,2,1              ! failed to save or restore area
        map2 MME'OK,B,2,0               ! ok
        map2 MME'NOR,B,2,0              ! ok (used in D01 manual by accident)


map1 XFR'OPCODES
        map2 XFR'CPY,B,1,0              ! XFRMMO copy mode
        map2 XFR'MOV,B,1,1              ! XFRMMO move mode

map1 XFR'ERRORS
        map2 XFR'SUC,B,1,0              ! success
        map2 XFR'LKE,B,1,1              ! Link error
        map2 XFR'FUL,B,1,2              ! file full
        map2 XFR'SYS,B,1,3              ! system error


map1 MMO'CTL'CODES                      ! various useful ctrl defs
        map2 MMO'INVHDR,S,2,chr(19)+chr(8)  ! invisible header command
        map2 MMO'STMNU,S,1,"["          ! start menu item
        map2 MMO'ENDMNU,S,1,"]"         ! end menu item

++include ashinc:xtext.map              ! [103]
!map1 TXTCTL
!    map2 TXC'CTLNO      ,B,1        !   0 [U] control # 0-3
!    map2 TXC'KBDSTR$    ,S,11       !   1 kbd string
!    map2 TXC'PARENTID   ,B,2        !  12 parent ID (I,2)
!    map2 TXC'WRAPWIDTH  ,B,2        !  14 if set, wrap at this char width
!    map2 TXC'DOCFMT'SRC ,B,2        !  16 input doc format (TXFF_xxx)
!    map2 TXC'DOCFMT'DST ,B,2        !  18 output doc format (TXFF_xxx)
!    map2 TXC'OUTBYTES   ,B,4        !  20 [U] # bytes output
!    map2 TXC'OUTLINES   ,B,4        !  24 [U] # lines output
!    map2 TXC'SAVEOFFSET ,B,4        !  28 [U] offset to next save
!    map2 TXC'OVERFLOW   ,B,1        !  32 [U] output overflowed buffer (truncated)
!    map2 TXC'MODIFIED   ,B,1        !  33 [U] text modified
!    map2 TXC'FONTFACE$  ,S,32       !  34
!    map2 TXC'FONTSIZE   ,B,2        !  68 >0 for points, <0 for twips (I,2)
!    map2 TXC'FONTATTR   ,B,4        !  70
!    map2 TXC'XROW       ,B,4        !  74 [U] cursor row on exit
!    map2 TXC'XCOL       ,B,2        !  78 [U] cursor col on exit
!    map2 TXC'XCUROFFSET ,B,4        !  80 [U] abs cursor offset (from start)
!    map2 TXC'SRCHKEY$   ,S,30       !  84
!    map2 TXC'SFLAGS     ,B,2        ! 114     Search flags (see TXSF_xxx)
!    map2 TXC'CTLID      ,B,2        ! 116 [U] A-Shell control ID #
!    map2 TXC'DELCTLID   ,B,4        ! 118
!    map2 TXC'FLAGMASK   ,B,2        ! 122 1's indicate which flags to update
!    map2 TXC'FLAGS1     ,B,4        ! 124 0x0001
!    map2 TXC'FLAGS2     ,B,4        ! 128 0x0002
!    map2 TXC'FLAGS3     ,B,4        ! 132 0x0004
!    map2 TXC'FLAGS4     ,B,4        ! 136 0x0008
!    map2 TXC'FLAGS5     ,B,4        ! 140 0x0010
!    map2 TXC'FLAGS6     ,B,4        ! 144 0x0020
!    map2 TXC'FLAGS7     ,B,4        ! 140 unused
!    map2 TXC'FLAGS8     ,B,4        ! 144 unused
!    map2 TXC'FMAPCTL    ,B,4        ! 148 1's enable FKEY to be sent to ctl
!    map2 TXC'FMAPAPP    ,B,4        ! 152 1's enable FKEY to be sent to app
!    map2 TXC'TOOLBARMASK,B,4        ! 156 1's rmv obj from toolbar (TXTB_xxx)
!    map2 TXC'FCOLOR     ,B,4        ! 160 Override text color (&h80bbggrr)
!    map2 TXC'BCOLOR     ,B,4        ! 164 Override bg color (&h80bbggrr)
!    map2 TXC'FPROTCLR   ,B,4        ! 168 Override prot fg color (&h80bbggrr)
!    map2 TXC'BPROTCLR   ,B,4        ! 172 Override prot bg color (&h80bbggrr)
!    map2 TXC'PROTROWS   ,B,4        ! 176 rows to prot (-1=all,-2=all but last, etc.)
!    map2 TXC'UNUSED$    ,X,70       ! 180 (256 total)

![101] use TXF_ symbols from ashinc:ashell.def
!        TXF'FKEY        = 1             ! Function keys F1-F32 exit (exitcode -1 thru -32); see txc'fmapapp
!        TXF'LEFT        = 2             ! Left arrow exits (exitcode -40)
!        TXF'RIGHT       = 2^2           ! Right arrow exits (exitcode -41)
!        TXF'UP          = 2^3           ! Up arrow or shift-TAB exits (exitcode -42)
!        TXF'TAB         = 2^5           ! TAB exits (exitcode -44)
!        TXF'HOME        = 2^6           ! HOME exits (exitcode -45)
!        TXF'END         = 2^7           ! END exits (exitcode -46)
!        TXF'ENTESC      = 2^8           ! ENTER exits (exitcode 0)
!        TXF'MODELESS    = 2^9           ! Modeless (leave text control on screen when inactive)
!        TXF'EOT         = 2^10          ! Start editing with cursor at end of text
!        TXF'DEFERSAVE   = 2^11          ! Defer saving text
!        TXF'HSCROLL     = 2^13          ! Horizontal scroll bar
!        TXF'VSCROLL     = 2^14          ! Vertical scroll bar
!        TXF'RULER       = 2^15          ! Display ruler
!        TXF'STATUS      = 2^16          ! Display status bar
!        TXF'TOOLBAR     = 2^17          ! Display toolbar
!        TXF'READONLY    = 2^18          ! Read only
!        TXF'POSCUR      = 2^19          ! Set initial cursor pos to txc'xcuroffset
!        TXF'DISABLE     = 2^21          ! Disable text control when inactive
!        TXF'DEL         = 2^22          ! DEL key exits (exitcode -47)
!        TXF'MARGIN      = 2^23          ! Show margin space around page border
!        TXF'NOCLICKOUT = 2^24`         ! No click out of field
!        TXF'WRAP        = 2^25          ! Word wrap
!        TXF'PRINTVIEW   = 2^26          ! Wrap as it would print with current printer setup
!        TXF'PAGEMODE    = 2^27          ! Single page mode
!        TXF'FITVIEW     = 2^28          ! Wrap based on control display width
!        TXF'POPUP       = 2^29          ! Let edit control float as a popup window
!        TXF'DLGNOCREEP  = 2^30          ! Pop coords relative to main window, not dlg
!

map1 MISC
    ![101]MAP2 MMO'DATA$,S,10000
    map2 MMO'FILE$,S,32                 ! [101] name of memo file
    map2 MMO'RECORD,F,6                 ! [101] recno var for memo file
    map2 XTXT'SRC$,S,MAX_MMO_LEN               ! [101] (must be large enough to hold memo)
    map2 XTXT'DST$,S,MAX_MMO_LEN               ! [101]     "   "   "
    map2 S1$,S,2000                     ! [101] must be large enough for 1 line [103]
    map2 SROW,B,2                       ! [101] memo window coordinates
    map2 SCOL,B,2                       ! [101]
    map2 EROW,B,2                       ! [101]
    map2 ECOL,B,2                       ! [101]
    map2 T'OPCODE,B,2                   ! [101] XTEXT opcode
    map2 T'FLAGS,B,4                    ! [101] XTEXT flags
    map2 T'EXITCODE,F,6                 ! [101] XTEXT exitcode
    map2 T'AUXTXT,S,10                  ! [101] XTEXT auxtxt (not actually used)
    map2 C$,S,1                         ! [101] misc input
    map2 XX,F
    map2 I,F                            ! [103]
    map2 J,F                            ! [103]
    map2 LAST'BREAK,F                   ! [103]
    map2 TXTLEN,F                       ! [103]

map1 CTL'REC2                           ! [101] Memo Control Record
	map2 MARKER2,S,6
	map2 NXT'FRE2,B,2
	map2 NXT'DEL2,B,2
	map2 FILL2,S,54
map1 CTL'REC4,@CTL'REC2
	map2 MARKER4,S,6
	map2 NXT'FRE4,B,4
	map2 NXT'DEL4,B,4
	map2 FLAG4,S,1
	map2 FILL4,S,49



! ******************** Open MMO file and read it in ************** !



strsiz 100
FILEBASE 1              ! [101] Not important except when we try to manually read
                        ! [101] memo control rec to determine the link size

OPENMEMO:
        ! [101]  Prompt for info
        ?tab(-1,0);"XTXMMO - Sample using XTEXT to edit INMEMO file"
        ? "Note: Sample memo TSTMMO.MMO has memos at link 2 and 27"
        ? "      You can also use INMEMO:DMPMMO to get list of memos in any file"
        ? "      But: this program assumes memo files with 4 byte links!"
        input line "Enter memo file: [TSTMMO.MMO] ",MMO'FILE$
        if MMO'FILE$ = "" then MMO'FILE$ = "TSTMMO.MMO"
        if lookup(MMO'FILE$) = 0 then
            ? "Memo file not found! Try Another"
            stop
            goto OPENMEMO
        endif
        ! ******* ! Put your own file name here ! ******** !
        ! MMO'FILE$   = "INMEMO.MMO"              ! INMEMO data file name
        MMO'CHANNEL = 99                        ! MMO file channel
        open # MMO'CHANNEL,MMO'FILE$, RANDOM'FORCED, 64, MMO'RECORD

        ![101] verify the the file uses 4 byte links
    	MMO'RECORD = 1
    	read #MMO'CHANNEL, CTL'REC2
    	if FLAG4#"4" then
    	   ? "Sorry - this program only supports memo files with 4 byte links"
    	   close #MMO'CHANNEL
    	   stop
    	   goto OPENMEMO
    	endif

        trace.print "--------------------------------------------------"

START:
        ?tab(2,1);tab(-1,10);
        if MMO'LINK4 <> 0 then ?"Last Pointer was ";MMO'LINK4
        ?tab(3,5);
        input "INMEMO Pointer 0=New :",MMO'LINK4

        ![101]MMO'DATA$   = ""                        ! Memo Data
        XTXT'SRC$   = ""                        ! Xtxt source data
        XTXT'DST$   = ""                        ! Xtxt destination data
        MMO'ROWS    = 0                         ! # of Rows
        SROW = 5
        SCOL = 5
        EROW = 10
        ECOL = 40
        if MMO'LINK4 = 0 goto NOTE'EDIT         ! [103]


NOTE'XTXT: ! *********** Use xtext to edit data *********** !
        MMO'OPCODE   = MMO'LIN                  ! Read One Line

NOTE'NEXT: ! ***** Read the text from Memo File ***** !

        xcall INMEMO,MMO'OPCODE,S1$,MMO'CHANNEL,SROW,SCOL,EROW,ECOL,MMO'LINK4, &
            MMO'XPOS,MMO'SPEC,MMO'CLR,MMO'CTL
        ![103] (what was this for?) if S1$[1;1] = "[" then goto NOTE'NEXT
        if MMO'POS = MMO'LKE then
            ? "Illegal memo link"
            End
        endif

        if MMO'POS = 0 and S1$ = "" goto NOTE'EDIT

        MMO'ROWS = MMO'ROWS + 1
        if MMO'ROWS > 1 then                                    ! [103] was <> 1 !!!!!
!           XTXT'SRC$ = XTXT'SRC$ + CHR$(13) + CHR$(10)         ! [102] add CHR$(10)
            XTXT'SRC$ = XTXT'SRC$ + CHR$(13)        ! [102] add CHR$(10)

            trace.print "Adding LF at location "+len(XTXT'SRC$) ! [103]
        endif
        XTXT'SRC$ = XTXT'SRC$ + S1$

        if MMO'POS # 0 goto NOTE'NEXT

NOTE'EDIT: ! ****** Set up Xtext to edit text ****** !

        T'OPCODE = 1                            ! Create new control
        T'FLAGS  = 0
        T'FLAGS  = T'FLAGS  + TXF_WRAP          ! Word Wrap
        T'FLAGS  = T'FLAGS  + TXF_FITVIEW       ! Word Wrap based on width
        T'FLAGS  = T'FLAGS  + TXF_TOOLBAR       ! Display Toolbar
        TXC'TOOLBARMASK = 65536                 ! No 2nd line of toolbar
        TXC'DOCFMT'SRC = 0                      ! Source is text
        TXC'DOCFMT'DST = 0                      ! Destination is text
        TXC'WRAPWIDTH = 0                       ! [103] (use ruler width)
        TXC'MAXBYTES = 0                        ! [103] (unlimited; will be truncated to dst length)


NOTE'XTEXT: ! ***** Use Xtext to edit the memo ****** !

        ! [103] output the text to a file for study...
        open #299, "XTXMMO.L1", output
        print #299, XTXT'SRC$
        close #299
        trace.print "Original text (as input to XTEXT) saved to XTXMMO.L1..."

        xcall XTEXT,T'OPCODE,T'FLAGS,XTXT'SRC$,XTXT'DST$,SROW,SCOL,EROW,ECOL,T'EXITCODE,TXTCTL,T'AUXTXT

        trace.print "exitcode = "+T'EXITCODE            ! [103] check for non-update status
        if T'EXITCODE > 0 then
            if T'EXITCODE = 1 or T'EXITCODE = 10 then   ! ESC or ^C
                print "Text not updated, nothing to save"
            else
                print "Error in XTEXT"
            endif
            goto NOTE'CONTINUE
        endif

        trace.print "text=["+XTXT'DST$+"]"
        ![103] XX = instr(1,XTXT'DST$,chr(13))
        ![103] trace.print "CR position: "+XX
        ![103] XX = instr(1,XTXT'DST$,chr(10))
        ![103] trace.print "LF position: "+XX


        ! [103] output the text to a file for study...
        open #299, "XTXMMO.L2", output
        print #299, XTXT'DST$
        close #299
        trace.print "Original text (as output by XTEXT) saved to XTXMMO.L2..."

        trace.print "TXC'OUTBYTES = "+TXC'OUTBYTES+ " but len(XTXT'DST$) = "+str(len(XTXT'DST$))
        J= TXC'OUTBYTES min 10
        trace.print "(last "+J+" chars = """+XTXT'DST$[TXC'OUTBYTES-(J-1);J]+""")"

        ! Note: XTEXT will not count trailing whitespace, so the discrepancy may be ok,
        ! but if we lose the last char or two, examine the above trace more closely

        call CONVERT'CRLF'TO'CR           ![103]
        call Get'Vspec(XTXT'DST$,MMO'ROWS,MMO'WIDTH)   ![103] compute max width and rows for VSPEC

NOTE'STORE: ! ***** Use Inmemo to update Memo without edit or display ******

        ![102] first delete the old memo
        ![103] (why delete old one?)
        ![103]    xcall INMEMO,MMO'DEL,"",MMO'CHANNEL,SROW,SCOL,EROW,ECOL,MMO'LINK4,MMO'XPOS,MMO'SPEC,MMO'CLR,MMO'CTL
        ![103] MMO'LINK4 = 0
        ! Now add replacement (using link 0 if new, or old link if old)

        ![103] XTXT'DST$ = "~" + XTXT'DST$ + CHR$(27)  ! Escape
        ![103] MMO'OPCODE = MMO'EDT !+ MMO'SIL          ! Edit & Silent just to update

        trace.print "Saving text to INMEMO (MMO'UWE), MMO'ROWS="+MMO'ROWS+", MMO'WIDTH="+MMO'WIDTH
        MMO'OPCODE = MMO'UWE + MMO'SIL                  ![103]
        xcall INMEMO,MMO'OPCODE,XTXT'DST$,MMO'CHANNEL,SROW,SCOL,EROW,ECOL, &
            MMO'LINK4,MMO'XPOS,MMO'SPEC,MMO'CLR,MMO'CTL


NOTE'SAVE: ! ****** Update the Pointer to the data File ****** !
        ?tab(15,5);
        ?"Inmemo Pointer is ";MMO'LINK4;" , POS=";MMO'POS
        trace.print "Memo saved at link "+MMO'LINK4+" ,POS = "+MMO'POS     ![103]

NOTE'CONTINUE:
        INPUT LINE "Continue? : ",C$
        if UCS(C$) = "N" then END
        goto START

! [103] this routine could be used to manually convert the CRLF terminators returned by XTEXT
! [103] into the CR terminators used by INMEMO, but with MMO'UWE it appears to be unnecessary

CONVERT'CRLF'TO'CR:

!    if ucs(CVTCRLF$)="Y" then
!        trace.print "Scanning "+TXC'OUTBYTES+" bytes for CRLF conversion..."
!
!        I = 0
!        LAST'BREAK = 0
!        MMO'WIDTH = 1
!        MMO'ROWS = 1
!        TXTLEN = len(XTXT'DST$)
!        for J = 1 to TXC'OUTBYTES
!
!            if XTXT'DST$[J;1] # chr(10) then
!                I = I + 1
!                XTXT'DST$[I;1] = XTXT'DST$[J;1]
!            else
!                trace.print "Removing LF at location "+J
                ! see how long the line was and increment
                ! MMO'WIDTH and MMO'ROWS
!                if (J - LAST'BREAK) > MMO'WIDTH then
!                   MMO'WIDTH = J - LAST'BREAK
!                endif
!                LAST'BREAK = J
!                MMO'ROWS = MMO'ROWS + 1
!            endif
!        next J
!
        ! (check last line for length (may not have been explicitly terminated)
!        if (J - LAST'BREAK) > MMO'WIDTH then
!           MMO'WIDTH = J - LAST'BREAK
!        endif
!        MMO'WIDTH = MMO'WIDTH + 5       ! provide a little breathing room
!
        ! now truncate the source line (shortened by removing the LFs)
!        XTXT'DST$ = XTXT'DST$[1,I]
!        trace.print "String shortened to "+I+" bytes"
!        trace.print "Max paragraph length = "+MMO'WIDTH
!        trace.print "# of logical lines/paragraphs = "+MMO'ROWS
        ! -[103]
    return

![103] Scan the XTXT'DST$ string to determine the longest line and
![103] number of rows for VSPEC

Procedure Get'Vspec(mmotxt$ as S8000, vrows as b2, vwidth as b2)

    map1 x,f
    map1 y,f

    y = 1
    vrows = 1
    vwidth = 0
    do
        x = instr(y,mmotxt$,chr(13))
        if x > 0 then
            if (x-y) > vwidth then
                vwidth = x - y
            endif
            y = x + 1
            vrows = vrows + 1
        else
            exit
        endif
    loop

    vwidth = vwidth + 2 ! add a little breathing room
    xputarg 2,vrows
    xputarg 3,vwidth
    trace.print "Setting vrows = "+vrows+", vwidth = "+vwidth
End Procedure
