:<(ISAM-A) ISAM-A (formerly ISAMPLUS) utilities
    ISMCHK - Verify ISAM-A files, report status (w/ email,fax options)
    CVTISM - Convert AMOS ISAMPLUS file directory to A-Shell ISAM-A format
    ISRCVR - Recover (replay) transactions from log (ISRCVR.LIT)
    ISPDEL - Sample program illustrating advanced delete'record tecnique
    ISPDEL2- Variation illustrating use of READL to lock prior to del or upd
    ISPTSX - Sample program torture test 
    ISPUSH - Sample program demonstrating ISAM'PUSH, ISAM'POP
    ISPVAR - Sample program demonstrating compressed/variable recs
    ISPTRX - Sample program demonstrating transactions
    PURIDX - Sample program demonstrating pure idx (no DAT file)
>
