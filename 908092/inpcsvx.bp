program inpcsvx, 1.0(101)  ! test/samples for extended INPUT CSV variations
!------------------------------------------------------------------------
!EDIT HISTORY
!Version 1.0:-
! [100] 16-Nov-23 / jdm / created
! [101] 16-Nov-23 / jdm / experiment with iteration
!------------------------------------------------------------------------
!REQUIREMENTS
!   INPUT CSV into Gridmap requires 7.0
!   INPUT CSV into Array() requires 6.5.1697
!   
!NOTES
!   Illustrates:
!       INPUT CSV #CH, ARRAY()
!       INPUT CSV #CH, $GRIDI()      ! row,col,value
!       INPUT CSV #CH, $GRIDS()      ! row,colname,value
!------------------------------------------------------------------------

map1 GLOBALS 
    map2 CSVSPEC$, s, 100
    map2 OP,B,1
    map2 I,I,2
    map2 ROW,I,4
    map2 ROWS,I,4
    map2 LASTROW,I,4
    map2 COL,I,2
    map2 COLS,I,2
    
dimx ARY(0), s, 50, auto_extend             ! auto_extend mandatory for logic below
dimx $GRIDI, gridmap(int;int;varstr)        ! row, col, value
dimx $GRIDS, gridmap(int;varstr;varstr)     ! row, colname, value


    ? tab(-1,0);.PGMNAME;" ";.PGMVERSION;" - Test INPUT CSV variations"
    ?
    input "CSV file to input (blank to create one): ",CSVSPEC$
    if CSVSPEC$ = "" then
        CSVSPEC$ = "INPCSVX.CSV"
        call CREATE'INPCSV
    endif
        
    input "1) ARRAY(), 2) GRIDMAP : ",OP     
    
    on OP call INPUT'CSV'ARRAY, INPUT'CSV'GRID
    end
    
INPUT'CSV'ARRAY:
    ? "INPUT CSV #1, ARY() ..."
    open #1, CSVSPEC$, input
    do while eof(1) = 0
        input csv #1, ARY()
        if .extent(ARY()) > 0 then
            for I = 1 to .extent(ARY())
                ? "ARY(";str(I);") = ";ARY(I)
            next I
            input "Enter for next row...",OP
        endif
    loop
    close #1
    return

INPUT'CSV'GRID:
    input "Grid type: 1) int;int;varstr  2) int;varstr;varstr : ",OP
    on OP call GRIDI, GRIDS
    return

GRIDI:
    ? "INPUT CSV #1, $GRIDI() ..."
    open #1, CSVSPEC$, input
    input #1, $GRIDI()
    ROWS = .maxrow($GRIDI())
    ? .extent($GRIDI());"elements input in"; ROWS;"rows"
    ? "  (Note that first row contains the column names, if applicable)"
    for ROW = 1 to .maxrow($GRIDI())
        ?
        ? "Enter to display ROW";ROW;" : ";
        input "",OP
        for COL = 1 to .maxcol($GRIDI(),ROW)
            ? "$GRIDI(";str(ROW);",";str(COL);") = "; $GRIDI(ROW,COL)
        next COL
    next ROW
    ? "<No more rows>"
    
    ! [101] new veature in 7.1.1752.0
    ? 
    input "Enter Row # to test iteration with start/end key: ",ROW
    ? "Iterating row ";ROW;"..."
    foreach $$i in $GRIDI(ROW,ROW)
        ? "Row="; .key($$i,1);", Col="; .key($$i,2);" -> ";$$i
    next $$i
    
    return
    
GRIDS:
    ? "INPUT CSV #1, $GRIDS() ..."
    open #1, CSVSPEC$, input
    input #1, $GRIDS()
    ROWS = .maxrow($GRIDS())
    ? .extent($GRIDS());"elements input in"; ROWS;"rows"
    ! note: we can't easily iterate across individual rows; instead,
    ! we'll itereate through entire grid, stopping when the row # changes
    LASTROW = -99
    foreach $$i in $GRIDS()
        ROW = .key($$i,1)      ! extract row from iterator key
        if ROW > LASTROW then
            LASTROW = ROW
            ?
            ? "Enter to display ROW";ROW;" : ";
            input "",OP
        endif
        ? "Row=";ROW;", Colname=";.key($$i,2);", Value=";$$i
    next $$i
    ? "<No more rows>"
    
    ! [101] new veature in 7.1.1752.0
    ? 
    input "Enter ROW # to test iteration with start/end key: ",ROW
    ? "Iterating row ";ROW;"..."
    foreach $$i in $GRIDS(ROW,ROW)
        ? "Row="; .key($$i,1);", Colname="; .key($$i,2);" -> ";$$i
    next $$i
    
    return
    
   
CREATE'INPCSV:
    ? "Creating ";CSVSPEC$;" ..."
    open #1, CSVSPEC$, output
    ? #1, "animal, mineral, Vegetable, fruit, language"
    ? #1, "dog,gold,rutabaga,apple,english"
    ? #1, "cat,silver,kale,orange,spanish"
    ? #1, "squirrel,aluminum,potato,kiwi,french"
    ? #1, "rattlesnake,,""radish,daikon"",persimmon,german"    ! (test empty and compound fields
    close #1
    return