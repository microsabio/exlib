:<(APEX) Ashell Preview & EXport samples

    XLREP1.PRT - Sample report #1 (simple export example)
    XLREP2.PRT - Sample report #2 (6 pages)
    PVREP3.PRT - Sample report #3 (GDI)

To test preview/export:  

    PRINT {spooler=}filename/PREVIEW 

Note: this requires A-Shell or ATE 5.1, and your Settings..Preview 
Preferences must NOT be set to "Never".

Also note: XL* indicates a report suitable for exporting; PV* indicates
a report mainly suitable for previewing.
>
 
