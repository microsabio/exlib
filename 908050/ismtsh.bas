program ISMTSH,2.0(014)     ! version with filehooks
!------------------------------------------------------------------------
!Simple ISAM 1.0/1.1 test program.  
!
!It starts by generating a file (ISMTST.IDA/X, ISMTS2.IDX) of a size you 
!specify or, if the files already exist, it will give you the option to
!reuse them.  Then it simply repeats an endless loop in which it generates
!a random key and looks it up. If it exists, it deletes it (and the record
!associated with it.)  If it does not exist, it adds a record.  You can
!terminate the loop at any time by pressing any key.
!
!The test is good for both performance and reliability (although you 
!really need to run it on multiple machines to really test reliability.)
!
!There are options to reduce the disk-intensiveness of the test by 
!performing CPU calculations between each operation, and to use XLOCK.SBR
!for locking.   (If LOKSER is on, you don't really need XLOCK, although
!there is a slight possibility that two processes could both look up the
!same key simultaneously, and both try to add it.  The use of XLOCK
!prevents that possibility entirely, but otherwise is overkill.)
!
!Program displays an "a" (add) or "d" (delete) on every 4th iteration
!to give you a visual sense of what it is doing.  (It also displays
!a "L" if waiting on XLOCK or a "W" if "wasting" time with CPU 
!operations between iterations.)  It also gives a running count, and for
!each 100 operations, it reports the number of iterations per second
!for the last 100, as well as the number of iterations per second 
!since the start of the program.  (The first figure may not be very
!reliable when the time interval to perform 100 iterations is less
!than 2 seconds, given that the TIME function only returns integer
!seconds elapsed.)
!
!Also see ISMTSP.BAS, which performs a related test on the same 
!sample file, focusing on sequential reads.  (You can run them both
!simultaneously to get a sense of how fast sequential reads are 
!serviced on a very busy file.)
!
!If you want to run the same test under AMOS, the non-standard
!necessary subroutines (tinkey.sbr, ccon.sbr, ccoff.sbr) are provided
!with A-Shell or on our web site.  You also need noecho.sbr, echo.sbr,
!sleep.sbr, and xlock.sbr.  Make sure all are loaded into memory!
!-----------------------------------------------------------------------
! Edit History
! 1. 13-Oct-94 Add ISMTS2 secondary key /jdm
! 2. 10-Jan-95 Added CCOFF, more error info /jdm
! 3. 16-Feb-95 Added WAIT'FILE, WAIT'RECORD, option to delete file /jdm
! 4. 27-Feb-95 Check for error on initial ISAM #1 /jdm
! 5. 05-Oct-97 Add option to use XLOCK to make sure 2 processes don't	
!		decide to add the same key at same time.  Also, add
!		a bunch of CPU calculations to make it a bit more
!		realistic. /jdm
! 6. 11-May-98 Reduce output to decrease effect of scrolling speed /jdm
! 7. 10-Apr-99 Recompute rate each block of 25 /jdm
! 8. 31-Aug-01 Also show accumulated rate /jdm
! 9. 29-Mar-02 Base random keys on a range 4 times larger than initial
!		file size so we can test auto expansion /jdm
!10. 29-Aug-02 Add MMAP option /jdm
!
!Version 2.0 --
!11. 17-Aug-08 Revamp to perform more validity tests (rather than just
!               performance); Use isam'indexed to avoid compiler confusion; 
!               adjust mask to allow for >9999/sec; prompt for levels, 
!               block size; always check that both primary & secondary
!               match /jdm
!12. 22-Sep-08 Add read-only option /jdm
!13. 12-Feb-10 Add filehooks /jdm
!14. 09-Jun-10 Add HFF_DATA_CHG_ONLY; remove non-functional IDX hook 
!    
!-----------------------------------------------------------------------
	strsiz 80

++include ashinc:ashell.def
++include ashinc:hook.def

MAP1 RECORD     
    MAP2    RECORD'JUNK,S,3
    MAP2    RECORD'KEY,S,22
    MAP2    RECORD'B5,B,5
    MAP2    RECORD'B4,B,4
    MAP2    RECORD'F,F,6
    MAP2    RECORD'S,S,24

MAP1 TESTFILENAME,S,25,"ismtst"
MAP1 TESTSECONDARY,S,25,"ismts2"

MAP1 MISC
    MAP2 IDAALC,F
    MAP2 BIAS,F
    MAP2 COUNT,F
    MAP2 LAST'COUNT,F
    MAP2 ADD'COUNT,F            ! [11]
    MAP2 DEL'COUNT,F            ! [11]
    MAP2 MAXREC,F
    MAP2 INIREC,F
    MAP2 MAXSEC,F
    MAP2 op$,s,20
    MAP2 CMDFLG,F,6
    MAP2 A$,S,1
    MAP2 AFLAG,F
    MAP2 TESTRECNUM,F
    MAP2 SAVE'RECNO,F
    MAP2 T1,F
    MAP2 T2,F
    MAP2 T3,F
    MAP2 FILENO,F,6,1
    MAP2 FILEN2,F,6,2
    MAP2 LOKFLG,F,6
    MAP2 AFLAGS,B,2
    MAP2 NUM,F
    MAP2 W,F
    MAP2 KEY$,S,30
    MAP2 KEY2$,S,24
    MAP2 IDX'LEVELS,B,1         ! [11]
    MAP2 IDX'BLKSIZ,B,2         ! [11]
    MAP2 ID2'LEVELS,B,1         ! [11]
    MAP2 ID2'BLKSIZ,B,2         ! [11]
    MAP2 SKIP,F
    MAP2 LOGFILE$,S,32
    MAP2 LCH,B,2,88
    MAP2 RO$,S,1                ! [12] 

MAP1 WASTE
    MAP2 W1,F
    MAP2 W2,F
    MAP2 W3,F
    MAP2 W4,F
    MAP2 W5,F
    MAP2 W6,F
    MAP2 WBUF,S,2000
    MAP2 WFLAG,F

MAP1 LOCKSTUFF
    MAP2 MODE,B,2
    MAP2 LOCK1,B,2
    MAP2 LOCK2,B,2

MAP1 HOOKSTUFF                  ! [13]
    MAP2 HOOKEVENTS,B,4    
    MAP2 HOOKFLAGS,B,4
    MAP1 HOOKSTATUS,F


    RANDOMIZE

    ! [13] set up the filehooks
    XCALL MIAMEX, MX_FILEHOOK, HOOKOP_ADD, HOOKSTATUS, "ISMTST.IDA", 1, &
        "SBX:FHOOK1", &
        HFE_PRE_OPEN or HFE_POST_OPEN or HFE_POST_WRITE or HFE_POST_WRITEL &
        or HFE_POST_READ or HFE_POST_READL or HFE_POST_CLOSE &
        or HFE_POST_ALLOC or HFE_POST_KILL, &
        HFF_DATA or HFF_DATA_WAS or HFF_DATA_CHG_ONLY   ! [14]

![14] hook doesn't apply to IDX (is ignored; was put here as a test)
!    ! [13] IDX also
!    XCALL MIAMEX, MX_FILEHOOK, HOOKOP_ADD, HOOKSTATUS, "ISMTST.IDX", 2, &
!        "SBX:FHOOK1", &
!        HFE_PRE_OPEN or HFE_POST_OPEN or HFE_POST_WRITE or HFE_POST_WRITEL &
!        or HFE_POST_READ or HFE_POST_READL or HFE_POST_CLOSE &
!        or HFE_POST_ALLOC or HFE_POST_KILL, &
![14]        HFF_DATA or HFF_DATA_WAS

    XCALL ECHO
    XCALL CCON
    ? "Simple test of random ISAM 1.x adds & deletes (with 2nd key, filehooks)"

    ! if ISMTST.CMD exists, we must be returning from create operation
    lookup "ISMTST.CMD", CMDFLG
    if CMDFLG#0 then kill "ISMTST.CMD"

    lookup TESTFILENAME+".ida",INIREC
    if INIREC <> 0 then
        lookup TESTSECONDARY+".idx",MAXSEC
    else
        goto NOT'FOUND
    endif

    if INIREC = 0 or MAXSEC = 0 goto NOT'FOUND
    
    INIREC = abs(INIREC) * 8                      
    ? TESTFILENAME;" has";INIREC;"records.";      
    if CMDFLG = 1 goto BEGIN  ! if coming from cmd file, skip questions

    ? "  Do you want to re-initialize the file? "; 
    input "",A$				
    if ucs(A$)="Y" then
        goto CREATE
    else
        goto BEGIN
    endif

NOT'FOUND:
    if INIREC <> 0 then
        kill TESTFILENAME+".ida"
        kill TESTFILENAME+".idx"
    endif

    ? "File ";TESTFILENAME;" and/or ";TESTSECONDARY;" does not exist.  Create it? ";
    input "",A$
    if ucs(A$[1,1]) # "Y" end

CREATE:
    input "Enter initial # of records: ",INIREC
    input "Enter expected max # of records: ",MAXREC

LEVELS:                                         ! [11]   
    input "Enter main,secondary IDX levels [3,3]  (>3 req 1123+) ",IDX'LEVELS,ID2'LEVELS 
    if IDX'LEVELS = 0 then IDX'LEVELS = 3
    if IDX'LEVELS = 0 then ID2'LEVELS = 3
    if IDX'LEVELS < 3 or IDX'LEVELS > 9 &
    or ID2'LEVELS < 3 or ID2'LEVELS > 9 then
        ? "Levels must be between 3 and 9"
        goto LEVELS
    endif
BLKSIZ:                                         ! [11]   
    input "Enter main, secondary IDX block size [512,512] (>512 req 1123+) ",IDX'BLKSIZ
    if IDX'BLKSIZ = 0 then IDX'BLKSIZ = 512
    if ID2'BLKSIZ = 0 then ID2'BLKSIZ = 512
    if IDX'BLKSIZ # 512 and IDX'BLKSIZ # 1024 and IDX'BLKSIZ # 2048 &
    and IDX'BLKSIZ # 4096 and IDX'BLKSIZ # 8192 and IDX'BLKSIZ # 16384 then
        ? "Block size must be one of: 512,1024,2048,4096,8192,16384"
        goto BLKSIZ
    endif
    if ID2'BLKSIZ # 512 and ID2'BLKSIZ # 1024 and ID2'BLKSIZ # 2048 &
    and ID2'BLKSIZ # 4096 and ID2'BLKSIZ # 8192 and ID2'BLKSIZ # 16384 then
        ? "Block size must be one of: 512,1024,2048,4096,8192,16384"
        goto BLKSIZ
    endif

    ! generate command file to create file
    open #1, "ismtst.cmd", output
    ? #1, ":T"
    ? #1, "erase ";TESTFILENAME;".ID?"
	
    ? #1, ":T"
    ? #1, "ISMBLD ";TESTFILENAME;"/V";                      ! [11]
    if IDX'LEVELS # 3 then ? #1,"/L:";str(IDX'LEVELS);      ! [11]
    if IDX'BLKSIZ # 512 then ? #1, "/B:";STR(IDX'BLKSIZ);   ! [11]
    ? #1                                                    ! [11]
    ? #1, "22"                      ! key size
    ? #1, "4"                       ! key pos
    ? #1, "64"                      ! rec size
    ? #1, str(INIREC)               ! # recs
    ? #1, str(int(INIREC/16))       ! extra index blocks
    ? #1, "Y"                       ! primary?
    ? #1, ""                        ! data device
    ? #1, ""                ! load from file
    ? #1, ""

    ? #1,":< Hit ENTER to proceed with secondary key build: >"
    ? #1,":K"
    ![1] generate secondary key too...
    ? #1, "ERASE ";TESTSECONDARY;".IDX"
    ? #1, "ISMBLD ";TESTSECONDARY;"/V";                     ! [11]
    if ID2'LEVELS # 3 then ? #1,"/L:";str(ID2'LEVELS);      ! [11]
    if ID2'BLKSIZ # 512 then ? #1, "/B:";STR(ID2'BLKSIZ);   ! [11]
    ? #1                                                    ! [11]
    ? #1, "24"                      ! key size
    ? #1, "41"                      ! key pos
    ? #1, "64"                      ! rec size
    ? #1, str(INIREC)               ! # recs
    ? #1, str(int(INIREC/16))       ! extra index blocks
    ? #1, "N"                       ! primary?
    ? #1, TESTFILENAME              ! primary file
    ? #1, ""                
    ? #1, ""
    ? #1,":<Hit ENTER to proceed with test (then any key to pause) > "
    ? #1,":K"
    ? #1, "run ISMTSH"
    close #1

    XCALL CCON			! enable controlc
    chain "ismtst.cmd"

BEGIN:
    if MAXREC = 0 then MAXREC = INIREC * 5

    input "Do you want to use XLOCK to ensure multiuser safety? ",A$
    if ucs(A$[1,1])="Y" then LOKFLG = 1 else LOKFLG = 0
    input "Do you want to perform CPU calcs between I/O ops? ",A$
    if ucs(A$[1,1])="Y" then WFLAG = 1 else WFLAG = 0
    input "Memory map the file? ",A$
    if ucs(A$)="Y" then AFLAG = 16 else AFLAG = 0	
    input "Read only? ",RO$             ! [12]
    RO$ = ucs(RO$)                      ! [12]

    XCALL ASFLAG,AFLAG			! [10]
!   XCALL CCOFF						! [2]

    input "Enter log file name (or blank for none: ",LOGFILE$

    ? "Opening ";TESTFILENAME;"..."
    OPEN #FILENO,TESTFILENAME,ISAM'INDEXED,64,TESTRECNUM,WAIT'FILE,WAIT'RECORD
    ? "Opening ";TESTSECONDARY;"..."
    OPEN #FILEN2,TESTSECONDARY,ISAM'INDEXED,64,TESTRECNUM,WAIT'FILE,WAIT'RECORD
    T1 = TIME
    T2 = TIME
    XCALL NOECHO

    if LOGFILE$ # "" then
        open #LCH,LOGFILE$,OUTPUT
    else
        LCH = 0
    endif
        
LOOP:
  xcall TINKEY,A$                 ! check for keyboard pending
  if A$ # "" goto INTERRUPT

    IF WFLAG=1 CALL WASTE'TIME			! [5] waste some time

    NUM=INT(RND(0)*MAXREC)		! [9]
    if BIAS >= 0 NUM = NUM + TIME       ! this will reduce dup keys

    KEY$ = NUM USING "00000000000000#ZZZZZZZ"            ! primary key
    KEY2$ = (NUM using "#ZZZZZZZZZZ") + "]]]]]]]]]]]]]"  ! secondary key

    IF LOKFLG = 1 CALL LOCKIT			! [5]

    ! look up key in primary, then verify that secondary agrees
    ! if no agreement, that's bad
    ! otherwise, if key exists, delete record, else add it

!trace.print KEY$+","+COUNT

    ISAM #FILENO,1,KEY$
    SKIP = 0
    switch erf(FILENO)
    case 0                          ! key found in primary
        ! first, verify it is also in secondary idx     ! [11]
        SAVE'RECNO = TESTRECNUM
        ISAM #FILEN2,1,KEY2$
        if erf(FILEN2)#0 goto ISAM'ERROR3
        if TESTRECNUM # SAVE'RECNO goto ISAM'ERROR5
        call DELETE'RECORD
        if SKIP = 0 and LCH # 0 then
            ? #LCH, "D,";KEY$
        endif
        exit
    case 33                         ! not found in primary
        ! first, verify it is NOT in secondary idx     ! [11]
        ISAM #FILEN2,1,KEY2$
        if erf(FILEN2)#33 goto ISAM'ERROR4
        call ADD'RECORD
        if SKIP = 0 and LCH # 0 then
            ? #LCH, "A,";KEY$
        endif
        exit
    default
        op$ = "ISAM #1" 
        goto ISAM'ERROR
    endswitch

    if SKIP = 0 then
        COUNT = COUNT + 1
        ! update stats after every 1000 recs
        if INT(COUNT/1000) = COUNT/1000 then
            ? TAB(30);"COUNT = ";COUNT;     &
                "  RATE = ";(1000/((TIME-T1) max 1) using "#####");",";&
                (COUNT/((TIME-T2) max 1) using "#####");" / sec" 
            T1 = TIME		! restart 100-count timer
        endif
    endif
    SKIP = 0
    call UNLOCK			! [5]
    goto LOOP

DELETE'RECORD:

    if BIAS > 0 then                    ! bias is add so skip some dels
      if COUNT/3 = int(COUNT/3) then
        SKIP = 1
        return
      endif
    endif
    LAST'COUNT = COUNT

    if RO$ = "Y" then       ! just read
        op$ = "read"
        read #FILENO, RECORD
        if (COUNT/40)=int(COUNT/40) ? "f";      ! [12] f for found
    else
        op$ = "readl"
        readl #FILENO, RECORD
        op$ = "delete secondary idx"
        ISAM #FILEN2,4,RECORD'S			! [1] delete secondary
        IF ERF(FILEN2) THEN GOTO ISAM'ERROR2
    	
        op$ = "delete primary idx"
        ISAM #FILENO,4,KEY$
        if ERF(FILENO) THEN GOTO ISAM'ERROR
    
        op$ = "delete data"
        ISAM #FILENO,6,KEY$
        IF ERF(FILENO) THEN GOTO ISAM'ERROR
        DEL'COUNT = DEL'COUNT + 1       ! [11]
        if (COUNT/40)=int(COUNT/40) ? "d";
    endif

!~  ? "B5=";RECORD'B5;"B4=";RECORD'B4;"F=";RECORD'F;"S=";RECORD'S
    op$=""
    return

ADD'RECORD:

    if RO$ = "Y" then           ! [12] read only
        if (COUNT/40)=int(COUNT/40) ? "n";  ! n for not found
        return                  ! [12]
    endif

    if BIAS < 0 then                    ! bias is del so skip some adds
      if COUNT/2 = int(COUNT/2) then
        SKIP = 1
        return
      endif
    endif
    LAST'COUNT = COUNT

    op$ = "add primary data"
    RECORD'KEY = KEY$
    RECORD'JUNK = "ash"
    RECORD'B5 = NUM
    RECORD'B4 = NUM
    RECORD'F = NUM
    RECORD'S = (NUM using "#ZZZZZZZZZZ") + "]]]]]]]]]]]]]"	! [1]
    ISAM #FILENO,5,KEY$
    IF ERF(FILENO) THEN GOTO ISAM'ERROR
    WRITEL #FILENO, RECORD
!   WRITE #FILENO, RECORD

    op$ = "add secondary idx"
    ISAM #FILEN2,3,RECORD'S			! [1] add secondary key
    if ERF(FILEN2) THEN GOTO ISAM'ERROR2
	
    op$ = "add primary idx"
    ISAM #FILENO,3,KEY$
    IF ERF(FILENO) THEN GOTO ISAM'ERROR
    if (COUNT/40)=int(COUNT/40) ? "a";
    op$ = ""
    ADD'COUNT = ADD'COUNT + 1           ! [11]
    return

ISAM'ERROR:
    ? "Isam error ";ERF(FILENO);" during ";op$
    ? "Main key = ";KEY$
    call UNLOCK
    call DISPLAY'ISAM'ERRORS
    end

ISAM'ERROR2:
    ? "Isam error on secondary ";ERF(FILEN2);" during ";op$
    ? "Main key = ";KEY$;" Secondary key = ";RECORD'S
    call UNLOCK
    call DISPLAY'ISAM'ERRORS
    end

ISAM'ERROR3:        ! [11] key in primary but not secondary
    ? "Primary/Secondary mismatch: key in primary but not secondary"
    ? "Main key = ";KEY$;" Secondary key = ";RECORD'S
    ? "ERF(secondary) = ";ERF(FILEN2)
    call UNLOCK
    call DISPLAY'ISAM'ERRORS
    end

ISAM'ERROR4:        ! [11] key not in primary but secondary
    ? "Primary/Secondary mismatch: key not in primary but is in secondary"
    ? "Main key = ";KEY$;" Secondary key = ";RECORD'S
    ? "ERF(secondary) = ";ERF(FILEN2)
    call UNLOCK
    call DISPLAY'ISAM'ERRORS
    end

ISAM'ERROR5:        ! [11] key in both idxs but with different rec#
    ? "Primary/Secondary mismatch in record #"
    ? "Main key = ";KEY$;" recno = ";SAVE'RECNO
    ? "Secondary key = ";KEY2$;" recno = ";TESTRECNUM
    ? "ERF(secondary) = ";ERF(FILEN2)
    call UNLOCK
    call DISPLAY'ISAM'ERRORS
    end

INTERRUPT:
    T3 = TIME
    ?
    xcall ISMROK,FILENO,IDAALC

    ? ADD'COUNT;"adds and ";DEL'COUNT;"deletes (net this prog run=";str(ADD'COUNT-DEL'COUNT);
    ? ", tot=";str(IDAALC);")"
    xcall ECHO
    input "Options (Q=quit, A=Favor adds, D=Favor deletes, 0=neutral)",A$
    if ucs(A$)="A" then BIAS = 1
    if ucs(A$)="D" then BIAS = -1
    if A$="0"then BIAS = 0    
    if ucs(A$) # "Q" then
        ? 
        T3 = TIME - T3
        T2 = T2 + T3     ! remove total time so as to not dilute perf
        goto LOOP
    endif
    close #FILENO
    close #FILEN2
    call UNLOCK
    ? ADD'COUNT;"adds and ";DEL'COUNT;"deletes ( net = ";ADD'COUNT-DEL'COUNT;")"
    if LCH close #LCH
    end

UNLOCK:
    if LOKFLG = 2 then
        MODE = 2
        LOCK1 = 128
        LOCK2 = 65535

        xcall XLOCK,MODE,LOCK1,LOCK2
        if MODE#1 ? "Unable to release lock: ";LOCK1;LOCK2 : end
        LOKFLG = 1
    endif
    return

LOCKIT:
! ? "X";
    MODE = 1
    LOCK1 = 128
    LOCK2 = 65535
    XCALL XLOCK,MODE,LOCK1,LOCK2
    if MODE#0 then
        ? "L"; 
        xcall SLEEP,0.1
        xcall TINKEY,A$ 
        if A$ # "" then
            goto INTERRUPT 
        else 
            goto LOCKIT
        endif
    endif
    LOKFLG = 2
    return

WASTE'TIME:	! perform a bunch of needless CPU calculations
		! just to make the test more "realistic"
    ? "W";tab(-1,254);
    for W = 1 to 10
	W1 = W * 3.14
	W2 = W1 / 2.19
	W3 = INT(W2)
	WBUF = ""
	for W4 = 1 to (W3 * 10)
		W5 = (rnd(0) * 95) + 32
		WBUF = WBUF + chr(W5)
	next W4
	W6 = instr(1,WBUF,"XXX")	! (never found)
    next W
    ? tab(-1,5);		! erase the "W"
    return

DISPLAY'ISAM'ERRORS:
    XCALL ECHO
    input "Do you want to display the ISAM error legend?",A$
    if ucs(A$)="Y" then
        ?
        ? "32 = Illegal ISAM statement code"
        ? "33 = Key not found during index search"
        ? "34 = Duplicate key found in the index during attempted key add"
        ? "35 = Link structure is smashed and must be re-created"
        ? "36 = Index file is full"
        ? "37 = Data file is full"
        ? "38 = End of file reached during get-next operation"
        ? "39 = Illegal ISAM sequence (LOKSER rules violation)"
    endif
    return
