program TSTLOK,1.0(105)
! test record/file locking
! This program also demonstrates/tests auto span'block mode and ability
! to read into a larger unformatted rec than the rec size
! EDIT HISTORY:
![101] March 16, 1999 09:46 PM          Edited by jack
!	Add WRITEN, WRITELN options 
![102] July 25, 2000 03:31 PM           Edited by jack
!	Add RRONLY option
![103] January 14, 2006 08:31 AM        Edited by jack
!	Allow use up to size of file
![104] February 16, 2010 01:12 PM       Edited by jacques
!       Add file hook
![105] February 20, 2011 11:45 AM       Edited by jack
!       Fix failure to exit properly on close

++include ashinc:ashell.def
++include ashinc:hook.def

significance 11

map1 tstrec
	map2 recjunk,x,600
	map2 recval,f
	map2 recsts,s,10
map1 tstrec2,x,1024,@tstrec

map1 recsiz,f,6,616
map1 maxrec,f,6

map1 A$,s,1

map1 opntyp,f
map1 action,f
map1 file1,f
map1 x,f
map1 waitflag,f
!map1 FNAME$,S,80,"g:\vm\miame\dsk0\150277\tstlok.dat"
map1 FNAME$,S,80,"TSTLOK.DAT"
!map1 FNAME$,S,80,"MEM:TSTLOK.DAT"
map1 IHOOKS,B,1
map1 HOOKSTATUS,F

	on error goto TRAP
	? tab(-1,0);"TSTLOK - test record/file locking"

        input "Install file hooks? (1/0): ",IHOOKS      ! [104]

        if IHOOKS then                                  ! [104]
            ? "Installing FHOOK1 file hooks..."
            XCALL MIAMEX, MX_FILEHOOK, HOOKOP_ADD, HOOKSTATUS, "TSTLOK.DAT", 1, &
                "SBX:FHOOK1", &
                HFE_PRE_OPEN or HFE_POST_OPEN or HFE_POST_CLOSE &
                or HFE_PRE_WRITE or HFE_PRE_WRITEL &
                or HFE_POST_WRITE or HFE_POST_WRITEL &
                or HFE_PRE_READ or HFE_PRE_READL &
                or HFE_POST_READ or HFE_POST_READL &
                or HFE_POST_ALLOC or HFE_POST_KILL, &
                HFF_DATA or HFF_DATA_WAS
        endif

10	lookup FNAME$,x
        ? "lookup ";FNAME$;" ";x;" blocks"

        if x # 0 then
            xcall SIZE,FNAME$,x
            ? "size ";FNAME$;" ";x;" bytes"
            maxrec = int(x/recsiz)-1
            ? "max recs = ";maxrec
        endif

20	if x <= 0 then
		allocate FNAME$,10 
		? "FNAME$ created with 8 records"
                xcall SIZE,FNAME$,x
                maxrec = int(x/recsiz)-1
                ? "max recs = ";maxrec
        endif

	input "Enter 1)open exclusive or 2)open random forced: ",opntyp
	if opntyp < 1 or opntyp > 2 then
            end 
        endif
wf:
	input "Enter wait flags 0=none,1=wait'record,2=wait'file,3=both: ",waitflag
	if waitflag < 0 or waitflag > 3 goto wf	

	if opntyp = 1 goto open'exc
	? "opening file [shared]...";

!xcall ASFLAG,1
30	if waitflag = 0 then						 &
	    open #6101, FNAME$,random'forced,recsiz,file1	 	 &
	else if waitflag = 1 then					 &
	    ? "[wait'record]..."					:&
	    open #6101, FNAME$,random'forced,recsiz,file1,wait'record 	 &
	else if waitflag = 2 then 					 &
	    ? "[wait'file]..."						:&
	    open #6101, FNAME$,random'forced,recsiz,file1,wait'file       &
	else 								 &
	    ? "[wait'record,wait'file]..."				:&
	    open #6101, FNAME$,random'forced,recsiz,file1,wait'record,wait'file
	goto loop			

open'exc:
	? "opening file [exclusive]...";

40	if waitflag = 0 then						 &
	    open #6101, FNAME$,random,recsiz,file1		 	 &
	else if waitflag = 1 then					 &
	    ? "[wait'record]..."					:&
	    open #6101, FNAME$,random,recsiz,file1,wait'record 	 	 &
	else if waitflag = 2 then 					 &
	    ? "[wait'file]..."						:&
	    open #6101, FNAME$,random,recsiz,file1,wait'file   	 	 &
	else 								 &
	    ? "[wait'record,wait'file]..."				:&
	    open #6101, FNAME$,random,recsiz,file1,wait'record,wait'file

loop:
	?
	input "1)read,2)readl,3)rronly,4)write,5)writel,6)cls,7)abt,8)writen,9)writeln: ",action
	on action call a'read,a'readl,a'rronly,a'write,a'writel,a'close
![105]	if action=6 goto a'close
        if action=6 or action=7 then
            if IHOOKS then                          ! [104]
                xcall EZTYP,"OPR:FHOOK1.LOG"
            endif
            end
        endif
	on action-7 call a'writen,a'writeln
	goto loop

a'read:
	? "Enter record # to read (0-";str(maxrec);") [";str(file1);"] : ";
	input "",file1

	? "attempting to read record ";file1;"...";
100	read #6101, tstrec2
	?
	? "Success; recval = ";recval
	return

a'readl:
	? "Enter record # to readl (0-";str(maxrec);") [";str(file1);"] : ";
	input "",file1

	? "attempting to read record ";file1;"...";
	?
200	readl #6101, tstrec2
	?
	? "Success; recval = ";recval
	? "[Record";file1;"is now locked]"
	return

a'rronly:
	? "Enter record # to read (0-";str(maxrec);") [";str(file1);"] : ";
	input "",file1

	? "attempting to read record ";file1;"...";
300	read'read'only #6101, tstrec2
	?
	? "Success; recval = ";recval
	return


a'write:
	? "Enter record # to write (0-";str(maxrec);") [";str(file1);"] : ";
	input "",file1
	? "enter new recval [";str(recval);"] : ";
	input recval
	? "attempting to write record...";file1;"...";
	write #6101, tstrec
	?
	? "Success; recval = ";recval
	? "[Record";file1;"is now unlocked]"
	return
	
a'writel:
	? "Enter record # to writel (0-";str(maxrec);") [";str(file1);"] : ";
	input "",file1
	? "enter new recval [";str(recval);"] : ";
	input recval
	? "attempting to writel record...";file1;"...";
	writel #6101, tstrec
	?
	? "Success; recval = ";recval
	return

a'writen:
	? "Enter record # to writen (0-";str(maxrec);") [";str(file1);"] : ";
	input "",file1
	? "enter new recval [";str(recval);"] : ";
	input recval
	? "attempting to write record...";file1;"...";
	writen #6101, tstrec
	?
	? "Success; recval = ";recval
	? "[Record";file1;"is STILL unlocked]"
	return

a'writeln:
	? "Enter record # to writeln (0-";str(maxrec);") [";str(file1);"] : ";
	input "",file1
	? "enter new recval [";str(recval);"] : ";
	input recval
	? "attempting to writeln record...";file1;"...";
	writeln #6101, tstrec
	?
	? "Success; recval = ";recval
	? "[Record";file1;"is now locked]"
	return


a'close:
	? "Closing file..."
	close #6101
        return

TRAP:
	? "Error #";err(0);" in line ";err(1);" file: ";err(2)
	input "Retry? ";A$
	if ucs(A$)="Y" resume
	end
