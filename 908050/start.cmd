:R
:<(HOOK) Test FILE HOOKs - MX_FILEHOOK, auditing, triggers

    FHOOKTST1.BP - Example of setting a hook, doing file i/o
    FHOOKTST3.BP - Variation using ISAM 1.X
    FHOOKTST4.BP - Variation using ISAM and LOGFIL: handler
    FHOOKTST5.BP - LOGFIL: reader (use dynstruct to report fld-level chgs)
    HOOKDEFIDX.DAT-Sample file/defstruct xref used by FHOOKTST5 (VUE-able)
    FHOOK1.BP    - Sample file hook SBX (needed by FHOOKTST1)
    QRYHOOKS.BP  - Query existing file hooks 
    ENADISHOOK.BP- Enable/disable existing file hooks
    ISMTSH.BAS   - Variation of ISMTST with using FHOOK1 hook
    ISPTSTH.BP   - ISAM-A test
    TSTLOK.BAS   - Simple step-by-step file ops with FHOOK1  hook
    HKLOGVUE.BP  - Generic viewer for LOGFIL: -style log files
>
 
