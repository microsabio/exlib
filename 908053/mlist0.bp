program mlist0, 1.0(100)  ! test simple operations on mlist
!------------------------------------------------------------------------
!EDIT HISTORY
!Version 1.0:-
![100] 09/23/16 15:37:26      Edited by Jack
!	Created 
!------------------------------------------------------------------------
!REQUIREMENTS
!   6.3.1528+
!NOTES
!------------------------------------------------------------------------

++include'once ashinc:ashell.def
++include'once mlistdemo.bsi        ! misc routines used in the mlist demos

    map1 misc
        map2 count,i,4
        
    ! declare an mlist containing string elements
    dimx $m, mlist(varstr)

    ? tab(-1,11);"mlist0 - simple example of multi-level list (mlist)"
    ?

    ! add some elements to the end of the list
    ? "Pushing 'red','blue','green' to back of list using $m(.pushback) ..."
    $m(.pushback) = "red"
    $m(.pushback) = "blue"
    $m(.pushback) = "green"
    
    ? "Pushing 'yellow' and then 'black' on the front using $m(.pushfront) ..."
    $m(.pushfront) = "yellow"
    $m(.pushfront) = "black"
    
    
    ? ".extent($m()) = ";.extent($m())
    gosub iterate
    
    ? "Direct access to first item via $m(.front) : ";Fn'Element$($m(.front))
    ? "Direct access to last item via  $m(.back) :  ";Fn'Element$($m(.back))

    call Pause()
    
    ? "Prepending 'light-' on first element and 'dark-' on last element..."
    ? "$m(.front) was ";Fn'Element$($m(.front));
    $m(.front) = "light-" + $m(.front)
    ? ", after update is ";Fn'Element$($m(.front))
    
    ? "$m(.back) was ";Fn'Element$($m(.back));
    $m(.back)  = "dark-" + $m(.back)
    ? ", after update is ";Fn'Element$($m(.back))
    
    gosub iterate
    call Pause()

    ? "Removing first element ";Fn'Element$($m(.front));" using .popfront $m() ..."
    .popfront $m()
    ? "Removing last element ";Fn'Element$($m(.back));" using .popback $m() ..."
    .popback $m()
    gosub iterate
    call Pause()

    ? "Inserting an element prior to 3rd element via $m(.before($$i)) = ""inserted"" ..."
    count = 0
    foreach $$i in $m()
        count += 1
        if count = 3 then   ! insert before 3rd element
            $m(.before($$i)) = "inserted"
            exit
        endif
    next $$i
    
    gosub iterate
    call Pause()
    
    ? "Deleting 3rd element via $m(.ref($$i)) = .null ..."
    count = 0
    foreach $$i in $m()
        count += 1
        if count = 3 then   ! delete 3rd element
            $m(.ref($$i)) = .null
            exit
        endif
    next $$i
    
    gosub iterate
    
end

! simple iteration
iterate:
    ? "Iterating list..."
    count = 0
    foreach $$i in $m()
        if count > 0 then
            ? ", ";             ! display commas between elements
        endif
        ? Fn'Element$($$i);
        count += 1
    next $$i
    ?
    ? count;" elements iterated"
    
    if count # .extent($m()) then   ! verify that .extent matches
        ? "!!! Mismatch between iteration count and .extent: ";.extent($m())
        stop
    endif
    ?
    return

