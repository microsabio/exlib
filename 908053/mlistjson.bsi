!mlistjson.bsi [100]  ! routines for parsing JSON with mlist
!------------------------------------------------------------------------
!EDIT HISTORY
!Version 1.0:-
![100] 10/22/16 17:10:03      Edited by Jack
!	Created 
![101] 07-Nov-16 12:56       Edited by Jack
!   Add Fn'MLJ'Serialize() (inverse of MLJ'Load)
![102] 07-Nov-16 14:59       Edited by Jack
!   Fix bug in the fn'mlj'next'token$ routine where it was confusing
!   quoted terminator characters with real terminators (when the field
!   was so long that the closing quote was outside the current buffer.)
!   (The instr INSTRF_ANYQT flag only works with MATCHED sets of quotes.)
!------------------------------------------------------------------------
!REQUIREMENTS
!   6.3.1532 (for MLIST)
!   6.3.1534.0 (for EDIT$ enhancements)
!NOTES
!   Each node of the resulting MLIST will contain one of the following:
!
!       []              (sublist contains array)
!       {}              (sublist contains object)    
!       string          (string scalar)
!       scalar          (number, true, false, null)
!       "name":"string" (name:value pair)
!       "name":scalar   (name:value pair)
!       "name":{}       (value part of pair is object in sublist)
!       "name":[]       (value part of pair is array in sublist)
!
!   Name part of name:value pairs will be quoted (as in normal JSON)
!   to eliminate ambiguity over embedded special characters. Similarly,
!   string values will be quoted to eliminate ambiguity between the 
!   string values "true", "false" and "null" vs. the special scalars
!   true, false, and null.
!   
!   All whitespace not contained within quoted strings in the JSON
!   document will be removed.
!
!   During parsing, the open [ or { will initially terminate the contents
!   of that node, until the closing ] or } is encountered. Hence any 
!   nodes with only an opening [ or { would suggest a syntax error 
!   (unterminated object or array) in the underlying JSON document.
!
!   Input buffering: since processing string data byte-by-byte is relatively
!   costly in BASIC (due to the lack of pointers), processing the JSON
!   one byte at a time seems unwise. At the opposite extreme, loading the
!   entire document into a single string is also inefficent since each
!   operation on the string would require pushing the entire thing on to
!   the string expression stack. The optimum appears to be somewhere in
!   the middle, i.e. to operate on chunks of text at at time. One 
!   possibility would be to use line terminators to delimite these chunks,
!   but we can't count on the JSON being split into logical lines. So
!   the compromise plan is to input a fixed block of bytes (512) from the
!   file, and then from that buffer, copy logical segments (JSON 'values'?)
!   chunks to a working buffer for parsing/manipulation. In the case of 
!   very large string values (>512 bytes) we may need to expand the 
!   working buffer (by reading additional 512 byte blocks) until we can 
!   get a complete value.
!------------------------------------------------------------------------
!Public Functions
! Fn'MLJ'Load(fspec$, $json()) - load JSON doc into MLIST
! Fn'MLJ'Serialize($json(), fspec$) - serialize to file
!Private Functions
! fn'mlj'next'token$()  - return next string token from input stream
! fn'mlj'read'block()   - read another block into working buffer
! fn'mlj'serialize($json()) - serializer
! fn'count'quotes(strbuf$, limit) - count quotes in string up to limit pos
! out'item(text$)       - output an item; handle indenting
!------------------------------------------------------------------------
                     
++include'once ashinc:ashell.def
++include'once sosfunc:fnextch.bsi
++include'once sosfunc:fnminasver.bsi
++include'once sosfunc:fnfilestr.bsi    ! [101] for option to return as string    

define JSON_DELIMITERS$ = "{}[],"   

++ifnmap Privates

++pragma PRIVATE_BEGIN
map1 Privates
    map2 Chin,b,2           ! input channel
    map2 Chout,b,2          ! output channel
    map2 Workbuf$,s,0       ! buffer containing unparsed text
    map2 Level,i,2          ! nesting level (mostly for debugging)
    map2 Indent,b,2         ! # spaces to indent per level
    map2 Outbytes,b,4       ! # bytes output when serializing
++pragma PRIVATE_END

++endif

++ifmap Privates
++message "Privates has been mapped"
++endif

!---------------------------------------------------------------------
!Function:
!   Load JSON document from file into MLIST
!Params:
!   fspec$  (str) [in] - JSON file
!   $json() (MLIST) [byref] - mlist to return document in
!Returns:
!   file size in bytes (0 for non-existent, <0 for error)
!   -1 = does not appear to be JSON (doesn't start with "[" or "{")
!   -2 = Version of A-Shell not sufficient
!   
!Module vars:
!   Chin - open input channel
!Notes:
!---------------------------------------------------------------------
Function Fn'MLJ'Load(fspec$ as T_NATIVEPATH:inputonly, &
                     $json() as mlist(varstr)) as i4
                     
    map1 locals
        map2 token$,s,2
        map2 status,i,4

    ! check if A-Shell version sufficent
    if Fn'MinAshVer(6,3,1534,0) < 0 then
        ? "Sorry, this routine requires 6.3.1534.0+"
        Fn'MLJ'Load = -2
        exitfunction
    endif
        
    Chin = Fn'NextCh(50000)
    xcall SIZE, fspec$, Fn'MLJ'Load
    if Fn'MLJ'Load > 0 then
        open #Chin, fspec$, input
    endif
    
    token$ = fn'mlj'next'token$()   ! get first token
    if token$ # "[" and token$ # "{" then
        Fn'MLJ'Load = -1
    else

        $json(.pushback) = token$
        token$ = ifelse$(token$="[","]","}")    ! expected ending token
        Level = 0
        status = fn'mlj'parse($json(.back).sublist, token$)
        if status >= 0 then                     ! successful completion
            $json(.back) += token$              ! node now contains {} or []
        else
            $json(.back) += " !!Error: missing terminator; status="+status
        endif
    endif
   
    close #Chin
    Chin = 0

EndFunction

!---------------------------------------------------------------------
!Function:
!   Write JSON data from the MLIST format created by Fn'MLJ'Load back
!   to JSON file format (i.e. serialize it to a file)
!Params:
!   $json() (MLIST) [byref] - mlist representation of JSON data
!   fspec$  (s0) [in/out] - file to output to. If "", data is output
!                           directly to the string (must be s0)
!   indent (num) [in] - spaces to indent per level; 0=compact stream format
!Returns:
!   # bytes output (0 for no data, <0 for error)
!   -1 = does not appear to be JSON (doesn't start with "[" or "{")
!   -2 = Version of A-Shell not sufficient
!   
!Module vars:
!   Chout (output channel)
!   Level
!   Indent
!   Outbytes (updated by lower level output routine)
!Notes:
!---------------------------------------------------------------------
Function Fn'MLJ'Serialize($json() as mlist(varstr), &
                    fspec$ as s0, indent as b2) as i4
                     
    map1 locals
        map2 status,i,4
        map2 tempspec$,s,32

    ! check if A-Shell version sufficent
    if Fn'MinAshVer(6,3,1532,0) < 0 then
        ? "Sorry, this routine requires 6.3.1532.0+"
        Fn'MLJ'Serialize = -2
        exitfunction
    endif
        
    ! if no file specified, generate a temp file name
    ! (we'll read it back to a string later)
    if fspec$ = "" then
        tempspec$ = strip(.JOBNAME) + ".tmp"
        fspec$ = tempspec$
    endif
    
    Chout = Fn'NextCh(50000)
    open #Chout, fspec$, output

    Indent = indent     ! save indent at module level
    Level = 0           ! init level
    Outbytes = 0        ! init output byte count
    
    Fn'MLJ'Serialize = fn'mlj'serialize($json())
    
    close #Chout
    if tempspec$ # "" then          ! load file back into return var
        fspec$ = Fn'FileToStr$(tempspec$)
        xputarg 2, fspec$
        Fn'MLJ'Serialize = len(fspec$)  ! return # bytes output    
        kill tempspec$
    else
        Fn'MLJ'Serialize = Outbytes     ! return # bytes output
    endif
    Chout = 0
    
EndFunction

!===================================================================
!Private Functions
!===================================================================


!---------------------------------------------------------------------
!Function:
!   get next token
!Params:
!   p1  (str) [in] - first param
!Returns:
!   a token (possibly empty)
!   "" indicates we hit end of file
!Examples:
!   {     - start of JSON doc containing object
!   [     - start of JSON doc containing array
!   }     - end of object
!   ]     - end of array
!   "name":{        - pair whose value is an object to follow
!   "name":[        - pair whose value is an array to follow
!   "name":"string" - a name:string-value pair
!   "string"        - a string value (in an array)
!   scalar          - a scalar value (in an array)
!   
!Module vars:
!   Chin (open input file channel) - closed on eof
!   buf$ (characters read but not yet parsed)
!Notes:
!   Whitespace outside of quoted strings stripped, as are commas   
!---------------------------------------------------------------------
Private Function fn'mlj'next'token$() as s0

    map1 locals
        map2 i,i,4
        
    do 
        ! remove leading spaces, and any ctl chars outside quotes
        ! note: we know that buffer starts outside of a token, so
        ! no danger of the leading spaces/tabs being part of a string.
        Workbuf$ = edit$(Workbuf$,EDITF_CTLS+EDITF_SPTB+EDITF_EXQT)
        
        ! look for next delimiter (not within quotes)
        ! [102] Warning: this only works for matching sets of quotes
        ! [102] (We'll have to check for that below, after a match...)
        i = instr(1,Workbuf$,JSON_DELIMITERS$,INSTRF_ANYQT)
        debug.print "instr anyqt = "+i 
        
        ! if we didn't get one, then we need to read in more
        if i = 1 then           ! first char is a delimiter
            if Workbuf$[i;1] = "," then         ! skip over leading commas
                Workbuf$ = Workbuf$[i+1,-1]
                repeat
            endif
        
        elseif i > 1 then       ! [102] make sure the found character
                                ! [102] isn't within an unterminated quoted string
                                ! [102] by counting quotes before it
            if (fn'count'quotes(Workbuf$,i) mod 2) then ! [102] if odd, then we need
                i = 0                                   ! [102] to read in more
            endif
        endif
        
        if i = 0 then
            if fn'mlj'read'block() <= 0 then
                exit            ! abort if we run out of data
            endif
        endif

    loop until i > 0
    
    debug.print "exit loop: i = "+i+" Workbuf$[i;1] = "+Workbuf$[i;1]
    ! return buffer with just the one token 
    ! if no delimiter, we must be at end of document and buffer must just
    ! be whitespace (which we'll edit out); anything else we return but
    ! it's probably garbage if not ""
    if i = 0 then
        fn'mlj'next'token$ = Workbuf$
        Workbuf$ = ""
    elseif i = 1 then
        fn'mlj'next'token$ = Workbuf$[1,1]
        Workbuf$ = Workbuf$[2,-1]
    elseif instr(1,"{[",Workbuf$[i;1]) then     ! leave opening delimeters on end of token
        fn'mlj'next'token$ = Workbuf$[1,i]
        Workbuf$ = Workbuf$[i+1,-1]
    else                                        ! return buffer up to just before delimiter
        fn'mlj'next'token$ = Workbuf$[1,i-1]
        Workbuf$ = Workbuf$[i,-1]
    endif
    
    debug.print "Return token len="+len(fn'mlj'next'token$)+" : "+fn'mlj'next'token$
EndFunction

!---------------------------------------------------------------------
!Function:
!   read another block from the file into the working buffer
!Params:
!Returns:
!   # bytes read (0=eof, -1=input file not open; <-1 error)
!Module Vars:
!   Chin (input channel)
!   Workbuf$ (working bufffer)
!Notes:
!   See notes at top for buffering strategy
!---------------------------------------------------------------------
Private Function fn'mlj'read'block() as i4

    define JSON_BUF_SIZ = 512      ! # chars to read at a time

    map1 locals
        map2 blockbuf$,s,JSON_BUF_SIZ
        
    if Chin then
        if eof(Chin) # 1 then       ! if not eof
            blockbuf$ = fill(chr(0),sizeof(blockbuf$))
            xcall GET, blockbuf$, Chin, JSON_BUF_SIZ, fn'mlj'read'block
            Workbuf$ += blockbuf$
        endif
    else
        fn'mlj'read'block = -1
    endif
    debug.print "read "+fn'mlj'read'block+" into buf; buflen = "+len(Workbuf$)
EndFunction

!---------------------------------------------------------------------
!Function:
!   parse a JSON array or object, up to final "]" or "}"
!Params:
!   $json()  (mlist) [ref] - parsed JSON data stored here (see notes at top)
!   endtoken$ (s) [in] - ending token to look for ("]" or "}")
!Returns:
!   >= 0 for success
!   < 0 for error
!Module vars:
!   Chin, Workbuf$
!Notes:
!---------------------------------------------------------------------
Private Function fn'mlj'parse($json() as mlist(varstr), endtoken$ as s2:inputonly) as i4
    map1 locals
        map2 token$,s,0
        map2 status,i,4
        map2 bdone,BOOLEAN
        map2 subendtoken$,s,2
    
    Level += 1
    debug.print "fn'mlj'parse(endtoken$="+endtoken$+", level="+Level+")"
    do 
        token$ = fn'mlj'next'token$()
            
        switch token$
            case "["            ! parse a new array
            case "{"            ! parse an object
                $json(.pushback) = token$
                subendtoken$ = ifelse$(token$="[","]","}")
                status = fn'mlj'parse($json(.back).sublist,subendtoken$)
                if status >= 0 then
                    $json(.back)  += subendtoken$
                else 
                    $json(.back) += " !!Error: missing terminator: "+subendtoken$
                endif
                exit
            case "]"
            case "}"
                debug.print "terminating token = "+token$+", endtoken = "+endtoken$+", level="+Level
                status = ifelse(token$=endtoken$,0,-1)
                bdone = .TRUE
                exit
            case ""             ! no more data (error?)
                bdone = .TRUE
                status = -1
                exit
            default
                switch token$[-1,-1]
                    case "{"            ! name:{
                    case "["            ! name:[
                        $json(.pushback) = token$
                        subendtoken$ = ifelse$(token$[-1,-1]="[","]","}")
                        status = fn'mlj'parse($json(.back).sublist,subendtoken$)
                        if status >= 0 then
                            $json(.back) += subendtoken$
                        else 
                            $json(.back) += " !!Error: missing terminator: "+subendtoken$
                        endif
                        exit
                    default
                        ! add an name:value, or scalar element to our list
                        $json(.pushback) = token$
                        exit
                endswitch
                exit
        endswitch
    loop until bdone
    
    if status < 0 then
        fn'mlj'parse = -1
    endif
    Level -= 1
EndFunction


!---------------------------------------------------------------------
!Function:
!   Recursive worker for Fn'MLJ'Serialize 
!Params:
!   $json() (MLIST) [byref] - mlist representation of JSON data
!   outbuf$ (s0) [out] - if Chout=0, output goes here, else to #Chout
!Returns:
!   # bytes output
!   -2 = parse error
!Module Vars
!   Chout (output channel, already open)
!   Level
!   Indent
!Notes:
!   Replaces the file if it already exists.
!   See notes at top of this file for mlist encoding details.
!   Similar to Fn'XMList'Save'Doc() in mlistxml.bsi
!---------------------------------------------------------------------
Private Function fn'mlj'serialize($json() as mlist(varstr)) as i4

    map1 locals
        map2 element$,s,0
        map2 status,i,4
        map2 x,i,4
        map2 y,i,4
        map2 char$,s,1
        
    Level += 1
    
    foreach $$i in $json()
        element$ = $$i      ! slightly more efficient than many refs to iterator
        x = instr(1,element$,"[{",INSTRF_ANYQT)
        if x then
            char$ = element$[x;1]
        
            if x = 1 then
                call out'item(char$)
            else                                    ! must be "name":[] or "name":{}
                call out'item(element$[1,x-1])      ! output "name:":
                call out'item(char$)                ! output [ or {  (possible new line)
            endif
            call fn'mlj'serialize($$i.sublist)      ! recurse for sublist
            call out'item(element$[x+1;1])          ! outut closing ] or }
        else
            call out'item(element$)                 ! scalar or "name":value
        endif
    next $$i
    
    Level -= 1

EndFunction


!---------------------------------------------------------------------
!Procedure:
!   output a new JSON item to output file; handle breaks and indenting
!Params:
!   item$ (str) [in] - item to output
!Module vars:
!   Chout    (output channel)
!   Outbytes (# bytes output, counting only one per line terminator)
!   Level    (nesting level)
!   Indent   (spaces per level to indent; if 0 no line breaks or indenting)
!Notes:
!   Determines whether a comma is needed based on last character of
!   previous item and first character of this item. 
!---------------------------------------------------------------------
Procedure out'item(item$ as s0) 

    static map1 s_lastchar$,s,1
    map1 locals
        map2 spacecount,b,2
        map2 firstchar$,s,1
        
    ! append a comma between items as needed...
    firstchar$ = item$[1,1]
    if instr(1,"}]",firstchar$) = 0 and s_lastchar$ # "" &
    and instr(1,"[{:",s_lastchar$) = 0 then
        ? #Chout, ",";
        Outbytes += 1
    endif
    ! start a new line for each item unless first item or no indent
    if s_lastchar$ # "" and Indent > 0 then
        ? #Chout
        Outbytes += 1
    endif
    if Indent > 0 and Level > 0 then
        spacecount = Indent*(Level-1)
        ? #Chout, space(spacecount);
        Outbytes += spacecount
    endif
    ? #Chout, item$;
    Outbytes += len(item$)
    s_lastchar$ = item$[-1,-1]
EndProcedure


!---------------------------------------------------------------------
!Function:
!   count quote chars in given string, with optional limit pos
!Params:
!   strbuf  (str) [in] - string
!   limit   (num) [in] - optional limit position
!Returns:
!   # of quotes in entire string (or up to limit position)
!Notes:
!---------------------------------------------------------------------
Private Function fn'count'quotes(strbuf$ as s0:inputonly, limit as b4:inputonly) as b4

    map1 q,i,4

    if limit = 0 then
        limit = len(strbuf$)
    endif
    
    do while q <= limit      
        q = instr(q+1,strbuf$,"""")
        if q > 0 and q < limit then
            fn'count'quotes += 1
        else
            exit
        endif
    loop
    debug.print "fn'count'quotes = "+fn'count'quotes+", limit="+limit
    
EndFunction

