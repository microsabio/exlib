:<Collections (aka containers) experimentation
    Collection types: 
      ORDMAP  (associative array, aka 'map')
      ORDMAPM (associative array duplicate keys allowed, aka 'multimap')
      MLIST   (multi-level list)

    FOREACH - Simple test of assoc. arrays, iterators
    MAPZIPS - Larger test using uszips.csv[908,48]
    ORDMAPX - Test of ordmap(varstr;varx)

    MLIST#     - Tests on Multi-Level Lists (MLIST)
    MLISTXML#  - Tests using MLIST to store XML data
    MLISTJSON# - Tests using MLIST to store JSON data

    See [908,63] for newer, more JSON-focused samples
>


