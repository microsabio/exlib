program ISXTST,2.1(017)  ! variation of ISYTST with 64 byte rec
!------------------------------------------------------------------------
!ISAM 1.0/1.1 test program - variation of the original ISMTST, designed
!for more thorough testing of really big files.
!
!It starts by generating a file (ISXTST.IDA/X, ISXTS2.IDX) of a size you 
!specify or, if the files already exist, it will give you the option to
!reuse them. It then presents various options:
!
!   1. Add recs until the largest idx or ida reaches a certain size
!       (generate random integers, and for each one, append a sequence
!       of 1000 incremental values)
!   2. Random add/del operations (with a bias towards add or del)
!       (generate keys from random integers; look up; if exists, 
!       delete; else add)
!   3. Sequential
!
!In all modes it checks for kbd input frequently, allowing you to 
!pause or stop the test with a keystroke. It can be restarted at any
!time on any number of workstations. It does not require LOKSER, 
!but uses XLOCK to lock against the possibility that two processes
!both perform the same key lookup at the same time, then either 
!decided to both add or both delete the same key.
!
!The test is good for both performance and reliability (although you 
!really need to run it on multiple machines to really test reliability.)
!
!Program displays an "a" (add) or "d" (delete) on every 50th iteration
!to give you a visual sense of what it is doing.  (It also displays
!a "L" if waiting on XLOCK or a "W" if "wasting" time with CPU 
!operations between iterations.)  It also gives a running count, and for
!each 100 operations, it reports the number of iterations per second
!for the last 100, as well as the number of iterations per second 
!since the start of the program.  (The first figure may not be very
!reliable when the time interval to perform 100 iterations is less
!than 2 seconds, given that the TIME function only returns integer
!seconds elapsed.)
!
!Also see ISMTSP.BAS, a somewhat smaller scale version.
!
!-----------------------------------------------------------------------
! Edit History
! 1. 13-Oct-94 Add ISMTS2 secondary key /jdm
! 2. 10-Jan-95 Added CCOFF, more error info /jdm
! 3. 16-Feb-95 Added WAIT'FILE, WAIT'RECORD, option to delete file /jdm
! 4. 27-Feb-95 Check for error on initial ISAM #1 /jdm
! 5. 05-Oct-97 Add option to use XLOCK to make sure 2 processes don't	
!		decide to add the same key at same time.  Also, add
!		a bunch of CPU calculations to make it a bit more
!		realistic. /jdm
! 6. 11-May-98 Reduce output to decrease effect of scrolling speed /jdm
! 7. 10-Apr-99 Recompute rate each block of 25 /jdm
! 8. 31-Aug-01 Also show accumulated rate /jdm
! 9. 29-Mar-02 Base random keys on a range 4 times larger than initial
!		file size so we can test auto expansion /jdm
!10. 29-Aug-02 Add MMAP option /jdm
!
!Version 2.0 --
!11. 17-Aug-08 Revamp to perform more validity tests (rather than just
!               performance); Use isam'indexed to avoid compiler confusion; 
!               adjust mask to allow for >9999/sec; prompt for levels, 
!               block size; always check that both primary & secondary
!               match /jdm
!12. 22-Sep-08 Add read-only option /jdm
!
!Version 2.1 --
!13. 05-Mar-13 Add option to rapidly grow file to a specified size,
!               to make it easier to test 2GB boundary issues
!14. 08-Mar-13 Improve responsiveness to kbd during delete bias /jdm
!15. 13-Mar-13 Add option to build from IDA /jdm
!16. 24-Mar-13 Add previous operation /jdm
!17. 30-Jul-15 Add xlock locking to the sequential key ops /jdm
!-----------------------------------------------------------------------
	strsiz 80
        significance 11

MAP1 RECORD     
    MAP2 RECORD'KEY1,S,50
    MAP2 RECORD'KEY2,S,14

MAP1 TESTRECLEN,F,6,64
MAP1 TESTFILENAME,S,25,"ISXTST"
MAP1 TESTSECONDARY,S,25,"ISXTS2"

MAP1 ISMROK
    MAP2 IDAALC,F
    MAP2 IDAAVL,F
    MAP2 RECSIZ,F
    MAP2 KEYSIZ,F
    MAP2 KEYPOS,F
    MAP2 DEVDRV,S,10
    MAP2 IDXALC,F
    MAP2 IDXAVL,F

MAP1 MISC
    MAP2 BIAS,F
    MAP2 COUNT,F
    MAP2 LAST'COUNT,F
    MAP2 ADD'COUNT,F            
    MAP2 DEL'COUNT,F            
    MAP2 NEXT'COUNT,F
    MAP2 MAXREC,F
    MAP2 INIREC,F
    MAP2 MAXSEC,F
    MAP2 OP$,s,20
    MAP2 LASTOP,B,1
    MAP2 CMDFLG,F,6
    MAP2 A$,S,1
    MAP2 AFLAG,F
    MAP2 TESTRECNUM,F
    MAP2 SAVE'RECNO,F
    MAP2 TMSECS,F
    MAP2 T1,F
    MAP2 T2,F
    MAP2 T3,F
    MAP2 TZ,F
    MAP2 FILENO,F,6,1
    MAP2 FILEN2,F,6,2
    MAP2 DATFNO,F,6,3		! [15]
    MAP2 DATFILE$,S,100		! [15]
    MAP2 DATRECNO,F		! [15]
    MAP2 MAXDATRECNO,F		! [15]
    MAP2 I,F
    MAP2 B1,F
    MAP2 LOKFLG,F,6
    MAP2 AFLAGS,B,2
    MAP2 NUM,F
    MAP2 W,F
    MAP2 KEY1$,S,50
    MAP2 KEY2$,S,14
    MAP2 LAST'KEY1$,S,50
    MAP2 LAST'KEY2$,S,14
    MAP2 IDX'LEVELS,B,1         ! [11]
    MAP2 IDX'BLKSIZ,B,2         ! [11]
    MAP2 ID2'LEVELS,B,1         ! [11]
    MAP2 ID2'BLKSIZ,B,2         ! [11]
    MAP2 SKIP,F
    MAP2 LOGFILE$,S,32
    MAP2 LCH,B,2,88
    MAP2 RO,B,1                    
    MAP2 TSTMODE,F,6
    MAP2 TARGET,F
    MAP2 BYTES,F
    MAP2 NEXT'SIZE'CHECK,F
    MAP2 N1,F
    MAP2 N2,F
MAP1 WASTE
    MAP2 W1,F
    MAP2 W2,F
    MAP2 W3,F
    MAP2 W4,F
    MAP2 W5,F
    MAP2 W6,F
    MAP2 WBUF,S,2000
    MAP2 WFLAG,F

MAP1 LOCKSTUFF
    MAP2 MODE,B,2
    MAP2 LOCK1,B,2
    MAP2 LOCK2,B,2

    RANDOMIZE

    XCALL ECHO
    XCALL CCON
    ? "ISAM 1.x torture test utility"

    ! if ISXTST.CMD exists, we must be returning from create operation
    lookup "ISXTST.CMD", CMDFLG
    if CMDFLG#0 then kill "ISXTST.CMD"

    lookup TESTFILENAME+".ida",INIREC
    if INIREC <> 0 then
        lookup TESTSECONDARY+".idx",MAXSEC
    else
        goto NOT'FOUND
    endif

    if INIREC = 0 or MAXSEC = 0 goto NOT'FOUND
    
    INIREC = abs(INIREC) * 8                      
    ? TESTFILENAME;" has";INIREC;"records.";      
    if CMDFLG = 1 goto BEGIN  ! if coming from cmd file, skip questions

    ? "  Do you want to re-initialize the file? "; 
    input "",A$				
    if ucs(A$)="Y" then
        goto CREATE
    else
        goto BEGIN
    endif

NOT'FOUND:
    if INIREC <> 0 then
        kill TESTFILENAME+".ida"
        kill TESTFILENAME+".idx"
    endif

    ? "File ";TESTFILENAME;" and/or ";TESTSECONDARY;" does not exist.  Create it? ";
    input "",A$
    if ucs(A$[1,1]) # "Y" end

CREATE:
    input "Enter initial # of records: ",INIREC
    input "Enter expected max # of records: ",MAXREC

LEVELS:                                         ! [11]   
    input "Enter main,secondary IDX levels [3,3]  (>3 req 1123+) ",IDX'LEVELS,ID2'LEVELS 
    if IDX'LEVELS = 0 then IDX'LEVELS = 3
    if IDX'LEVELS = 0 then ID2'LEVELS = 3
    if IDX'LEVELS < 3 or IDX'LEVELS > 9 &
    or ID2'LEVELS < 3 or ID2'LEVELS > 9 then
        ? "Levels must be between 3 and 9"
        goto LEVELS
    endif
BLKSIZ:                                         ! [11]   
    input "Enter main, secondary IDX block size [512,512] (>512 req 1123+) ",IDX'BLKSIZ,ID2'BLKSIZ
    if IDX'BLKSIZ = 0 then IDX'BLKSIZ = 512
    if ID2'BLKSIZ = 0 then ID2'BLKSIZ = 512
    if IDX'BLKSIZ # 512 and IDX'BLKSIZ # 1024 and IDX'BLKSIZ # 2048 &
    and IDX'BLKSIZ # 4096 and IDX'BLKSIZ # 8192 and IDX'BLKSIZ # 16384 then
        ? "Block size must be one of: 512,1024,2048,4096,8192,16384"
        goto BLKSIZ
    endif
    if ID2'BLKSIZ # 512 and ID2'BLKSIZ # 1024 and ID2'BLKSIZ # 2048 &
    and ID2'BLKSIZ # 4096 and ID2'BLKSIZ # 8192 and ID2'BLKSIZ # 16384 then
        ? "Block size must be one of: 512,1024,2048,4096,8192,16384"
        goto BLKSIZ
    endif

    ! generate command file to create file
    open #1, "ISXTST.cmd", output
    ? #1, ":T"
    ? #1, "erase ";TESTFILENAME;".ID?"
	
    ? #1, ":T"
    ? #1, "ISMBLD ";TESTFILENAME;"/V";                      ! [11]
    if IDX'LEVELS # 3 then ? #1,"/L:";str(IDX'LEVELS);      ! [11]
    if IDX'BLKSIZ # 512 then ? #1, "/B:";STR(IDX'BLKSIZ);   ! [11]
    ? #1                                                    ! [11]
    ? #1, "50"                     ! key size
    ? #1, "1"                       ! key pos
    ? #1, str(TESTRECLEN)           ! rec size
    ? #1, str(INIREC)               ! # recs
    ? #1, "0"                       ! no extra idx blocks (let it auto-exp)
    ? #1, "Y"                       ! primary?
    ? #1, ""                        ! data device
    ? #1, ""                ! load from file
    ? #1, ""

    ? #1,":< Hit ENTER to proceed with secondary key build: >"
    ? #1,":K"
    ![1] generate secondary key too...
    ? #1, "ERASE ";TESTSECONDARY;".IDX"
    ? #1, "ISMBLD ";TESTSECONDARY;"/V";                     ! [11]
    if ID2'LEVELS # 3 then ? #1,"/L:";str(ID2'LEVELS);      ! [11]
    if ID2'BLKSIZ # 512 then ? #1, "/B:";STR(ID2'BLKSIZ);   ! [11]
    ? #1                                                    ! [11]
    ? #1, "14"                     ! key size
    ? #1, "51"                       ! key pos
    ? #1, str(TESTRECLEN)           ! rec size
    ? #1, str(INIREC)               ! # recs
    ? #1, "0"                       ! 0 extra idx blocks
    ? #1, "N"                       ! primary?
    ? #1, TESTFILENAME              ! primary file
    ? #1, ""                
    ? #1, ""
    ? #1,":<Hit ENTER to proceed with test (then any key to pause) > "
    ? #1,":K"
    ? #1, "run ISXTST"
    close #1

    XCALL CCON			! enable controlc
    chain "ISXTST.cmd"

BEGIN:
    if MAXREC = 0 then MAXREC = INIREC * 5

    ? "Test modes:"
    ? "  0) Random add/delete"
    ? "  1) Build rapidly to specified size"
    ? "  2) Sequential (primary key)"
    ? "  3) Sequential (secondary key)"
    ? "  4) Reverse sequential (primary)"    ! [16]
    ? "  5) Reverse sequential (secondary) " ! [16]
    ? "  6) Load from DAT"		     ! [15]
    input "Enter test mode: ",TSTMODE
    if TSTMODE = 1 THEN 
        input "Enter target max file (IDX or IDA) size in MB: ",TARGET
        TARGET *= 1048576   ! convert to bytes
    elseif TSTMODE = 6 then		! [15]
	input line "DATFILE spec: ",DATFILE$
	MAXDATRECNO = ABS(LOOKUP(DATFILE$)) * 2 - 1
	open #DATFNO, DATFILE$, RANDOM, TESTRECLEN, DATRECNO
    else
        input "Enter 1 to perform CPU calcs between I/O ops? ";WFLAG
        if TSTMODE < 2 then     ! not seq mode
            input "Enter 1 to use XLOCK to ensure multiuser safety? ",LOKFLG
        endif
    endif


++ifdef NEVER
    input "Memory map the file? ",A$
    if ucs(A$)="Y" then AFLAG = 16 else AFLAG = 0	
    input "Enter 1 for read only ",RO             ! [12]
    input "Enter log file name (or blank for none): ",LOGFILE$
++endif

    if TSTMODE >= 2 and TSTMODE <= 5 then       ! [16]
        input "Enter log file name (or blank for none): ",LOGFILE$
    endif

    ? "Opening ";TESTFILENAME;"..."
    OPEN #FILENO,TESTFILENAME,ISAM'INDEXED,TESTRECLEN,TESTRECNUM,WAIT'FILE,WAIT'RECORD
    ? "Opening ";TESTSECONDARY;"..."
    OPEN #FILEN2,TESTSECONDARY,ISAM'INDEXED,TESTRECLEN,TESTRECNUM,WAIT'FILE,WAIT'RECORD
    XCALL TIMES,2,T1        ! like TIME but in msecs
    XCALL TIMES,2,T2        ! 
    XCALL NOECHO

    if LOGFILE$ # "" then
        open #LCH,LOGFILE$,OUTPUT
    else
        LCH = 0
    endif


LOOP:
    if TSTMODE < 0 then             ! force an interrupt
        if TSTMODE = -1 then        ! if we were in rapid add mode
            TSTMODE = 0             ! switch now to random mode
        else
            TSTMODE = ABS(TSTMODE)  ! else return to previous mode
        endif
        goto INTERRUPT
    else
        xcall TINKEY,A$                 ! check for keyboard pending
        if A$ # "" goto INTERRUPT
    endif

    IF WFLAG=1 CALL WASTE'TIME			! [5] waste some time

    if TSTMODE = 6 then		! [15] load from dat
	call LOAD'FROM'DAT	! [15]
	goto LOOP
    endif


    if TSTMODE = 2 or TSTMODE = 3 then
        call DO'NEXT'KEY
        goto LOOP
    elseif TSTMODE = 4 or TSTMODE = 5 then     ! [16]
        call DO'PREV'KEY
        goto LOOP
    endif

    ! random test mode...
    ! start with a random #
    if BIAS > 0 or TSTMODE = 1                  ! if bias towards add,
        NUM = int(RND(0) * 999999999)		! use max range of random nums
    else                                        ! else use range based on 
        NUM = int(RND(0) * (MAXREC max 999999999))  ! actual or target size
    endif

    KEY1$ = (NUM using "#ZZZZZZZZ") + "--" + (TIME using "#ZZZZZZZZ") + space(80)
    KEY2$ = Fn'MakeKey2$(KEY1$)

    call DO'KEY'OP      ! lookup followed by add or delete  

    ! In mode 1, check size and if not yet there, and we just 
    ! did an added a rec, then add 1000 more using the same key
    ! but with the time portion set to the range 90000-90999
    if TSTMODE = 1 then
        if LASTOP = 2               ! just did an add 
            NEXT'SIZE'CHECK -= 1000
            IF NEXT'SIZE'CHECK < 0 then
                xcall SIZE,TESTFILENAME+".IDA",BYTES
                if BYTES > TARGET then
                    ?
                    ? TESTFILENAME;".IDA has reached target size"
                    TSTMODE = -1    ! force interrupt
                else
                    NEXT'SIZE'CHECK = (TARGET - BYTES) / TESTRECLEN
                endif
                xcall SIZE,TESTFILENAME+".IDX",BYTES
                if BYTES > TARGET then
                    ?
                    ? TESTFILENAME;".IDX has reached target size"
                    TSTMODE = -1    
                else
                    N1  = (TARGET - BYTES) / TESTRECLEN / 3
                ENDIF
                xcall SIZE,TESTSECONDARY+".IDX",BYTES
                if BYTES > TARGET then
                    ?
                    ? TESTSECONDARY;".IDX has reached target size"
                    TSTMODE = -1    
                else
                    N2  = (TARGET - BYTES) / TESTRECLEN
                endif

                ! estimate how many adds it will take before we need
                ! to check again
                NEXT'SIZE'CHECK = (NEXT'SIZE'CHECK min N1 min N2) / 3
            endif

            ! if still in rapid increase mode, add 1000 recs using 
            ! same key NUM value but imaginary times (these sequential
            ! adds are much faster than random key adds, presumably
            ! due to improved cache performance)
            if TSTMODE = 1 then
                A$ = ""     ! [14]
                for TZ = 90000 to 90999
                    KEY1$ = (NUM using "#ZZZZZZZZ") + "--" + (TZ using "#ZZZZZZZZ") + space(30)
                    KEY2$ = Fn'MakeKey2$(KEY1$)
                    call DO'KEY'OP
                    if getkey(-2) # -1 exit ! [14] key avail
                next TZ
            endif
        endif
    endif

    ! if bias towards delete, delete the next 1000 recs now
    if BIAS < 0 then
        A$ = ""             ! [14]
        for TZ = 1 to 5 !1000 
            OP$ = "get next (to delete)"
            ISAM #FILENO, 1, KEY1$  
            if erf(FILENO) = 0 then
                read #FILENO, RECORD
            elseif erf(FILENO) = 33 then
                unlokr #FILENO 
            else 
                goto ISAM'ERROR
            endif
            ISAM #FILENO, 7, KEY1$
            if erf(FILENO) = 38 then
                exit
            elseif erf(FILENO) # 0 then
                goto ISAM'ERROR
            else
                KEY2$ = Fn'MakeKey2$(KEY1$)
                call DO'KEY'OP  ! this should delete key just found
            endif                       

            if getkey(-2) # -1 exit ! [14] key avail

        next TZ

    endif
    goto LOOP

!---------------------------------------------------------------------
! Lookup key, then add or delete
! Input: KEY1$
!---------------------------------------------------------------------

DO'KEY'OP:

    IF LOKFLG = 1 CALL LOCKIT			! [5]

    ! look up key in primary, then verify that secondary agrees
    ! if no agreement, that's bad
    ! otherwise, if key exists, delete record, else add it

    isam #FILENO,1,KEY1$
    SKIP = 0

    ! else do an add or delete

    switch erf(FILENO)
    case 0                          ! key found in primary
        LASTOP = 1      ! delete
        ! first, verify it is also in secondary idx     ! [11]
        SAVE'RECNO = TESTRECNUM
        isam #FILEN2,1,KEY2$
        if erf(FILEN2)#0 goto ISAM'ERROR3
        if TESTRECNUM # SAVE'RECNO goto ISAM'ERROR5
        call DELETE'RECORD
        if SKIP = 0 and LCH # 0 then
            ? #LCH, "D,";KEY1$
        endif
        exit
    case 33                         ! not found in primary
        LASTOP = 2      ! add
        ! first, verify it is NOT in secondary idx     ! [11]
        isam #FILEN2,1,KEY2$
        if erf(FILEN2)#33 goto ISAM'ERROR4
        call ADD'RECORD
        if SKIP = 0 and LCH # 0 then
            ? #LCH, "A,";KEY1$
        endif
        exit
    default
        LASTOP = 0 
        OP$ = "ISAM #1" 
        goto ISAM'ERROR
    endswitch

    if SKIP = 0 then
        COUNT = COUNT + 1
        ! update stats after every 1000 recs
        if INT(COUNT/1000) = COUNT/1000 then
            XCALL TIMES,2,TMSECS  ! get time since midnight (in msec)

            ? TAB(25);"Count = ";COUNT;     &
                " Rate (Cur,Total) = ";    &
                (1000000/((TMSECS-T1) max 1) using "#####"); ","; &
                ((COUNT*1000)/((TMSECS-T2) max 1) using "#####");" /sec" 
!           T1 = TIME		! restart 1000-count timer
            T1 = TMSECS         ! restart 1000-count timer (in msec)
        endif
    endif
    SKIP = 0
    call UNLOCK			! [5]

    return

!---------------------------------------------------------------------

DELETE'RECORD:

    if BIAS > 0 then                    ! bias is add so skip some dels
      if COUNT/3 = int(COUNT/3) then
        SKIP = 1
        return
      endif
    endif
    LAST'COUNT = COUNT

    if RO then       ! just read
        OP$ = "read"
        read #FILENO, RECORD
        if (COUNT/50)=int(COUNT/50) ? "f";      ! [12] f for found
    else
        OP$ = "readl"
        readl #FILENO, RECORD
        OP$ = "delete secondary idx"
        KEY2$ = RECORD'KEY2
        isam #FILEN2,4,KEY2$			! [1] delete secondary
        IF ERF(FILEN2) THEN GOTO ISAM'ERROR2
    	
        OP$ = "delete primary idx"
        KEY1$ = RECORD'KEY1
        isam #FILENO,4,KEY1$
        if ERF(FILENO) THEN GOTO ISAM'ERROR
    
        OP$ = "delete data"
        isam #FILENO,6,KEY1$
        IF ERF(FILENO) THEN GOTO ISAM'ERROR
        DEL'COUNT = DEL'COUNT + 1       ! [11]
        if (COUNT/50)=int(COUNT/50) ? "d";
    endif

    OP$=""
    return

!-------------------------------------------------------------------

ADD'RECORD:

    if RO then           ! [12] read only
        if (COUNT/50)=int(COUNT/50) ? "n";  ! n for not found
        return                  ! [12]
    endif

    if BIAS < 0 then                    ! bias is del so skip some adds
      if COUNT/3 = int(COUNT/3) then
        SKIP = 1
        return
      endif
    endif
    LAST'COUNT = COUNT

    OP$ = "add primary data"
    RECORD'KEY1 = KEY1$
    RECORD'KEY2 = KEY2$
    isam #FILENO,5,KEY1$
    IF ERF(FILENO) THEN GOTO ISAM'ERROR
    WRITEL #FILENO, RECORD

    OP$ = "add secondary idx"
    isam #FILEN2,3,KEY2$			! [1] add secondary key
    if ERF(FILEN2) THEN GOTO ISAM'ERROR2
	
    OP$ = "add primary idx"
    isam #FILENO,3,KEY1$
    IF ERF(FILENO) THEN GOTO ISAM'ERROR
    if (COUNT/50)=int(COUNT/50) ? "a";
    OP$ = ""
    ADD'COUNT = ADD'COUNT + 1           ! [11]
    return

!--------------------------------------------------------------------
! Do a get-next key operation
! Input: KEY1$,KEYS$,TSTMODE
!---------------------------------------------------------------------

DO'NEXT'KEY:

    ! if incoming key is blank, ask for starting key

    IF TSTMODE = 2 then     ! primary key
        OP$ = "get next primary"
        if KEY1$ = "" then
            xcall ECHO
            input "Starting key: ",KEY1$
            xcall NOECHO
            isam #FILENO, 1, KEY1$
            if erf(FILENO) = 33 then
                unlokr #FILENO 
            elseif erf(FILENO) # 0 then
                goto ISAM'ERROR
            endif
        endif
        LAST'KEY1$ = KEY1$
        
        IF LOKFLG = 1 CALL LOCKIT			! [ZZ]
        isam #FILENO, 7, KEY1$
        if erf(FILENO) = 38 then
            ? : ? "End of index reached"
            TSTMODE = -TSTMODE
        elseif erf(FILENO) # 0 then
            goto ISAM'ERROR
        else        
            if KEY1$ <= LAST'KEY1$ then
                goto ISAM'ERROR8
            endif
            READ #FILENO, RECORD
            if RECORD'KEY1 # KEY1$ then
                goto ISAM'ERROR6
            endif
            if erf(LCH) = 0 then ? #LCH, KEY1$  ! [16]
        endif

    ELSE                    ! secondary key
        OP$ = "get next secondary"

        if KEY2$ = "" then
            xcall ECHO
            input "Starting key: ",KEY2$
            xcall NOECHO
            lock #FILENO
            isam #FILEN2, 1, KEY2$
            if erf(FILEN2) = 33 then
                unlokr #FILENO 
            elseif erf(FILEN2) # 0 then
                goto ISAM'ERROR2
            endif
            unlokr #FILENO
        endif
        LAST'KEY2$ = KEY2$

        IF LOKFLG = 1 CALL LOCKIT			

        lock #FILENO
        isam #FILEN2, 7, KEY2$
        if erf(FILEN2) = 38 then
            ? : ? "End of index reached"
            TSTMODE = -TSTMODE
        elseif erf(FILEN2) # 0 then
            goto ISAM'ERROR2
        else        
            if KEY2$ < LAST'KEY2$ then
                goto ISAM'ERROR9
            endif
            READ #FILENO, RECORD
            if RECORD'KEY2 # KEY2$ then
                goto ISAM'ERROR6
            endif
            if erf(LCH) = 0 then ? #LCH, KEY2$  ! [16]
        endif
        unlokr #FILENO
        call UNLOCK
    endif
    
    if (COUNT/50)=int(COUNT/50) ? "n";
    OP$ = ""
    NEXT'COUNT = NEXT'COUNT + 1         

    if SKIP = 0 then
        COUNT = COUNT + 1
        ! update stats after every 1000 recs
        if INT(COUNT/1000) = COUNT/1000 then
            XCALL TIMES,2,TMSECS  ! get time since midnight (in msec)

            ? TAB(22);"Count = ";COUNT;     &
                " Rate (Cur,Total) = ";    &
                (1000000/((TMSECS-T1) max 1) using "#####"); ","; &
                ((1000*COUNT)/((TMSECS-T2) max 1) using "#####");" /sec" 
            T1 = TMSECS         ! restart 1000-count timer (in msec)
        endif
    endif
    call UNLOCK                 ! [zz]
    return

!--------------------------------------------------------------------
! Do a get-prev key operation   ! [16]
! Input: KEY1$,KEYS$,TSTMODE
!---------------------------------------------------------------------

DO'PREV'KEY:

    ! if incoming key is blank, ask for starting key

    IF TSTMODE = 4 then     ! primary key
        OP$ = "get prev primary"
        if KEY1$ = "" then
            xcall ECHO
            input "Starting key: ",KEY1$
            xcall NOECHO
            isam #FILENO, 1, KEY1$
            if erf(FILENO) = 33 then
                unlokr #FILENO 
            elseif erf(FILENO) # 0 then
                goto ISAM'ERROR
            endif
        endif
        LAST'KEY1$ = KEY1$
        
        isam #FILENO, 8, KEY1$
        if erf(FILENO) = 38 then
            ? : ? "Beginning of index reached"
            TSTMODE = -TSTMODE
        elseif erf(FILENO) # 0 then
            goto ISAM'ERROR
        else        
            if KEY1$ >= LAST'KEY1$ then
                goto ISAM'ERROR8
            endif
            READ #FILENO, RECORD
            if RECORD'KEY1 # KEY1$ then
                goto ISAM'ERROR6
            endif
            if erf(LCH) = 0 then ? #LCH, KEY1$  ! [16]
        endif

    ELSE                    ! secondary key
        OP$ = "get prev secondary"

        if KEY2$ = "" then
            xcall ECHO
            input "Starting key: ",KEY2$
            xcall NOECHO
            lock #FILENO
            isam #FILEN2, 1, KEY2$
            if erf(FILEN2) = 33 then
                unlokr #FILENO 
            elseif erf(FILEN2) # 0 then
                goto ISAM'ERROR2
            endif
            unlokr #FILENO
        endif
        LAST'KEY2$ = KEY2$

        IF LOKFLG = 1 CALL LOCKIT			

        lock #FILENO
        isam #FILEN2, 8, KEY2$
        if erf(FILEN2) = 38 then
            ? : ? "Beginning of index reached"
            TSTMODE = -TSTMODE
        elseif erf(FILEN2) # 0 then
            goto ISAM'ERROR2
        else        
            if KEY2$ >= LAST'KEY2$ then
                goto ISAM'ERROR9
            endif
            READ #FILENO, RECORD
            if RECORD'KEY2 # KEY2$ then
                goto ISAM'ERROR6
            endif
            if erf(LCH) = 0 then ? #LCH, KEY2$  ! [16]

        endif
        unlokr #FILENO
        call UNLOCK
    endif
    
    if (COUNT/50)=int(COUNT/50) ? "n";
    OP$ = ""
    NEXT'COUNT = NEXT'COUNT + 1         

    if SKIP = 0 then
        COUNT = COUNT + 1
        ! update stats after every 1000 recs
        if INT(COUNT/1000) = COUNT/1000 then
            XCALL TIMES,2,TMSECS  ! get time since midnight (in msec)

            ? TAB(22);"Count = ";COUNT;     &
                " Rate (Cur,Total) = ";    &
                (1000000/((TMSECS-T1) max 1) using "#####"); ","; &
                ((1000*COUNT)/((TMSECS-T2) max 1) using "#####");" /sec" 
            T1 = TMSECS         ! restart 1000-count timer (in msec)
        endif
    endif

    return


!--------------------------------------------------------------------

ISAM'ERROR:
    ? "Isam error ";ERF(FILENO);" during ";OP$
    ? "Main key = ";KEY1$
    call UNLOCK
    call DISPLAY'ISAM'ERRORS
    end

ISAM'ERROR2:
    ? "Isam error on secondary ";ERF(FILEN2);" during ";OP$
    ? "Main key = ";KEY1$;" Secondary key = ";KEY2$
    call UNLOCK
    call DISPLAY'ISAM'ERRORS
    end

ISAM'ERROR3:        ! [11] key in primary but not secondary
    ? "Primary/Secondary mismatch: key in primary but not secondary"
    ? "Main key = ";KEY1$
    ? "Secondary key = ";KEY2$
    ? "ERF(secondary) = ";ERF(FILEN2)
    call UNLOCK
    call DISPLAY'ISAM'ERRORS
    end

ISAM'ERROR4:        ! [11] key not in primary but secondary
    ? "Primary/Secondary mismatch: key not in primary but is in secondary"
    ? "Main key = ";KEY1$
    ? "Secondary key = ";KEY2$
    ? "ERF(secondary) = ";ERF(FILEN2)
    call UNLOCK
    call DISPLAY'ISAM'ERRORS
    end

ISAM'ERROR5:        ! [11] key in both idxs but with different rec#
    ? "Primary/Secondary mismatch in record #"
    ? "Main key = ";KEY1$;" recno = ";SAVE'RECNO
    ? "Secondary key = ";KEY2$;" recno = ";TESTRECNUM
    ? "ERF(secondary) = ";ERF(FILEN2)
    call UNLOCK
    call DISPLAY'ISAM'ERRORS
    end

ISAM'ERROR6:        ! primary key found but doesn't match rec
    ? "Primary Key IDX/IDA mismatch!"
    ? "Key =        ";KEY1$
    ? "Record key = ";RECORD'KEY1
    call UNLOCK
    call DISPLAY'ISAM'ERRORS
    end

ISAM'ERROR7:        ! secondary key found but doesn't match rec
    ? "Secondary Key IDX/IDA mismatch!"
    ? "Key =        ";KEY2$
    ? "Record key = ";RECORD'KEY2
    call UNLOCK
    call DISPLAY'ISAM'ERRORS
    end

ISAM'ERROR8:        ! primary key sequence error
    ? "Primary key out of sequence!"
    ? "Key:      ";KEY1$
    ? "Last Key: ";LAST'KEY1$
    call UNLOCK
    call DISPLAY'ISAM'ERRORS
    end

ISAM'ERROR9:        ! secondary key sequence error
    ? "Secondary key out of sequence!"
    ? "Key:      ";KEY2$
    ? "Last Key: ";LAST'KEY2$
    call UNLOCK
    call DISPLAY'ISAM'ERRORS
    end

!-----------------------------------------------------------------------

INTERRUPT:
    T3 = TIME
    ?
    xcall ISMROK,FILENO,IDAALC,IDAAVL,RECSIZ,KEYSIZ,KEYPOS,DEVDRV,IDXALC,IDXAVL

    if TSTMODE = 0 or TSTMODE = 1 then
        ? str(ADD'COUNT);" adds and";DEL'COUNT;"deletes (net this prog run = ";str(ADD'COUNT-DEL'COUNT)+")"
    elseif TSTMODE = 2 or TSTMODE = 3 then
        ? str(NEXT'COUNT);" get'nexts"
        if TSTMODE = 2 then 
            ? "Last Primary: ";KEY1$
        else
            ? "Last Secondary: ";KEY2$
        endif
    elseif TSTMODE = 4 or TSTMODE = 5 then  ! [16]
        ? str(NEXT'COUNT);" get'prevs"
        if TSTMODE = 4 then 
            ? "Last(first) Primary: ";KEY1$
        else
            ? "Last(first) Secondary: ";KEY2$
        endif

    endif   
    ? "IDA recs alc: ";IDAALC;" avail: ";IDAAVL;" IDX alc: ";IDXALC;" avail: ";IDXAVL
    if TSTMODE = 1 then
        ? "Target size: ";TARGET;"bytes (next size check in ";int(NEXT'SIZE'CHECK);" adds)" 
    endif
    xcall ECHO
    A$ = "0"
    if TSTMODE < 2 then             ! add or random mode
        if BIAS = 1 then A$ = "A"
        if BIAS = -1 then A$ = "D" 
        input "Options (Q=quit, A=Favor adds, D=Favor deletes, 0=neutral) ",A$
        if ucs(A$)="A" then BIAS = 1
        if ucs(A$)="D" then BIAS = -1
        if A$="0" or A$="" then BIAS = 0    

    else                            ! get next mode
        input "Options (Q=quit, else start new sequence) ",A$
        KEY1$ = ""                  ! this will request a new starting key
        KEY2$ = ""
    endif

    if ucs(A$) # "Q" then
        ? 
        T3 = TIME - T3        ! elapsed time waiting in this prompt  
        T2 = T2 + (T3*1000)   ! remove total time so as to not dilute perf
        goto LOOP
    endif
    close #FILENO
    close #FILEN2
    call UNLOCK
    if TSTMODE # 1 and TSTMODE # 2 then
        ? ADD'COUNT;"adds and ";DEL'COUNT;"deletes ( net = ";ADD'COUNT-DEL'COUNT;")"
    endif
    if LCH close #LCH
    end

!-----------------------------------------------------------------

UNLOCK:
    if LOKFLG = 2 then
        MODE = 2
        LOCK1 = 128
        LOCK2 = 65535

        xcall XLOCK,MODE,LOCK1,LOCK2
        if MODE#1 ? "Unable to release lock: ";LOCK1;LOCK2 : end
        LOKFLG = 1
    endif
    return

LOCKIT:
    MODE = 1
    LOCK1 = 128
    LOCK2 = 65535
    XCALL XLOCK,MODE,LOCK1,LOCK2
    if MODE#0 then
        ? "L"; 
        xcall SLEEP,0.1
        xcall TINKEY,A$ 
        if A$ # "" then
            goto INTERRUPT 
        else 
            goto LOCKIT
        endif
    endif
    LOKFLG = 2
    return

!--------------------------------------------------------------------

WASTE'TIME:	! either do some CPU calcs, or just sleep for a bit
		! just to make the test more "realistic"

    ? "W";tab(-1,254);
    if rnd(0) < .05 then        ! 5% of time, do CPU calcs
        for W = 1 to 10
            W1 = W * 3.14
            W2 = W1 / 2.19
            W3 = INT(W2)
            WBUF = ""
            for W4 = 1 to (W3 * 10)
                W5 = (rnd(0) * 95) + 32
                WBUF = WBUF + chr(W5)
            next W4
            W6 = instr(1,WBUF,"XXX")	! (never found)
        next W
    else                        ! 95% of time, do sleep
        xcall SLEEP,0.001       ! sleep a millisecond
    endif
    ? tab(-1,5);		! erase the "W"
    return

![15]
!-----------------------------------------------------------------------
LOAD'FROM'DAT:
    IF DATRECNO > MAXDATRECNO then
	? : ? "Hit end of DAT file" 
	TSTMODE = 0
        return
    ENDIF

    READ #DATFNO, RECORD
    FOR I = 1 TO 4
        B1 = asc(RECORD[I;1])
	IF B1 < 48 OR B1 > 57 THEN 
            EXIT	    
	ENDIF
    NEXT I
    IF B1 < 48 OR B1 > 57 THEN
        DATRECNO += 1
        GOTO LOAD'FROM'DAT
    ENDIF

    KEY1$ = RECORD'KEY1 
    KEY2$ = RECORD'KEY2 

    isam #FILENO,1,KEY1$
    if erf(FILENO) = 33 then
        CALL ADD'RECORD
    else
        unlokr #FILENO
        ? "[Key already on file - skip!]"
    endif

    DATRECNO += 1
    RETURN
!-----------------------------------------------------------------------

DISPLAY'ISAM'ERRORS:
    XCALL ECHO
    input "Do you want to display the ISAM error legend?",A$
    if ucs(A$)="Y" then
        ?
        ? "32 = Illegal ISAM statement code"
        ? "33 = Key not found during index search"
        ? "34 = Duplicate key found in the index during attempted key add"
        ? "35 = Link structure is smashed and must be re-created"
        ? "36 = Index file is full"
        ? "37 = Data file is full"
        ? "38 = End of file reached during get-next operation"
        ? "39 = Illegal ISAM sequence (LOKSER rules violation)"
    endif
    return

Function Fn'MakeKey2$(key1$ as s100) as s100
    Fn'MakeKey2$ = key1$[1;9] + key1$[16;5]  !  + space(86)
EndFunction


