:<(UI) A-Shell/Windows menu customization
    ASMENU - Sample program showing use of MIAMEX,AUI_MENU
    ASHMNU.TXT - Notes on menu customization techniques
    MADMNU - Example program showing use of TAB(-10,21)
    NOHELP - Example of removing unwanted items from HELP menu
    DLGMNU - Example of adding a menu bar to a dialog
    RSTMNU - Example of resetting menu bar
    DPPMNU - Example of disabling/enabling a built-in menu item
    ASMNU2 - Example of checked menu items and nested submenus (& dialog mnu)
    ASMNU4 - Example of menu items with bitmaps
    TOPMNU - Simplest possible example of adding a single top level item
    DELMNU1- Examples of deleting internal menu items by position
    DELMNU2- Example of deleting a built-in menu item by IDM_xxx value
    CTXMNU-  Example of simple context menu in main window
    CTXMNU2- Example of simple context menu in dialog
>
