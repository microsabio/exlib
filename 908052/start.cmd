:<(Bar Codes) 

  New US Post Office smart bar code 
    USPSTST.BP - Test USPSBS.SBR (using test cases approved by USPS)
    USPSGDI.BP - Joe Leibel's sample of converting bar string to GDI

  EAN13   (European Article Numbering - International version of UPC)
    EAN13.BP - Joe Leibel's implementation/demo
  EAN128  (New variation of EAN13)
    EAN128.BP - Joe Leibel's implementation/demo
>
 
