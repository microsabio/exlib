program USPSTST,1.0(100)     ! test USPSBS.SBR
!----------------------------------------------------------------------
![100] Janurary 6, 2011 02:41 PM        Edited by jack
!    Created to test USPSBS.SBR (added in 5.1.1201.0)
!----------------------------------------------------------------------
!NOTES:
!Converts a "track string" and "route string" to an encoded version
!of the bar code (which must then be converted to an actual bar code
!using some other logic, presumably with GDI printing directives.)
!
!Requires USPS4CB.DLL to be in the path.  It can be downloaded from
!   https://ribbs.usps.gov/onecodesolution/download.cfm
!
!Calling syntax:
!
! MAP1 TRACKSTR,S,21            ! [in] 20 + 1 null
! MAP1 ROUTESTR,S,12            ! [in] 12 + 1 null
! MAP1 BARSTR,S,66              ! [out] 65 + 1 null  
! MAP1 RC,F                     ! Return Code
!
!   XCALL USPSBS,TRACKSTR,ROUTESTR,BARSTR,RC 
!   IF RC <> 0 THEN GOTO ERROR'HANDLER 
!
! Error codes:
!   -1 Parameter error 
!   -2 Unable to load the USPS4CB.DLL 
!   -3 Unable to locate the USPS4CB() function within DLL 
!  -99 Not supported on current platform
!   10 Track string invalid
!   11 Track String invalid (variation)
!   13 Route String invalid
!----------------------------------------------------------------------

MAP1 PARAMS
    MAP2 BARSTR,S,66              ! [out] 65 + 1 null  
    MAP2 RC,F                 ! Return (

DEFSTRUCT ST_UTEST
    MAP2 TESTNUM,B,1
    MAP2 TRACKSTR,S,21
    MAP2 ROUTESTR,S,12
    MAP2 EXPECTBAR,S,66
    MAP2 EXPECTRC,F
ENDSTRUCT

DEFINE MAX_TESTCASES = 11
MAP1 TESTCASE(MAX_TESTCASES),ST_UTEST
MAP1 I,F
MAP1 ANOMALIES,B,1

        ! Test Case 1: Valid Track & Route RC=0 
DATA 1,"53379777234994544928","51135759461"
DATA "DAFDTDAFFDFTDADTDDFTTFDTATATFFFDFTTFFFTFDDTDAAFATDFTFDFDTTTDTTFDA",0

        ! Test Case 2: Valid Track & Route RC=0 
DATA 2,"53055494689272602879","13765583689"
DATA "AFDADAFTAFDTDFTFTFDAAFDDTAFDFDTTFADATAATAAAADDDFAAAAATDADAFADFTTT",0

        ! Test Case 3: Valid Track & Route RC=0 
DATA 3,"40120111574675115924","62176609110"
DATA "DDAFFFDAFTFDFFAAATTTDDFFTFADDFAFTTDAAAAAADFTTFDTAFDDTDDADATDAFAFF",0

        ! Test Case 4: Valid Track & Route RC=0 
DATA 4,"82205455868913559972","54765515722"
DATA "TTTADTFFADAFDTTFDTAADATFFFADFFTDDFFFATDADAATAAADDATFTAADFADTADADD",0

        ! Test Case 5: 2nd byte Track inv. RC =11
DATA 5,"57379777234994544928","51135759461"
DATA " ",11

        ! Test Case 6: invalid char Route RC=13 
DATA 6,"53055494689272602879","137655B3689"
DATA " ",13

        ! Test Case 7: invalid char Track RC=10 
DATA 7,"4012X111574675115924","62176609110"
DATA " ",10

        ! Test Case 8: Valid Route 0 bytes RC=0 
DATA 8,"01234567094987654321",""
DATA "ATTFATTDTTADTAATTDTDTATTDAFDDFADFDFTFFFFFTATFAAAATDFFTDAADFTFDTDT",0

        ! Test Case 9: Valid Route 5 bytes RC=0 
DATA 9,"01234567094987654321","01234"
DATA "DTTAFADDTTFTDTFTFDTDDADADAFADFATDDFTAAAFDTTADFAAATDFDTDFADDDTDFFT",0

        ! Test Case 10: Valid Route 9 bytes RC=0 
DATA 10,"01234567094987654321","012345678"
DATA "ADFTTAFDTTTTFATTADTAAATFTFTATDAAAFDDADATATDTDTTDFDTDATADADTDFFTFA",0

        ! Test Case 11: Valid Route 11 bytes RC=0 
DATA 11,"01234567094987654321","01234567891"
DATA "AADTFFDFTDADTAADAATFDTDDAAADDTDTTDAFADADDDTFFFDDTTTADFAAADFTDAADA",0

    ? tab(-1,0);"Testing USPSBS.SBR ..."
    ?

    FOR I = 1 TO MAX_TESTCASES
        READ TESTCASE.TESTNUM(I),TESTCASE.TRACKSTR(I),TESTCASE.ROUTESTR(I), &
            TESTCASE.EXPECTBAR(I),TESTCASE.EXPECTRC(I)
        
        ? "Test #";I;tab(10);"Track=";TESTCASE.TRACKSTR(I); &
                ", Route=";TESTCASE.ROUTESTR(I)

        xcall USPSBS, TESTCASE.TRACKSTR(I), TESTCASE.ROUTESTR(I), &
            BARSTR, RC

        ? " rc=";RC;",Bar=";BARSTR
        if RC=0 and TESTCASE.EXPECTRC(I)=0 and BARSTR=TESTCASE.EXPECTBAR(I) then
            ? "  [SUCCESS]"
        elseif RC#0 and RC=TESTCASE.EXPECTRC(I) then
            ? "  [OK] (Test fails as intended)" 
        else
            ? "  ERROR: expected rc = ";TESTCASE.EXPECTRC(I)
            ? "    Exp. bar=";TESTCASE.EXPECTBAR(I)
            ANOMALIES = ANOMALIES + 1
        endif

        ? "----------------------------------------------------------------"
        input "Hit ENTER to continue: ",RC
        ? "----------------------------------------------------------------"
    NEXT I

    ? 
    ? "Test complete (";ANOMALIES;"anomalies )"
    end
