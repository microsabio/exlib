! ***************** EAN128 ***************** 
! *               01-Dec-12                *
! *  Subroutine to create EAN128 Bar code  *
! ******************************************
!
! Also known as GS1-128 and UCC128 and UCC/EAN-128
!
! 01-Dec-12 12.12(1) Created for Jorge by Joe from EAN13.BAS
!
! Everything will be expressed in 1000th of an inch for positioning
!
! Maximum data characters are 48 
! Physical length is 6.5 inches or 165 mm  ( 128 gallons )
! Mimimum Height is 1.25 inches or 32 mm   ( 128 gallons )
!
! References:
!   http://www.barcodeisland.com/ean128.phtml
!   http://www.barcodesoft.com/code128_font.aspx
! 
! Code 128A is used to encode uppercase alpha and control characters.
! Code 128B is used to encode both uppercase and lowercase alpha-numeric data.
! Code 128C is used to encode numeric-only data.
!
! [JT] 11.11(3): 21-Nov.-2011
!      . bc'extra'length: to define the percentage for the longest bars
!      . bottom align all the digits for the human readable part of the code
!      . OCR-B font face for human readable
!      . slight reduce the bars thickness	

PROGRAM EAN128,12.12(1)

MAP1 BC'TABLE$(106),S,11	! Table elements 0-105
MAP1 BC'TABLE,F,6		! Position in Table 1-106

MAP1 BC$,S,13			! Bar Code String to be printed
MAP1 BC'START,F,6		! Start Code 103, 104 or 105
MAP1 BC'CHARACTER'SET$,S,1 	! Character Set A B C 
MAP1 BX$,S,5			! Use for Lead-In, Int & Trailing pieces
MAP1 BC'BIN$,S,800		! Binary digits of the Bar Code
MAP1 BC'WIDTH,F			! Width of a single Bar Code Line
MAP1 BC'LENGTH,F		! Length of the usual Bar Code Line
map1 bc'extra'length,f		! [JT] 11.11(3)	
MAP1 BC'COL,F			! Starting Column Position in 1000th
MAP1 BEG'COL,F			! Begining column (calculated)
MAP1 BC'ROW,F			! Starting Row Position in 1000th
MAP1 HR'ROW,F			! Human Readable Row (calculated)
MAP1 BC'TROW,F			! Begining Row (calculated)
MAP1 BOT'ROW,F			! Bottom Row (calculated)
MAP1 BC'BROW,F			! Row used when calling print line routine

MAP1 BC'CHECK,F			! Check Digit
MAP1 BC'SUM,F			! Sum of the Bar Code for check digit calc
MAP1 BC'POS,F			! Current Position within the BC$ string
MAP1 BC'DIGIT,F			! Position within the current TABLE$
MAP1 BC'FONT$,S,50		! Font used for Human Readble....
				! This needs to be changed based on BC'WIDTH
MAP1 BC'STYLE$,S,3		! Pen Style

MAP1 BC'START$(3),S,100         ! Start characters (A,B,C)
MAP1 BC'STOP$,S,100             ! Stop character
MAP1 BC'CSET,F                  ! Character set (101,100,99)

STRSIZ 100

	! ***** Table for all bar code values            **** !
	! ***** Offset by 1 to avoud zero value element  **** !
	BC'TABLE$( 1 ) = "11011001100"		! 00=SP	SP	00
	BC'TABLE$( 2 ) = "11001101100"		! 01=!	!	01
	BC'TABLE$( 3 ) = "11001100110"		! 02="	"	02
	BC'TABLE$( 4 ) = "10010011000"		! 03=#	#	03
	BC'TABLE$( 5 ) = "10010001100"		! 04=$	$	04
	BC'TABLE$( 6 ) = "10001001100"		! 05=%	%	05
	BC'TABLE$( 7 ) = "10011001000"		! 06=&	&	06
	BC'TABLE$( 8 ) = "10011000100"		! 07='	'	07
	BC'TABLE$( 9 ) = "10001100100"		! 08=(	(	08
	BC'TABLE$( 10 ) = "11001001000"		! 09=)	)	09
	BC'TABLE$( 11 ) = "11001000100"		! 10=*	*	10
	BC'TABLE$( 12 ) = "11000100100"		! 11=+	+	11
	BC'TABLE$( 13 ) = "10110011100"		! 12=,	,	12
	BC'TABLE$( 14 ) = "10011011100"		! 13=-	-	13
	BC'TABLE$( 15 ) = "10011001110"		! 14=.	.	14
	BC'TABLE$( 16 ) = "10111001100"		! 15=/	/	15
	BC'TABLE$( 17 ) = "10011101100"		! 16=0	0	16
	BC'TABLE$( 18 ) = "10011100110"		! 17=1	1	17
	BC'TABLE$( 19 ) = "11001110010"		! 18=2	2	18
	BC'TABLE$( 20 ) = "11001011100"		! 19=3	3	19
	BC'TABLE$( 21 ) = "11001001110"		! 20=4	4	20
	BC'TABLE$( 22 ) = "11011100100"		! 21=5	5	21
	BC'TABLE$( 23 ) = "11001110100"		! 22=6	6	22
	BC'TABLE$( 24 ) = "11101101110"		! 23=7	7	23
	BC'TABLE$( 25 ) = "11101001100"		! 24=8	8	24
	BC'TABLE$( 26 ) = "11100101100"		! 25=9	9	25
	BC'TABLE$( 27 ) = "11100100110"		! 26=:	:	26
	BC'TABLE$( 28 ) = "11101100100"		! 27=;	;	27
	BC'TABLE$( 29 ) = "11100110100"		! 28=<	<	28
	BC'TABLE$( 30 ) = "11100110010"		! 29==	=	29
	BC'TABLE$( 31 ) = "11011011000"		! 30=>	>	30
	BC'TABLE$( 32 ) = "11011000110"		! 31=?	?	31
	BC'TABLE$( 33 ) = "11000110110"		! 32=@	@	32
	BC'TABLE$( 34 ) = "10100011000"		! 33=A	A	33
	BC'TABLE$( 35 ) = "10001011000"		! 34=B	B	34
	BC'TABLE$( 36 ) = "10001000110"		! 35=C	C	35
	BC'TABLE$( 37 ) = "10110001000"		! 36=D	D	36
	BC'TABLE$( 38 ) = "10001101000"		! 37=E	E	37
	BC'TABLE$( 39 ) = "10001100010"		! 38=F	F	38
	BC'TABLE$( 40 ) = "11010001000"		! 39=G	G	39
	BC'TABLE$( 41 ) = "11000101000"		! 40=H	H	40
	BC'TABLE$( 42 ) = "11000100010"		! 41=I	I	41
	BC'TABLE$( 43 ) = "10110111000"		! 42=J	J	42
	BC'TABLE$( 44 ) = "10110001110"		! 43=K	K	43
	BC'TABLE$( 45 ) = "10001101110"		! 44=L	L	44
	BC'TABLE$( 46 ) = "10111011000"		! 45=M	M	45
	BC'TABLE$( 47 ) = "10111000110"		! 46=N	N	46
	BC'TABLE$( 48 ) = "10001110110"		! 47=O	O	47
	BC'TABLE$( 49 ) = "11101110110"		! 48=P	P	48
	BC'TABLE$( 50 ) = "11010001110"		! 49=Q	Q	49
	BC'TABLE$( 51 ) = "11000101110"		! 50=R	R	50
	BC'TABLE$( 52 ) = "11011101000"		! 51=S	S	51
	BC'TABLE$( 53 ) = "11011100010"		! 52=T	T	52
	BC'TABLE$( 54 ) = "11011101110"		! 53=U	U	5
	BC'TABLE$( 55 ) = "11101011000"		! 54=V	V	5
	BC'TABLE$( 56 ) = "11101000110"		! 55=W	W	5
	BC'TABLE$( 57 ) = "11100010110"		! 56=X	X	5
	BC'TABLE$( 58 ) = "11101101000"		! 57=Y	Y	5
	BC'TABLE$( 59 ) = "11101100010"		! 58=Z	Z	5
	BC'TABLE$( 60 ) = "11100011010"		! 59=[	[	5
	BC'TABLE$( 61 ) = "11101111010"		! 60=\	\	6
	BC'TABLE$( 62 ) = "11001000010"		! 61=]	]	6
	BC'TABLE$( 63 ) = "11110001010"		! 62=^	^	6
	BC'TABLE$( 64 ) = "10100110000"		! 63=_	_	6
	BC'TABLE$( 65 ) = "10100001100"		! 64=NUL	`	6
	BC'TABLE$( 66 ) = "10010110000"		! 65=SOH	a	6
	BC'TABLE$( 67 ) = "10010000110"		! 66=STX	b	6
	BC'TABLE$( 68 ) = "10000101100"		! 67=ETX	c	6
	BC'TABLE$( 69 ) = "10000100110"		! 68=EOT	d	6
	BC'TABLE$( 70 ) = "10110010000"		! 69=ENQ	e	6
	BC'TABLE$( 71 ) = "10110000100"		! 70=ACK	f	7
	BC'TABLE$( 72 ) = "10011010000"		! 71=BEL	g	7
	BC'TABLE$( 73 ) = "10011000010"		! 72=BS	h	7
	BC'TABLE$( 74 ) = "10000110100"		! 73=HT	i	7
	BC'TABLE$( 75 ) = "10000110010"		! 74=LF	j	7
	BC'TABLE$( 76 ) = "11000010010"		! 75=VT	k	7
	BC'TABLE$( 77 ) = "11001010000"		! 76=FF	l	7
	BC'TABLE$( 78 ) = "11110111010"		! 77=CR	m	7
	BC'TABLE$( 79 ) = "11000010100"		! 78=SO	n	7
	BC'TABLE$( 80 ) = "10001111010"		! 79=SI	o	7
	BC'TABLE$( 81 ) = "10100111100"		! 80=DLE	p	8
	BC'TABLE$( 82 ) = "10010111100"		! 81=DC1	q	8
	BC'TABLE$( 83 ) = "10010011110"		! 82=DC2	r	8
	BC'TABLE$( 84 ) = "10111100100"		! 83=DC3	s	8
	BC'TABLE$( 85 ) = "10011110100"		! 84=DC4	t	8
	BC'TABLE$( 86 ) = "10011110010"		! 85=NAK	u	8
	BC'TABLE$( 87 ) = "11110100100"		! 86=SYN	v	8
	BC'TABLE$( 88 ) = "11110010100"		! 87=ETB	w	8
	BC'TABLE$( 89 ) = "11110010010"		! 88=CAN	x	8
	BC'TABLE$( 90 ) = "11011011110"		! 89=EM	y	8
	BC'TABLE$( 91 ) = "11011110110"		! 90=SUB	z	9
	BC'TABLE$( 92 ) = "11110110110"		! 91=ESC	{	9
	BC'TABLE$( 93 ) = "10101111000"		! 92=FS	|	9
	BC'TABLE$( 94 ) = "10100011110"		! 93=GS	}	9
	BC'TABLE$( 95 ) = "10001011110"		! 94=RS	~	9
	BC'TABLE$( 96 ) = "10111101000"		! 95=US	DEL	9
	BC'TABLE$( 97 ) = "10111100010"		! 96=FNC3	FNC3	9
	BC'TABLE$( 98 ) = "11110101000"		! 97=FNC2	FNC2	9
	BC'TABLE$( 99 ) = "11110100010"		! 98=SHIFT	SHIFT	9
	BC'TABLE$( 100 ) = "10111011110"	! 99= Code C	
	BC'TABLE$( 101 ) = "10111101110"	! 100=Code B	
	BC'TABLE$( 102 ) = "11101011110"	! 101=Code A	
	BC'TABLE$( 103 ) = "11110101110"	! 102=FNC1	
	BC'TABLE$( 104 ) = "11010000100"	! 103=START A
	BC'TABLE$( 105 ) = "11010010000"	! 104=START B
	BC'TABLE$( 106 ) = "11010011100"	! 105=START C	

	! **** Start Characters **** !
	BC'START$( 1 ) = "11010000100"		! 103=	START A
	BC'START$( 2 ) = "11010010000"		! 104=	START B
	BC'START$( 3 ) = "11010011100"		! 105=	START C	

	! **** Stop Character **** !
	BC'STOP$ = "11000111010"		! STOP	

	! *** Set variables for position and size of the bar code *** !

	! *** Set the thickness of the Bars and Spaces *** !
	! *** Overall size of the barcode will be 102  *** !
	! *** times this number in 1000th of an inch.  *** !
!!	BC'WIDTH  = 20			! 20/1000th Inch
	BC'WIDTH  = 15			! [JT] 11.11(3)
	BC'WIDTH  = 20			! 20/1000th Inch 12.12(1)

	! *** Set the Length of the Bars *** !
	BC'LENGTH = 500			! 1/2 Inch (500/1000)
	bc'extra'length = .25		! [JT] 11.11(3)	

	! *** Font size and face for Human Readable      *** !
	! *** This needs to be changed based on BC'WIDTH *** !
!!	BC'FONT$ = "140,COURIER NEW"
	BC'FONT$ = "120,OCR-B"		! [JT] 11.11(3)	

COL1: ! **** Open the spool file **** !
	OPEN # 50,"EAN128.LST",OUTPUT

	! *** Print UPC barcode # 1 *** !
	BC'COL = 5000
	BC'ROW = 100
	BC'START = 103			! 103, 104 or 105 !
	BC'CHARACTER'SET$ = "C"		! A  B or C !
	BC$ = "HI345678"
 	GOSUB PRINT'EAN128'BARCODE

	CLOSE # 50
	XCALL SPOOL,"EAN128.LST"
	END

PRINT'EAN128'BARCODE: ! ********** European Article Numbering **********

?"Check Digit ";
	! ****** GENERATE THE CHECK DIGIT ******** !
	BC'SUM   = BC'START				! Start Character
?BC'START;" ";
	BC'SUM   = BC'SUM + (ASC(BC$[1;1]) -32)		! 1st Digit
?(ASC(BC$[1;1]) -32);" ";
	BC'SUM   = BC'SUM + ((ASC(BC$[2;1]) -32) * 2)	! 2nd Digit
?((ASC(BC$[2;1]) -32) * 2);" ";
	! Character Set !
	IF BC'CHARACTER'SET$ = "A" THEN BC'CSET = 101
	IF BC'CHARACTER'SET$ = "B" THEN BC'CSET = 100
	IF BC'CHARACTER'SET$ = "C" THEN BC'CSET = 99
	BC'SUM   = BC'SUM + (BC'CSET * 3)
?BC'CSET * 3;" ";
	! Data !
	BC'TABLE = 3
	FOR BC'POS = 3 TO LEN(BC$) STEP 2
	  BC'TABLE = BC'TABLE + 1
	  BC'SUM = BC'SUM + VAL(BC$[BC'POS;2]) * BC'TABLE
?VAL(BC$[BC'POS;2]) * BC'TABLE,BC$[BC'POS;2];" ";
	NEXT BC'POS
?
	! ******* Get the remainder ******* !
	BC'CHECK = INT(BC'SUM / 103) 
	BC'CHECK = BC'SUM - ( BC'CHECK * 103)
?BC'SUM,BC'CHECK 

	BC$ = BC$ + STR$(BC'CHECK)
?"Bar Code "
?BC$

	BEG'COL = BC'COL 	
	BC'TROW = BC'ROW
	BOT'ROW = BC'ROW + BC'LENGTH ! In 1000th of an Inch

	! ******* Form the Bar Code Binary String ******** !
	
	! **** Start code 1 ***** !
	BC'BIN$ = BC'START$(1)
?BC'BIN$,"Start"
	! **** 1st Digit ***** !
	BC'TABLE = (ASC(BC$[1;1]) -32) + 1	 
	BC'BIN$ = BC'BIN$ + BC'TABLE$( BC'TABLE )
?BC'TABLE$( BC'TABLE ),BC'TABLE
	! **** 2nd Digit ***** !
	BC'TABLE = (ASC(BC$[2;1]) -32) + 1
	BC'BIN$ = BC'BIN$ + BC'TABLE$( VAL(BC$[2;1]) + 1 )
?BC'TABLE$( BC'TABLE ),BC'TABLE

	! **** Character set ****** !
	BC'BIN$ = BC'BIN$ + BC'TABLE$( BC'CSET + 1 )
?BC'TABLE$( BC'CSET + 1 ),BC'CSET
	! **** Add the Data ***** !
	FOR BC'POS = 3 TO LEN(BC$) STEP 2
	  BC'TABLE = VAL(BC$[BC'POS;2]) + 1
	  BC'BIN$ = BC'BIN$ + BC'TABLE$( BC'TABLE )
?BC'TABLE$( BC'TABLE ),BC$[BC'POS;2]
	NEXT BC'POS

	! ******* Stop Character ***** !
	BC'BIN$ = BC'BIN$ + BC'STOP$
	BC'BIN$ = BC'BIN$ + "11"
?BC'STOP$;"11","Stop"

	! *********** Output to the File ************* !
	?#50,"//; ****** Print EAN128 Barcode ******"
	! ** 1000th of an Inch ** !
	?#50,"//SETMAPMODE,HIENGLISH"			
	! ** Set Pen Solid at specified width & Square End Caps ** !
	! ** Width at 1000th thick bars                        ** !
	BC'STYLE$ = 256
	?#50,"//SETPENEX,";BC'STYLE$;",";STR$(BC'WIDTH);",1"	
	! ** Font used for Human Readable ** !
	?#50,"//SETFONT,";BC'FONT$;",1,0,600,0"		

	! ********** Print the Bar Code ************ !

	BC'COL = BEG'COL - BC'WIDTH

	! *** Print the Code **** !
	BC'BROW = BOT'ROW + (BC'LENGTH * bc'extra'length)	! Slightly longer line
	FOR BC'POS = 1 TO LEN(BC'BIN$)
	  BC'COL = BC'COL + BC'WIDTH
	  IF BC'BIN$[BC'POS;1] = "1" THEN GOSUB PRINT'BAR
	NEXT BC'POS

	! *** Print the Human Readable *** !
	HR'ROW = BC'BROW + 10
	BC'COL = BEG'COL + (BC'WIDTH * 10)
	?#50,"//MOVETO,";STR$(BC'COL);",";STR$(HR'ROW)
	?#50,BC$
	RETURN

PRINT'BAR: ! *** Print a single bar code line *** !
	?#50,"//MOVETO,";STR$(BC'COL);",";STR$(BC'TROW)
	?#50,"//LINETO,";STR$(BC'COL);",";STR$(BC'BROW)
	RETURN


ERROR'EXIT:
?"ERROR ";ERR(0)
END
 
