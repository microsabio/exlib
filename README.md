# A-Shell Basic (ASB) Example Library  (EXLIB) 



The EXLIB is dedicated to providing examples illustrating the usage of various A-Shell capabilities. It is organized into many [908,*] directories, with each directory grouping related example/test/demo programs. Each directory has a start.cmd with further details.



## History: ##

The original SOSLIB combined the [907,*] and [908,*] directories. The examples  have been split off into a separate library, EXLIB, because of differences in its usage patterns and in order to keep the more heavily used SOSLIB smaller (and therefore easier to keep updated). 



## How do I get set up? ##

The EXLIB can occupy the [908,*] directories on any logical device. We recommend that you create a separate directory, perhaps \vm\miame\exlib61 and then define a DEVICE (e.g. EXL61:) and an ersatz: (e.g. EXLIB:) to clearly distinguish it from anything else.



Note that many of the examples require components from the SOSLIB, particularly the ASHINC: and SOSFUNC: directories.



## Downloading / Updating ##

There are two approaches.  One is to just use the download link (below left), which gives you a zip file that you can then unzip into the desired directory tree. The other is to set up Mercurial (or the Windows GUI front-end Tortoise Hg) and then you can just sync the updates to your system.





## Testing ##

To verify that the library is set up properly, try compiling/running any .bp programs in any [908,*] directory. 



## Usage ##

Each directory has a START.CMD that gives a brief overview of what is there and may list individual programs.  In most cases you should be able to just compile and run any program, although in some cases you may need to look at the source to understand what the program is illustrating.



## Contributing ##

To contribute bug reports or enhancement requests, please use the Issue Tracker. To contribute patches or new routines, please email them to MicroSabio (for verification/testing/editing) or otherwise contact us to set up a direct access account.

We'll grant direct updating privileges to developers who have the skills and interest to post their own updates directly to the library.  (Anyone can easily track those from the commits panel.)